rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__1524600402-layer" class="layer" name="__containerId__pageLayer" data-layer-id="1524600402" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-1524600402-layer-602023636" style="position: absolute; left: -1px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="602023636" data-review-reference-id="602023636">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024" viewBox="0 0 601 1024">\
                     <svg:g width="601" height="1024">\
                        <svg:rect x="0" y="0" width="601" height="1024" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="601" y2="1024" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="1024" x2="601" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-1178807388" style="position: absolute; left: 0px; top: 240px; width: 600px; height: 80px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1178807388" data-review-reference-id="1178807388">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div id="1178807388-700202742" style="position: absolute; left: 0px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="700202742" data-review-reference-id="700202742">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1178807388-700202742_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1178807388-700202742div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1178807388-700202742\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1178807388-700202742\', \'result\');">\
                           				RFPL\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1178807388-2085045111" style="position: absolute; left: 100px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="2085045111" data-review-reference-id="2085045111">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1178807388-2085045111_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1178807388-2085045111div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1178807388-2085045111\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1178807388-2085045111\', \'result\');">\
                           				Premier League\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1178807388-1567203740" style="position: absolute; left: 200px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1567203740" data-review-reference-id="1567203740">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1178807388-1567203740_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1178807388-1567203740div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1178807388-1567203740\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1178807388-1567203740\', \'result\');">\
                           				BundesLiga\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1178807388-1136023639" style="position: absolute; left: 400px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1136023639" data-review-reference-id="1136023639">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1178807388-1136023639_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1178807388-1136023639div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1178807388-1136023639\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1178807388-1136023639\', \'result\');">\
                           				Ligue 1\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1178807388-1962094851" style="position: absolute; left: 300px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1962094851" data-review-reference-id="1962094851">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1178807388-1962094851_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1178807388-1962094851div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1178807388-1962094851\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1178807388-1962094851\', \'result\');">\
                           				Ligue BBVA\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1178807388-1094007743" style="position: absolute; left: 500px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1094007743" data-review-reference-id="1094007743">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1178807388-1094007743_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1178807388-1094007743div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1178807388-1094007743\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1178807388-1094007743\', \'result\');">\
                           				Serie A\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-766730" style="position: absolute; left: 0px; top: 320px; width: 600px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="766730" data-review-reference-id="766730">\
            <div class="stencil-wrapper" style="width: 600px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px; width:600px;" width="600" height="465" viewBox="0 0 600 465">\
                     <svg:g width="600" height="465">\
                        <svg:rect x="0" y="0" width="600" height="465" style="stroke-width:1;stroke:black;fill:#a7a77c;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-1149758023" style="position: absolute; left: 1px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1149758023" data-review-reference-id="1149758023">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:599px;" width="599" height="80" viewBox="0 0 599 80">\
                     <svg:g width="599" height="80">\
                        <svg:rect x="0" y="0" width="599" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-1304250031" style="position: absolute; left: 0px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1304250031" data-review-reference-id="1304250031">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:600px;" width="600" height="80" viewBox="0 0 600 80">\
                     <svg:g width="600" height="80">\
                        <svg:rect x="0" y="0" width="600" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-376351437" style="position: absolute; left: 115px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="376351437" data-review-reference-id="376351437">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60" viewBox="0 0 200 60">\
                     <svg:g width="200" height="60">\
                        <svg:rect x="0" y="0" width="200" height="60" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="200" y2="60" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="60" x2="200" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-483149906" style="position: absolute; left: 343px; top: 20px; width: 242px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="483149906" data-review-reference-id="483149906">\
            <div class="stencil-wrapper" style="width: 242px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Коэффициенты </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-718547274" style="position: absolute; left: 95px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="718547274" data-review-reference-id="718547274">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-1545702130" style="position: absolute; left: 0px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1545702130" data-review-reference-id="1545702130">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:86px;height:80px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">=<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 86px; height: 80px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-1524600402-layer-1545702130\', \'interaction299074184\', {"button":"left","id":"action309743741","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction420937191","options":"reloadOnly","target":"page106900939","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-1524600402-layer-2058537725" style="position: absolute; left: 0px; top: 350px; width: 600px; height: 395px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="2058537725" data-review-reference-id="2058537725">\
            <div class="stencil-wrapper" style="width: 600px; height: 395px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-1524600402-layer-2058537725-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 395px;width:600px;" width="600" height="395">\
                     <svg:g id="__containerId__-1524600402-layer-2058537725svg" width="600" height="395"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 1.72, 22.55, 1.62 Q 32.83, 1.92, 43.10, 1.86 Q 53.38, 2.34,\
                        63.66, 2.48 Q 73.93, 2.49, 84.21, 2.01 Q 94.48, 2.48, 104.76, 2.77 Q 115.03, 2.50, 125.31, 3.22 Q 135.59, 1.75, 145.86, 2.51\
                        Q 156.14, 2.36, 166.41, 2.35 Q 176.69, 1.32, 186.97, 2.08 Q 197.24, 2.51, 207.52, 2.13 Q 217.79, 2.62, 228.07, 2.25 Q 238.34,\
                        3.04, 248.62, 3.25 Q 258.90, 0.98, 269.17, 0.33 Q 279.45, 0.75, 289.72, 0.95 Q 300.00, 1.09, 310.28, 0.63 Q 320.55, 0.24,\
                        330.83, 1.27 Q 341.10, 1.03, 351.38, 0.85 Q 361.66, 0.85, 371.93, 1.41 Q 382.21, 1.78, 392.48, 1.29 Q 402.76, 1.55, 413.03,\
                        1.48 Q 423.31, 3.15, 433.59, 3.59 Q 443.86, 2.70, 454.14, 2.20 Q 464.41, 2.54, 474.69, 2.18 Q 484.97, 2.15, 495.24, 2.83 Q\
                        505.52, 0.92, 515.79, 0.94 Q 526.07, 0.62, 536.34, 0.44 Q 546.62, 0.40, 556.90, 0.97 Q 567.17, 0.43, 577.45, 0.17 Q 587.72,\
                        0.36, 598.87, 1.13 Q 599.02, 11.95, 599.85, 22.32 Q 600.06, 32.73, 600.11, 43.09 Q 599.27, 53.43, 598.87, 63.73 Q 599.00,\
                        74.02, 599.12, 84.31 Q 599.10, 94.60, 599.19, 104.89 Q 598.74, 115.18, 598.86, 125.47 Q 599.06, 135.76, 598.80, 146.05 Q 598.66,\
                        156.34, 599.33, 166.63 Q 599.08, 176.92, 599.13, 187.21 Q 598.46, 197.50, 597.97, 207.79 Q 599.10, 218.08, 598.91, 228.37\
                        Q 597.41, 238.66, 597.45, 248.95 Q 597.33, 259.24, 596.75, 269.53 Q 597.67, 279.82, 597.37, 290.11 Q 597.49, 300.39, 598.00,\
                        310.68 Q 598.66, 320.97, 598.43, 331.26 Q 597.90, 341.55, 599.81, 351.84 Q 599.78, 362.13, 599.58, 372.42 Q 598.08, 382.71,\
                        597.76, 392.76 Q 587.79, 393.20, 577.56, 393.77 Q 567.22, 393.72, 556.89, 392.82 Q 546.62, 392.78, 536.35, 393.90 Q 526.07,\
                        393.58, 515.79, 393.68 Q 505.52, 393.90, 495.24, 393.81 Q 484.97, 393.44, 474.69, 393.02 Q 464.41, 392.48, 454.14, 393.81\
                        Q 443.86, 394.73, 433.59, 394.05 Q 423.31, 393.30, 413.03, 393.65 Q 402.76, 394.72, 392.48, 395.10 Q 382.21, 395.27, 371.93,\
                        395.25 Q 361.66, 395.43, 351.38, 395.31 Q 341.10, 394.48, 330.83, 394.19 Q 320.55, 394.75, 310.28, 394.00 Q 300.00, 394.28,\
                        289.72, 394.54 Q 279.45, 395.23, 269.17, 395.08 Q 258.90, 394.97, 248.62, 394.71 Q 238.34, 394.41, 228.07, 394.34 Q 217.79,\
                        394.38, 207.52, 394.44 Q 197.24, 394.26, 186.97, 394.23 Q 176.69, 393.87, 166.41, 393.77 Q 156.14, 393.82, 145.86, 393.99\
                        Q 135.59, 394.20, 125.31, 394.36 Q 115.03, 394.44, 104.76, 394.02 Q 94.48, 394.51, 84.21, 394.24 Q 73.93, 394.67, 63.66, 394.73\
                        Q 53.38, 394.10, 43.10, 393.78 Q 32.83, 393.66, 22.55, 394.10 Q 12.28, 394.24, 1.26, 393.74 Q 0.87, 383.09, 1.17, 372.54 Q\
                        1.20, 362.18, 0.66, 351.89 Q 0.95, 341.57, 1.31, 331.27 Q 1.86, 320.97, 2.23, 310.68 Q 2.12, 300.39, 1.62, 290.11 Q 1.06,\
                        279.82, 1.91, 269.53 Q 2.53, 259.24, 3.20, 248.95 Q 1.97, 238.66, 1.32, 228.37 Q 1.49, 218.08, 1.38, 207.79 Q 1.30, 197.50,\
                        1.30, 187.21 Q 1.25, 176.92, 1.37, 166.63 Q 1.56, 156.34, 1.36, 146.05 Q 0.81, 135.76, 1.25, 125.47 Q 0.95, 115.18, 1.73,\
                        104.89 Q 0.51, 94.61, 0.46, 84.32 Q 0.91, 74.03, 1.72, 63.74 Q 1.40, 53.45, 1.05, 43.16 Q 1.25, 32.87, 1.39, 22.58 Q 2.00,\
                        12.29, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 596px; height:391px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1524600402-layer-2058537725-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1524600402-layer-2058537725-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1524600402-layer-2058537725-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1524600402-layer-2058537725-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-1524600402-layer-2058537725-accordion", "596", "391", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-1524600402-layer-1291856583" style="position: absolute; left: 0px; top: 380px; width: 598px; height: 273px" data-interactive-element-type="default.table" class="table stencil mobile-interaction-potential-trigger " data-stencil-id="1291856583" data-review-reference-id="1291856583">\
            <div class="stencil-wrapper" style="width: 598px; height: 273px">\
               <div title=""><table width=\'588.0\' height=\'273.0\' cellspacing=\'0\' class=\'tableStyle\'><tr style=\'height: 39px\'><td style=\'width:163px;\' class=\'tableCells\'><span\
                  style=\'\'>BK</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>1</span><br /></td><td style=\'width:78px;\'\
                  class=\'tableCells\'><span style=\'\'>X</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>2</span><br\
                  /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height: 39px\'><td style=\'width:163px;\'\
                  class=\'tableCells\'><span style=\'\'>1X ставка</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br\
                  /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height:\
                  39px\'><td style=\'width:163px;\' class=\'tableCells\'><span style=\'\'>Winline</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\'\
                  class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br\
                  /></td></tr><tr style=\'height: 39px\'><td style=\'width:163px;\' class=\'tableCells\'><span style=\'\'>Лига ставок</span><br /></td><td\
                  style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:51px;\'\
                  class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height: 39px\'><td style=\'width:163px;\' class=\'tableCells\'><span\
                  style=\'\'>Фонбет</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\'\
                  class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br\
                  /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height: 39px\'><td style=\'width:163px;\'\
                  class=\'tableCells\'><span style=\'\'>Леон</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br\
                  /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height:\
                  39px\'><td style=\'width:163px;\' class=\'tableCells\'><span style=\'\'>Pinnacle</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\'\
                  class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br\
                  /></td></tr></table>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="1524600402"] .border-wrapper, body[data-current-page-id="1524600402"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="1524600402"] .border-wrapper, body.has-frame[data-current-page-id="1524600402"] .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="1524600402"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="1524600402"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "1524600402",\
      			"name": "coefficients",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
</div>');