rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__492670737-layer" class="layer" name="__containerId__pageLayer" data-layer-id="492670737" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-492670737-layer-1339902317" style="position: absolute; left: -1px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="1339902317" data-review-reference-id="1339902317">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024">\
                     <svg:g width="601" height="1024"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.29, 1.53, 22.59, 0.69 Q 32.88, 0.79, 43.17, 0.19 Q\
                        53.47, 0.52, 63.76, 0.87 Q 74.05, 1.27, 84.34, 1.26 Q 94.64, -0.12, 104.93, 0.29 Q 115.22, 0.22, 125.52, -0.28 Q 135.81, -0.23,\
                        146.10, 1.05 Q 156.40, 2.19, 166.69, 2.51 Q 176.98, 1.23, 187.28, -0.10 Q 197.57, -0.25, 207.86, -0.08 Q 218.16, -0.27, 228.45,\
                        -0.41 Q 238.74, -0.42, 249.03, -0.58 Q 259.33, -0.13, 269.62, 0.21 Q 279.91, 0.92, 290.21, 1.29 Q 300.50, 1.35, 310.79, 1.96\
                        Q 321.09, 2.00, 331.38, 2.64 Q 341.67, 2.57, 351.97, 1.88 Q 362.26, 0.91, 372.55, 0.32 Q 382.84, 0.89, 393.14, -0.03 Q 403.43,\
                        0.05, 413.72, 0.44 Q 424.02, 0.52, 434.31, 0.27 Q 444.60, 0.19, 454.90, 0.05 Q 465.19, 0.24, 475.48, 0.25 Q 485.78, -0.03,\
                        496.07, 0.10 Q 506.36, 0.33, 516.65, 0.72 Q 526.95, 1.20, 537.24, 1.22 Q 547.53, 0.96, 557.83, 0.96 Q 568.12, 0.28, 578.41,\
                        0.36 Q 588.71, 0.38, 599.69, 1.31 Q 599.48, 11.84, 599.17, 21.98 Q 598.75, 32.02, 598.31, 42.02 Q 598.53, 52.01, 598.53, 62.00\
                        Q 598.96, 72.00, 599.22, 82.00 Q 599.73, 92.00, 599.00, 102.00 Q 598.62, 112.00, 599.66, 122.00 Q 600.79, 132.00, 600.67,\
                        142.00 Q 599.76, 152.00, 599.48, 162.00 Q 600.63, 172.00, 601.05, 182.00 Q 600.64, 192.00, 600.24, 202.00 Q 600.34, 212.00,\
                        599.74, 222.00 Q 599.84, 232.00, 600.04, 242.00 Q 600.57, 252.00, 600.42, 262.00 Q 600.65, 272.00, 600.61, 282.00 Q 600.88,\
                        292.00, 601.17, 302.00 Q 601.25, 312.00, 601.02, 322.00 Q 600.32, 332.00, 600.59, 342.00 Q 600.31, 352.00, 600.98, 362.00\
                        Q 601.08, 372.00, 600.80, 382.00 Q 600.96, 392.00, 599.45, 402.00 Q 599.51, 412.00, 599.43, 422.00 Q 599.60, 432.00, 599.26,\
                        442.00 Q 599.70, 452.00, 599.28, 462.00 Q 599.31, 472.00, 599.57, 482.00 Q 599.28, 492.00, 599.20, 502.00 Q 599.38, 512.00,\
                        600.24, 522.00 Q 600.13, 532.00, 599.70, 542.00 Q 600.15, 552.00, 600.28, 562.00 Q 600.08, 572.00, 600.09, 582.00 Q 600.61,\
                        592.00, 599.97, 602.00 Q 599.90, 612.00, 598.91, 622.00 Q 599.02, 632.00, 599.05, 642.00 Q 598.63, 652.00, 598.61, 662.00\
                        Q 599.23, 672.00, 599.39, 682.00 Q 599.06, 692.00, 599.18, 702.00 Q 598.98, 712.00, 599.91, 722.00 Q 599.93, 732.00, 599.68,\
                        742.00 Q 599.09, 752.00, 599.17, 762.00 Q 599.32, 772.00, 598.96, 782.00 Q 599.32, 792.00, 598.96, 802.00 Q 599.53, 812.00,\
                        600.10, 822.00 Q 599.74, 832.00, 600.98, 842.00 Q 600.53, 852.00, 600.69, 862.00 Q 599.99, 872.00, 600.49, 882.00 Q 599.54,\
                        892.00, 600.33, 902.00 Q 600.02, 912.00, 600.47, 922.00 Q 600.06, 932.00, 599.99, 942.00 Q 599.85, 952.00, 599.67, 962.00\
                        Q 600.61, 972.00, 599.74, 982.00 Q 600.49, 992.00, 600.07, 1002.00 Q 599.54, 1012.00, 598.80, 1021.80 Q 588.67, 1021.89, 578.41,\
                        1021.98 Q 568.11, 1021.86, 557.84, 1022.43 Q 547.53, 1021.76, 537.25, 1023.16 Q 526.95, 1022.37, 516.66, 1022.67 Q 506.36,\
                        1022.51, 496.07, 1022.55 Q 485.78, 1022.42, 475.48, 1022.55 Q 465.19, 1023.70, 454.90, 1023.63 Q 444.60, 1023.91, 434.31,\
                        1024.04 Q 424.02, 1023.87, 413.72, 1023.31 Q 403.43, 1023.39, 393.14, 1022.97 Q 382.84, 1022.35, 372.55, 1022.14 Q 362.26,\
                        1022.96, 351.97, 1023.26 Q 341.67, 1022.99, 331.38, 1022.48 Q 321.09, 1022.71, 310.79, 1022.88 Q 300.50, 1023.63, 290.21,\
                        1023.63 Q 279.91, 1023.33, 269.62, 1023.25 Q 259.33, 1023.70, 249.03, 1023.15 Q 238.74, 1022.48, 228.45, 1021.32 Q 218.16,\
                        1022.72, 207.86, 1022.08 Q 197.57, 1022.10, 187.28, 1022.53 Q 176.98, 1021.89, 166.69, 1022.80 Q 156.40, 1022.50, 146.10,\
                        1022.06 Q 135.81, 1022.32, 125.52, 1022.63 Q 115.22, 1022.22, 104.93, 1022.05 Q 94.64, 1021.76, 84.34, 1021.66 Q 74.05, 1021.97,\
                        63.76, 1022.18 Q 53.47, 1021.82, 43.17, 1021.73 Q 32.88, 1021.31, 22.59, 1020.87 Q 12.29, 1020.75, 2.26, 1021.74 Q 1.50, 1012.17,\
                        1.18, 1002.12 Q 0.98, 992.07, 1.09, 982.03 Q 1.26, 972.01, 1.69, 962.00 Q 1.83, 952.00, 1.51, 942.00 Q 1.39, 932.00, 1.26,\
                        922.00 Q 1.73, 912.00, 1.57, 902.00 Q 1.29, 892.00, 1.72, 882.00 Q 1.70, 872.00, 1.41, 862.00 Q 1.36, 852.00, 1.74, 842.00\
                        Q 1.65, 832.00, 1.11, 822.00 Q 1.27, 812.00, 2.36, 802.00 Q 2.26, 792.00, 2.91, 782.00 Q 3.17, 772.00, 2.16, 762.00 Q 2.49,\
                        752.00, 2.66, 742.00 Q 2.16, 732.00, 2.40, 722.00 Q 2.07, 712.00, 1.97, 702.00 Q 0.93, 692.00, 1.91, 682.00 Q 2.33, 672.00,\
                        1.73, 662.00 Q 1.38, 652.00, 1.47, 642.00 Q 1.97, 632.00, 1.76, 622.00 Q 0.88, 612.00, 0.60, 602.00 Q 1.50, 592.00, 1.61,\
                        582.00 Q 1.64, 572.00, 1.75, 562.00 Q 1.08, 552.00, 1.57, 542.00 Q 1.22, 532.00, 2.07, 522.00 Q 1.15, 512.00, 1.73, 502.00\
                        Q 2.36, 492.00, 2.18, 482.00 Q 1.61, 472.00, 1.55, 462.00 Q 1.78, 452.00, 2.39, 442.00 Q 1.89, 432.00, 1.64, 422.00 Q 1.56,\
                        412.00, 1.11, 402.00 Q 1.72, 392.00, 2.04, 382.00 Q 0.94, 372.00, 0.72, 362.00 Q 0.97, 352.00, 0.23, 342.00 Q 0.10, 332.00,\
                        0.05, 322.00 Q 0.74, 312.00, 1.76, 302.00 Q 0.99, 292.00, 0.79, 282.00 Q 1.81, 272.00, 2.01, 262.00 Q 1.52, 252.00, 1.11,\
                        242.00 Q 0.96, 232.00, 1.66, 222.00 Q 1.08, 212.00, 1.00, 202.00 Q 0.94, 192.00, 1.47, 182.00 Q 1.79, 172.00, 1.40, 162.00\
                        Q 1.26, 152.00, 1.65, 142.00 Q 2.24, 132.00, 2.19, 122.00 Q 1.44, 112.00, 0.86, 102.00 Q 2.39, 92.00, 2.06, 82.00 Q 2.39,\
                        72.00, 1.41, 62.00 Q 1.67, 52.00, 1.90, 42.00 Q 2.10, 32.00, 2.73, 22.00 Q 2.00, 12.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 7.65, 10.30, 12.28, 19.19 Q 17.03, 28.02, 21.71, 36.88 Q 27.35,\
                        45.19, 31.84, 54.16 Q 36.50, 63.04, 41.37, 71.80 Q 47.81, 79.64, 52.64, 88.41 Q 55.88, 98.12, 61.51, 106.43 Q 67.21, 114.70,\
                        72.58, 123.16 Q 77.89, 131.66, 82.16, 140.77 Q 87.47, 149.27, 93.26, 157.48 Q 98.79, 165.85, 103.27, 174.83 Q 107.15, 184.17,\
                        111.93, 192.98 Q 117.50, 201.32, 122.81, 209.82 Q 128.48, 218.11, 133.83, 226.57 Q 138.82, 235.26, 144.44, 243.58 Q 149.14,\
                        252.43, 154.77, 260.74 Q 159.49, 269.58, 164.12, 278.48 Q 169.15, 287.14, 174.00, 295.91 Q 178.78, 304.72, 183.73, 313.42\
                        Q 189.21, 321.82, 194.84, 330.13 Q 200.77, 338.27, 204.85, 347.48 Q 208.91, 356.71, 213.71, 365.51 Q 218.83, 374.12, 223.95,\
                        382.73 Q 229.65, 391.00, 234.20, 399.94 Q 238.85, 408.82, 243.93, 417.45 Q 249.49, 425.80, 254.63, 434.40 Q 260.21, 442.74,\
                        264.88, 451.61 Q 269.66, 460.42, 275.40, 468.67 Q 281.19, 476.88, 286.54, 485.36 Q 291.40, 494.12, 295.86, 503.11 Q 301.50,\
                        511.42, 307.02, 519.79 Q 312.04, 528.46, 317.03, 537.14 Q 321.57, 546.09, 326.38, 554.88 Q 330.44, 564.11, 336.31, 572.28\
                        Q 341.17, 581.04, 346.61, 589.46 Q 351.61, 598.14, 356.13, 607.10 Q 360.66, 616.05, 365.54, 624.80 Q 371.61, 632.85, 376.44,\
                        641.63 Q 382.04, 649.96, 387.15, 658.57 Q 392.23, 667.20, 397.91, 675.48 Q 401.96, 684.72, 406.61, 693.61 Q 411.85, 702.14,\
                        416.20, 711.20 Q 421.22, 719.87, 426.05, 728.64 Q 431.66, 736.97, 437.17, 745.35 Q 442.65, 753.75, 447.81, 762.33 Q 453.73,\
                        770.47, 459.01, 778.99 Q 463.61, 787.90, 467.87, 797.01 Q 472.44, 805.94, 477.56, 814.55 Q 482.69, 823.15, 487.87, 831.73\
                        Q 493.30, 840.15, 498.86, 848.51 Q 503.93, 857.14, 508.59, 866.02 Q 513.47, 874.77, 518.31, 883.54 Q 523.51, 892.10, 528.09,\
                        901.03 Q 533.62, 909.39, 538.72, 918.01 Q 544.07, 926.49, 549.76, 934.77 Q 555.17, 943.20, 559.80, 952.10 Q 564.92, 960.71,\
                        569.79, 969.47 Q 574.71, 978.19, 580.35, 986.49 Q 585.72, 994.95, 590.42, 1003.81 Q 593.94, 1013.35, 599.00, 1022.00" style="\
                        fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 1022.00 Q 7.87, 1013.82, 13.00, 1005.21 Q 17.34, 996.13, 22.71, 987.66\
                        Q 26.56, 978.30, 31.70, 969.69 Q 36.93, 961.14, 42.19, 952.60 Q 46.42, 943.46, 51.20, 934.64 Q 56.26, 925.99, 61.47, 917.42\
                        Q 66.87, 908.97, 71.48, 900.05 Q 77.30, 891.84, 82.34, 883.18 Q 87.90, 874.82, 91.96, 865.58 Q 96.98, 856.90, 102.17, 848.33\
                        Q 107.52, 839.84, 113.00, 831.43 Q 117.62, 822.52, 123.05, 814.08 Q 128.33, 805.56, 133.52, 796.98 Q 137.64, 787.78, 142.89,\
                        779.23 Q 148.38, 770.84, 154.03, 762.53 Q 158.36, 753.44, 163.44, 744.80 Q 169.47, 736.72, 174.39, 727.98 Q 179.52, 719.37,\
                        183.68, 710.19 Q 188.73, 701.53, 193.90, 692.94 Q 199.26, 684.46, 204.42, 675.87 Q 209.57, 667.27, 214.67, 658.64 Q 219.93,\
                        650.10, 225.36, 641.67 Q 229.39, 632.41, 235.18, 624.18 Q 240.56, 615.72, 245.16, 606.79 Q 249.58, 597.76, 254.81, 589.21\
                        Q 260.46, 580.90, 265.30, 572.12 Q 269.65, 563.05, 274.70, 554.39 Q 279.86, 545.79, 284.74, 537.03 Q 289.69, 528.32, 294.85,\
                        519.72 Q 301.04, 511.73, 306.00, 503.02 Q 310.97, 494.31, 316.18, 485.75 Q 320.37, 476.58, 326.25, 468.41 Q 331.76, 460.02,\
                        337.18, 451.58 Q 341.64, 442.58, 346.54, 433.83 Q 351.75, 425.26, 356.92, 416.67 Q 361.85, 407.94, 366.27, 398.91 Q 371.05,\
                        390.10, 376.28, 381.54 Q 381.52, 372.99, 386.50, 364.29 Q 391.95, 355.87, 396.65, 347.00 Q 401.98, 338.51, 406.73, 329.67\
                        Q 412.12, 321.21, 416.84, 312.35 Q 422.48, 304.04, 427.34, 295.27 Q 431.61, 286.16, 437.04, 277.72 Q 442.85, 269.51, 448.37,\
                        261.12 Q 453.73, 252.65, 457.86, 243.44 Q 463.68, 235.24, 469.12, 226.81 Q 473.66, 217.85, 479.28, 209.52 Q 483.13, 200.16,\
                        488.96, 191.96 Q 493.76, 183.15, 498.37, 174.24 Q 503.82, 165.81, 508.57, 156.97 Q 513.48, 148.23, 518.46, 139.53 Q 523.32,\
                        130.76, 528.33, 122.08 Q 533.64, 113.57, 538.80, 104.98 Q 543.94, 96.37, 549.22, 87.84 Q 554.40, 79.26, 559.60, 70.69 Q 564.82,\
                        62.13, 569.77, 53.41 Q 574.60, 44.62, 579.41, 35.83 Q 584.22, 27.02, 589.04, 18.23 Q 595.92, 10.65, 601.00, 2.00" style="\
                        fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-1488392406" style="position: absolute; left: 0px; top: 240px; width: 600px; height: 80px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1488392406" data-review-reference-id="1488392406">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div id="1488392406-34549728" style="position: absolute; left: 0px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="34549728" data-review-reference-id="34549728">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 6.22, 71.50, 5.95, 61.00 Q 5.75, 50.50, 6.17, 40.00 Q 5.97, 29.50,\
                                 6.03, 19.00 Q 6.30, 17.50, 5.50, 15.65 Q 5.49, 14.27, 6.34, 13.28 Q 7.66, 12.61, 8.04, 11.06 Q 9.67, 11.04, 10.78, 10.63 Q\
                                 11.40, 9.40, 12.65, 9.20 Q 14.02, 8.24, 15.62, 6.93 Q 29.01, 7.23, 42.27, 7.59 Q 55.49, 8.34, 68.67, 8.89 Q 81.83, 8.81, 95.10,\
                                 8.38 Q 96.58, 9.16, 97.96, 10.11 Q 98.68, 11.23, 99.84, 11.34 Q 101.02, 11.46, 102.46, 11.54 Q 102.64, 12.90, 103.19, 13.89\
                                 Q 104.00, 14.73, 104.73, 15.68 Q 106.19, 16.85, 106.96, 18.64 Q 106.99, 29.32, 106.77, 39.92 Q 106.72, 50.46, 106.89, 60.98\
                                 Q 107.07, 71.49, 106.04, 83.03 Q 95.41, 83.21, 85.19, 83.33 Q 75.09, 83.34, 65.05, 83.40 Q 55.02, 83.44, 45.01, 83.48 Q 35.01,\
                                 83.51, 25.00, 83.27 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'34549728\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'34549728\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="34549728_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">RFPL\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="34549728_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">RFPL\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1488392406-1063968397" style="position: absolute; left: 100px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1063968397" data-review-reference-id="1063968397">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 5.67, 71.50, 6.06, 61.00 Q 5.97, 50.50, 6.11, 40.00 Q 7.06, 29.50,\
                                 6.40, 19.00 Q 6.73, 17.50, 6.43, 15.87 Q 7.86, 15.13, 8.57, 14.25 Q 8.69, 13.09, 8.76, 11.76 Q 9.77, 11.18, 10.80, 10.68 Q\
                                 11.44, 9.48, 12.37, 8.56 Q 14.14, 8.57, 15.94, 8.65 Q 29.09, 8.21, 42.32, 8.61 Q 55.48, 8.31, 68.65, 7.88 Q 81.83, 8.61, 95.13,\
                                 8.20 Q 96.52, 9.44, 98.14, 9.63 Q 99.15, 10.16, 100.21, 10.55 Q 101.45, 10.57, 102.46, 11.54 Q 103.13, 12.54, 103.85, 13.49\
                                 Q 104.00, 14.73, 105.23, 15.46 Q 104.79, 17.39, 105.33, 18.94 Q 105.85, 29.42, 106.14, 39.95 Q 106.47, 50.47, 105.75, 60.99\
                                 Q 104.86, 71.50, 105.63, 82.62 Q 95.56, 83.67, 85.24, 83.70 Q 75.08, 83.21, 65.03, 82.85 Q 55.02, 83.25, 45.01, 82.73 Q 35.00,\
                                 82.08, 25.00, 82.31 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1063968397\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1063968397\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1063968397_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1063968397_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1488392406-555069017" style="position: absolute; left: 200px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="555069017" data-review-reference-id="555069017">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 9.00, 71.50, 9.06, 61.00 Q 8.46, 50.50, 8.48, 40.00 Q 8.51, 29.50,\
                                 7.44, 19.00 Q 7.53, 17.50, 7.67, 16.16 Q 8.57, 15.39, 8.85, 14.36 Q 8.79, 13.14, 8.73, 11.74 Q 9.55, 10.88, 11.01, 11.02 Q\
                                 11.93, 10.37, 13.14, 10.32 Q 14.62, 9.82, 16.01, 9.03 Q 29.17, 9.08, 42.37, 9.77 Q 55.50, 9.09, 68.67, 9.02 Q 81.83, 8.92,\
                                 95.09, 8.42 Q 96.62, 9.01, 98.40, 8.92 Q 99.25, 9.92, 100.22, 10.53 Q 101.11, 11.27, 102.07, 11.93 Q 101.73, 13.55, 102.07,\
                                 14.56 Q 102.82, 15.38, 102.92, 16.47 Q 103.53, 17.87, 104.72, 19.05 Q 104.64, 29.53, 104.77, 40.01 Q 104.55, 50.51, 103.35,\
                                 61.02 Q 104.23, 71.50, 104.70, 81.70 Q 95.27, 82.81, 85.10, 82.68 Q 75.06, 82.94, 65.04, 83.13 Q 55.02, 83.35, 45.01, 83.26\
                                 Q 35.00, 81.61, 25.00, 81.86 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'555069017\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'555069017\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="555069017_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="555069017_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1488392406-228247813" style="position: absolute; left: 400px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="228247813" data-review-reference-id="228247813">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 4.89, 71.50, 5.75, 61.00 Q 5.86, 50.50, 7.12, 40.00 Q 6.52, 29.50,\
                                 6.10, 19.00 Q 6.45, 17.50, 6.76, 15.94 Q 7.79, 15.11, 7.40, 13.74 Q 8.82, 13.15, 8.82, 11.82 Q 10.04, 11.56, 10.85, 10.76\
                                 Q 11.76, 10.06, 12.67, 9.24 Q 14.26, 8.88, 15.91, 8.53 Q 29.13, 8.60, 42.30, 8.19 Q 55.49, 8.77, 68.67, 9.53 Q 81.83, 8.53,\
                                 95.18, 7.86 Q 96.85, 8.09, 98.52, 8.59 Q 99.42, 9.53, 100.68, 9.53 Q 101.53, 10.41, 103.20, 10.78 Q 103.68, 12.15, 104.90,\
                                 12.86 Q 105.58, 13.86, 105.91, 15.16 Q 106.08, 16.89, 106.61, 18.70 Q 106.23, 29.39, 105.93, 39.96 Q 106.58, 50.46, 106.38,\
                                 60.98 Q 106.85, 71.49, 105.58, 82.58 Q 95.09, 82.27, 85.05, 82.36 Q 75.02, 82.34, 65.04, 83.28 Q 55.03, 83.61, 45.00, 82.49\
                                 Q 35.00, 82.49, 25.00, 82.08 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'228247813\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'228247813\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="228247813_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="228247813_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1488392406-1638266604" style="position: absolute; left: 300px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1638266604" data-review-reference-id="1638266604">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 6.27, 71.50, 6.50, 61.00 Q 7.30, 50.50, 6.55, 40.00 Q 7.42, 29.50,\
                                 7.78, 19.00 Q 7.38, 17.50, 6.57, 15.90 Q 7.45, 14.98, 8.53, 14.23 Q 9.04, 13.25, 9.58, 12.57 Q 10.32, 11.95, 11.21, 11.36\
                                 Q 12.26, 10.98, 12.86, 9.68 Q 14.51, 9.53, 15.98, 8.90 Q 29.20, 9.41, 42.33, 8.84 Q 55.52, 9.99, 68.67, 8.87 Q 81.83, 8.28,\
                                 95.02, 8.88 Q 96.44, 9.73, 97.99, 10.04 Q 99.16, 10.13, 99.88, 11.26 Q 100.95, 11.61, 102.28, 11.72 Q 103.76, 12.09, 104.66,\
                                 13.00 Q 104.77, 14.30, 105.25, 15.46 Q 105.59, 17.08, 106.21, 18.78 Q 105.80, 29.43, 106.27, 39.94 Q 106.87, 50.46, 107.16,\
                                 60.98 Q 105.38, 71.50, 104.88, 81.88 Q 94.88, 81.65, 85.03, 82.18 Q 75.07, 82.99, 65.03, 82.91 Q 55.01, 82.74, 45.01, 82.87\
                                 Q 35.00, 82.88, 25.00, 82.64 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1638266604\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1638266604\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1638266604_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1638266604_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1488392406-232978909" style="position: absolute; left: 500px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="232978909" data-review-reference-id="232978909">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 5.59, 71.50, 5.82, 61.00 Q 5.95, 50.50, 6.54, 40.00 Q 7.76, 29.50,\
                                 7.03, 19.00 Q 7.50, 17.50, 6.57, 15.90 Q 7.73, 15.08, 8.36, 14.16 Q 8.30, 12.91, 9.15, 12.15 Q 9.96, 11.45, 11.54, 11.90 Q\
                                 12.14, 10.75, 12.74, 9.41 Q 14.26, 8.88, 15.91, 8.53 Q 29.11, 8.39, 42.33, 8.86 Q 55.50, 9.14, 68.67, 9.31 Q 81.84, 9.53,\
                                 94.93, 9.41 Q 96.77, 8.41, 98.45, 8.79 Q 99.20, 10.05, 100.16, 10.65 Q 101.22, 11.04, 102.54, 11.45 Q 103.45, 12.32, 104.35,\
                                 13.19 Q 105.00, 14.17, 105.80, 15.22 Q 105.50, 17.12, 106.55, 18.71 Q 106.92, 29.33, 106.98, 39.91 Q 106.69, 50.46, 106.77,\
                                 60.98 Q 106.87, 71.49, 105.81, 82.80 Q 95.23, 82.70, 85.12, 82.87 Q 75.05, 82.75, 65.02, 82.70 Q 55.02, 82.98, 45.00, 82.55\
                                 Q 35.00, 82.76, 25.00, 83.67 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'232978909\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'232978909\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="232978909_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="232978909_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-1750759685" style="position: absolute; left: 0px; top: 320px; width: 600px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1750759685" data-review-reference-id="1750759685">\
            <div class="stencil-wrapper" style="width: 600px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px;width:600px;" width="600" height="465">\
                     <svg:g width="600" height="465"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, -0.52, 22.55, -0.04 Q 32.83, 1.00, 43.10, 0.60 Q 53.38,\
                        1.28, 63.66, 1.97 Q 73.93, 1.54, 84.21, 0.96 Q 94.48, 0.47, 104.76, 0.47 Q 115.03, 0.65, 125.31, 0.74 Q 135.59, 0.54, 145.86,\
                        0.51 Q 156.14, 0.57, 166.41, 0.83 Q 176.69, 0.89, 186.97, 1.15 Q 197.24, 1.10, 207.52, 1.02 Q 217.79, 0.74, 228.07, 0.69 Q\
                        238.34, 0.82, 248.62, 1.24 Q 258.90, 1.34, 269.17, 0.60 Q 279.45, 0.48, 289.72, 0.63 Q 300.00, 0.52, 310.28, 0.39 Q 320.55,\
                        1.15, 330.83, 1.11 Q 341.10, 0.77, 351.38, 1.03 Q 361.66, 1.13, 371.93, 1.24 Q 382.21, 1.14, 392.48, 1.11 Q 402.76, 1.46,\
                        413.03, 1.39 Q 423.31, 1.16, 433.59, 1.07 Q 443.86, 1.04, 454.14, 1.27 Q 464.41, 1.50, 474.69, 1.27 Q 484.97, 1.20, 495.24,\
                        1.95 Q 505.52, 1.34, 515.79, 1.84 Q 526.07, 2.11, 536.34, 1.36 Q 546.62, 1.34, 556.90, 1.12 Q 567.17, 1.12, 577.45, 1.27 Q\
                        587.72, 1.67, 598.52, 1.48 Q 598.71, 11.78, 599.28, 21.86 Q 598.79, 32.01, 598.77, 42.06 Q 598.91, 52.09, 598.89, 62.12 Q\
                        599.21, 72.15, 599.85, 82.17 Q 599.75, 92.19, 599.98, 102.22 Q 599.69, 112.24, 600.28, 122.26 Q 600.09, 132.28, 598.91, 142.30\
                        Q 598.44, 152.33, 598.92, 162.35 Q 599.21, 172.37, 599.31, 182.39 Q 599.07, 192.41, 598.66, 202.43 Q 598.37, 212.46, 598.46,\
                        222.48 Q 598.37, 232.50, 598.48, 242.52 Q 598.82, 252.54, 599.05, 262.57 Q 599.43, 272.59, 599.48, 282.61 Q 599.66, 292.63,\
                        599.23, 302.65 Q 599.04, 312.67, 598.53, 322.70 Q 598.26, 332.72, 598.78, 342.74 Q 598.36, 352.76, 598.92, 362.78 Q 598.74,\
                        372.80, 597.87, 382.83 Q 598.04, 392.85, 598.21, 402.87 Q 598.25, 412.89, 597.90, 422.91 Q 597.81, 432.93, 597.56, 442.96\
                        Q 598.60, 452.98, 598.22, 463.22 Q 587.87, 463.43, 577.44, 462.92 Q 567.16, 462.79, 556.91, 463.38 Q 546.63, 463.31, 536.35,\
                        463.26 Q 526.07, 463.04, 515.79, 463.12 Q 505.52, 462.41, 495.24, 462.23 Q 484.97, 462.65, 474.69, 463.40 Q 464.41, 463.31,\
                        454.14, 463.48 Q 443.86, 464.29, 433.59, 464.26 Q 423.31, 464.07, 413.03, 462.95 Q 402.76, 462.84, 392.48, 462.81 Q 382.21,\
                        463.20, 371.93, 462.43 Q 361.66, 462.72, 351.38, 462.53 Q 341.10, 463.02, 330.83, 463.48 Q 320.55, 462.78, 310.28, 464.14\
                        Q 300.00, 464.48, 289.72, 463.37 Q 279.45, 464.32, 269.17, 463.79 Q 258.90, 464.49, 248.62, 463.70 Q 238.34, 464.63, 228.07,\
                        464.71 Q 217.79, 464.67, 207.52, 464.40 Q 197.24, 464.94, 186.97, 463.08 Q 176.69, 463.87, 166.41, 463.80 Q 156.14, 463.09,\
                        145.86, 464.01 Q 135.59, 463.57, 125.31, 464.46 Q 115.03, 464.10, 104.76, 464.88 Q 94.48, 464.97, 84.21, 464.57 Q 73.93, 464.03,\
                        63.66, 463.60 Q 53.38, 463.62, 43.10, 463.13 Q 32.83, 463.18, 22.55, 463.09 Q 12.28, 463.62, 1.74, 463.26 Q 1.93, 453.00,\
                        2.10, 442.94 Q 2.01, 432.93, 1.56, 422.93 Q 1.87, 412.89, 0.87, 402.88 Q 0.34, 392.85, 0.22, 382.83 Q 0.20, 372.81, 0.53,\
                        362.78 Q 0.54, 352.76, 0.36, 342.74 Q 0.44, 332.72, 0.93, 322.70 Q 1.22, 312.67, 0.92, 302.65 Q 1.03, 292.63, 1.54, 282.61\
                        Q 1.95, 272.59, 2.04, 262.57 Q 1.59, 252.54, 1.02, 242.52 Q 1.10, 232.50, 1.45, 222.48 Q 1.51, 212.46, 1.07, 202.43 Q 0.76,\
                        192.41, 0.71, 182.39 Q 0.85, 172.37, 1.04, 162.35 Q 1.38, 152.33, 1.53, 142.30 Q 2.04, 132.28, 2.40, 122.26 Q 1.51, 112.24,\
                        1.34, 102.22 Q 1.53, 92.20, 0.42, 82.17 Q 0.73, 72.15, 0.27, 62.13 Q 0.68, 52.11, 1.18, 42.09 Q 0.86, 32.07, 0.50, 22.04 Q\
                        2.00, 12.02, 2.00, 2.00" style=" fill:#a7a77c;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-64528672" style="position: absolute; left: 1px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="64528672" data-review-reference-id="64528672">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:599px;" width="599" height="80">\
                     <svg:g width="599" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.26, 0.44, 22.52, -0.10 Q 32.78, -0.22, 43.03, 0.19 Q 53.29,\
                        0.32, 63.55, -0.08 Q 73.81, 0.69, 84.07, 0.69 Q 94.33, -0.20, 104.59, 0.70 Q 114.84, 0.24, 125.10, 0.54 Q 135.36, 1.18, 145.62,\
                        1.57 Q 155.88, 1.91, 166.14, 1.48 Q 176.40, 0.47, 186.66, 0.17 Q 196.91, 0.46, 207.17, 1.32 Q 217.43, 1.45, 227.69, 1.06 Q\
                        237.95, 0.82, 248.21, 0.87 Q 258.47, 0.78, 268.72, 0.78 Q 278.98, 1.24, 289.24, 0.94 Q 299.50, 0.81, 309.76, 0.76 Q 320.02,\
                        0.78, 330.28, 0.16 Q 340.53, 0.20, 350.79, 0.11 Q 361.05, 0.43, 371.31, 0.52 Q 381.57, 0.33, 391.83, 0.11 Q 402.09, 0.02,\
                        412.34, -0.09 Q 422.60, -0.13, 432.86, -0.00 Q 443.12, 0.59, 453.38, 1.19 Q 463.64, 1.65, 473.90, 1.45 Q 484.15, 1.76, 494.41,\
                        1.32 Q 504.67, 1.10, 514.93, 0.67 Q 525.19, 0.79, 535.45, 0.80 Q 545.71, 2.12, 555.97, 2.02 Q 566.22, 1.99, 576.48, 1.77 Q\
                        586.74, 1.72, 597.49, 1.51 Q 597.80, 14.40, 597.85, 27.21 Q 598.13, 39.92, 597.77, 52.64 Q 597.37, 65.33, 597.72, 78.72 Q\
                        587.09, 79.07, 576.66, 79.29 Q 566.33, 79.64, 556.03, 79.96 Q 545.74, 80.18, 535.46, 79.41 Q 525.20, 79.50, 514.93, 78.64\
                        Q 504.67, 79.38, 494.41, 79.59 Q 484.16, 79.74, 473.90, 79.89 Q 463.64, 79.85, 453.38, 79.96 Q 443.12, 79.82, 432.86, 79.87\
                        Q 422.60, 79.51, 412.34, 79.46 Q 402.09, 79.75, 391.83, 79.97 Q 381.57, 78.91, 371.31, 79.47 Q 361.05, 79.41, 350.79, 78.85\
                        Q 340.53, 79.25, 330.28, 78.56 Q 320.02, 78.88, 309.76, 79.00 Q 299.50, 78.32, 289.24, 78.40 Q 278.98, 78.62, 268.72, 78.84\
                        Q 258.47, 78.92, 248.21, 79.25 Q 237.95, 78.99, 227.69, 79.92 Q 217.43, 80.05, 207.17, 79.92 Q 196.91, 79.33, 186.66, 79.46\
                        Q 176.40, 78.98, 166.14, 77.66 Q 155.88, 78.04, 145.62, 77.72 Q 135.36, 77.34, 125.10, 77.06 Q 114.84, 77.68, 104.59, 77.87\
                        Q 94.33, 78.12, 84.07, 78.02 Q 73.81, 78.22, 63.55, 78.87 Q 53.29, 78.24, 43.03, 78.36 Q 32.78, 77.61, 22.52, 78.70 Q 12.26,\
                        79.14, 1.45, 78.55 Q 1.33, 65.56, 1.39, 52.75 Q 1.37, 40.04, 2.52, 27.32 Q 2.00, 14.67, 2.00, 2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-1938931723" style="position: absolute; left: 0px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1938931723" data-review-reference-id="1938931723">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:600px;" width="600" height="80">\
                     <svg:g width="600" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 1.17, 22.55, 1.11 Q 32.83, 1.51, 43.10, 1.57 Q 53.38, 1.22,\
                        63.66, 1.60 Q 73.93, 1.39, 84.21, 1.86 Q 94.48, 1.47, 104.76, 1.33 Q 115.03, 2.00, 125.31, 1.78 Q 135.59, 2.70, 145.86, 1.76\
                        Q 156.14, 1.76, 166.41, 2.40 Q 176.69, 2.39, 186.97, 1.76 Q 197.24, 1.21, 207.52, 1.83 Q 217.79, 2.22, 228.07, 2.21 Q 238.34,\
                        1.47, 248.62, 1.35 Q 258.90, 1.36, 269.17, 2.19 Q 279.45, 1.43, 289.72, 1.00 Q 300.00, 1.06, 310.28, 0.64 Q 320.55, 1.21,\
                        330.83, 1.21 Q 341.10, 0.99, 351.38, 1.65 Q 361.66, 1.41, 371.93, 1.05 Q 382.21, 1.13, 392.48, 1.16 Q 402.76, 0.62, 413.03,\
                        1.41 Q 423.31, 0.73, 433.59, 1.56 Q 443.86, 1.45, 454.14, 2.31 Q 464.41, 2.07, 474.69, 2.01 Q 484.97, 1.26, 495.24, 1.54 Q\
                        505.52, 1.12, 515.79, 0.72 Q 526.07, 1.59, 536.34, 2.18 Q 546.62, 2.05, 556.90, 2.14 Q 567.17, 2.00, 577.45, 2.12 Q 587.72,\
                        1.47, 598.21, 1.79 Q 599.02, 14.33, 599.31, 27.15 Q 598.67, 39.96, 598.13, 52.66 Q 598.26, 65.33, 598.51, 78.51 Q 588.00,\
                        78.83, 577.62, 79.24 Q 567.25, 79.25, 556.91, 78.53 Q 546.63, 78.73, 536.34, 77.96 Q 526.07, 78.22, 515.79, 78.21 Q 505.52,\
                        78.33, 495.24, 79.00 Q 484.97, 78.88, 474.69, 79.11 Q 464.41, 79.01, 454.14, 79.44 Q 443.86, 77.72, 433.59, 78.27 Q 423.31,\
                        79.06, 413.03, 79.33 Q 402.76, 78.72, 392.48, 78.10 Q 382.21, 78.17, 371.93, 78.05 Q 361.66, 78.08, 351.38, 76.89 Q 341.10,\
                        77.35, 330.83, 77.28 Q 320.55, 77.30, 310.28, 77.41 Q 300.00, 77.66, 289.72, 78.56 Q 279.45, 77.71, 269.17, 77.88 Q 258.90,\
                        77.53, 248.62, 78.59 Q 238.34, 79.38, 228.07, 78.55 Q 217.79, 78.89, 207.52, 78.54 Q 197.24, 78.73, 186.97, 78.19 Q 176.69,\
                        78.44, 166.41, 78.45 Q 156.14, 78.33, 145.86, 78.77 Q 135.59, 77.63, 125.31, 78.20 Q 115.03, 77.84, 104.76, 77.88 Q 94.48,\
                        78.37, 84.21, 79.21 Q 73.93, 78.66, 63.66, 78.26 Q 53.38, 78.44, 43.10, 78.80 Q 32.83, 79.44, 22.55, 79.02 Q 12.28, 78.65,\
                        1.84, 78.16 Q 1.37, 65.54, 1.93, 52.68 Q 1.98, 40.00, 1.29, 27.36 Q 2.00, 14.67, 2.00, 2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-984392688" style="position: absolute; left: 150px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="984392688" data-review-reference-id="984392688">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60">\
                     <svg:g width="200" height="60"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.89, 0.52, 23.78, 0.33 Q 34.67, 0.26, 45.56, 0.17 Q\
                        56.44, 0.64, 67.33, 0.46 Q 78.22, 0.29, 89.11, 0.27 Q 100.00, 0.65, 110.89, 0.83 Q 121.78, 0.99, 132.67, 0.81 Q 143.56, 1.29,\
                        154.44, 1.11 Q 165.33, 1.11, 176.22, 1.12 Q 187.11, 1.07, 198.42, 1.58 Q 198.29, 15.90, 198.63, 29.91 Q 198.07, 44.00, 198.24,\
                        58.24 Q 187.15, 58.13, 176.31, 58.65 Q 165.32, 57.77, 154.44, 57.97 Q 143.56, 58.32, 132.67, 57.97 Q 121.78, 58.59, 110.89,\
                        58.25 Q 100.00, 58.49, 89.11, 57.72 Q 78.22, 57.85, 67.33, 58.06 Q 56.44, 58.06, 45.56, 59.02 Q 34.67, 58.65, 23.78, 58.67\
                        Q 12.89, 58.21, 1.60, 58.40 Q 1.87, 44.04, 2.13, 29.98 Q 2.00, 16.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.34, 2.92, 21.93, 6.43 Q 31.58, 9.76, 41.23, 13.10 Q 50.92,\
                        16.27, 60.66, 19.30 Q 70.60, 21.62, 80.08, 25.50 Q 89.85, 28.43, 99.76, 30.83 Q 109.52, 33.78, 119.33, 36.55 Q 129.23, 39.00,\
                        139.14, 41.42 Q 149.02, 43.93, 158.78, 46.88 Q 168.62, 49.52, 178.25, 52.92 Q 188.20, 55.20, 198.00, 58.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 58.00 Q 11.91, 55.23, 21.81, 52.44 Q 31.62, 49.31, 41.52, 46.51 Q 51.40,\
                        43.66, 61.18, 40.41 Q 70.98, 37.25, 81.15, 35.42 Q 91.31, 33.53, 101.04, 30.14 Q 110.95, 27.37, 120.66, 23.90 Q 130.67, 21.50,\
                        140.08, 16.96 Q 150.37, 15.53, 160.07, 12.04 Q 170.39, 10.70, 180.36, 8.18 Q 190.10, 4.80, 200.00, 2.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-766132793" style="position: absolute; left: 408px; top: 20px; width: 172px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="766132793" data-review-reference-id="766132793">\
            <div class="stencil-wrapper" style="width: 172px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Изменения</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-1198888294" style="position: absolute; left: 95px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1198888294" data-review-reference-id="1198888294">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-600342421" style="position: absolute; left: 0px; top: 355px; width: 600px; height: 400px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="600342421" data-review-reference-id="600342421">\
            <div class="stencil-wrapper" style="width: 600px; height: 400px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-492670737-layer-600342421-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 400px;width:600px;" width="600" height="400">\
                     <svg:g id="__containerId__-492670737-layer-600342421svg" width="600" height="400"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 1.63, 22.55, 1.57 Q 32.83, 1.51, 43.10, 0.99 Q 53.38, 0.48,\
                        63.66, 0.65 Q 73.93, 0.98, 84.21, 0.79 Q 94.48, 0.66, 104.76, 0.46 Q 115.03, 0.45, 125.31, 0.49 Q 135.59, 0.86, 145.86, 0.76\
                        Q 156.14, 0.58, 166.41, 0.51 Q 176.69, 0.94, 186.97, 0.80 Q 197.24, 0.73, 207.52, 0.53 Q 217.79, 0.44, 228.07, 0.80 Q 238.34,\
                        0.81, 248.62, 0.74 Q 258.90, 1.04, 269.17, 1.17 Q 279.45, 1.29, 289.72, 1.15 Q 300.00, 1.65, 310.28, 0.57 Q 320.55, 1.28,\
                        330.83, 0.97 Q 341.10, 1.34, 351.38, 1.47 Q 361.66, 1.32, 371.93, 1.12 Q 382.21, 1.26, 392.48, 0.89 Q 402.76, 1.83, 413.03,\
                        0.63 Q 423.31, 1.37, 433.59, 0.75 Q 443.86, 1.09, 454.14, 1.69 Q 464.41, 0.83, 474.69, 1.69 Q 484.97, 1.96, 495.24, 0.55 Q\
                        505.52, 0.55, 515.79, 0.66 Q 526.07, 0.50, 536.34, 1.07 Q 546.62, 0.95, 556.90, 0.94 Q 567.17, 0.52, 577.45, 0.62 Q 587.72,\
                        1.13, 598.16, 1.84 Q 598.93, 12.11, 599.70, 22.60 Q 599.00, 33.20, 599.33, 43.64 Q 599.13, 54.09, 599.72, 64.51 Q 599.18,\
                        74.94, 598.71, 85.37 Q 598.50, 95.79, 598.91, 106.21 Q 598.70, 116.63, 598.86, 127.05 Q 598.65, 137.47, 599.21, 147.89 Q 598.17,\
                        158.32, 599.03, 168.74 Q 598.61, 179.16, 599.12, 189.58 Q 598.39, 200.00, 597.68, 210.42 Q 598.49, 220.84, 599.86, 231.26\
                        Q 599.65, 241.68, 598.74, 252.11 Q 598.68, 262.53, 599.69, 272.95 Q 599.67, 283.37, 598.95, 293.79 Q 597.97, 304.21, 598.05,\
                        314.63 Q 599.13, 325.05, 598.96, 335.47 Q 597.95, 345.89, 597.68, 356.32 Q 597.37, 366.74, 597.51, 377.16 Q 598.34, 387.58,\
                        598.37, 398.37 Q 587.86, 398.41, 577.62, 399.19 Q 567.25, 399.19, 556.93, 399.04 Q 546.64, 399.04, 536.35, 399.27 Q 526.07,\
                        399.37, 515.79, 398.13 Q 505.52, 398.47, 495.24, 398.48 Q 484.97, 398.52, 474.69, 398.51 Q 464.41, 398.61, 454.14, 398.64\
                        Q 443.86, 398.63, 433.59, 398.43 Q 423.31, 398.12, 413.03, 398.02 Q 402.76, 397.05, 392.48, 397.35 Q 382.21, 398.11, 371.93,\
                        398.81 Q 361.66, 399.18, 351.38, 399.37 Q 341.10, 398.56, 330.83, 398.03 Q 320.55, 398.70, 310.28, 398.02 Q 300.00, 398.00,\
                        289.72, 398.25 Q 279.45, 398.36, 269.17, 398.86 Q 258.90, 398.88, 248.62, 397.99 Q 238.34, 397.09, 228.07, 397.74 Q 217.79,\
                        398.71, 207.52, 398.28 Q 197.24, 398.70, 186.97, 398.88 Q 176.69, 399.29, 166.41, 398.70 Q 156.14, 398.57, 145.86, 398.34\
                        Q 135.59, 398.02, 125.31, 397.90 Q 115.03, 399.02, 104.76, 399.26 Q 94.48, 399.04, 84.21, 397.75 Q 73.93, 397.94, 63.66, 398.52\
                        Q 53.38, 397.60, 43.10, 398.76 Q 32.83, 398.92, 22.55, 398.50 Q 12.28, 399.28, 2.12, 397.88 Q 2.46, 387.43, 3.06, 377.01 Q\
                        2.76, 366.69, 2.51, 356.30 Q 1.22, 345.91, 0.73, 335.48 Q 0.56, 325.06, 0.67, 314.63 Q 2.21, 304.21, 1.82, 293.79 Q 1.78,\
                        283.37, 1.58, 272.95 Q 0.88, 262.53, 0.00, 252.11 Q -0.47, 241.68, 0.04, 231.26 Q 0.68, 220.84, 1.11, 210.42 Q 0.73, 200.00,\
                        1.99, 189.58 Q 1.99, 179.16, 1.28, 168.74 Q 0.99, 158.32, 1.79, 147.89 Q 1.45, 137.47, 1.29, 127.05 Q 1.22, 116.63, 1.21,\
                        106.21 Q 1.29, 95.79, 1.14, 85.37 Q 1.15, 74.95, 1.64, 64.53 Q 1.23, 54.11, 1.22, 43.68 Q 1.25, 33.26, 0.74, 22.84 Q 2.00,\
                        12.42, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 596px; height:396px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-492670737-layer-600342421-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-492670737-layer-600342421-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-492670737-layer-600342421-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-492670737-layer-600342421-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-492670737-layer-600342421-accordion", "596", "396", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-492670737-layer-1575802654" style="position: absolute; left: 0px; top: 385px; width: 600px; height: 280px" data-interactive-element-type="static.iphoneListBackground" class="iphoneListBackground stencil mobile-interaction-potential-trigger " data-stencil-id="1575802654" data-review-reference-id="1575802654">\
            <div class="stencil-wrapper" style="width: 600px; height: 280px">\
               <div title="" style="height: 280px;width:600px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="600" height="280" viewBox="0 0 600 280"><svg:path class=" svg_unselected_element" d="M 2.00, 268.00 Q 1.92, 257.33, 2.77, 246.67 Q 2.94, 236.00, 3.33, 225.33 Q 2.77,\
                     214.67, 2.78, 204.00 Q 2.91, 193.33, 3.01, 182.67 Q 3.72, 172.00, 2.69, 161.33 Q 1.67, 150.67, 1.60, 140.00 Q 1.90, 129.33,\
                     1.58, 118.67 Q 1.73, 108.00, 1.96, 97.33 Q 2.49, 86.67, 3.32, 76.00 Q 2.01, 65.33, 2.59, 54.67 Q 1.17, 44.00, 1.76, 33.33\
                     Q 1.19, 22.67, 1.00, 11.84 Q 1.32, 10.21, 1.80, 8.56 Q 1.97, 7.34, 2.40, 6.25 Q 3.69, 5.61, 4.37, 4.37 Q 5.92, 4.38, 6.92,\
                     3.86 Q 7.79, 3.12, 8.82, 2.59 Q 10.50, 2.50, 12.11, 2.61 Q 22.23, 1.34, 32.56, 1.73 Q 42.87, 2.42, 53.16, 3.82 Q 63.43, 2.72,\
                     73.71, 1.03 Q 84.00, -0.28, 94.28, 0.96 Q 104.57, 1.10, 114.86, 0.24 Q 125.14, 0.98, 135.43, 0.65 Q 145.71, 0.51, 156.00,\
                     0.41 Q 166.29, 0.62, 176.57, 0.77 Q 186.86, 0.20, 197.14, 0.20 Q 207.43, -0.16, 217.71, -0.28 Q 228.00, 0.18, 238.29, 0.96\
                     Q 248.57, 0.81, 258.86, 0.27 Q 269.14, 1.22, 279.43, 0.95 Q 289.71, 1.09, 300.00, 1.15 Q 310.29, 0.99, 320.57, 1.13 Q 330.86,\
                     1.60, 341.14, 1.28 Q 351.43, 0.93, 361.71, 1.08 Q 372.00, 1.83, 382.29, 1.71 Q 392.57, 1.14, 402.86, 1.37 Q 413.14, 1.56,\
                     423.43, 1.97 Q 433.71, 2.06, 444.00, 1.53 Q 454.29, 1.55, 464.57, 2.69 Q 474.86, 2.72, 485.14, 1.75 Q 495.43, 1.25, 505.71,\
                     1.37 Q 516.00, 0.67, 526.29, 0.14 Q 536.57, 0.82, 546.86, 1.52 Q 557.14, 2.25, 567.43, 1.78 Q 577.71, 1.14, 588.14, 1.12 Q\
                     589.59, 2.15, 590.86, 3.38 Q 592.02, 3.46, 593.19, 3.58 Q 594.33, 3.81, 595.71, 4.28 Q 595.71, 5.85, 596.63, 6.62 Q 597.22,\
                     7.60, 597.26, 8.89 Q 597.68, 10.43, 598.09, 11.98 Q 599.02, 22.53, 599.27, 33.19 Q 599.23, 43.85, 599.23, 54.49 Q 599.57,\
                     65.12, 599.03, 75.75 Q 598.73, 86.37, 599.23, 97.00 Q 600.09, 107.62, 600.16, 118.25 Q 600.22, 128.87, 600.38, 139.50 Q 600.43,\
                     150.12, 600.35, 160.75 Q 599.99, 171.37, 599.85, 182.00 Q 599.22, 192.62, 598.57, 203.25 Q 598.87, 213.87, 599.46, 224.50\
                     Q 599.32, 235.12, 599.14, 245.75 Q 599.32, 256.37, 598.63, 267.10 Q 598.74, 268.80, 598.27, 270.47 Q 597.79, 271.56, 597.34,\
                     272.62 Q 596.81, 273.63, 595.46, 274.46 Q 594.39, 275.04, 593.39, 275.66 Q 592.40, 276.23, 591.34, 276.77 Q 589.92, 277.61,\
                     588.24, 278.32 Q 577.84, 278.37, 567.49, 278.47 Q 557.18, 278.51, 546.87, 278.52 Q 536.58, 278.80, 526.29, 278.55 Q 516.00,\
                     278.13, 505.72, 278.21 Q 495.43, 278.11, 485.14, 278.27 Q 474.86, 278.41, 464.57, 278.03 Q 454.29, 278.32, 444.00, 277.61\
                     Q 433.71, 277.21, 423.43, 277.44 Q 413.14, 278.27, 402.86, 278.04 Q 392.57, 278.11, 382.29, 277.52 Q 372.00, 277.60, 361.71,\
                     277.59 Q 351.43, 277.81, 341.14, 278.25 Q 330.86, 277.90, 320.57, 277.27 Q 310.29, 276.80, 300.00, 277.54 Q 289.71, 277.56,\
                     279.43, 277.90 Q 269.14, 278.08, 258.86, 277.98 Q 248.57, 277.62, 238.29, 277.23 Q 228.00, 278.15, 217.71, 278.21 Q 207.43,\
                     277.84, 197.14, 278.11 Q 186.86, 277.72, 176.57, 278.52 Q 166.29, 278.78, 156.00, 278.19 Q 145.71, 278.13, 135.43, 277.16\
                     Q 125.14, 277.48, 114.86, 276.86 Q 104.57, 277.02, 94.29, 276.80 Q 84.00, 277.30, 73.71, 277.75 Q 63.43, 277.90, 53.14, 277.96\
                     Q 42.86, 276.95, 32.57, 277.65 Q 22.29, 278.37, 11.77, 278.42 Q 10.19, 277.75, 8.93, 276.18 Q 7.96, 275.58, 6.74, 275.57 Q\
                     5.67, 275.18, 4.98, 274.02 Q 4.45, 273.03, 4.26, 271.84 Q 2.79, 271.39, 2.11, 270.39 Q 2.50, 268.50, 2.00, 267.00" style="\
                     fill:white;"/><svg:path class=" svg_unselected_element" d="M 1.00, 40.00 Q 11.31, 39.76, 21.62, 40.08 Q 31.93, 39.75, 42.24, 39.91 Q 52.55,\
                     39.35, 62.86, 38.99 Q 73.17, 39.29, 83.48, 38.99 Q 93.79, 38.97, 104.10, 38.53 Q 114.41, 37.95, 124.72, 37.70 Q 135.03, 37.84,\
                     145.34, 37.62 Q 155.66, 37.57, 165.97, 38.55 Q 176.28, 38.59, 186.59, 38.68 Q 196.90, 38.32, 207.21, 38.20 Q 217.52, 38.22,\
                     227.83, 38.07 Q 238.14, 37.91, 248.45, 38.24 Q 258.76, 38.14, 269.07, 38.18 Q 279.38, 37.88, 289.69, 37.64 Q 300.00, 38.19,\
                     310.31, 37.49 Q 320.62, 37.56, 330.93, 38.74 Q 341.24, 39.53, 351.55, 39.11 Q 361.86, 38.67, 372.17, 39.27 Q 382.48, 39.06,\
                     392.79, 38.66 Q 403.10, 38.93, 413.41, 38.37 Q 423.72, 38.90, 434.03, 38.80 Q 444.34, 38.62, 454.65, 38.49 Q 464.97, 38.93,\
                     475.28, 38.79 Q 485.59, 39.15, 495.90, 39.22 Q 506.21, 39.02, 516.52, 38.63 Q 526.83, 38.75, 537.14, 38.77 Q 547.45, 39.05,\
                     557.76, 38.87 Q 568.07, 38.72, 578.38, 38.55 Q 588.69, 40.00, 599.00, 40.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 1.00, 80.00 Q 11.31, 80.76, 21.62, 79.89 Q 31.93, 79.37, 42.24, 79.09 Q 52.55,\
                     79.31, 62.86, 80.13 Q 73.17, 80.15, 83.48, 80.18 Q 93.79, 79.84, 104.10, 79.43 Q 114.41, 79.26, 124.72, 80.21 Q 135.03, 79.47,\
                     145.34, 80.38 Q 155.66, 80.35, 165.97, 79.77 Q 176.28, 79.48, 186.59, 80.19 Q 196.90, 79.85, 207.21, 79.45 Q 217.52, 79.44,\
                     227.83, 79.09 Q 238.14, 80.55, 248.45, 80.89 Q 258.76, 80.09, 269.07, 80.03 Q 279.38, 80.61, 289.69, 81.25 Q 300.00, 80.49,\
                     310.31, 80.86 Q 320.62, 79.51, 330.93, 79.64 Q 341.24, 79.63, 351.55, 79.16 Q 361.86, 78.55, 372.17, 80.48 Q 382.48, 79.95,\
                     392.79, 80.01 Q 403.10, 80.20, 413.41, 79.63 Q 423.72, 80.35, 434.03, 79.85 Q 444.34, 78.83, 454.65, 79.18 Q 464.97, 80.11,\
                     475.28, 79.74 Q 485.59, 79.55, 495.90, 79.90 Q 506.21, 79.27, 516.52, 79.60 Q 526.83, 79.40, 537.14, 80.21 Q 547.45, 80.05,\
                     557.76, 80.15 Q 568.07, 80.54, 578.38, 80.49 Q 588.69, 80.00, 599.00, 80.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 1.00, 120.00 Q 11.31, 117.46, 21.62, 117.43 Q 31.93, 118.11, 42.24, 117.70\
                     Q 52.55, 118.39, 62.86, 118.56 Q 73.17, 118.77, 83.48, 117.92 Q 93.79, 118.94, 104.10, 119.09 Q 114.41, 119.56, 124.72, 119.77\
                     Q 135.03, 120.16, 145.34, 120.18 Q 155.66, 119.65, 165.97, 118.38 Q 176.28, 118.45, 186.59, 119.26 Q 196.90, 119.55, 207.21,\
                     118.70 Q 217.52, 118.94, 227.83, 119.11 Q 238.14, 118.85, 248.45, 118.97 Q 258.76, 119.39, 269.07, 119.41 Q 279.38, 119.71,\
                     289.69, 119.32 Q 300.00, 119.95, 310.31, 119.36 Q 320.62, 118.03, 330.93, 118.02 Q 341.24, 117.96, 351.55, 117.87 Q 361.86,\
                     117.71, 372.17, 117.96 Q 382.48, 117.98, 392.79, 118.10 Q 403.10, 118.02, 413.41, 118.14 Q 423.72, 119.43, 434.03, 119.46\
                     Q 444.34, 119.17, 454.65, 119.29 Q 464.97, 119.76, 475.28, 119.06 Q 485.59, 119.21, 495.90, 119.42 Q 506.21, 120.24, 516.52,\
                     120.15 Q 526.83, 120.09, 537.14, 120.76 Q 547.45, 120.67, 557.76, 120.59 Q 568.07, 120.23, 578.38, 120.04 Q 588.69, 120.00,\
                     599.00, 120.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 1.00, 160.00 Q 11.31, 157.55, 21.62, 157.72 Q 31.93, 157.76, 42.24, 158.10\
                     Q 52.55, 158.24, 62.86, 158.17 Q 73.17, 158.14, 83.48, 158.50 Q 93.79, 158.59, 104.10, 159.02 Q 114.41, 159.03, 124.72, 159.36\
                     Q 135.03, 158.71, 145.34, 158.39 Q 155.66, 158.26, 165.97, 158.24 Q 176.28, 158.20, 186.59, 158.47 Q 196.90, 158.87, 207.21,\
                     158.90 Q 217.52, 158.79, 227.83, 158.88 Q 238.14, 160.34, 248.45, 159.09 Q 258.76, 159.22, 269.07, 158.87 Q 279.38, 158.64,\
                     289.69, 158.21 Q 300.00, 157.99, 310.31, 157.89 Q 320.62, 157.96, 330.93, 157.78 Q 341.24, 158.56, 351.55, 158.12 Q 361.86,\
                     158.58, 372.17, 158.59 Q 382.48, 158.71, 392.79, 159.39 Q 403.10, 159.54, 413.41, 158.95 Q 423.72, 159.79, 434.03, 158.56\
                     Q 444.34, 159.54, 454.65, 159.30 Q 464.97, 160.30, 475.28, 161.72 Q 485.59, 161.12, 495.90, 159.70 Q 506.21, 159.78, 516.52,\
                     159.64 Q 526.83, 160.11, 537.14, 159.37 Q 547.45, 159.43, 557.76, 159.34 Q 568.07, 158.93, 578.38, 159.07 Q 588.69, 160.00,\
                     599.00, 160.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 1.00, 200.00 Q 11.31, 199.14, 21.62, 199.71 Q 31.93, 200.11, 42.24, 200.47\
                     Q 52.55, 201.30, 62.86, 200.28 Q 73.17, 199.87, 83.48, 199.56 Q 93.79, 199.81, 104.10, 199.96 Q 114.41, 199.62, 124.72, 199.22\
                     Q 135.03, 199.13, 145.34, 199.23 Q 155.66, 199.17, 165.97, 199.29 Q 176.28, 199.83, 186.59, 199.61 Q 196.90, 199.71, 207.21,\
                     199.09 Q 217.52, 199.01, 227.83, 199.41 Q 238.14, 199.58, 248.45, 199.35 Q 258.76, 199.64, 269.07, 198.68 Q 279.38, 199.65,\
                     289.69, 199.14 Q 300.00, 198.97, 310.31, 198.87 Q 320.62, 198.82, 330.93, 199.05 Q 341.24, 199.44, 351.55, 200.00 Q 361.86,\
                     199.50, 372.17, 199.30 Q 382.48, 199.75, 392.79, 199.45 Q 403.10, 199.72, 413.41, 199.09 Q 423.72, 199.34, 434.03, 198.33\
                     Q 444.34, 198.88, 454.65, 198.85 Q 464.97, 199.05, 475.28, 199.34 Q 485.59, 199.41, 495.90, 200.19 Q 506.21, 200.50, 516.52,\
                     200.30 Q 526.83, 199.42, 537.14, 198.78 Q 547.45, 199.56, 557.76, 200.33 Q 568.07, 199.96, 578.38, 199.77 Q 588.69, 200.00,\
                     599.00, 200.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 1.00, 240.00 Q 11.31, 239.07, 21.62, 239.25 Q 31.93, 239.28, 42.24, 238.84\
                     Q 52.55, 238.18, 62.86, 239.18 Q 73.17, 239.43, 83.48, 239.53 Q 93.79, 238.33, 104.10, 238.60 Q 114.41, 238.67, 124.72, 239.28\
                     Q 135.03, 238.79, 145.34, 239.25 Q 155.66, 239.35, 165.97, 240.12 Q 176.28, 240.27, 186.59, 238.62 Q 196.90, 238.33, 207.21,\
                     239.04 Q 217.52, 238.90, 227.83, 238.95 Q 238.14, 238.87, 248.45, 239.81 Q 258.76, 239.77, 269.07, 239.21 Q 279.38, 239.26,\
                     289.69, 240.12 Q 300.00, 239.89, 310.31, 239.82 Q 320.62, 239.78, 330.93, 239.81 Q 341.24, 240.31, 351.55, 239.90 Q 361.86,\
                     239.82, 372.17, 238.27 Q 382.48, 238.44, 392.79, 238.14 Q 403.10, 237.81, 413.41, 238.54 Q 423.72, 237.79, 434.03, 237.77\
                     Q 444.34, 237.58, 454.65, 238.23 Q 464.97, 237.77, 475.28, 238.82 Q 485.59, 238.82, 495.90, 239.25 Q 506.21, 239.63, 516.52,\
                     238.55 Q 526.83, 238.53, 537.14, 238.49 Q 547.45, 238.56, 557.76, 238.37 Q 568.07, 238.60, 578.38, 239.45 Q 588.69, 240.00,\
                     599.00, 240.00" style=" fill:none;"/>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-771123316" style="position: absolute; left: 0px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="771123316" data-review-reference-id="771123316">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 80px;width:86px;" width="86" height="80">\
                     <svg:g width="86" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 15.17, -0.49, 28.33, -0.58 Q 41.50, 0.16, 54.67, 0.00 Q 67.83,\
                        -0.35, 81.63, 1.37 Q 82.03, 13.82, 82.44, 26.13 Q 83.01, 38.37, 83.28, 50.59 Q 81.72, 62.82, 81.24, 75.24 Q 67.85, 75.04,\
                        54.69, 75.15 Q 41.55, 75.75, 28.37, 76.14 Q 15.19, 76.59, 1.10, 75.90 Q 1.12, 63.12, 0.52, 50.87 Q 0.74, 38.58, 1.66, 26.34\
                        Q 2.00, 14.17, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 82.00, 4.00 Q 82.39, 16.33, 82.43, 28.67 Q 81.96, 41.00, 82.18, 53.33 Q 82.00,\
                        65.67, 82.00, 78.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 83.00, 5.00 Q 82.59, 17.33, 82.67, 29.67 Q 83.07, 42.00, 82.77, 54.33 Q 83.00,\
                        66.67, 83.00, 79.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 84.00, 6.00 Q 84.73, 18.33, 84.67, 30.67 Q 85.21, 43.00, 84.79, 55.33 Q 84.00,\
                        67.67, 84.00, 80.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 76.00 Q 14.00, 73.74, 24.00, 73.70 Q 34.00, 73.56, 44.00, 74.09 Q 54.00,\
                        73.93, 64.00, 74.38 Q 74.00, 76.00, 84.00, 76.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 77.00 Q 15.00, 75.93, 25.00, 75.74 Q 35.00, 75.67, 45.00, 75.49 Q 55.00,\
                        75.61, 65.00, 75.88 Q 75.00, 77.00, 85.00, 77.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 78.00 Q 16.00, 77.82, 26.00, 77.83 Q 36.00, 77.98, 46.00, 77.76 Q 56.00,\
                        77.09, 66.00, 77.77 Q 76.00, 78.00, 86.00, 78.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-492670737-layer-771123316button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-492670737-layer-771123316button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-492670737-layer-771123316button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:82px;height:76px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				=<br />  \
                     			</button></div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="492670737"] .border-wrapper, body[data-current-page-id="492670737"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="492670737"] .border-wrapper, body.has-frame[data-current-page-id="492670737"] .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="492670737"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="492670737"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "492670737",\
      			"name": "changes",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
   <div id="border-wrapper">\
      <div xmlns="http://www.w3.org/1999/xhtml" xmlns:json="http://json.org/" class="svg-border svg-border-600-1024" style="display: none;">\
         <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" class="svg_border" style="position:absolute;left:-43px;top:-13px;width:657px;height:1048px;"><svg:path class=" svg_unselected_element" d="M 32.00, 3.00 Q 42.17, 1.56, 52.33, 2.15 Q 62.50, 2.56, 72.67, 2.64 Q 82.83,\
            2.54, 93.00, 2.68 Q 103.17, 2.26, 113.33, 2.86 Q 123.50, 2.01, 133.67, 2.43 Q 143.83, 2.23, 154.00, 2.51 Q 164.17, 2.65, 174.33,\
            2.06 Q 184.50, 2.70, 194.67, 3.20 Q 204.83, 2.43, 215.00, 2.46 Q 225.17, 2.11, 235.33, 2.40 Q 245.50, 3.01, 255.67, 2.08 Q\
            265.83, 2.20, 276.00, 2.52 Q 286.17, 3.12, 296.33, 2.93 Q 306.50, 1.50, 316.67, 1.56 Q 326.83, 2.66, 337.00, 2.91 Q 347.17,\
            2.86, 357.33, 3.18 Q 367.50, 2.88, 377.67, 2.13 Q 387.83, 2.22, 398.00, 2.20 Q 408.17, 1.78, 418.33, 2.78 Q 428.50, 2.91,\
            438.67, 2.75 Q 448.83, 3.51, 459.00, 3.30 Q 469.17, 2.59, 479.33, 1.84 Q 489.50, 2.10, 499.67, 2.06 Q 509.83, 2.44, 520.00,\
            2.23 Q 530.17, 2.05, 540.33, 2.82 Q 550.50, 3.23, 560.67, 3.04 Q 570.83, 2.92, 581.00, 1.80 Q 591.17, 1.70, 601.33, 1.77 Q\
            611.50, 2.88, 621.67, 2.27 Q 631.83, 2.32, 641.99, 3.01 Q 642.13, 13.04, 642.22, 23.14 Q 642.87, 33.21, 642.80, 43.33 Q 643.07,\
            53.42, 643.81, 63.52 Q 644.13, 73.61, 643.25, 83.70 Q 642.45, 93.79, 642.13, 103.88 Q 641.26, 113.97, 641.84, 124.06 Q 641.95,\
            134.15, 642.42, 144.24 Q 642.40, 154.32, 641.26, 164.41 Q 641.89, 174.50, 642.69, 184.59 Q 643.27, 194.68, 642.85, 204.76\
            Q 642.37, 214.85, 641.94, 224.94 Q 642.38, 235.03, 643.21, 245.12 Q 643.90, 255.21, 643.22, 265.29 Q 642.92, 275.38, 642.82,\
            285.47 Q 642.01, 295.56, 642.01, 305.65 Q 641.36, 315.74, 642.46, 325.82 Q 642.30, 335.91, 642.66, 346.00 Q 642.77, 356.09,\
            643.12, 366.18 Q 642.90, 376.26, 643.03, 386.35 Q 643.37, 396.44, 643.02, 406.53 Q 642.89, 416.62, 642.75, 426.71 Q 642.61,\
            436.79, 642.88, 446.88 Q 642.20, 456.97, 642.29, 467.06 Q 643.17, 477.15, 643.37, 487.24 Q 642.87, 497.32, 643.22, 507.41\
            Q 643.22, 517.50, 643.97, 527.59 Q 643.21, 537.68, 642.57, 547.76 Q 642.67, 557.85, 643.48, 567.94 Q 643.39, 578.03, 643.12,\
            588.12 Q 642.46, 598.21, 642.25, 608.29 Q 643.11, 618.38, 643.14, 628.47 Q 641.92, 638.56, 642.72, 648.65 Q 642.49, 658.74,\
            643.00, 668.82 Q 643.11, 678.91, 643.28, 689.00 Q 643.00, 699.09, 643.54, 709.18 Q 643.66, 719.27, 644.11, 729.35 Q 644.00,\
            739.44, 643.92, 749.53 Q 643.72, 759.62, 643.46, 769.71 Q 643.24, 779.79, 643.74, 789.88 Q 643.19, 799.97, 643.17, 810.06\
            Q 642.29, 820.15, 642.51, 830.24 Q 643.25, 840.32, 643.27, 850.41 Q 643.49, 860.50, 644.00, 870.59 Q 642.98, 880.68, 643.45,\
            890.77 Q 643.48, 900.85, 643.11, 910.94 Q 642.54, 921.03, 641.24, 931.12 Q 641.90, 941.21, 642.20, 951.29 Q 642.56, 961.38,\
            643.01, 971.47 Q 643.73, 981.56, 643.86, 991.65 Q 643.34, 1001.74, 643.38, 1011.82 Q 643.21, 1021.91, 642.38, 1032.38 Q 631.97,\
            1032.40, 621.69, 1032.17 Q 611.50, 1032.08, 601.35, 1032.48 Q 591.18, 1032.87, 581.01, 1033.43 Q 570.84, 1033.57, 560.67,\
            1033.57 Q 550.50, 1033.62, 540.33, 1032.27 Q 530.17, 1032.28, 520.00, 1032.88 Q 509.83, 1033.89, 499.67, 1032.82 Q 489.50,\
            1032.08, 479.33, 1032.36 Q 469.17, 1032.74, 459.00, 1033.30 Q 448.83, 1032.87, 438.67, 1031.99 Q 428.50, 1031.99, 418.33,\
            1033.23 Q 408.17, 1033.98, 398.00, 1032.74 Q 387.83, 1032.46, 377.67, 1033.26 Q 367.50, 1032.76, 357.33, 1033.15 Q 347.17,\
            1033.16, 337.00, 1033.08 Q 326.83, 1033.79, 316.67, 1033.20 Q 306.50, 1032.92, 296.33, 1032.90 Q 286.17, 1033.00, 276.00,\
            1033.42 Q 265.83, 1033.70, 255.67, 1033.95 Q 245.50, 1033.94, 235.33, 1033.58 Q 225.17, 1033.94, 215.00, 1033.90 Q 204.83,\
            1033.58, 194.67, 1033.43 Q 184.50, 1032.56, 174.33, 1033.06 Q 164.17, 1032.98, 154.00, 1032.91 Q 143.83, 1033.29, 133.67,\
            1033.02 Q 123.50, 1033.08, 113.33, 1032.55 Q 103.17, 1032.39, 93.00, 1033.03 Q 82.83, 1033.16, 72.67, 1032.32 Q 62.50, 1032.91,\
            52.33, 1032.74 Q 42.17, 1032.51, 31.94, 1032.07 Q 31.91, 1021.94, 31.10, 1011.95 Q 31.78, 1001.75, 30.61, 991.69 Q 30.93,\
            981.58, 30.10, 971.49 Q 29.87, 961.39, 30.16, 951.30 Q 30.36, 941.21, 30.59, 931.12 Q 30.45, 921.03, 30.95, 910.94 Q 30.40,\
            900.85, 31.03, 890.77 Q 30.67, 880.68, 31.59, 870.59 Q 32.17, 860.50, 31.94, 850.41 Q 31.39, 840.32, 31.38, 830.24 Q 31.21,\
            820.15, 30.98, 810.06 Q 32.29, 799.97, 32.02, 789.88 Q 30.29, 779.79, 29.88, 769.71 Q 30.38, 759.62, 30.76, 749.53 Q 30.71,\
            739.44, 31.36, 729.35 Q 30.96, 719.27, 31.65, 709.18 Q 31.10, 699.09, 31.28, 689.00 Q 32.25, 678.91, 31.92, 668.82 Q 31.43,\
            658.74, 31.24, 648.65 Q 31.28, 638.56, 30.96, 628.47 Q 31.23, 618.38, 30.30, 608.29 Q 30.16, 598.21, 30.30, 588.12 Q 30.89,\
            578.03, 30.80, 567.94 Q 30.90, 557.85, 30.99, 547.76 Q 30.86, 537.68, 30.93, 527.59 Q 31.62, 517.50, 31.40, 507.41 Q 31.33,\
            497.32, 30.73, 487.24 Q 31.03, 477.15, 31.80, 467.06 Q 31.93, 456.97, 31.10, 446.88 Q 31.89, 436.79, 31.77, 426.71 Q 32.15,\
            416.62, 31.05, 406.53 Q 31.06, 396.44, 31.10, 386.35 Q 31.32, 376.26, 31.27, 366.18 Q 30.82, 356.09, 31.16, 346.00 Q 31.35,\
            335.91, 31.10, 325.82 Q 31.61, 315.74, 32.48, 305.65 Q 31.76, 295.56, 31.24, 285.47 Q 31.48, 275.38, 30.80, 265.29 Q 31.40,\
            255.21, 31.04, 245.12 Q 31.73, 235.03, 31.71, 224.94 Q 32.22, 214.85, 31.62, 204.76 Q 31.37, 194.68, 30.93, 184.59 Q 31.33,\
            174.50, 31.93, 164.41 Q 32.55, 154.32, 31.68, 144.24 Q 31.29, 134.15, 31.07, 124.06 Q 30.37, 113.97, 30.19, 103.88 Q 30.02,\
            93.79, 29.87, 83.71 Q 30.24, 73.62, 30.19, 63.53 Q 30.04, 53.44, 29.86, 43.35 Q 29.70, 33.26, 29.53, 23.18 Q 32.00, 13.09,\
            32.00, 3.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 23.00, 7.00 Q 33.17, 8.10, 43.33, 7.70 Q 53.50, 7.28, 63.67, 7.28 Q 73.83,\
            7.13, 84.00, 7.40 Q 94.17, 7.73, 104.33, 7.47 Q 114.50, 6.25, 124.67, 6.90 Q 134.83, 7.25, 145.00, 7.52 Q 155.17, 6.13, 165.33,\
            7.50 Q 175.50, 7.30, 185.67, 7.98 Q 195.83, 6.20, 206.00, 6.21 Q 216.17, 7.37, 226.33, 7.42 Q 236.50, 7.08, 246.67, 7.37 Q\
            256.83, 7.93, 267.00, 8.33 Q 277.17, 6.22, 287.33, 6.01 Q 297.50, 5.16, 307.67, 6.61 Q 317.83, 6.52, 328.00, 6.09 Q 338.17,\
            7.22, 348.33, 7.87 Q 358.50, 6.75, 368.67, 6.44 Q 378.83, 6.33, 389.00, 6.13 Q 399.17, 6.50, 409.33, 6.32 Q 419.50, 6.55,\
            429.67, 6.26 Q 439.83, 6.11, 450.00, 6.23 Q 460.17, 6.35, 470.33, 6.13 Q 480.50, 6.34, 490.67, 6.61 Q 500.83, 7.28, 511.00,\
            7.07 Q 521.17, 6.10, 531.33, 5.17 Q 541.50, 5.53, 551.67, 6.41 Q 561.83, 5.78, 572.00, 4.86 Q 582.17, 5.71, 592.33, 5.75 Q\
            602.50, 5.46, 612.67, 5.06 Q 622.83, 5.36, 634.10, 5.90 Q 634.19, 16.69, 634.07, 27.02 Q 634.00, 37.20, 633.06, 47.35 Q 633.06,\
            57.44, 634.50, 67.52 Q 634.63, 77.61, 634.45, 87.70 Q 634.18, 97.79, 634.21, 107.88 Q 633.87, 117.97, 634.46, 128.06 Q 634.27,\
            138.15, 633.94, 148.24 Q 633.76, 158.32, 633.93, 168.41 Q 633.29, 178.50, 633.65, 188.59 Q 634.28, 198.68, 634.39, 208.76\
            Q 634.31, 218.85, 634.37, 228.94 Q 633.70, 239.03, 633.63, 249.12 Q 633.75, 259.21, 634.04, 269.29 Q 634.32, 279.38, 634.96,\
            289.47 Q 634.75, 299.56, 634.93, 309.65 Q 634.57, 319.74, 634.17, 329.82 Q 633.17, 339.91, 632.47, 350.00 Q 633.04, 360.09,\
            633.13, 370.18 Q 633.61, 380.26, 634.37, 390.35 Q 634.54, 400.44, 634.71, 410.53 Q 634.47, 420.62, 633.66, 430.71 Q 632.84,\
            440.79, 632.55, 450.88 Q 633.12, 460.97, 633.68, 471.06 Q 634.12, 481.15, 634.81, 491.24 Q 633.74, 501.32, 633.16, 511.41\
            Q 633.47, 521.50, 633.57, 531.59 Q 634.05, 541.68, 633.96, 551.76 Q 634.63, 561.85, 634.08, 571.94 Q 634.16, 582.03, 633.81,\
            592.12 Q 634.65, 602.21, 634.67, 612.29 Q 634.43, 622.38, 633.89, 632.47 Q 634.65, 642.56, 634.92, 652.65 Q 634.12, 662.74,\
            634.78, 672.82 Q 634.22, 682.91, 634.23, 693.00 Q 634.36, 703.09, 634.33, 713.18 Q 634.28, 723.26, 634.42, 733.35 Q 634.22,\
            743.44, 633.94, 753.53 Q 633.77, 763.62, 633.79, 773.71 Q 633.74, 783.79, 632.93, 793.88 Q 634.49, 803.97, 633.72, 814.06\
            Q 634.41, 824.15, 633.91, 834.24 Q 633.57, 844.32, 634.42, 854.41 Q 633.33, 864.50, 634.03, 874.59 Q 633.62, 884.68, 633.46,\
            894.77 Q 633.49, 904.85, 633.39, 914.94 Q 634.09, 925.03, 633.22, 935.12 Q 633.13, 945.21, 632.12, 955.29 Q 632.36, 965.38,\
            633.03, 975.47 Q 633.18, 985.56, 634.12, 995.65 Q 634.27, 1005.74, 633.83, 1015.82 Q 633.26, 1025.91, 633.69, 1036.69 Q 622.94,\
            1036.33, 612.74, 1036.51 Q 602.49, 1035.86, 592.33, 1035.99 Q 582.17, 1036.18, 572.00, 1036.09 Q 561.83, 1035.76, 551.67,\
            1035.61 Q 541.50, 1036.26, 531.33, 1035.99 Q 521.17, 1036.21, 511.00, 1036.83 Q 500.83, 1037.17, 490.67, 1037.77 Q 480.50,\
            1037.67, 470.33, 1037.59 Q 460.17, 1037.40, 450.00, 1037.96 Q 439.83, 1036.06, 429.67, 1037.16 Q 419.50, 1036.88, 409.33,\
            1036.96 Q 399.17, 1036.92, 389.00, 1036.80 Q 378.83, 1036.80, 368.67, 1037.09 Q 358.50, 1036.71, 348.33, 1037.46 Q 338.17,\
            1037.29, 328.00, 1038.12 Q 317.83, 1038.19, 307.67, 1038.01 Q 297.50, 1037.78, 287.33, 1037.62 Q 277.17, 1037.35, 267.00,\
            1036.70 Q 256.83, 1037.34, 246.67, 1037.09 Q 236.50, 1037.02, 226.33, 1037.00 Q 216.17, 1036.67, 206.00, 1036.84 Q 195.83,\
            1036.80, 185.67, 1037.04 Q 175.50, 1037.05, 165.33, 1037.33 Q 155.17, 1037.38, 145.00, 1037.76 Q 134.83, 1037.89, 124.67,\
            1037.87 Q 114.50, 1037.09, 104.33, 1037.40 Q 94.17, 1037.60, 84.00, 1037.58 Q 73.83, 1036.41, 63.67, 1035.22 Q 53.50, 1035.43,\
            43.33, 1035.49 Q 33.17, 1036.62, 22.77, 1036.23 Q 22.92, 1025.94, 22.83, 1015.85 Q 22.15, 1005.79, 22.25, 995.67 Q 22.40,\
            985.57, 23.32, 975.47 Q 22.39, 965.39, 21.86, 955.30 Q 22.40, 945.21, 22.61, 935.12 Q 22.70, 925.03, 23.24, 914.94 Q 22.39,\
            904.85, 23.89, 894.77 Q 22.67, 884.68, 22.25, 874.59 Q 21.81, 864.50, 22.08, 854.41 Q 22.40, 844.32, 22.25, 834.24 Q 22.27,\
            824.15, 22.70, 814.06 Q 22.01, 803.97, 21.67, 793.88 Q 21.48, 783.79, 22.04, 773.71 Q 22.61, 763.62, 23.02, 753.53 Q 22.52,\
            743.44, 22.53, 733.35 Q 21.77, 723.26, 23.14, 713.18 Q 22.63, 703.09, 23.06, 693.00 Q 22.86, 682.91, 22.90, 672.82 Q 22.53,\
            662.74, 22.48, 652.65 Q 22.36, 642.56, 22.43, 632.47 Q 22.20, 622.38, 22.54, 612.29 Q 22.59, 602.21, 22.54, 592.12 Q 22.29,\
            582.03, 22.22, 571.94 Q 22.01, 561.85, 21.95, 551.76 Q 21.95, 541.68, 22.46, 531.59 Q 21.50, 521.50, 22.28, 511.41 Q 22.92,\
            501.32, 23.37, 491.24 Q 23.02, 481.15, 21.15, 471.06 Q 21.47, 460.97, 22.71, 450.88 Q 22.30, 440.79, 21.54, 430.71 Q 21.95,\
            420.62, 22.19, 410.53 Q 23.36, 400.44, 23.22, 390.35 Q 22.85, 380.26, 21.59, 370.18 Q 21.89, 360.09, 23.29, 350.00 Q 23.38,\
            339.91, 22.14, 329.82 Q 22.18, 319.74, 22.67, 309.65 Q 23.61, 299.56, 23.28, 289.47 Q 23.09, 279.38, 23.34, 269.29 Q 23.49,\
            259.21, 23.23, 249.12 Q 23.12, 239.03, 22.67, 228.94 Q 23.68, 218.85, 24.05, 208.76 Q 24.33, 198.68, 23.59, 188.59 Q 22.72,\
            178.50, 23.51, 168.41 Q 23.02, 158.32, 22.89, 148.24 Q 21.59, 138.15, 21.89, 128.06 Q 22.55, 117.97, 22.12, 107.88 Q 21.81,\
            97.79, 22.05, 87.71 Q 22.53, 77.62, 21.54, 67.53 Q 21.91, 57.44, 21.92, 47.35 Q 21.94, 37.26, 22.36, 27.18 Q 23.00, 17.09,\
            23.00, 7.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 40.00, 11.00 Q 50.17, 10.03, 60.33, 10.39 Q 70.50, 10.83, 80.67, 11.11 Q 90.83,\
            9.95, 101.00, 12.00 Q 111.17, 11.23, 121.33, 11.70 Q 131.50, 11.23, 141.67, 9.44 Q 151.83, 11.37, 162.00, 13.09 Q 172.17,\
            12.11, 182.33, 11.63 Q 192.50, 10.56, 202.67, 11.29 Q 212.83, 11.87, 223.00, 11.10 Q 233.17, 9.95, 243.33, 9.18 Q 253.50,\
            9.14, 263.67, 11.90 Q 273.83, 11.74, 284.00, 13.32 Q 294.17, 12.08, 304.33, 10.84 Q 314.50, 10.49, 324.67, 11.09 Q 334.83,\
            10.79, 345.00, 10.38 Q 355.17, 10.36, 365.33, 10.12 Q 375.50, 9.99, 385.67, 10.64 Q 395.83, 10.51, 406.00, 10.37 Q 416.17,\
            10.12, 426.33, 9.76 Q 436.50, 9.39, 446.67, 9.67 Q 456.83, 9.24, 467.00, 8.75 Q 477.17, 9.56, 487.33, 10.89 Q 497.50, 10.97,\
            507.67, 10.17 Q 517.83, 10.80, 528.00, 12.48 Q 538.17, 12.26, 548.33, 10.69 Q 558.50, 9.59, 568.67, 9.31 Q 578.83, 9.06, 589.00,\
            9.25 Q 599.17, 10.10, 609.33, 11.37 Q 619.50, 11.43, 629.67, 11.79 Q 639.83, 11.59, 650.11, 10.89 Q 651.08, 20.73, 651.13,\
            31.02 Q 651.33, 41.18, 651.50, 51.30 Q 651.61, 61.42, 651.68, 71.52 Q 651.73, 81.61, 651.83, 91.70 Q 651.85, 101.79, 651.80,\
            111.88 Q 651.25, 121.97, 651.08, 132.06 Q 651.02, 142.15, 651.18, 152.24 Q 650.94, 162.32, 651.64, 172.41 Q 651.86, 182.50,\
            651.58, 192.59 Q 651.51, 202.68, 650.83, 212.76 Q 650.72, 222.85, 651.02, 232.94 Q 651.05, 243.03, 651.19, 253.12 Q 650.99,\
            263.21, 651.09, 273.29 Q 650.75, 283.38, 650.35, 293.47 Q 649.81, 303.56, 650.11, 313.65 Q 650.78, 323.74, 651.90, 333.82\
            Q 652.10, 343.91, 651.28, 354.00 Q 649.96, 364.09, 649.44, 374.18 Q 649.71, 384.26, 649.66, 394.35 Q 650.12, 404.44, 651.03,\
            414.53 Q 651.67, 424.62, 651.37, 434.71 Q 651.65, 444.79, 651.46, 454.88 Q 651.50, 464.97, 651.43, 475.06 Q 651.77, 485.15,\
            651.69, 495.24 Q 651.78, 505.32, 651.92, 515.41 Q 651.95, 525.50, 651.38, 535.59 Q 650.71, 545.68, 651.41, 555.76 Q 650.72,\
            565.85, 650.89, 575.94 Q 649.96, 586.03, 649.68, 596.12 Q 649.84, 606.21, 649.89, 616.29 Q 650.94, 626.38, 651.02, 636.47\
            Q 651.06, 646.56, 651.10, 656.65 Q 651.27, 666.74, 651.35, 676.82 Q 651.54, 686.91, 651.13, 697.00 Q 651.10, 707.09, 651.19,\
            717.18 Q 651.28, 727.27, 651.32, 737.35 Q 650.54, 747.44, 651.25, 757.53 Q 650.40, 767.62, 650.94, 777.71 Q 651.56, 787.79,\
            650.87, 797.88 Q 651.76, 807.97, 651.27, 818.06 Q 650.52, 828.15, 650.42, 838.24 Q 650.81, 848.32, 650.35, 858.41 Q 650.87,\
            868.50, 650.42, 878.59 Q 650.41, 888.68, 650.36, 898.77 Q 649.99, 908.85, 649.83, 918.94 Q 649.97, 929.03, 650.89, 939.12\
            Q 650.31, 949.21, 651.29, 959.29 Q 650.76, 969.38, 650.55, 979.47 Q 651.09, 989.56, 650.89, 999.65 Q 651.25, 1009.74, 650.09,\
            1019.82 Q 650.76, 1029.91, 650.29, 1040.29 Q 640.06, 1040.67, 629.79, 1040.84 Q 619.56, 1040.87, 609.36, 1040.77 Q 599.18,\
            1040.68, 589.01, 1040.90 Q 578.84, 1040.47, 568.67, 1040.69 Q 558.50, 1040.63, 548.33, 1041.45 Q 538.17, 1041.51, 528.00,\
            1041.59 Q 517.83, 1041.46, 507.67, 1041.42 Q 497.50, 1042.24, 487.33, 1040.85 Q 477.17, 1042.19, 467.00, 1041.41 Q 456.83,\
            1042.12, 446.67, 1040.81 Q 436.50, 1040.17, 426.33, 1041.30 Q 416.17, 1040.92, 406.00, 1040.87 Q 395.83, 1041.45, 385.67,\
            1040.61 Q 375.50, 1040.41, 365.33, 1040.80 Q 355.17, 1040.01, 345.00, 1040.98 Q 334.83, 1041.07, 324.67, 1040.96 Q 314.50,\
            1041.51, 304.33, 1041.58 Q 294.17, 1041.76, 284.00, 1041.84 Q 273.83, 1041.90, 263.67, 1041.57 Q 253.50, 1041.65, 243.33,\
            1041.53 Q 233.17, 1041.28, 223.00, 1040.77 Q 212.83, 1041.15, 202.67, 1040.68 Q 192.50, 1040.62, 182.33, 1040.62 Q 172.17,\
            1039.76, 162.00, 1040.93 Q 151.83, 1040.93, 141.67, 1040.81 Q 131.50, 1041.02, 121.33, 1041.32 Q 111.17, 1040.92, 101.00,\
            1040.88 Q 90.83, 1040.98, 80.67, 1041.07 Q 70.50, 1041.26, 60.33, 1041.03 Q 50.17, 1040.71, 39.38, 1040.62 Q 39.10, 1030.21,\
            39.49, 1019.90 Q 39.90, 1009.74, 39.87, 999.65 Q 39.41, 989.57, 39.12, 979.48 Q 39.52, 969.39, 40.15, 959.29 Q 39.66, 949.21,\
            38.83, 939.12 Q 38.65, 929.03, 39.50, 918.94 Q 39.88, 908.85, 40.30, 898.77 Q 39.99, 888.68, 39.71, 878.59 Q 39.86, 868.50,\
            39.31, 858.41 Q 38.75, 848.32, 38.67, 838.24 Q 38.97, 828.15, 39.12, 818.06 Q 39.47, 807.97, 39.37, 797.88 Q 39.42, 787.79,\
            39.29, 777.71 Q 38.94, 767.62, 38.98, 757.53 Q 38.89, 747.44, 38.84, 737.35 Q 38.64, 727.27, 38.82, 717.18 Q 39.41, 707.09,\
            39.56, 697.00 Q 39.86, 686.91, 39.40, 676.82 Q 39.33, 666.74, 39.11, 656.65 Q 38.66, 646.56, 38.69, 636.47 Q 38.58, 626.38,\
            38.70, 616.29 Q 40.19, 606.21, 40.50, 596.12 Q 40.38, 586.03, 39.17, 575.94 Q 39.12, 565.85, 40.35, 555.76 Q 39.60, 545.68,\
            39.33, 535.59 Q 39.19, 525.50, 39.27, 515.41 Q 39.97, 505.32, 39.97, 495.24 Q 39.74, 485.15, 38.40, 475.06 Q 37.99, 464.97,\
            38.94, 454.88 Q 40.04, 444.79, 40.17, 434.71 Q 39.78, 424.62, 39.61, 414.53 Q 39.28, 404.44, 38.66, 394.35 Q 38.63, 384.26,\
            39.05, 374.18 Q 39.12, 364.09, 38.33, 354.00 Q 39.08, 343.91, 39.31, 333.82 Q 39.73, 323.74, 39.33, 313.65 Q 38.91, 303.56,\
            39.39, 293.47 Q 40.20, 283.38, 40.22, 273.29 Q 40.22, 263.21, 40.41, 253.12 Q 40.29, 243.03, 40.83, 232.94 Q 39.78, 222.85,\
            39.43, 212.76 Q 39.19, 202.68, 39.83, 192.59 Q 40.42, 182.50, 38.53, 172.41 Q 38.98, 162.32, 39.20, 152.24 Q 39.79, 142.15,\
            39.50, 132.06 Q 39.16, 121.97, 39.39, 111.88 Q 38.99, 101.79, 39.41, 91.71 Q 39.07, 81.62, 39.16, 71.53 Q 39.78, 61.44, 40.58,\
            51.35 Q 40.38, 41.26, 39.94, 31.18 Q 40.00, 21.09, 40.00, 11.00" style=" fill:white;"/>\
         </svg:svg>\
      </div>\
   </div>\
</div>');