rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__132438759-layer" class="layer" name="__containerId__pageLayer" data-layer-id="132438759" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-132438759-layer-321818127" style="position: absolute; left: -1px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="321818127" data-review-reference-id="321818127">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024" viewBox="0 0 601 1024">\
                     <svg:g width="601" height="1024">\
                        <svg:rect x="0" y="0" width="601" height="1024" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="601" y2="1024" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="1024" x2="601" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-132438759-layer-1713799692" style="position: absolute; left: 1px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1713799692" data-review-reference-id="1713799692">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:599px;" width="599" height="80" viewBox="0 0 599 80">\
                     <svg:g width="599" height="80">\
                        <svg:rect x="0" y="0" width="599" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-132438759-layer-797320150" style="position: absolute; left: 0px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="797320150" data-review-reference-id="797320150">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:600px;" width="600" height="80" viewBox="0 0 600 80">\
                     <svg:g width="600" height="80">\
                        <svg:rect x="0" y="0" width="600" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-132438759-layer-428545992" style="position: absolute; left: 150px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="428545992" data-review-reference-id="428545992">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60" viewBox="0 0 200 60">\
                     <svg:g width="200" height="60">\
                        <svg:rect x="0" y="0" width="200" height="60" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="200" y2="60" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="60" x2="200" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-132438759-layer-1664374783" style="position: absolute; left: 97px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1664374783" data-review-reference-id="1664374783">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-132438759-layer-501579324" style="position: absolute; left: 0px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="501579324" data-review-reference-id="501579324">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:86px;height:80px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">=<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 86px; height: 80px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-132438759-layer-501579324\', \'interaction613346564\', {"button":"left","id":"action783727942","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction61111156","options":"reloadOnly","target":"page620342037","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-132438759-layer-1636533590" style="position: absolute; left: 0px; top: 115px; width: 600px; height: 795px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1636533590" data-review-reference-id="1636533590">\
            <div class="stencil-wrapper" style="width: 600px; height: 795px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 795px; width:600px;" width="600" height="795" viewBox="0 0 600 795">\
                     <svg:g width="600" height="795">\
                        <svg:rect x="0" y="0" width="600" height="795" style="stroke-width:1;stroke:black;fill:#a7a77c;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-132438759-layer-593402701" style="position: absolute; left: 15px; top: 360px; width: 570px; height: 114px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="593402701" data-review-reference-id="593402701">\
            <div class="stencil-wrapper" style="width: 570px; height: 114px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textblock2"><p style="font-size: 14px;">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt\
                        ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet\
                        clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing\
                        elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam\
                        et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet.</p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-132438759-layer-742900635" style="position: absolute; left: 15px; top: 155px; width: 570px; height: 175px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="742900635" data-review-reference-id="742900635">\
            <div class="stencil-wrapper" style="width: 570px; height: 175px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 175px;width:570px;" width="570" height="175" viewBox="0 0 570 175">\
                     <svg:g width="570" height="175">\
                        <svg:rect x="0" y="0" width="570" height="175" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="570" y2="175" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="175" x2="570" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-132438759-layer-312112944" style="position: absolute; left: 15px; top: 525px; width: 183px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="312112944" data-review-reference-id="312112944">\
            <div class="stencil-wrapper" style="width: 183px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Ваш вопрос</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-132438759-layer-1151080907" style="position: absolute; left: 15px; top: 575px; width: 570px; height: 270px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="1151080907" data-review-reference-id="1151080907">\
            <div class="stencil-wrapper" style="width: 570px; height: 270px">\
               <div title=""><textarea id="__containerId__-132438759-layer-1151080907input" rows="" cols="" style="width:568px;height:266px;padding: 0px;border-width:1px;"></textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-132438759-layer-86404080" style="position: absolute; left: 0px; top: 870px; width: 600px; height: 40px" data-interactive-element-type="default.iphoneButton" class="iphoneButton pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="86404080" data-review-reference-id="86404080">\
            <div class="stencil-wrapper" style="width: 600px; height: 40px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="" style="height: 40px;width:600px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="600" height="40" viewBox="0 0 600 40">\
                     <svg:a>\
                        <svg:path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" stroke="#666666" d="M5,39.5 l-2,-0.5 -1,-1 -1,-1.5 -0.5,-1.5 0,-30 0.5,-2 1,-1 1,-1 2,-0.5 590,0 1.5,0.5 1.5,1 1,1.5 0.5,1.5 0,30 -0.5,1.5 -1,1.5 -1.5,1 -1.5,0.5 z"></svg:path>\
                        <svg:text x="300" y="20" dy="0.3em" fill="#FFFFFF" style="font-size:1.8333333333333333em;stroke-width:0pt;" font-family="\'HelveticaNeue-Bold\'" text-anchor="middle" xml:space="preserve">отправить</svg:text>\
                     </svg:a>\
                  </svg:svg>\
               </div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 600px; height: 40px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-132438759-layer-86404080\', \'419427239\', {"button":"left","id":"1298193727","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1432478755","options":"reloadOnly","target":null,"transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-132438759-layer-196105395" style="position: absolute; left: 445px; top: 20px; width: 144px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="196105395" data-review-reference-id="196105395">\
            <div class="stencil-wrapper" style="width: 144px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Контакты</span></p></span></span></div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="132438759"] .border-wrapper, body[data-current-page-id="132438759"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="132438759"] .border-wrapper, body.has-frame[data-current-page-id="132438759"] .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="132438759"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="132438759"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "132438759",\
      			"name": "contacts",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
</div>');