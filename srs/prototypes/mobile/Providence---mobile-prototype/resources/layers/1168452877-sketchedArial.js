rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__1168452877-layer" class="layer" name="__containerId__pageLayer" data-layer-id="1168452877" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-1168452877-layer-1950224332" style="position: absolute; left: 0px; top: 0px; width: 600px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="1950224332" data-review-reference-id="1950224332">\
            <div class="stencil-wrapper" style="width: 600px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:600px;" width="600" height="1024" viewBox="0 0 600 1024">\
                     <svg:g width="600" height="1024">\
                        <svg:rect x="0" y="0" width="600" height="1024" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="600" y2="1024" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="1024" x2="600" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1168452877-layer-308816565" style="position: absolute; left: 0px; top: 255px; width: 600px; height: 515px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="308816565" data-review-reference-id="308816565">\
            <div class="stencil-wrapper" style="width: 600px; height: 515px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 515px; width:600px;" width="600" height="515" viewBox="0 0 600 515">\
                     <svg:g width="600" height="515">\
                        <svg:rect x="0" y="0" width="600" height="515" style="stroke-width:1;stroke:black;fill:white;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1168452877-layer-399632630" style="position: absolute; left: 185px; top: 675px; width: 230px; height: 50px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="399632630" data-review-reference-id="399632630">\
            <div class="stencil-wrapper" style="width: 230px; height: 50px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 50px;width:230px;" width="230" height="50" viewBox="0 0 230 50">\
                     <svg:g width="230" height="50">\
                        <svg:svg x="0" y="0" width="230" height="50">\
                           <svg:image width="230" height="50" xlink:href="../repoimages/636478.png" preserveAspectRatio="none" transform="scale(1,1) translate(-0,-0)  "></svg:image>\
                        </svg:svg>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1168452877-layer-1858554450" style="position: absolute; left: 170px; top: 440px; width: 265px; height: 40px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="1858554450" data-review-reference-id="1858554450">\
            <div class="stencil-wrapper" style="width: 265px; height: 40px">\
               <div title=""><textarea id="__containerId__-1168452877-layer-1858554450input" rows="" cols="" style="width:263px;height:36px;padding: 0px;border-width:1px;">email</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-1168452877-layer-77298120" style="position: absolute; left: 170px; top: 500px; width: 265px; height: 40px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="77298120" data-review-reference-id="77298120">\
            <div class="stencil-wrapper" style="width: 265px; height: 40px">\
               <div title=""><textarea id="__containerId__-1168452877-layer-77298120input" rows="" cols="" style="width:263px;height:36px;padding: 0px;border-width:1px;">password</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-1168452877-layer-2013569946" style="position: absolute; left: 170px; top: 610px; width: 265px; height: 35px" data-interactive-element-type="default.iphoneButton" class="iphoneButton pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="2013569946" data-review-reference-id="2013569946">\
            <div class="stencil-wrapper" style="width: 265px; height: 35px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="" style="height: 35px;width:265px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="265" height="35" viewBox="0 0 265 35">\
                     <svg:a>\
                        <svg:path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" stroke="#666666" d="M5,34.5 l-2,-0.5 -1,-1 -1,-1.5 -0.5,-1.5 0,-25 0.5,-2 1,-1 1,-1 2,-0.5 255,0 1.5,0.5 1.5,1 1,1.5 0.5,1.5 0,25 -0.5,1.5 -1,1.5 -1.5,1 -1.5,0.5 z"></svg:path>\
                        <svg:text x="132.5" y="17.5" dy="0.3em" fill="#FFFFFF" style="font-size:1.6666666666666667em;stroke-width:0pt;" font-family="\'HelveticaNeue-Bold\'" text-anchor="middle" xml:space="preserve">sign in</svg:text>\
                     </svg:a>\
                  </svg:svg>\
               </div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 265px; height: 35px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-1168452877-layer-2013569946\', \'1479746171\', {"button":"left","id":"1057299541","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"967184015","options":"reloadOnly","target":"1524600402","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-1168452877-layer-1624742872" style="position: absolute; left: 300px; top: 555px; width: 140px; height: 17px" data-interactive-element-type="default.text2" class="text2 pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1624742872" data-review-reference-id="1624742872">\
            <div class="stencil-wrapper" style="width: 140px; height: 17px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.link2"><p style="font-size: 14px;"><span class="underline">forget you password?</span></p></span></span></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 140px; height: 17px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-1168452877-layer-1624742872\', \'1705555465\', {"button":"left","id":"900768858","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1887649786","options":"reloadOnly","target":null,"transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-1168452877-layer-1431552952" style="position: absolute; left: 170px; top: 555px; width: 51px; height: 17px" data-interactive-element-type="default.text2" class="text2 pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1431552952" data-review-reference-id="1431552952">\
            <div class="stencil-wrapper" style="width: 51px; height: 17px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.link2"><p style="font-size: 14px;"><span class="underline">sign up</span></p></span></span></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 51px; height: 17px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-1168452877-layer-1431552952\', \'916389499\', {"button":"left","id":"1792655119","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1540629527","options":"reloadOnly","target":null,"transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-1168452877-layer-1856331323" style="position: absolute; left: 85px; top: 295px; width: 435px; height: 105px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1856331323" data-review-reference-id="1856331323">\
            <div class="stencil-wrapper" style="width: 435px; height: 105px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 105px; width:435px;" width="435" height="105" viewBox="0 0 435 105">\
                     <svg:g width="435" height="105">\
                        <svg:rect x="0" y="0" width="435" height="105" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1168452877-layer-552632662" style="position: absolute; left: 235px; top: 330px; width: 140px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="552632662" data-review-reference-id="552632662">\
            <div class="stencil-wrapper" style="width: 140px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">__logo__ </p></span></span></div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="1168452877"] .border-wrapper, body[data-current-page-id="1168452877"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="1168452877"] .border-wrapper, body.has-frame[data-current-page-id="1168452877"] .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="1168452877"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="1168452877"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "1168452877",\
      			"name": "sign in",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
</div>');