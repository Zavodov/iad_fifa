rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__1497021823-layer" class="layer" name="__containerId__pageLayer" data-layer-id="1497021823" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-1497021823-layer-1971788974" style="position: absolute; left: -1px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="1971788974" data-review-reference-id="1971788974">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024" viewBox="0 0 601 1024">\
                     <svg:g width="601" height="1024">\
                        <svg:rect x="0" y="0" width="601" height="1024" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="601" y2="1024" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="1024" x2="601" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1942985619" style="position: absolute; left: 0px; top: 240px; width: 600px; height: 80px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1942985619" data-review-reference-id="1942985619">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div id="1942985619-1853931572" style="position: absolute; left: 0px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1853931572" data-review-reference-id="1853931572">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-1853931572_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-1853931572div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-1853931572\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-1853931572\', \'result\');">\
                           				RFPL\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-165592102" style="position: absolute; left: 100px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="165592102" data-review-reference-id="165592102">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-165592102_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-165592102div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-165592102\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-165592102\', \'result\');">\
                           				Premier League\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-468989596" style="position: absolute; left: 200px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="468989596" data-review-reference-id="468989596">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-468989596_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-468989596div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-468989596\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-468989596\', \'result\');">\
                           				BundesLiga\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-266350341" style="position: absolute; left: 400px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="266350341" data-review-reference-id="266350341">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-266350341_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-266350341div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-266350341\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-266350341\', \'result\');">\
                           				Ligue 1\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-2065274472" style="position: absolute; left: 300px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="2065274472" data-review-reference-id="2065274472">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-2065274472_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-2065274472div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-2065274472\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-2065274472\', \'result\');">\
                           				Ligue BBVA\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-1052746445" style="position: absolute; left: 500px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1052746445" data-review-reference-id="1052746445">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-1052746445_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-1052746445div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-1052746445\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-1052746445\', \'result\');">\
                           				Serie A\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1506471845" style="position: absolute; left: 0px; top: 320px; width: 600px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1506471845" data-review-reference-id="1506471845">\
            <div class="stencil-wrapper" style="width: 600px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px; width:600px;" width="600" height="465" viewBox="0 0 600 465">\
                     <svg:g width="600" height="465">\
                        <svg:rect x="0" y="0" width="600" height="465" style="stroke-width:1;stroke:black;fill:#a7a77c;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1561898673" style="position: absolute; left: 1px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1561898673" data-review-reference-id="1561898673">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:599px;" width="599" height="80" viewBox="0 0 599 80">\
                     <svg:g width="599" height="80">\
                        <svg:rect x="0" y="0" width="599" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1877215721" style="position: absolute; left: 0px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1877215721" data-review-reference-id="1877215721">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:600px;" width="600" height="80" viewBox="0 0 600 80">\
                     <svg:g width="600" height="80">\
                        <svg:rect x="0" y="0" width="600" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-648635862" style="position: absolute; left: 150px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="648635862" data-review-reference-id="648635862">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60" viewBox="0 0 200 60">\
                     <svg:g width="200" height="60">\
                        <svg:rect x="0" y="0" width="200" height="60" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="200" y2="60" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="60" x2="200" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1061433121" style="position: absolute; left: 95px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1061433121" data-review-reference-id="1061433121">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1795695749" style="position: absolute; left: 0px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="1795695749" data-review-reference-id="1795695749">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:86px;height:80px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">=<br /></button></div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-538977365" style="position: absolute; left: 417px; top: 20px; width: 174px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="538977365" data-review-reference-id="538977365">\
            <div class="stencil-wrapper" style="width: 174px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Статистика</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-656263660" style="position: absolute; left: 0px; top: 355px; width: 600px; height: 395px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="656263660" data-review-reference-id="656263660">\
            <div class="stencil-wrapper" style="width: 600px; height: 395px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-1497021823-layer-656263660-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 395px;width:600px;" width="600" height="395">\
                     <svg:g id="__containerId__-1497021823-layer-656263660svg" width="600" height="395"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 2.22, 22.55, 2.31 Q 32.83, 2.04, 43.10, 3.02 Q 53.38, 1.93,\
                        63.66, 1.57 Q 73.93, 2.03, 84.21, 2.55 Q 94.48, 1.98, 104.76, 1.21 Q 115.03, 1.01, 125.31, 1.07 Q 135.59, 1.47, 145.86, 1.98\
                        Q 156.14, 2.64, 166.41, 2.74 Q 176.69, 2.41, 186.97, 1.84 Q 197.24, 1.40, 207.52, 0.99 Q 217.79, 1.36, 228.07, 0.92 Q 238.34,\
                        0.73, 248.62, 0.98 Q 258.90, 1.00, 269.17, 2.12 Q 279.45, 1.54, 289.72, 1.80 Q 300.00, 1.99, 310.28, 1.69 Q 320.55, 1.35,\
                        330.83, 1.35 Q 341.10, 1.30, 351.38, 0.82 Q 361.66, 0.93, 371.93, 0.94 Q 382.21, 0.46, 392.48, 0.83 Q 402.76, 1.76, 413.03,\
                        0.71 Q 423.31, 1.13, 433.59, 1.36 Q 443.86, 0.31, 454.14, 1.47 Q 464.41, 0.41, 474.69, 0.42 Q 484.97, 0.69, 495.24, 0.75 Q\
                        505.52, 1.08, 515.79, 0.44 Q 526.07, 0.94, 536.34, 1.94 Q 546.62, 0.87, 556.90, 1.32 Q 567.17, -0.08, 577.45, 1.53 Q 587.72,\
                        2.24, 598.19, 1.80 Q 598.30, 12.19, 598.18, 22.55 Q 599.06, 32.80, 599.30, 43.12 Q 599.15, 53.43, 599.35, 63.73 Q 598.71,\
                        74.02, 599.10, 84.31 Q 599.36, 94.60, 599.43, 104.89 Q 599.50, 115.18, 598.65, 125.47 Q 598.33, 135.76, 597.51, 146.05 Q 598.68,\
                        156.34, 598.88, 166.63 Q 598.05, 176.92, 598.90, 187.21 Q 597.91, 197.50, 597.70, 207.79 Q 598.63, 218.08, 599.21, 228.37\
                        Q 598.51, 238.66, 598.12, 248.95 Q 598.00, 259.24, 597.83, 269.53 Q 598.77, 279.82, 598.86, 290.11 Q 598.21, 300.39, 597.64,\
                        310.68 Q 597.79, 320.97, 598.99, 331.26 Q 599.33, 341.55, 599.05, 351.84 Q 597.98, 362.13, 598.71, 372.42 Q 599.36, 382.71,\
                        598.20, 393.20 Q 587.81, 393.27, 577.44, 392.92 Q 567.15, 392.62, 556.93, 393.90 Q 546.64, 394.53, 536.34, 392.61 Q 526.06,\
                        391.70, 515.79, 391.11 Q 505.52, 392.20, 495.24, 392.80 Q 484.97, 392.37, 474.69, 392.66 Q 464.41, 393.37, 454.14, 394.77\
                        Q 443.86, 392.88, 433.59, 392.69 Q 423.31, 391.78, 413.03, 392.17 Q 402.76, 393.10, 392.48, 393.16 Q 382.21, 393.29, 371.93,\
                        393.52 Q 361.66, 392.97, 351.38, 392.69 Q 341.10, 393.58, 330.83, 393.69 Q 320.55, 393.72, 310.28, 394.39 Q 300.00, 394.28,\
                        289.72, 394.30 Q 279.45, 394.67, 269.17, 394.72 Q 258.90, 393.67, 248.62, 393.05 Q 238.34, 393.35, 228.07, 393.68 Q 217.79,\
                        393.80, 207.52, 394.50 Q 197.24, 394.33, 186.97, 394.57 Q 176.69, 394.13, 166.41, 394.19 Q 156.14, 393.72, 145.86, 392.85\
                        Q 135.59, 391.79, 125.31, 393.02 Q 115.03, 392.86, 104.76, 391.79 Q 94.48, 392.12, 84.21, 393.36 Q 73.93, 394.22, 63.66, 393.47\
                        Q 53.38, 392.01, 43.10, 392.43 Q 32.83, 393.78, 22.55, 394.41 Q 12.28, 394.56, 1.32, 393.68 Q 1.27, 382.95, 1.42, 372.50 Q\
                        1.70, 362.15, 0.75, 351.88 Q 0.97, 341.57, 0.47, 331.28 Q 0.59, 320.98, 1.28, 310.69 Q 2.20, 300.39, 1.83, 290.11 Q 1.54,\
                        279.82, 1.79, 269.53 Q 2.06, 259.24, 2.84, 248.95 Q 1.14, 238.66, 0.97, 228.37 Q 0.80, 218.08, 0.93, 207.79 Q 0.48, 197.50,\
                        1.97, 187.21 Q 2.52, 176.92, 2.36, 166.63 Q 2.70, 156.34, 2.27, 146.05 Q 0.98, 135.76, 0.35, 125.47 Q 0.15, 115.18, -0.24,\
                        104.89 Q 0.06, 94.61, 0.72, 84.32 Q 1.81, 74.03, 2.17, 63.74 Q 1.55, 53.45, 1.10, 43.16 Q 0.35, 32.87, 0.92, 22.58 Q 2.00,\
                        12.29, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 596px; height:391px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-1497021823-layer-656263660-accordion", "596", "391", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-1497021823-layer-200451967" style="position: absolute; left: 5px; top: 390px; width: 595px; height: 265px" data-interactive-element-type="static.chart" class="chart stencil mobile-interaction-potential-trigger " data-stencil-id="200451967" data-review-reference-id="200451967">\
            <div class="stencil-wrapper" style="width: 595px; height: 265px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 265px;width:595px;" viewBox="0 0 595 265" width="595" height="265">\
                     <svg:g width="595" height="265">\
                        <svg:g xmlns:xi="http://www.w3.org/2001/XInclude" transform="scale(3.3055555555555554, 1.7666666666666666)">\
                           <svg:rect x="0" y="0" width="180" height="150" style="fill:white;stroke:none;"></svg:rect>\
                           <svg:g name="line" style="fill:none;stroke:black;stroke-width:1px;">\
                              <svg:path d="M 9,3 L 9,140 L 168,140 L 176,140"></svg:path>\
                              <svg:path d="M 167,135 L 177,140 L 167,145"></svg:path>\
                              <svg:path d="M 4,13 L 9,3 L 14,13"></svg:path>\
                              <svg:path d="M 9,140 L 30,112 L 46,98 L 54,106 L 68,89 L 74,82 L 76,74 L 82,67 L 92,96 L 97,76 L 106,66 L 111,50 L 122,37 L 127,45 L 129,51 L 139,51 L 151,29 L 159,26 L 161,16 L 166,12"></svg:path>\
                              <svg:path d="M 10,140 L 18,107 L 29,91 L 34,73 L 44,80 L 51,55 L 62,52 L 74,34 L 86,51 L 99,43 L 111,59 L 121,70 L 128,98 L 139,119 L 147,103 L 154,125 L 165,136"></svg:path>\
                           </svg:g>\
                        </svg:g>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="1497021823"] .border-wrapper, body[data-current-page-id="1497021823"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="1497021823"] .border-wrapper, body.has-frame[data-current-page-id="1497021823"] .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="1497021823"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="1497021823"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "1497021823",\
      			"name": "stats",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
</div>');