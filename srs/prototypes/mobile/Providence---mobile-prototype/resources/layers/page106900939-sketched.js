rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page106900939-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page106900939" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page106900939-layer-368883164" style="position: absolute; left: 275px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="368883164" data-review-reference-id="368883164">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024">\
                     <svg:g width="601" height="1024"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.29, 1.44, 22.59, 0.63 Q 32.88, 0.90, 43.17, 0.42 Q\
                        53.47, 1.10, 63.76, 0.69 Q 74.05, -0.27, 84.34, 0.15 Q 94.64, 0.36, 104.93, 1.84 Q 115.22, 2.20, 125.52, 1.30 Q 135.81, 0.44,\
                        146.10, -0.06 Q 156.40, -0.11, 166.69, 0.41 Q 176.98, 0.59, 187.28, 0.07 Q 197.57, 0.04, 207.86, 0.44 Q 218.16, 0.54, 228.45,\
                        0.39 Q 238.74, 0.23, 249.03, 0.08 Q 259.33, 0.34, 269.62, 0.50 Q 279.91, 0.35, 290.21, 0.15 Q 300.50, -0.00, 310.79, -0.24\
                        Q 321.09, 0.37, 331.38, 0.22 Q 341.67, 0.24, 351.97, 0.30 Q 362.26, 0.92, 372.55, -0.02 Q 382.84, 0.73, 393.14, 0.15 Q 403.43,\
                        0.38, 413.72, 0.69 Q 424.02, 1.37, 434.31, 1.12 Q 444.60, 0.95, 454.90, 0.87 Q 465.19, 0.71, 475.48, 0.69 Q 485.78, 0.81,\
                        496.07, 0.44 Q 506.36, 0.81, 516.65, 1.28 Q 526.95, 1.54, 537.24, 1.64 Q 547.53, 1.24, 557.83, 0.98 Q 568.12, 0.99, 578.41,\
                        0.72 Q 588.71, 0.72, 599.57, 1.43 Q 599.93, 11.69, 600.42, 21.80 Q 600.30, 31.91, 599.74, 41.98 Q 600.28, 51.98, 600.12, 61.99\
                        Q 600.20, 72.00, 600.13, 82.00 Q 599.48, 92.00, 599.44, 102.00 Q 598.72, 112.00, 598.50, 122.00 Q 599.62, 132.00, 600.57,\
                        142.00 Q 600.52, 152.00, 599.67, 162.00 Q 599.62, 172.00, 600.74, 182.00 Q 600.11, 192.00, 600.25, 202.00 Q 600.16, 212.00,\
                        599.93, 222.00 Q 600.43, 232.00, 599.94, 242.00 Q 599.41, 252.00, 600.09, 262.00 Q 599.65, 272.00, 599.65, 282.00 Q 600.23,\
                        292.00, 600.32, 302.00 Q 600.48, 312.00, 600.56, 322.00 Q 599.97, 332.00, 600.27, 342.00 Q 600.31, 352.00, 600.17, 362.00\
                        Q 600.53, 372.00, 600.75, 382.00 Q 601.07, 392.00, 600.36, 402.00 Q 600.65, 412.00, 599.69, 422.00 Q 599.14, 432.00, 598.99,\
                        442.00 Q 598.66, 452.00, 598.90, 462.00 Q 599.07, 472.00, 599.58, 482.00 Q 598.63, 492.00, 599.63, 502.00 Q 599.03, 512.00,\
                        599.46, 522.00 Q 599.65, 532.00, 599.77, 542.00 Q 600.23, 552.00, 600.20, 562.00 Q 600.22, 572.00, 599.43, 582.00 Q 599.55,\
                        592.00, 599.38, 602.00 Q 599.60, 612.00, 599.91, 622.00 Q 600.07, 632.00, 599.55, 642.00 Q 599.04, 652.00, 598.03, 662.00\
                        Q 597.93, 672.00, 599.41, 682.00 Q 598.81, 692.00, 598.91, 702.00 Q 598.77, 712.00, 598.78, 722.00 Q 599.49, 732.00, 599.23,\
                        742.00 Q 598.82, 752.00, 598.29, 762.00 Q 598.94, 772.00, 598.49, 782.00 Q 599.10, 792.00, 598.76, 802.00 Q 598.62, 812.00,\
                        598.97, 822.00 Q 599.00, 832.00, 600.11, 842.00 Q 599.90, 852.00, 600.08, 862.00 Q 600.47, 872.00, 599.81, 882.00 Q 600.41,\
                        892.00, 599.95, 902.00 Q 600.29, 912.00, 600.18, 922.00 Q 600.58, 932.00, 600.61, 942.00 Q 599.98, 952.00, 599.55, 962.00\
                        Q 600.00, 972.00, 599.94, 982.00 Q 600.11, 992.00, 599.33, 1002.00 Q 598.89, 1012.00, 599.20, 1022.20 Q 588.74, 1022.11, 578.55,\
                        1022.98 Q 568.22, 1023.53, 557.84, 1022.24 Q 547.54, 1022.10, 537.23, 1021.18 Q 526.95, 1023.31, 516.66, 1022.67 Q 506.36,\
                        1022.98, 496.07, 1022.05 Q 485.78, 1022.17, 475.48, 1022.01 Q 465.19, 1022.77, 454.90, 1023.03 Q 444.60, 1023.11, 434.31,\
                        1023.27 Q 424.02, 1023.34, 413.72, 1023.51 Q 403.43, 1022.72, 393.14, 1022.95 Q 382.84, 1023.00, 372.55, 1021.89 Q 362.26,\
                        1022.11, 351.97, 1023.05 Q 341.67, 1023.45, 331.38, 1023.17 Q 321.09, 1022.74, 310.79, 1022.39 Q 300.50, 1023.15, 290.21,\
                        1023.01 Q 279.91, 1023.15, 269.62, 1023.38 Q 259.33, 1023.69, 249.03, 1023.60 Q 238.74, 1022.79, 228.45, 1022.56 Q 218.16,\
                        1022.94, 207.86, 1023.44 Q 197.57, 1023.41, 187.28, 1022.80 Q 176.98, 1021.51, 166.69, 1022.44 Q 156.40, 1023.28, 146.10,\
                        1022.19 Q 135.81, 1021.34, 125.52, 1021.50 Q 115.22, 1021.76, 104.93, 1021.92 Q 94.64, 1021.82, 84.34, 1021.87 Q 74.05, 1022.19,\
                        63.76, 1022.46 Q 53.47, 1021.90, 43.17, 1021.11 Q 32.88, 1021.56, 22.59, 1022.27 Q 12.29, 1021.86, 2.24, 1021.76 Q 2.33, 1011.89,\
                        1.89, 1002.02 Q 1.51, 992.03, 1.63, 982.01 Q 1.76, 972.00, 1.31, 962.01 Q 1.27, 952.00, 1.14, 942.00 Q 1.46, 932.00, 0.63,\
                        922.00 Q 1.50, 912.00, 0.74, 902.00 Q 0.84, 892.00, 2.41, 882.00 Q 2.05, 872.00, 2.27, 862.00 Q 1.13, 852.00, 1.85, 842.00\
                        Q 2.36, 832.00, 1.50, 822.00 Q 1.69, 812.00, 1.26, 802.00 Q 1.70, 792.00, 1.72, 782.00 Q 1.96, 772.00, 2.61, 762.00 Q 2.80,\
                        752.00, 2.23, 742.00 Q 2.51, 732.00, 2.56, 722.00 Q 2.98, 712.00, 2.84, 702.00 Q 1.77, 692.00, 1.07, 682.00 Q 1.79, 672.00,\
                        2.15, 662.00 Q 2.14, 652.00, 1.35, 642.00 Q 0.66, 632.00, 1.20, 622.00 Q 1.87, 612.00, 1.60, 602.00 Q 1.66, 592.00, 1.55,\
                        582.00 Q 2.36, 572.00, 0.86, 562.00 Q 1.42, 552.00, 0.49, 542.00 Q 1.01, 532.00, 1.32, 522.00 Q 0.85, 512.00, 0.63, 502.00\
                        Q 1.20, 492.00, 2.28, 482.00 Q 1.67, 472.00, 1.67, 462.00 Q 1.65, 452.00, 2.31, 442.00 Q 2.62, 432.00, 3.08, 422.00 Q 2.12,\
                        412.00, 2.12, 402.00 Q 2.15, 392.00, 2.20, 382.00 Q 1.67, 372.00, 0.65, 362.00 Q 0.47, 352.00, 0.99, 342.00 Q 1.11, 332.00,\
                        0.95, 322.00 Q 0.71, 312.00, 0.31, 302.00 Q 0.62, 292.00, 1.46, 282.00 Q 1.78, 272.00, 0.69, 262.00 Q 0.78, 252.00, 1.38,\
                        242.00 Q 0.57, 232.00, 0.57, 222.00 Q -0.04, 212.00, 0.91, 202.00 Q 1.24, 192.00, 0.40, 182.00 Q 0.33, 172.00, 0.33, 162.00\
                        Q 0.75, 152.00, 1.14, 142.00 Q 1.86, 132.00, 2.06, 122.00 Q 2.53, 112.00, 1.40, 102.00 Q 1.81, 92.00, 1.63, 82.00 Q 1.59,\
                        72.00, 1.33, 62.00 Q 1.84, 52.00, 2.69, 42.00 Q 3.86, 32.00, 3.67, 22.00 Q 2.00, 12.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 7.19, 10.57, 12.30, 19.18 Q 17.65, 27.65, 22.27, 36.55 Q 27.61,\
                        45.04, 32.68, 53.67 Q 37.55, 62.43, 43.03, 70.83 Q 47.75, 79.67, 52.91, 88.26 Q 58.14, 96.80, 63.15, 105.47 Q 68.10, 114.18,\
                        73.13, 122.84 Q 78.32, 131.41, 83.85, 139.78 Q 88.71, 148.54, 93.20, 157.51 Q 97.94, 166.35, 103.46, 174.72 Q 109.14, 183.00,\
                        113.25, 192.20 Q 119.06, 200.41, 123.77, 209.25 Q 128.45, 218.12, 133.83, 226.57 Q 138.55, 235.42, 143.93, 243.88 Q 148.99,\
                        252.52, 154.01, 261.19 Q 159.32, 269.68, 164.86, 278.05 Q 170.09, 286.59, 174.76, 295.46 Q 178.98, 304.60, 183.53, 313.54\
                        Q 188.20, 322.41, 193.40, 330.97 Q 198.26, 339.74, 203.99, 347.99 Q 210.02, 356.06, 215.08, 364.71 Q 220.08, 373.39, 223.83,\
                        382.79 Q 230.16, 390.70, 235.15, 399.38 Q 240.14, 408.07, 245.40, 416.59 Q 249.86, 425.59, 255.22, 434.05 Q 260.26, 442.71,\
                        265.53, 451.23 Q 271.17, 459.53, 274.94, 468.93 Q 280.11, 477.51, 286.26, 485.52 Q 291.42, 494.11, 296.82, 502.55 Q 300.46,\
                        512.02, 306.13, 520.31 Q 311.49, 528.78, 316.62, 537.38 Q 321.54, 546.11, 326.29, 554.93 Q 331.64, 563.41, 336.52, 572.15\
                        Q 341.57, 580.80, 346.52, 589.51 Q 351.25, 598.35, 356.30, 607.00 Q 361.64, 615.48, 365.50, 624.82 Q 371.84, 632.72, 376.12,\
                        641.82 Q 381.53, 650.26, 386.93, 658.70 Q 392.23, 667.21, 397.13, 675.94 Q 402.24, 684.56, 407.69, 692.97 Q 412.89, 701.53,\
                        417.96, 710.17 Q 422.53, 719.10, 427.72, 727.67 Q 433.41, 735.94, 437.90, 744.92 Q 441.12, 754.64, 445.83, 763.49 Q 452.03,\
                        771.47, 457.80, 779.70 Q 462.86, 788.34, 467.87, 797.01 Q 472.95, 805.64, 477.93, 814.33 Q 483.23, 822.84, 488.49, 831.37\
                        Q 493.05, 840.30, 497.41, 849.35 Q 503.02, 857.68, 508.01, 866.36 Q 513.36, 874.84, 517.87, 883.80 Q 522.40, 892.75, 528.13,\
                        901.01 Q 533.63, 909.39, 539.15, 917.77 Q 543.39, 926.89, 547.51, 936.08 Q 552.58, 944.72, 557.63, 953.37 Q 563.65, 961.45,\
                        569.09, 969.88 Q 574.20, 978.49, 579.53, 986.98 Q 584.71, 995.55, 590.19, 1003.94 Q 593.94, 1013.35, 599.00, 1022.00" style="\
                        fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 1022.00 Q 5.52, 1012.44, 11.34, 1004.24 Q 16.53, 995.66, 22.71, 987.66\
                        Q 26.87, 978.48, 32.38, 970.09 Q 38.12, 961.83, 43.37, 953.29 Q 48.31, 944.57, 51.85, 935.02 Q 57.16, 926.52, 62.22, 917.87\
                        Q 68.11, 909.70, 73.10, 901.00 Q 77.38, 891.89, 83.59, 883.91 Q 88.71, 875.29, 93.90, 866.72 Q 97.92, 857.45, 102.09, 848.28\
                        Q 107.87, 840.04, 113.37, 831.65 Q 117.71, 822.58, 123.44, 814.32 Q 128.15, 805.45, 133.55, 797.00 Q 138.98, 788.57, 143.30,\
                        779.48 Q 148.10, 770.67, 152.26, 761.49 Q 157.54, 752.96, 163.70, 744.96 Q 168.58, 736.20, 173.65, 727.55 Q 179.19, 719.18,\
                        184.31, 710.56 Q 189.72, 702.11, 194.38, 693.22 Q 198.91, 684.25, 204.08, 675.67 Q 209.29, 667.10, 214.50, 658.54 Q 219.83,\
                        650.04, 224.53, 641.17 Q 229.18, 632.28, 233.58, 623.24 Q 238.85, 614.71, 245.04, 606.72 Q 249.63, 597.79, 254.63, 589.10\
                        Q 258.96, 580.02, 264.40, 571.59 Q 269.62, 563.03, 274.99, 554.56 Q 280.42, 546.12, 284.96, 537.16 Q 290.27, 528.66, 295.59,\
                        520.16 Q 300.28, 511.29, 304.90, 502.37 Q 309.96, 493.72, 315.24, 485.19 Q 320.90, 476.90, 326.25, 468.41 Q 331.25, 459.72,\
                        336.07, 450.93 Q 341.13, 442.27, 346.16, 433.60 Q 351.22, 424.95, 356.04, 416.15 Q 361.32, 407.63, 366.47, 399.03 Q 371.65,\
                        390.45, 377.37, 382.18 Q 382.82, 373.76, 387.02, 364.60 Q 392.08, 355.94, 396.50, 346.91 Q 401.85, 338.43, 406.72, 329.66\
                        Q 412.40, 321.37, 417.43, 312.70 Q 422.47, 304.04, 427.70, 295.49 Q 433.13, 287.05, 438.10, 278.34 Q 443.43, 269.84, 448.30,\
                        261.08 Q 453.26, 252.37, 458.66, 243.92 Q 463.61, 235.20, 467.86, 226.07 Q 472.37, 217.09, 477.63, 208.56 Q 483.24, 200.23,\
                        489.25, 192.13 Q 494.44, 183.55, 498.82, 174.50 Q 504.39, 166.14, 509.21, 157.35 Q 513.48, 148.24, 518.22, 139.39 Q 523.49,\
                        130.86, 528.68, 122.28 Q 534.00, 113.79, 539.42, 105.34 Q 543.89, 96.34, 549.85, 88.22 Q 554.31, 79.21, 559.46, 70.61 Q 564.71,\
                        62.07, 569.29, 53.13 Q 574.25, 44.42, 579.84, 36.08 Q 585.31, 27.66, 589.48, 18.49 Q 595.92, 10.65, 601.00, 2.00" style="\
                        fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-1254413406" style="position: absolute; left: 275px; top: 240px; width: 600px; height: 80px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1254413406" data-review-reference-id="1254413406">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div id="1254413406-1891757621" style="position: absolute; left: 0px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1891757621" data-review-reference-id="1891757621">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 5.73, 71.50, 5.67, 61.00 Q 6.03, 50.50, 5.84, 40.00 Q 6.54, 29.50,\
                                 6.30, 19.00 Q 6.23, 17.50, 6.26, 15.83 Q 6.88, 14.77, 7.70, 13.87 Q 7.24, 12.41, 8.42, 11.44 Q 9.78, 11.19, 10.83, 10.71 Q\
                                 11.67, 9.91, 12.98, 9.96 Q 14.50, 9.49, 16.03, 9.18 Q 29.16, 8.91, 42.33, 8.86 Q 55.49, 8.37, 68.65, 7.76 Q 81.83, 8.25, 95.05,\
                                 8.68 Q 96.75, 8.47, 98.10, 9.73 Q 99.30, 9.80, 99.93, 11.15 Q 101.27, 10.95, 102.50, 11.49 Q 102.91, 12.71, 102.80, 14.12\
                                 Q 103.27, 15.13, 103.53, 16.20 Q 104.15, 17.64, 104.68, 19.06 Q 105.32, 29.47, 105.11, 39.99 Q 105.24, 50.49, 104.89, 61.00\
                                 Q 104.48, 71.50, 105.30, 82.29 Q 95.25, 82.76, 85.24, 83.69 Q 75.11, 83.58, 65.04, 83.10 Q 55.01, 82.51, 45.01, 82.67 Q 35.01,\
                                 83.35, 25.00, 82.35 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1891757621\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1891757621\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1891757621_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">RFPL\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1891757621_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">RFPL\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1254413406-665663306" style="position: absolute; left: 100px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="665663306" data-review-reference-id="665663306">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 6.49, 71.50, 6.30, 61.00 Q 6.27, 50.50, 6.15, 40.00 Q 7.03, 29.50,\
                                 5.84, 19.00 Q 6.63, 17.50, 6.27, 15.83 Q 7.53, 15.01, 7.88, 13.95 Q 7.57, 12.57, 8.30, 11.32 Q 9.20, 10.39, 10.93, 10.88 Q\
                                 11.76, 10.06, 12.67, 9.25 Q 14.32, 9.02, 16.04, 9.23 Q 29.17, 9.03, 42.35, 9.28 Q 55.50, 8.89, 68.65, 7.91 Q 81.82, 7.04,\
                                 95.18, 7.88 Q 96.82, 8.20, 98.38, 8.96 Q 98.96, 10.60, 99.87, 11.28 Q 100.58, 12.38, 102.10, 11.90 Q 102.90, 12.72, 103.62,\
                                 13.63 Q 104.25, 14.59, 104.90, 15.61 Q 105.42, 17.15, 105.65, 18.88 Q 106.05, 29.41, 106.67, 39.92 Q 106.80, 50.46, 106.49,\
                                 60.98 Q 106.33, 71.49, 105.64, 82.64 Q 95.39, 83.17, 85.20, 83.37 Q 75.06, 82.92, 65.06, 83.93 Q 55.01, 82.67, 45.00, 82.34\
                                 Q 35.00, 81.76, 25.00, 82.49 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'665663306\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'665663306\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="665663306_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="665663306_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1254413406-584488950" style="position: absolute; left: 200px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="584488950" data-review-reference-id="584488950">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 6.46, 71.50, 7.13, 61.00 Q 6.61, 50.50, 8.13, 40.00 Q 6.87, 29.50,\
                                 6.11, 19.00 Q 7.24, 17.50, 7.46, 16.11 Q 8.40, 15.33, 7.11, 13.62 Q 7.15, 12.37, 8.04, 11.06 Q 9.65, 11.02, 10.84, 10.74 Q\
                                 11.63, 9.82, 12.69, 9.30 Q 14.53, 9.57, 16.05, 9.29 Q 29.14, 8.71, 42.29, 8.08 Q 55.46, 7.31, 68.65, 7.28 Q 81.82, 7.14, 95.29,\
                                 7.15 Q 96.99, 7.52, 98.61, 8.34 Q 99.71, 8.85, 100.80, 9.28 Q 101.85, 9.73, 103.44, 10.55 Q 104.44, 11.60, 105.25, 12.64 Q\
                                 104.10, 14.67, 105.11, 15.51 Q 106.05, 16.91, 106.04, 18.81 Q 105.88, 29.42, 106.35, 39.94 Q 106.75, 50.46, 106.03, 60.99\
                                 Q 106.12, 71.49, 105.57, 82.57 Q 95.26, 82.79, 85.15, 83.04 Q 75.08, 83.22, 65.04, 83.23 Q 55.02, 83.45, 45.01, 83.71 Q 35.01,\
                                 83.74, 25.00, 83.16 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'584488950\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'584488950\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="584488950_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="584488950_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1254413406-1298398196" style="position: absolute; left: 400px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1298398196" data-review-reference-id="1298398196">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 7.60, 71.50, 8.16, 61.00 Q 7.56, 50.50, 8.35, 40.00 Q 8.22, 29.50,\
                                 7.07, 19.00 Q 6.95, 17.50, 7.53, 16.12 Q 8.84, 15.49, 8.07, 14.03 Q 8.02, 12.78, 7.87, 10.90 Q 8.98, 10.09, 10.21, 9.69 Q\
                                 11.33, 9.29, 12.58, 9.04 Q 14.42, 9.30, 16.00, 9.01 Q 29.07, 7.96, 42.26, 7.43 Q 55.48, 8.12, 68.66, 8.38 Q 81.83, 8.45, 95.16,\
                                 7.97 Q 96.78, 8.37, 98.40, 8.92 Q 99.33, 9.75, 100.38, 10.18 Q 101.30, 10.87, 102.24, 11.76 Q 102.57, 12.95, 103.68, 13.59\
                                 Q 104.67, 14.36, 104.92, 15.60 Q 105.73, 17.03, 106.50, 18.72 Q 107.17, 29.30, 107.01, 39.91 Q 106.88, 50.46, 106.91, 60.98\
                                 Q 106.95, 71.49, 105.72, 82.71 Q 95.57, 83.72, 85.16, 83.15 Q 75.01, 82.21, 65.05, 83.60 Q 55.02, 83.52, 45.01, 83.65 Q 35.01,\
                                 83.97, 25.00, 83.53 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1298398196\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1298398196\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1298398196_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1298398196_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1254413406-1360158057" style="position: absolute; left: 300px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1360158057" data-review-reference-id="1360158057">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 5.92, 71.50, 6.58, 61.00 Q 7.27, 50.50, 6.51, 40.00 Q 6.01, 29.50,\
                                 6.68, 19.00 Q 6.79, 17.50, 6.38, 15.85 Q 6.55, 14.65, 7.28, 13.69 Q 8.01, 12.77, 8.59, 11.60 Q 9.57, 10.90, 10.51, 10.20 Q\
                                 11.62, 9.81, 12.69, 9.29 Q 14.17, 8.65, 15.79, 7.86 Q 29.06, 7.85, 42.26, 7.41 Q 55.46, 7.15, 68.65, 7.09 Q 81.82, 6.96, 95.33,\
                                 6.94 Q 97.02, 7.39, 98.66, 8.21 Q 99.78, 8.70, 100.88, 9.10 Q 101.89, 9.66, 103.12, 10.87 Q 103.11, 12.56, 103.93, 13.44 Q\
                                 104.80, 14.28, 105.19, 15.48 Q 105.49, 17.12, 106.07, 18.80 Q 105.64, 29.44, 104.99, 40.00 Q 104.61, 50.51, 105.28, 61.00\
                                 Q 106.23, 71.49, 105.62, 82.61 Q 95.33, 83.00, 85.18, 83.26 Q 75.10, 83.50, 65.06, 83.76 Q 55.02, 83.38, 45.01, 83.12 Q 35.00,\
                                 83.01, 25.00, 83.37 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1360158057\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1360158057\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1360158057_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1360158057_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1254413406-322290237" style="position: absolute; left: 500px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="322290237" data-review-reference-id="322290237">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 4.42, 71.50, 4.92, 61.00 Q 5.29, 50.50, 5.45, 40.00 Q 5.72, 29.50,\
                                 6.02, 19.00 Q 6.76, 17.50, 6.39, 15.86 Q 6.64, 14.69, 7.05, 13.59 Q 7.47, 12.52, 8.21, 11.23 Q 9.56, 10.89, 10.87, 10.79 Q\
                                 11.72, 10.00, 12.64, 9.17 Q 14.14, 8.55, 15.83, 8.11 Q 29.09, 8.19, 42.29, 8.02 Q 55.47, 7.80, 68.66, 8.08 Q 81.83, 7.89,\
                                 95.08, 8.50 Q 96.55, 9.28, 98.16, 9.57 Q 98.76, 11.05, 99.89, 11.24 Q 100.89, 11.73, 101.93, 12.07 Q 103.00, 12.64, 103.54,\
                                 13.68 Q 103.23, 15.15, 104.52, 15.77 Q 105.25, 17.21, 105.60, 18.89 Q 104.98, 29.50, 105.45, 39.98 Q 105.51, 50.49, 105.41,\
                                 61.00 Q 105.61, 71.50, 105.22, 82.22 Q 94.99, 81.98, 85.10, 82.70 Q 74.98, 81.67, 65.03, 82.92 Q 55.01, 82.85, 45.00, 82.53\
                                 Q 35.00, 83.07, 25.00, 81.88 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'322290237\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'322290237\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="322290237_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="322290237_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-245548156" style="position: absolute; left: 275px; top: 320px; width: 600px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="245548156" data-review-reference-id="245548156">\
            <div class="stencil-wrapper" style="width: 600px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px;width:600px;" width="600" height="465">\
                     <svg:g width="600" height="465"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 0.50, 22.55, 1.28 Q 32.83, 1.00, 43.10, 3.03 Q 53.38, 2.39,\
                        63.66, 1.62 Q 73.93, 2.63, 84.21, 3.48 Q 94.48, 2.90, 104.76, 2.11 Q 115.03, 0.89, 125.31, 0.32 Q 135.59, 0.42, 145.86, 1.16\
                        Q 156.14, 2.01, 166.41, 2.94 Q 176.69, 2.96, 186.97, 2.19 Q 197.24, 1.71, 207.52, 1.36 Q 217.79, 1.40, 228.07, 1.16 Q 238.34,\
                        1.34, 248.62, 1.69 Q 258.90, 0.90, 269.17, 1.50 Q 279.45, 1.73, 289.72, 1.00 Q 300.00, 1.04, 310.28, 0.95 Q 320.55, 1.54,\
                        330.83, 1.51 Q 341.10, 2.09, 351.38, 2.51 Q 361.66, 1.90, 371.93, 2.21 Q 382.21, 1.25, 392.48, 1.73 Q 402.76, 1.27, 413.03,\
                        1.52 Q 423.31, 1.63, 433.59, 1.36 Q 443.86, 1.19, 454.14, 0.92 Q 464.41, 0.72, 474.69, 2.05 Q 484.97, 2.48, 495.24, 2.07 Q\
                        505.52, 1.98, 515.79, 1.12 Q 526.07, 1.92, 536.34, 1.73 Q 546.62, 1.54, 556.90, 3.00 Q 567.17, 1.54, 577.45, 1.25 Q 587.72,\
                        1.20, 598.43, 1.57 Q 598.75, 11.77, 598.87, 21.92 Q 598.49, 32.03, 597.69, 42.10 Q 597.14, 52.12, 597.89, 62.13 Q 596.95,\
                        72.16, 597.10, 82.18 Q 596.91, 92.20, 596.74, 102.22 Q 596.88, 112.24, 597.40, 122.26 Q 597.22, 132.28, 596.69, 142.30 Q 597.59,\
                        152.33, 598.19, 162.35 Q 598.62, 172.37, 597.58, 182.39 Q 597.01, 192.41, 598.20, 202.43 Q 598.74, 212.46, 598.46, 222.48\
                        Q 598.45, 232.50, 598.96, 242.52 Q 598.95, 252.54, 598.87, 262.57 Q 598.60, 272.59, 598.94, 282.61 Q 599.48, 292.63, 599.43,\
                        302.65 Q 599.56, 312.67, 599.79, 322.70 Q 599.34, 332.72, 598.18, 342.74 Q 597.54, 352.76, 597.05, 362.78 Q 596.99, 372.80,\
                        597.84, 382.83 Q 597.60, 392.85, 598.31, 402.87 Q 598.75, 412.89, 599.39, 422.91 Q 597.23, 432.93, 597.70, 442.96 Q 597.84,\
                        452.98, 597.89, 462.89 Q 587.59, 462.60, 577.48, 463.20 Q 567.12, 462.18, 556.86, 461.86 Q 546.62, 462.78, 536.35, 463.70\
                        Q 526.07, 463.71, 515.79, 463.47 Q 505.52, 463.30, 495.24, 463.41 Q 484.97, 463.50, 474.69, 464.02 Q 464.41, 464.17, 454.14,\
                        464.44 Q 443.86, 464.19, 433.59, 463.91 Q 423.31, 463.51, 413.03, 463.50 Q 402.76, 463.76, 392.48, 463.87 Q 382.21, 463.71,\
                        371.93, 463.51 Q 361.66, 464.01, 351.38, 464.48 Q 341.10, 463.67, 330.83, 462.52 Q 320.55, 462.23, 310.28, 463.03 Q 300.00,\
                        464.15, 289.72, 463.70 Q 279.45, 463.42, 269.17, 463.35 Q 258.90, 463.13, 248.62, 462.57 Q 238.34, 463.04, 228.07, 462.80\
                        Q 217.79, 463.24, 207.52, 463.14 Q 197.24, 463.30, 186.97, 462.41 Q 176.69, 462.66, 166.41, 463.35 Q 156.14, 463.39, 145.86,\
                        463.54 Q 135.59, 464.38, 125.31, 464.59 Q 115.03, 464.60, 104.76, 464.21 Q 94.48, 463.35, 84.21, 462.91 Q 73.93, 463.20, 63.66,\
                        463.44 Q 53.38, 463.54, 43.10, 463.68 Q 32.83, 462.94, 22.55, 463.58 Q 12.28, 463.99, 1.32, 463.68 Q 0.78, 453.39, 0.30, 443.20\
                        Q 1.20, 432.99, 1.06, 422.94 Q 1.27, 412.90, 1.33, 402.87 Q 0.56, 392.85, 0.75, 382.83 Q 0.64, 372.81, 0.45, 362.78 Q -0.10,\
                        352.76, 0.14, 342.74 Q 0.32, 332.72, 0.29, 322.70 Q 0.21, 312.67, 0.22, 302.65 Q 0.27, 292.63, 0.71, 282.61 Q 0.80, 272.59,\
                        0.43, 262.57 Q 1.13, 252.54, 1.85, 242.52 Q 2.67, 232.50, 2.20, 222.48 Q 2.00, 212.46, 2.21, 202.43 Q 2.80, 192.41, 1.86,\
                        182.39 Q 1.38, 172.37, 1.11, 162.35 Q 1.17, 152.33, 1.83, 142.30 Q 1.36, 132.28, 0.96, 122.26 Q 1.50, 112.24, 2.06, 102.22\
                        Q 1.83, 92.20, 1.33, 82.17 Q 1.78, 72.15, 2.75, 62.13 Q 2.47, 52.11, 1.14, 42.09 Q 1.02, 32.07, 0.97, 22.04 Q 2.00, 12.02,\
                        2.00, 2.00" style=" fill:#a7a77c;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-1001972155" style="position: absolute; left: 276px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1001972155" data-review-reference-id="1001972155">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:599px;" width="599" height="80">\
                     <svg:g width="599" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.26, 1.88, 22.52, 0.76 Q 32.78, 1.10, 43.03, 0.72 Q 53.29, 1.46,\
                        63.55, 2.46 Q 73.81, 2.00, 84.07, 1.56 Q 94.33, 1.35, 104.59, 1.93 Q 114.84, 2.39, 125.10, 2.58 Q 135.36, 1.81, 145.62, 2.46\
                        Q 155.88, 2.13, 166.14, 1.22 Q 176.40, 1.03, 186.66, 0.59 Q 196.91, 0.74, 207.17, 1.46 Q 217.43, 1.92, 227.69, 2.43 Q 237.95,\
                        3.40, 248.21, 3.24 Q 258.47, 3.00, 268.72, 2.28 Q 278.98, 1.33, 289.24, 2.71 Q 299.50, 2.59, 309.76, 2.32 Q 320.02, 1.59,\
                        330.28, 0.65 Q 340.53, 0.87, 350.79, 0.38 Q 361.05, -0.17, 371.31, -0.11 Q 381.57, 0.46, 391.83, 0.30 Q 402.09, 0.61, 412.34,\
                        1.23 Q 422.60, 1.84, 432.86, 1.19 Q 443.12, 1.44, 453.38, 1.99 Q 463.64, 1.64, 473.90, 2.72 Q 484.15, 2.95, 494.41, 2.26 Q\
                        504.67, 1.63, 514.93, 2.36 Q 525.19, 2.05, 535.45, 2.82 Q 545.71, 1.79, 555.97, 1.65 Q 566.22, 2.39, 576.48, 2.57 Q 586.74,\
                        2.99, 596.80, 2.20 Q 596.14, 14.95, 596.86, 27.35 Q 596.35, 40.04, 597.12, 52.66 Q 598.64, 65.31, 597.94, 78.94 Q 587.25,\
                        79.53, 576.74, 79.83 Q 566.34, 79.75, 556.04, 80.22 Q 545.73, 79.74, 535.46, 80.09 Q 525.20, 80.14, 514.93, 80.04 Q 504.67,\
                        79.31, 494.41, 78.95 Q 484.16, 79.43, 473.90, 79.64 Q 463.64, 79.73, 453.38, 79.87 Q 443.12, 79.13, 432.86, 78.98 Q 422.60,\
                        78.47, 412.34, 77.64 Q 402.09, 78.37, 391.83, 79.03 Q 381.57, 78.99, 371.31, 78.82 Q 361.05, 78.03, 350.79, 77.65 Q 340.53,\
                        77.70, 330.28, 78.40 Q 320.02, 77.43, 309.76, 77.51 Q 299.50, 77.65, 289.24, 77.54 Q 278.98, 77.95, 268.72, 78.14 Q 258.47,\
                        78.67, 248.21, 78.93 Q 237.95, 78.82, 227.69, 78.56 Q 217.43, 78.97, 207.17, 79.49 Q 196.91, 78.71, 186.66, 77.76 Q 176.40,\
                        77.21, 166.14, 79.07 Q 155.88, 79.61, 145.62, 79.86 Q 135.36, 79.90, 125.10, 79.48 Q 114.84, 78.60, 104.59, 78.38 Q 94.33,\
                        78.75, 84.07, 78.31 Q 73.81, 79.30, 63.55, 79.07 Q 53.29, 77.92, 43.03, 77.70 Q 32.78, 77.80, 22.52, 78.86 Q 12.26, 78.44,\
                        2.02, 77.98 Q 2.31, 65.23, 1.69, 52.71 Q 0.82, 40.08, 0.35, 27.39 Q 2.00, 14.67, 2.00, 2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-110308917" style="position: absolute; left: 275px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="110308917" data-review-reference-id="110308917">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:600px;" width="600" height="80">\
                     <svg:g width="600" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 2.19, 22.55, 1.79 Q 32.83, 1.10, 43.10, 1.06 Q 53.38, 1.27,\
                        63.66, 0.27 Q 73.93, 1.20, 84.21, 1.11 Q 94.48, 1.47, 104.76, 1.06 Q 115.03, 0.51, 125.31, -0.14 Q 135.59, -0.19, 145.86,\
                        0.98 Q 156.14, 1.48, 166.41, 1.75 Q 176.69, 1.44, 186.97, 1.06 Q 197.24, 0.34, 207.52, 0.03 Q 217.79, -0.06, 228.07, 0.03\
                        Q 238.34, 0.39, 248.62, 0.25 Q 258.90, 1.10, 269.17, -0.00 Q 279.45, 0.22, 289.72, 1.56 Q 300.00, 2.72, 310.28, 2.11 Q 320.55,\
                        2.52, 330.83, 2.69 Q 341.10, 2.65, 351.38, 2.53 Q 361.66, 1.69, 371.93, 1.85 Q 382.21, 1.60, 392.48, 0.75 Q 402.76, 0.73,\
                        413.03, 0.70 Q 423.31, 0.55, 433.59, 0.67 Q 443.86, 0.79, 454.14, 0.67 Q 464.41, 0.90, 474.69, 1.31 Q 484.97, 1.65, 495.24,\
                        1.25 Q 505.52, 1.13, 515.79, 1.89 Q 526.07, 1.59, 536.34, 0.28 Q 546.62, 0.28, 556.90, 0.21 Q 567.17, 0.05, 577.45, 0.16 Q\
                        587.72, 0.67, 598.16, 1.84 Q 597.77, 14.74, 597.76, 27.37 Q 597.83, 40.01, 597.71, 52.68 Q 598.18, 65.33, 598.03, 78.03 Q\
                        587.56, 77.51, 577.43, 77.90 Q 567.15, 77.68, 556.88, 77.60 Q 546.61, 77.59, 536.35, 78.68 Q 526.07, 79.17, 515.80, 79.53\
                        Q 505.52, 79.68, 495.24, 79.67 Q 484.97, 79.95, 474.69, 78.81 Q 464.41, 79.10, 454.14, 79.01 Q 443.86, 79.03, 433.59, 79.04\
                        Q 423.31, 78.82, 413.03, 79.33 Q 402.76, 79.56, 392.48, 79.82 Q 382.21, 79.71, 371.93, 79.67 Q 361.66, 78.91, 351.38, 79.46\
                        Q 341.10, 78.83, 330.83, 79.63 Q 320.55, 79.24, 310.28, 78.67 Q 300.00, 79.04, 289.72, 77.59 Q 279.45, 78.63, 269.17, 78.07\
                        Q 258.90, 78.59, 248.62, 79.06 Q 238.34, 78.90, 228.07, 79.81 Q 217.79, 79.59, 207.52, 79.79 Q 197.24, 79.81, 186.97, 79.97\
                        Q 176.69, 79.03, 166.41, 79.02 Q 156.14, 79.33, 145.86, 79.30 Q 135.59, 79.58, 125.31, 79.44 Q 115.03, 79.57, 104.76, 79.14\
                        Q 94.48, 79.90, 84.21, 79.68 Q 73.93, 79.56, 63.66, 80.17 Q 53.38, 80.26, 43.10, 79.58 Q 32.83, 79.63, 22.55, 79.80 Q 12.28,\
                        79.54, 1.19, 78.81 Q 0.82, 65.73, 0.78, 52.84 Q 0.42, 40.11, 0.43, 27.38 Q 2.00, 14.67, 2.00, 2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-231704857" style="position: absolute; left: 395px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="231704857" data-review-reference-id="231704857">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60">\
                     <svg:g width="200" height="60"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.89, 0.25, 23.78, 0.16 Q 34.67, 0.06, 45.56, 0.35 Q\
                        56.44, 0.18, 67.33, 0.18 Q 78.22, 0.61, 89.11, 0.68 Q 100.00, 0.64, 110.89, 0.96 Q 121.78, 0.76, 132.67, 0.33 Q 143.56, 0.85,\
                        154.44, 0.97 Q 165.33, 1.17, 176.22, 1.61 Q 187.11, 1.64, 198.24, 1.76 Q 198.70, 15.76, 198.15, 29.98 Q 197.88, 44.01, 198.19,\
                        58.19 Q 187.41, 58.92, 176.37, 59.07 Q 165.36, 58.49, 154.45, 58.20 Q 143.57, 58.75, 132.67, 58.57 Q 121.78, 58.67, 110.89,\
                        59.34 Q 100.00, 58.59, 89.11, 58.63 Q 78.22, 58.50, 67.33, 58.26 Q 56.44, 58.56, 45.56, 59.01 Q 34.67, 59.38, 23.78, 58.96\
                        Q 12.89, 59.38, 1.90, 58.10 Q 3.06, 43.65, 2.18, 29.97 Q 2.00, 16.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 11.39, 6.22, 21.27, 8.74 Q 31.01, 11.76, 41.07, 13.66 Q 50.81,\
                        16.66, 60.60, 19.49 Q 70.45, 22.12, 80.57, 23.80 Q 90.46, 26.29, 100.01, 29.98 Q 109.80, 32.80, 119.72, 35.17 Q 129.71, 37.31,\
                        139.36, 40.65 Q 149.24, 43.16, 159.12, 45.68 Q 168.77, 48.99, 178.55, 51.87 Q 188.20, 55.20, 198.00, 58.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 58.00 Q 11.77, 54.72, 21.59, 51.67 Q 31.44, 48.69, 41.34, 45.87 Q 51.10,\
                        42.58, 61.08, 40.07 Q 71.01, 37.36, 80.85, 34.35 Q 90.75, 31.56, 100.55, 28.39 Q 110.65, 26.33, 120.53, 23.45 Q 130.51, 20.93,\
                        140.37, 17.99 Q 150.25, 15.11, 160.24, 12.63 Q 170.22, 10.12, 180.12, 7.31 Q 190.10, 4.80, 200.00, 2.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-69641287" style="position: absolute; left: 625px; top: 20px; width: 242px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="69641287" data-review-reference-id="69641287">\
            <div class="stencil-wrapper" style="width: 242px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Коэффициенты </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-858768943" style="position: absolute; left: 377px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="858768943" data-review-reference-id="858768943">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-1556518736" style="position: absolute; left: 275px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1556518736" data-review-reference-id="1556518736">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 80px;width:86px;" width="86" height="80">\
                     <svg:g width="86" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 15.17, -0.54, 28.33, -0.33 Q 41.50, 0.34, 54.67, 0.12 Q 67.83,\
                        1.40, 81.52, 1.48 Q 82.28, 13.74, 82.73, 26.09 Q 81.94, 38.44, 81.59, 50.65 Q 82.65, 62.81, 81.95, 75.95 Q 68.39, 76.69, 54.90,\
                        76.66 Q 41.62, 76.79, 28.39, 76.79 Q 15.19, 76.63, 1.41, 75.59 Q 1.22, 63.09, 1.28, 50.77 Q 1.03, 38.56, 0.16, 26.39 Q 2.00,\
                        14.17, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 82.00, 4.00 Q 81.29, 16.33, 81.31, 28.67 Q 81.89, 41.00, 81.64, 53.33 Q 82.00,\
                        65.67, 82.00, 78.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 83.00, 5.00 Q 84.43, 17.33, 84.38, 29.67 Q 83.50, 42.00, 83.92, 54.33 Q 83.00,\
                        66.67, 83.00, 79.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 84.00, 6.00 Q 85.01, 18.33, 85.08, 30.67 Q 84.38, 43.00, 85.10, 55.33 Q 84.00,\
                        67.67, 84.00, 80.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 76.00 Q 14.00, 76.75, 24.00, 76.67 Q 34.00, 76.36, 44.00, 76.13 Q 54.00,\
                        76.57, 64.00, 76.55 Q 74.00, 76.00, 84.00, 76.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 77.00 Q 15.00, 76.11, 25.00, 75.93 Q 35.00, 75.84, 45.00, 75.68 Q 55.00,\
                        76.02, 65.00, 76.03 Q 75.00, 77.00, 85.00, 77.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 78.00 Q 16.00, 76.38, 26.00, 77.48 Q 36.00, 77.07, 46.00, 78.45 Q 56.00,\
                        78.41, 66.00, 76.73 Q 76.00, 78.00, 86.00, 78.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page106900939-layer-1556518736button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page106900939-layer-1556518736button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page106900939-layer-1556518736button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:82px;height:76px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				=<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 86px; height: 80px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-1556518736\', \'interaction95907894\', {"button":"left","id":"action739538240","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction9868519","options":"reloadOnly","target":"1524600402","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-1684263033" style="position: absolute; left: 275px; top: 350px; width: 600px; height: 395px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="1684263033" data-review-reference-id="1684263033">\
            <div class="stencil-wrapper" style="width: 600px; height: 395px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-page106900939-layer-1684263033-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 395px;width:600px;" width="600" height="395">\
                     <svg:g id="__containerId__-page106900939-layer-1684263033svg" width="600" height="395"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 2.09, 22.55, 1.47 Q 32.83, 1.40, 43.10, 0.77 Q 53.38, 1.64,\
                        63.66, 1.03 Q 73.93, 0.88, 84.21, 0.13 Q 94.48, -0.12, 104.76, 0.48 Q 115.03, 0.40, 125.31, 0.13 Q 135.59, 0.28, 145.86, 0.86\
                        Q 156.14, 0.81, 166.41, 1.02 Q 176.69, 1.08, 186.97, 0.36 Q 197.24, -0.10, 207.52, 0.05 Q 217.79, -0.03, 228.07, -0.03 Q 238.34,\
                        0.37, 248.62, 0.29 Q 258.90, 0.00, 269.17, 0.38 Q 279.45, 1.16, 289.72, 1.36 Q 300.00, 1.47, 310.28, 1.92 Q 320.55, 1.63,\
                        330.83, 2.21 Q 341.10, 1.80, 351.38, 2.26 Q 361.66, 1.35, 371.93, 0.81 Q 382.21, 0.86, 392.48, 0.45 Q 402.76, 0.74, 413.03,\
                        -0.06 Q 423.31, -0.29, 433.59, -0.27 Q 443.86, -0.08, 454.14, -0.30 Q 464.41, 0.03, 474.69, 0.17 Q 484.97, 0.11, 495.24, 0.43\
                        Q 505.52, 0.26, 515.79, 0.65 Q 526.07, 0.34, 536.34, 0.36 Q 546.62, 0.32, 556.90, 0.19 Q 567.17, -0.04, 577.45, -0.17 Q 587.72,\
                        -0.21, 598.81, 1.19 Q 598.35, 12.17, 598.23, 22.55 Q 598.87, 32.81, 598.85, 43.13 Q 598.42, 53.44, 598.43, 63.73 Q 598.60,\
                        74.02, 598.35, 84.32 Q 597.66, 94.61, 597.72, 104.89 Q 597.83, 115.18, 598.17, 125.47 Q 598.54, 135.76, 598.90, 146.05 Q 597.62,\
                        156.34, 598.62, 166.63 Q 598.64, 176.92, 599.18, 187.21 Q 599.62, 197.50, 598.87, 207.79 Q 599.62, 218.08, 599.92, 228.37\
                        Q 599.98, 238.66, 600.29, 248.95 Q 599.92, 259.24, 599.79, 269.53 Q 599.55, 279.82, 599.37, 290.11 Q 598.65, 300.39, 600.14,\
                        310.68 Q 600.08, 320.97, 599.62, 331.26 Q 599.71, 341.55, 599.39, 351.84 Q 598.66, 362.13, 598.79, 372.42 Q 598.98, 382.71,\
                        598.28, 393.28 Q 587.88, 393.45, 577.63, 394.25 Q 567.24, 394.01, 556.92, 393.77 Q 546.63, 393.56, 536.35, 393.56 Q 526.07,\
                        393.20, 515.79, 393.33 Q 505.52, 393.23, 495.24, 394.28 Q 484.97, 394.53, 474.69, 394.62 Q 464.41, 394.56, 454.14, 394.59\
                        Q 443.86, 394.22, 433.59, 394.67 Q 423.31, 394.87, 413.03, 394.90 Q 402.76, 395.03, 392.48, 394.57 Q 382.21, 394.49, 371.93,\
                        394.11 Q 361.66, 393.91, 351.38, 393.28 Q 341.10, 393.60, 330.83, 393.90 Q 320.55, 393.94, 310.28, 393.95 Q 300.00, 394.03,\
                        289.72, 394.12 Q 279.45, 393.81, 269.17, 394.04 Q 258.90, 394.02, 248.62, 393.81 Q 238.34, 393.75, 228.07, 393.21 Q 217.79,\
                        393.55, 207.52, 392.73 Q 197.24, 392.94, 186.97, 393.50 Q 176.69, 393.58, 166.41, 393.77 Q 156.14, 394.14, 145.86, 394.19\
                        Q 135.59, 393.91, 125.31, 394.37 Q 115.03, 393.97, 104.76, 394.70 Q 94.48, 394.00, 84.21, 392.89 Q 73.93, 392.62, 63.66, 394.14\
                        Q 53.38, 394.45, 43.10, 393.30 Q 32.83, 393.25, 22.55, 393.39 Q 12.28, 393.60, 1.65, 393.35 Q 2.23, 382.63, 2.18, 372.40 Q\
                        1.61, 362.16, 1.72, 351.85 Q 0.69, 341.57, 0.64, 331.27 Q 1.01, 320.98, 0.68, 310.69 Q 0.80, 300.40, 0.48, 290.11 Q 0.67,\
                        279.82, 0.79, 269.53 Q 0.33, 259.24, 0.49, 248.95 Q 0.15, 238.66, 1.09, 228.37 Q 1.20, 218.08, 1.98, 207.79 Q 2.34, 197.50,\
                        2.33, 187.21 Q 1.53, 176.92, 0.13, 166.63 Q 0.94, 156.34, 0.54, 146.05 Q 1.26, 135.76, 1.09, 125.47 Q 1.20, 115.18, 0.69,\
                        104.89 Q 0.89, 94.61, 0.57, 84.32 Q 1.04, 74.03, 1.58, 63.74 Q 1.29, 53.45, 1.74, 43.16 Q 0.72, 32.87, 1.21, 22.58 Q 2.00,\
                        12.29, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 596px; height:391px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page106900939-layer-1684263033-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page106900939-layer-1684263033-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page106900939-layer-1684263033-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page106900939-layer-1684263033-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-page106900939-layer-1684263033-accordion", "596", "391", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-page106900939-layer-1707740378" style="position: absolute; left: 277px; top: 380px; width: 598px; height: 273px" data-interactive-element-type="default.table" class="table stencil mobile-interaction-potential-trigger " data-stencil-id="1707740378" data-review-reference-id="1707740378">\
            <div class="stencil-wrapper" style="width: 598px; height: 273px">\
               <div title="">\
                  <svg:svg xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 273px;width:598px;" width="598" height="273">\
                     <svg:g x="0" y="0" width="598" height="273" style="stroke:black;stroke-width:1px;fill:white;"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.24, 1.62, 22.48, 1.60 Q 32.72, 1.45, 42.97, 1.42 Q 53.21, 1.25,\
                        63.45, 0.96 Q 73.69, 0.95, 83.93, 0.81 Q 94.17, 0.81, 104.41, 0.78 Q 114.66, 0.78, 124.90, 0.69 Q 135.14, 0.60, 145.38, 0.58\
                        Q 155.62, 0.48, 165.86, 0.68 Q 176.10, 0.65, 186.34, 0.54 Q 196.59, 0.51, 206.83, 0.52 Q 217.07, 1.13, 227.31, 0.84 Q 237.55,\
                        0.53, 247.79, 0.82 Q 258.03, 0.46, 268.28, 0.43 Q 278.52, 0.35, 288.76, 0.32 Q 299.00, 0.21, 309.24, 0.13 Q 319.48, 0.78,\
                        329.72, 1.02 Q 339.97, 0.99, 350.21, 0.85 Q 360.45, 0.77, 370.69, 0.72 Q 380.93, 1.04, 391.17, 0.65 Q 401.41, 0.82, 411.66,\
                        0.54 Q 421.90, 2.06, 432.14, 1.96 Q 442.38, 1.06, 452.62, 1.81 Q 462.86, 1.20, 473.10, 1.47 Q 483.35, 1.56, 493.59, 1.95 Q\
                        503.83, 1.71, 514.07, 1.32 Q 524.31, 1.40, 534.55, 1.27 Q 544.79, 1.18, 555.03, 1.22 Q 565.28, 1.65, 575.52, 0.57 Q 585.76,\
                        0.34, 596.87, 1.13 Q 596.76, 12.09, 597.02, 22.55 Q 596.96, 32.97, 596.45, 43.37 Q 596.85, 53.72, 596.18, 64.08 Q 596.44,\
                        74.42, 596.29, 84.77 Q 595.60, 95.12, 595.76, 105.46 Q 595.82, 115.81, 596.00, 126.15 Q 596.09, 136.50, 596.53, 146.85 Q 596.03,\
                        157.19, 596.06, 167.54 Q 596.30, 177.88, 596.71, 188.23 Q 596.17, 198.58, 596.12, 208.92 Q 596.42, 219.27, 596.94, 229.62\
                        Q 596.64, 239.96, 595.21, 250.31 Q 595.59, 260.65, 595.72, 270.72 Q 585.81, 271.14, 575.63, 271.79 Q 565.32, 271.70, 555.04,\
                        271.03 Q 544.79, 270.65, 534.56, 272.00 Q 524.32, 272.72, 514.07, 271.43 Q 503.83, 270.82, 493.59, 270.99 Q 483.35, 270.79,\
                        473.10, 270.56 Q 462.86, 270.99, 452.62, 270.61 Q 442.38, 271.57, 432.14, 271.39 Q 421.90, 272.27, 411.66, 271.12 Q 401.41,\
                        271.23, 391.17, 271.29 Q 380.93, 271.26, 370.69, 270.89 Q 360.45, 271.38, 350.21, 271.22 Q 339.97, 270.53, 329.72, 270.69\
                        Q 319.48, 271.01, 309.24, 272.27 Q 299.00, 272.45, 288.76, 272.36 Q 278.52, 272.12, 268.28, 272.30 Q 258.03, 272.40, 247.79,\
                        271.23 Q 237.55, 271.93, 227.31, 271.74 Q 217.07, 271.87, 206.83, 271.69 Q 196.59, 271.56, 186.34, 271.44 Q 176.10, 271.58,\
                        165.86, 271.68 Q 155.62, 271.32, 145.38, 271.34 Q 135.14, 271.86, 124.90, 272.32 Q 114.66, 272.43, 104.41, 272.28 Q 94.17,\
                        271.77, 83.93, 270.91 Q 73.69, 271.58, 63.45, 270.85 Q 53.21, 270.78, 42.97, 270.45 Q 32.72, 270.86, 22.48, 270.86 Q 12.24,\
                        271.19, 1.79, 271.21 Q 2.09, 260.62, 1.02, 250.45 Q 1.50, 240.00, 2.02, 229.61 Q 2.40, 219.26, 2.36, 208.92 Q 2.21, 198.58,\
                        1.82, 188.23 Q 0.62, 177.89, 1.00, 167.54 Q 1.12, 157.19, 1.96, 146.85 Q 2.54, 136.50, 2.56, 126.15 Q 1.70, 115.81, 1.96,\
                        105.46 Q 1.32, 95.12, 0.54, 84.77 Q 0.28, 74.42, 0.41, 64.08 Q 0.99, 53.73, 1.87, 43.38 Q 1.73, 33.04, 1.10, 22.69 Q 2.00,\
                        12.35, 2.00, 2.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 193.00, 0.00 Q 194.42, 10.50, 194.58,\
                        21.00 Q 194.20, 31.50, 194.58, 42.00 Q 194.30, 52.50, 193.80, 63.00 Q 194.29, 73.50, 194.47, 84.00 Q 193.38, 94.50, 193.51,\
                        105.00 Q 193.21, 115.50, 192.91, 126.00 Q 193.06, 136.50, 193.80, 147.00 Q 194.24, 157.50, 194.15, 168.00 Q 193.15, 178.50,\
                        192.63, 189.00 Q 192.13, 199.50, 192.27, 210.00 Q 193.04, 220.50, 192.76, 231.00 Q 193.46, 241.50, 193.26, 252.00 Q 193.00,\
                        262.50, 193.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 301.00, 0.00 Q 301.16, 10.50,\
                        301.32, 21.00 Q 301.41, 31.50, 301.56, 42.00 Q 301.77, 52.50, 301.84, 63.00 Q 302.00, 73.50, 302.04, 84.00 Q 302.38, 94.50,\
                        302.04, 105.00 Q 302.06, 115.50, 302.21, 126.00 Q 302.30, 136.50, 302.50, 147.00 Q 302.53, 157.50, 302.52, 168.00 Q 302.38,\
                        178.50, 302.55, 189.00 Q 302.61, 199.50, 302.03, 210.00 Q 302.18, 220.50, 302.50, 231.00 Q 302.47, 241.50, 302.63, 252.00\
                        Q 301.00, 262.50, 301.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 409.00, 0.00 Q 409.39,\
                        10.50, 409.42, 21.00 Q 409.72, 31.50, 410.01, 42.00 Q 410.26, 52.50, 409.47, 63.00 Q 409.44, 73.50, 409.38, 84.00 Q 409.79,\
                        94.50, 409.58, 105.00 Q 409.48, 115.50, 408.58, 126.00 Q 408.69, 136.50, 408.66, 147.00 Q 408.81, 157.50, 409.87, 168.00 Q\
                        409.54, 178.50, 410.60, 189.00 Q 409.68, 199.50, 409.84, 210.00 Q 409.93, 220.50, 409.39, 231.00 Q 408.53, 241.50, 407.91,\
                        252.00 Q 409.00, 262.50, 409.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 517.00, 0.00\
                        Q 516.32, 10.50, 515.83, 21.00 Q 515.48, 31.50, 516.12, 42.00 Q 517.22, 52.50, 516.52, 63.00 Q 516.01, 73.50, 516.51, 84.00\
                        Q 516.96, 94.50, 517.84, 105.00 Q 517.98, 115.50, 517.53, 126.00 Q 517.03, 136.50, 517.18, 147.00 Q 517.51, 157.50, 517.31,\
                        168.00 Q 516.82, 178.50, 517.60, 189.00 Q 518.74, 199.50, 518.23, 210.00 Q 518.61, 220.50, 518.69, 231.00 Q 518.86, 241.50,\
                        518.99, 252.00 Q 517.00, 262.50, 517.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 0.00,\
                        39.00 Q 10.31, 38.19, 20.62, 38.10 Q 30.93, 38.18, 41.24, 38.56 Q 51.55, 38.74, 61.86, 38.23 Q 72.17, 38.62, 82.48, 38.60\
                        Q 92.79, 38.58, 103.10, 39.30 Q 113.41, 38.43, 123.72, 38.45 Q 134.03, 38.80, 144.34, 38.51 Q 154.66, 39.51, 164.97, 38.83\
                        Q 175.28, 37.92, 185.59, 38.84 Q 195.90, 40.18, 206.21, 40.17 Q 216.52, 38.87, 226.83, 38.82 Q 237.14, 38.95, 247.45, 38.34\
                        Q 257.76, 39.38, 268.07, 38.84 Q 278.38, 38.23, 288.69, 38.89 Q 299.00, 38.95, 309.31, 39.06 Q 319.62, 39.19, 329.93, 37.60\
                        Q 340.24, 36.82, 350.55, 36.69 Q 360.86, 37.72, 371.17, 38.31 Q 381.48, 39.08, 391.79, 39.74 Q 402.10, 40.26, 412.41, 39.54\
                        Q 422.72, 39.12, 433.03, 37.44 Q 443.34, 38.40, 453.65, 38.43 Q 463.97, 38.52, 474.28, 39.09 Q 484.59, 38.46, 494.90, 37.86\
                        Q 505.21, 38.31, 515.52, 39.31 Q 525.83, 39.65, 536.14, 38.95 Q 546.45, 37.81, 556.76, 37.69 Q 567.07, 37.78, 577.38, 38.12\
                        Q 587.69, 39.00, 598.00, 39.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 0.00, 78.00 Q 10.31, 78.08,\
                        20.62, 77.78 Q 30.93, 77.75, 41.24, 77.28 Q 51.55, 77.52, 61.86, 77.37 Q 72.17, 76.86, 82.48, 77.22 Q 92.79, 76.86, 103.10,\
                        77.29 Q 113.41, 77.79, 123.72, 77.01 Q 134.03, 77.34, 144.34, 76.13 Q 154.66, 77.44, 164.97, 76.86 Q 175.28, 76.78, 185.59,\
                        76.81 Q 195.90, 76.63, 206.21, 77.47 Q 216.52, 78.14, 226.83, 77.68 Q 237.14, 76.54, 247.45, 76.18 Q 257.76, 76.44, 268.07,\
                        77.59 Q 278.38, 77.11, 288.69, 76.68 Q 299.00, 77.21, 309.31, 78.48 Q 319.62, 78.80, 329.93, 77.14 Q 340.24, 78.41, 350.55,\
                        77.33 Q 360.86, 77.93, 371.17, 78.31 Q 381.48, 78.07, 391.79, 78.05 Q 402.10, 76.94, 412.41, 77.52 Q 422.72, 76.59, 433.03,\
                        77.17 Q 443.34, 76.59, 453.65, 77.08 Q 463.97, 77.69, 474.28, 76.48 Q 484.59, 76.64, 494.90, 76.22 Q 505.21, 76.20, 515.52,\
                        77.26 Q 525.83, 78.11, 536.14, 78.32 Q 546.45, 77.69, 556.76, 76.96 Q 567.07, 77.07, 577.38, 77.38 Q 587.69, 78.00, 598.00,\
                        78.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 0.00, 117.00 Q 10.31, 114.60, 20.62, 114.54 Q 30.93,\
                        114.76, 41.24, 114.70 Q 51.55, 115.23, 61.86, 115.44 Q 72.17, 114.98, 82.48, 114.84 Q 92.79, 114.69, 103.10, 115.47 Q 113.41,\
                        115.76, 123.72, 116.37 Q 134.03, 115.30, 144.34, 115.54 Q 154.66, 115.89, 164.97, 115.20 Q 175.28, 115.01, 185.59, 115.23\
                        Q 195.90, 115.54, 206.21, 115.77 Q 216.52, 116.28, 226.83, 115.33 Q 237.14, 115.43, 247.45, 116.57 Q 257.76, 116.40, 268.07,\
                        115.62 Q 278.38, 115.40, 288.69, 116.03 Q 299.00, 116.60, 309.31, 116.54 Q 319.62, 115.96, 329.93, 115.19 Q 340.24, 114.86,\
                        350.55, 115.53 Q 360.86, 115.20, 371.17, 115.63 Q 381.48, 115.56, 391.79, 115.54 Q 402.10, 115.59, 412.41, 115.50 Q 422.72,\
                        116.24, 433.03, 116.78 Q 443.34, 117.50, 453.65, 116.12 Q 463.97, 115.48, 474.28, 115.62 Q 484.59, 115.92, 494.90, 115.84\
                        Q 505.21, 115.70, 515.52, 115.57 Q 525.83, 115.94, 536.14, 115.94 Q 546.45, 115.51, 556.76, 115.94 Q 567.07, 115.65, 577.38,\
                        115.69 Q 587.69, 117.00, 598.00, 117.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 0.00, 156.00\
                        Q 10.31, 155.12, 20.62, 155.09 Q 30.93, 154.95, 41.24, 154.80 Q 51.55, 154.95, 61.86, 155.18 Q 72.17, 155.10, 82.48, 154.80\
                        Q 92.79, 155.49, 103.10, 155.01 Q 113.41, 154.91, 123.72, 155.26 Q 134.03, 155.15, 144.34, 155.23 Q 154.66, 154.87, 164.97,\
                        154.58 Q 175.28, 154.66, 185.59, 155.48 Q 195.90, 156.13, 206.21, 155.38 Q 216.52, 155.53, 226.83, 155.13 Q 237.14, 155.94,\
                        247.45, 155.68 Q 257.76, 155.25, 268.07, 155.20 Q 278.38, 155.80, 288.69, 155.87 Q 299.00, 155.74, 309.31, 155.80 Q 319.62,\
                        155.59, 329.93, 155.31 Q 340.24, 155.12, 350.55, 155.08 Q 360.86, 155.97, 371.17, 155.97 Q 381.48, 155.96, 391.79, 156.05\
                        Q 402.10, 155.33, 412.41, 155.58 Q 422.72, 156.28, 433.03, 155.80 Q 443.34, 155.38, 453.65, 154.87 Q 463.97, 155.35, 474.28,\
                        155.51 Q 484.59, 155.56, 494.90, 155.35 Q 505.21, 155.82, 515.52, 154.21 Q 525.83, 155.76, 536.14, 154.42 Q 546.45, 155.05,\
                        556.76, 155.54 Q 567.07, 155.95, 577.38, 155.59 Q 587.69, 156.00, 598.00, 156.00" style=" fill:none;"/><svg:path class=" svg_unselected_element"\
                        d="M 0.00, 195.00 Q 10.31, 193.88, 20.62, 193.82 Q 30.93, 194.67, 41.24, 193.88 Q 51.55, 194.73, 61.86, 194.77 Q 72.17, 194.62,\
                        82.48, 194.13 Q 92.79, 194.23, 103.10, 194.95 Q 113.41, 194.96, 123.72, 194.69 Q 134.03, 194.36, 144.34, 194.35 Q 154.66,\
                        194.45, 164.97, 194.81 Q 175.28, 195.06, 185.59, 195.02 Q 195.90, 195.14, 206.21, 194.71 Q 216.52, 194.24, 226.83, 193.59\
                        Q 237.14, 193.52, 247.45, 193.58 Q 257.76, 194.02, 268.07, 194.41 Q 278.38, 194.34, 288.69, 194.13 Q 299.00, 193.84, 309.31,\
                        193.85 Q 319.62, 193.88, 329.93, 194.63 Q 340.24, 194.22, 350.55, 194.33 Q 360.86, 194.25, 371.17, 194.27 Q 381.48, 194.08,\
                        391.79, 194.34 Q 402.10, 194.12, 412.41, 193.25 Q 422.72, 193.23, 433.03, 193.07 Q 443.34, 193.20, 453.65, 193.30 Q 463.97,\
                        193.89, 474.28, 194.46 Q 484.59, 194.84, 494.90, 194.23 Q 505.21, 194.45, 515.52, 194.19 Q 525.83, 194.28, 536.14, 194.59\
                        Q 546.45, 194.48, 556.76, 194.94 Q 567.07, 195.32, 577.38, 194.72 Q 587.69, 195.00, 598.00, 195.00" style=" fill:none;"/><svg:path\
                        class=" svg_unselected_element" d="M 0.00, 234.00 Q 10.31, 232.17, 20.62, 232.11 Q 30.93, 232.36, 41.24, 232.13 Q 51.55, 232.70,\
                        61.86, 232.89 Q 72.17, 232.54, 82.48, 232.60 Q 92.79, 232.79, 103.10, 233.23 Q 113.41, 233.38, 123.72, 233.65 Q 134.03, 233.28,\
                        144.34, 232.88 Q 154.66, 233.75, 164.97, 233.58 Q 175.28, 233.87, 185.59, 233.43 Q 195.90, 233.59, 206.21, 233.30 Q 216.52,\
                        233.37, 226.83, 232.77 Q 237.14, 233.02, 247.45, 234.07 Q 257.76, 234.05, 268.07, 233.97 Q 278.38, 233.81, 288.69, 233.14\
                        Q 299.00, 233.24, 309.31, 234.08 Q 319.62, 233.93, 329.93, 234.01 Q 340.24, 235.21, 350.55, 234.80 Q 360.86, 235.10, 371.17,\
                        234.18 Q 381.48, 233.80, 391.79, 233.76 Q 402.10, 233.73, 412.41, 232.89 Q 422.72, 232.67, 433.03, 233.24 Q 443.34, 233.36,\
                        453.65, 232.61 Q 463.97, 232.30, 474.28, 232.65 Q 484.59, 233.28, 494.90, 233.30 Q 505.21, 232.83, 515.52, 233.36 Q 525.83,\
                        233.43, 536.14, 233.54 Q 546.45, 233.47, 556.76, 234.13 Q 567.07, 233.87, 577.38, 232.79 Q 587.69, 234.00, 598.00, 234.00"\
                        style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div style="height: 273px;width:598px; position:absolute; top: 0px; left: 0px;"><span style=\'position: absolute; top: 5px;left: 10px;width:163px;\'><span style=\'position: relative;\'>BK</span></span><span\
                     style=\'position: absolute; top: 5px;left: 203px;width:78px;\'><span style=\'position: relative;\'>1</span></span><span style=\'position:\
                     absolute; top: 5px;left: 311px;width:78px;\'><span style=\'position: relative;\'>X</span></span><span style=\'position: absolute;\
                     top: 5px;left: 419px;width:78px;\'><span style=\'position: relative;\'>2</span></span><span style=\'position: absolute; top: 5px;left:\
                     527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position: absolute; top: 44px;left: 10px;width:163px;\'><span\
                     style=\'position: relative;\'>1X ставка</span></span><span style=\'position: absolute; top: 44px;left: 203px;width:78px;\'><span\
                     style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 44px;left: 311px;width:78px;\'><span style=\'position:\
                     relative;\'>_cf_</span></span><span style=\'position: absolute; top: 44px;left: 419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span\
                     style=\'position: absolute; top: 44px;left: 527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position:\
                     absolute; top: 83px;left: 10px;width:163px;\'><span style=\'position: relative;\'>Winline</span></span><span style=\'position:\
                     absolute; top: 83px;left: 203px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute;\
                     top: 83px;left: 311px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top:\
                     83px;left: 419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 83px;left:\
                     527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position: absolute; top: 122px;left: 10px;width:163px;\'><span\
                     style=\'position: relative;\'>Лига ставок</span></span><span style=\'position: absolute; top: 122px;left: 203px;width:78px;\'><span\
                     style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 122px;left: 311px;width:78px;\'><span style=\'position:\
                     relative;\'>_cf_</span></span><span style=\'position: absolute; top: 122px;left: 419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span\
                     style=\'position: absolute; top: 122px;left: 527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position:\
                     absolute; top: 161px;left: 10px;width:163px;\'><span style=\'position: relative;\'>Фонбет</span></span><span style=\'position:\
                     absolute; top: 161px;left: 203px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute;\
                     top: 161px;left: 311px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top:\
                     161px;left: 419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 161px;left:\
                     527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position: absolute; top: 200px;left: 10px;width:163px;\'><span\
                     style=\'position: relative;\'>Леон</span></span><span style=\'position: absolute; top: 200px;left: 203px;width:78px;\'><span style=\'position:\
                     relative;\'>_cf_</span></span><span style=\'position: absolute; top: 200px;left: 311px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span\
                     style=\'position: absolute; top: 200px;left: 419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position:\
                     absolute; top: 200px;left: 527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position: absolute;\
                     top: 239px;left: 10px;width:163px;\'><span style=\'position: relative;\'>Pinnacle</span></span><span style=\'position: absolute;\
                     top: 239px;left: 203px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top:\
                     239px;left: 311px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 239px;left:\
                     419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 239px;left: 527px;width:51px;\'><span\
                     style=\'position: relative;\'>...</span></span>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-727200144" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 1024px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="727200144" data-review-reference-id="727200144">\
            <div class="stencil-wrapper" style="width: 275px; height: 1024px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 1024px;width:275px;" width="275" height="1024">\
                     <svg:g width="275" height="1024"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.42, 0.44, 22.85, 0.53 Q 33.27, 0.59, 43.69, 0.82 Q 54.12, 0.58,\
                        64.54, 0.87 Q 74.96, 1.03, 85.38, 1.13 Q 95.81, 1.03, 106.23, 0.79 Q 116.65, 0.95, 127.08, 1.75 Q 137.50, 1.77, 147.92, 1.46\
                        Q 158.35, 1.17, 168.77, 1.21 Q 179.19, 1.15, 189.62, 1.00 Q 200.04, 0.85, 210.46, 1.05 Q 220.88, 1.25, 231.31, 1.29 Q 241.73,\
                        1.88, 252.15, 1.86 Q 262.58, 1.32, 273.53, 1.47 Q 273.85, 11.72, 273.85, 21.88 Q 273.62, 31.96, 274.04, 41.97 Q 274.16, 51.98,\
                        274.03, 61.99 Q 273.52, 72.00, 274.33, 82.00 Q 274.54, 92.00, 274.78, 102.00 Q 274.23, 112.00, 274.67, 122.00 Q 274.30, 132.00,\
                        274.03, 142.00 Q 274.21, 152.00, 274.09, 162.00 Q 274.77, 172.00, 274.33, 182.00 Q 273.63, 192.00, 273.18, 202.00 Q 273.11,\
                        212.00, 272.83, 222.00 Q 272.88, 232.00, 273.27, 242.00 Q 272.67, 252.00, 273.84, 262.00 Q 274.13, 272.00, 274.12, 282.00\
                        Q 274.21, 292.00, 274.03, 302.00 Q 273.64, 312.00, 273.49, 322.00 Q 273.93, 332.00, 274.06, 342.00 Q 274.22, 352.00, 274.24,\
                        362.00 Q 273.89, 372.00, 273.74, 382.00 Q 273.54, 392.00, 273.06, 402.00 Q 273.21, 412.00, 274.42, 422.00 Q 274.40, 432.00,\
                        274.45, 442.00 Q 273.60, 452.00, 274.57, 462.00 Q 274.62, 472.00, 273.95, 482.00 Q 274.39, 492.00, 274.64, 502.00 Q 275.05,\
                        512.00, 273.73, 522.00 Q 274.09, 532.00, 273.24, 542.00 Q 273.48, 552.00, 273.60, 562.00 Q 272.88, 572.00, 273.08, 582.00\
                        Q 273.30, 592.00, 273.90, 602.00 Q 274.34, 612.00, 274.13, 622.00 Q 273.26, 632.00, 274.02, 642.00 Q 273.89, 652.00, 273.14,\
                        662.00 Q 273.25, 672.00, 273.29, 682.00 Q 273.43, 692.00, 273.06, 702.00 Q 273.87, 712.00, 273.03, 722.00 Q 272.92, 732.00,\
                        273.22, 742.00 Q 272.76, 752.00, 272.33, 762.00 Q 272.68, 772.00, 273.89, 782.00 Q 273.69, 792.00, 274.50, 802.00 Q 273.28,\
                        812.00, 273.96, 822.00 Q 274.64, 832.00, 274.52, 842.00 Q 274.63, 852.00, 274.22, 862.00 Q 274.42, 872.00, 273.51, 882.00\
                        Q 273.42, 892.00, 273.10, 902.00 Q 273.75, 912.00, 273.97, 922.00 Q 273.39, 932.00, 273.40, 942.00 Q 273.67, 952.00, 274.03,\
                        962.00 Q 274.08, 972.00, 274.25, 982.00 Q 273.41, 992.00, 273.28, 1002.00 Q 273.87, 1012.00, 273.03, 1022.03 Q 262.51, 1021.80,\
                        252.19, 1022.23 Q 241.75, 1022.23, 231.31, 1022.12 Q 220.89, 1022.17, 210.46, 1022.18 Q 200.04, 1022.32, 189.62, 1022.88 Q\
                        179.19, 1022.50, 168.77, 1022.30 Q 158.35, 1023.13, 147.92, 1023.30 Q 137.50, 1022.12, 127.08, 1022.62 Q 116.65, 1021.76,\
                        106.23, 1022.73 Q 95.81, 1024.20, 85.38, 1024.08 Q 74.96, 1024.10, 64.54, 1024.01 Q 54.12, 1023.91, 43.69, 1023.26 Q 33.27,\
                        1022.96, 22.85, 1023.09 Q 12.42, 1023.17, 1.31, 1022.69 Q 0.78, 1012.41, 1.60, 1002.06 Q 1.89, 992.01, 1.16, 982.03 Q 1.58,\
                        972.01, 1.62, 962.00 Q 1.64, 952.00, 1.29, 942.00 Q 1.02, 932.00, 1.42, 922.00 Q 2.15, 912.00, 2.35, 902.00 Q 1.94, 892.00,\
                        1.28, 882.00 Q 0.31, 872.00, 0.97, 862.00 Q 0.62, 852.00, 0.79, 842.00 Q 0.68, 832.00, 0.85, 822.00 Q 0.66, 812.00, 0.23,\
                        802.00 Q 0.22, 792.00, 0.96, 782.00 Q 0.86, 772.00, 0.31, 762.00 Q -0.34, 752.00, 1.01, 742.00 Q 0.87, 732.00, 1.30, 722.00\
                        Q 1.18, 712.00, 0.85, 702.00 Q 0.87, 692.00, 0.93, 682.00 Q 0.46, 672.00, 0.25, 662.00 Q 0.80, 652.00, 1.16, 642.00 Q 0.97,\
                        632.00, 0.20, 622.00 Q 0.27, 612.00, 1.52, 602.00 Q 2.55, 592.00, 2.43, 582.00 Q 1.25, 572.00, 1.73, 562.00 Q 1.86, 552.00,\
                        1.41, 542.00 Q 1.86, 532.00, 2.13, 522.00 Q 2.25, 512.00, 2.29, 502.00 Q 2.90, 492.00, 1.33, 482.00 Q 1.69, 472.00, 2.20,\
                        462.00 Q 1.37, 452.00, 0.79, 442.00 Q 1.21, 432.00, 2.41, 422.00 Q 2.31, 412.00, 1.97, 402.00 Q 0.85, 392.00, 0.79, 382.00\
                        Q 1.32, 372.00, 0.77, 362.00 Q 0.79, 352.00, 0.27, 342.00 Q 0.19, 332.00, 0.84, 322.00 Q 0.82, 312.00, 0.25, 302.00 Q 0.89,\
                        292.00, 1.33, 282.00 Q 0.54, 272.00, 0.46, 262.00 Q 0.60, 252.00, 1.40, 242.00 Q 1.21, 232.00, 1.25, 222.00 Q 0.80, 212.00,\
                        0.50, 202.00 Q 0.35, 192.00, 1.05, 182.00 Q 1.18, 172.00, 0.99, 162.00 Q 0.84, 152.00, 1.00, 142.00 Q 1.40, 132.00, 0.54,\
                        122.00 Q 0.29, 112.00, 0.39, 102.00 Q 0.70, 92.00, 0.47, 82.00 Q 0.71, 72.00, 1.62, 62.00 Q 1.45, 52.00, 1.69, 42.00 Q 1.54,\
                        32.00, 1.36, 22.00 Q 2.00, 12.00, 2.00, 2.00" style=" fill:#C1C1C1;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-983065824" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 160px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="983065824" data-review-reference-id="983065824">\
            <div class="stencil-wrapper" style="width: 275px; height: 160px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 160px;width:275px;" width="275" height="160">\
                     <svg:g width="275" height="160"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.42, 2.55, 22.85, 2.38 Q 33.27, 2.01, 43.69, 2.47 Q 54.12, 1.96,\
                        64.54, 1.44 Q 74.96, 1.41, 85.38, 1.07 Q 95.81, 2.47, 106.23, 1.46 Q 116.65, 0.88, 127.08, 0.90 Q 137.50, 0.81, 147.92, 1.12\
                        Q 158.35, 1.36, 168.77, 0.48 Q 179.19, 2.40, 189.62, 3.39 Q 200.04, 3.09, 210.46, 2.00 Q 220.88, 0.50, 231.31, 0.46 Q 241.73,\
                        0.60, 252.15, 0.27 Q 262.58, 0.07, 273.83, 1.17 Q 273.73, 12.90, 274.33, 24.10 Q 273.91, 35.37, 274.16, 46.53 Q 275.52, 57.67,\
                        275.71, 68.84 Q 274.17, 80.00, 273.33, 91.14 Q 271.46, 102.29, 272.50, 113.43 Q 272.87, 124.57, 272.49, 135.71 Q 272.73, 146.86,\
                        272.83, 157.83 Q 262.54, 157.88, 252.25, 158.66 Q 241.81, 159.18, 231.34, 158.90 Q 220.89, 158.51, 210.47, 159.00 Q 200.04,\
                        158.98, 189.62, 158.70 Q 179.19, 158.88, 168.77, 158.90 Q 158.35, 159.18, 147.92, 158.91 Q 137.50, 158.48, 127.08, 158.09\
                        Q 116.65, 158.40, 106.23, 158.98 Q 95.81, 158.24, 85.38, 157.54 Q 74.96, 157.60, 64.54, 158.03 Q 54.12, 159.07, 43.69, 160.23\
                        Q 33.27, 160.59, 22.85, 160.45 Q 12.42, 159.87, 2.14, 157.86 Q 3.07, 146.50, 2.66, 135.62 Q 3.20, 124.49, 3.13, 113.39 Q 1.75,\
                        102.29, 0.56, 91.15 Q 0.36, 80.01, 0.69, 68.86 Q 1.58, 57.71, 2.30, 46.57 Q 2.41, 35.43, 2.07, 24.29 Q 2.00, 13.14, 2.00,\
                        2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-1130817064" style="position: absolute; left: 15px; top: 35px; width: 240px; height: 87px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1130817064" data-review-reference-id="1130817064">\
            <div class="stencil-wrapper" style="width: 240px; height: 87px">\
               <div id="1130817064-1812207366" style="position: absolute; left: 0px; top: 0px; width: 85px; height: 85px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="1812207366" data-review-reference-id="1812207366">\
                  <div class="stencil-wrapper" style="width: 85px; height: 85px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 85px;width:85px;" width="85" height="85">\
                           <svg:g width="85" height="85"><svg:path class=" svg_unselected_element" d="M 82.00, 44.00 Q 81.07, 44.53, 79.93, 54.30 Q 75.98, 63.27, 70.28, 71.24 Q 62.02,\
                              76.67, 53.09, 80.60 Q 43.29, 81.53, 33.42, 81.15 Q 24.24, 76.98, 16.68, 70.38 Q 10.18, 62.84, 6.27, 53.51 Q 4.88, 43.45, 7.52,\
                              33.53 Q 11.35, 24.40, 17.11, 16.26 Q 25.09, 10.08, 34.56, 6.47 Q 44.65, 5.82, 54.60, 6.70 Q 64.07, 10.32, 72.38, 16.33 Q 77.76,\
                              25.20, 81.87, 34.43 Q 81.99, 44.66, 80.53, 54.47" style=" fill:white;"/>\
                           </svg:g>\
                        </svg:svg>\
                     </div>\
                  </div>\
               </div>\
               <div id="1130817064-1896090289" style="position: absolute; left: 100px; top: 0px; width: 91px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1896090289" data-review-reference-id="1896090289">\
                  <div class="stencil-wrapper" style="width: 91px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Name</span></p></span></span></div>\
                  </div>\
               </div>\
               <div id="1130817064-61490957" style="position: absolute; left: 100px; top: 50px; width: 145px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="61490957" data-review-reference-id="61490957">\
                  <div class="stencil-wrapper" style="width: 145px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Surname </p></span></span></div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-423162585" style="position: absolute; left: 0px; top: 180px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="423162585" data-review-reference-id="423162585">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 0.73, 22.62, 0.38 Q 32.92, 0.29, 43.23, 0.16 Q 53.54, 0.14,\
                        63.85, 0.56 Q 74.15, 0.37, 84.46, 0.27 Q 94.77, 0.11, 105.08, 0.07 Q 115.38, 0.60, 125.69, 0.41 Q 136.00, 0.63, 146.31, 0.60\
                        Q 156.62, 0.68, 166.92, 0.61 Q 177.23, 0.92, 187.54, 0.60 Q 197.85, 0.59, 208.15, 0.50 Q 218.46, 0.63, 228.77, 0.88 Q 239.08,\
                        0.72, 249.38, 0.84 Q 259.69, 1.00, 270.32, 1.68 Q 270.16, 12.70, 270.52, 23.43 Q 270.02, 34.25, 270.28, 45.28 Q 259.88, 45.58,\
                        249.48, 45.72 Q 239.08, 45.11, 228.78, 45.20 Q 218.46, 44.88, 208.16, 45.35 Q 197.85, 45.79, 187.54, 45.32 Q 177.23, 46.03,\
                        166.92, 45.88 Q 156.62, 46.12, 146.31, 45.75 Q 136.00, 45.00, 125.69, 45.19 Q 115.38, 45.74, 105.08, 46.07 Q 94.77, 45.98,\
                        84.46, 46.34 Q 74.15, 46.44, 63.85, 46.33 Q 53.54, 46.15, 43.23, 46.08 Q 32.92, 45.21, 22.62, 45.46 Q 12.31, 45.89, 1.93,\
                        45.07 Q 1.76, 34.33, 2.23, 23.47 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#a7a77c;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 273.09, 15.00, 272.07, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 274.21, 16.00, 274.25, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 272.79, 17.00, 272.80, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 46.51, 24.69, 45.37 Q 35.04, 44.47, 45.38, 43.92 Q 55.73,\
                        44.10, 66.08, 44.16 Q 76.42, 44.57, 86.77, 44.41 Q 97.12, 44.39, 107.46, 44.26 Q 117.81, 44.07, 128.15, 44.17 Q 138.50, 44.46,\
                        148.85, 45.11 Q 159.19, 45.18, 169.54, 44.70 Q 179.88, 44.54, 190.23, 44.45 Q 200.58, 44.45, 210.92, 44.33 Q 221.27, 44.07,\
                        231.62, 44.48 Q 241.96, 44.64, 252.31, 44.55 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 48.21, 25.69, 47.37 Q 36.04, 46.63, 46.38, 46.75 Q 56.73,\
                        47.67, 67.08, 46.42 Q 77.42, 45.98, 87.77, 45.91 Q 98.12, 46.63, 108.46, 46.44 Q 118.81, 45.99, 129.15, 45.75 Q 139.50, 45.69,\
                        149.85, 46.10 Q 160.19, 45.83, 170.54, 45.55 Q 180.88, 45.24, 191.23, 45.06 Q 201.58, 45.91, 211.92, 45.76 Q 222.27, 45.07,\
                        232.62, 45.62 Q 242.96, 46.38, 253.31, 45.87 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 46.21, 26.69, 46.18 Q 37.04, 46.05, 47.38, 45.94 Q 57.73,\
                        46.41, 68.08, 45.94 Q 78.42, 45.90, 88.77, 45.67 Q 99.12, 45.81, 109.46, 46.99 Q 119.81, 46.45, 130.15, 45.82 Q 140.50, 46.96,\
                        150.85, 46.33 Q 161.19, 46.38, 171.54, 45.69 Q 181.88, 45.79, 192.23, 45.81 Q 202.58, 46.45, 212.92, 47.59 Q 223.27, 47.72,\
                        233.62, 47.19 Q 243.96, 46.22, 254.31, 46.21 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page106900939-layer-423162585button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page106900939-layer-423162585button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page106900939-layer-423162585button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Коэффициенты<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-423162585\', \'interaction18279135\', {"button":"left","id":"action123693521","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction938923229","options":"reloadOnly","target":"1524600402","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-1360226194" style="position: absolute; left: 0px; top: 250px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1360226194" data-review-reference-id="1360226194">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 0.33, 22.62, 0.97 Q 32.92, 1.37, 43.23, 1.48 Q 53.54, 1.33,\
                        63.85, 1.39 Q 74.15, 1.76, 84.46, 1.46 Q 94.77, 1.43, 105.08, 1.26 Q 115.38, 0.84, 125.69, 0.78 Q 136.00, 1.45, 146.31, 1.85\
                        Q 156.62, 1.93, 166.92, 1.73 Q 177.23, 1.46, 187.54, 1.21 Q 197.85, 0.42, 208.15, 0.91 Q 218.46, 0.94, 228.77, 0.67 Q 239.08,\
                        0.38, 249.38, 0.43 Q 259.69, 1.00, 270.30, 1.70 Q 270.46, 12.60, 270.01, 23.50 Q 269.92, 34.26, 269.75, 44.75 Q 259.54, 44.54,\
                        249.39, 45.04 Q 239.14, 45.96, 228.78, 45.50 Q 218.47, 45.31, 208.16, 45.89 Q 197.85, 46.72, 187.54, 46.55 Q 177.23, 45.98,\
                        166.92, 45.85 Q 156.62, 45.93, 146.31, 45.89 Q 136.00, 45.64, 125.69, 46.67 Q 115.38, 46.63, 105.08, 46.72 Q 94.77, 46.38,\
                        84.46, 46.26 Q 74.15, 46.42, 63.85, 46.59 Q 53.54, 46.51, 43.23, 46.79 Q 32.92, 47.32, 22.62, 46.59 Q 12.31, 46.09, 1.73,\
                        45.27 Q 2.38, 34.12, 1.71, 23.54 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 273.38, 15.00, 272.99, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 272.62, 16.00, 272.54, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 271.18, 17.00, 271.55, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 44.82, 24.69, 45.34 Q 35.04, 45.23, 45.38, 45.68 Q 55.73,\
                        45.15, 66.08, 45.67 Q 76.42, 45.93, 86.77, 46.19 Q 97.12, 46.90, 107.46, 45.86 Q 117.81, 45.65, 128.15, 46.32 Q 138.50, 45.74,\
                        148.85, 45.38 Q 159.19, 44.91, 169.54, 45.31 Q 179.88, 46.04, 190.23, 46.32 Q 200.58, 44.97, 210.92, 45.31 Q 221.27, 44.99,\
                        231.62, 46.01 Q 241.96, 46.39, 252.31, 46.06 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 47.44, 25.69, 46.79 Q 36.04, 46.11, 46.38, 45.31 Q 56.73,\
                        45.04, 67.08, 44.98 Q 77.42, 45.56, 87.77, 45.70 Q 98.12, 45.31, 108.46, 45.22 Q 118.81, 44.97, 129.15, 44.81 Q 139.50, 44.82,\
                        149.85, 45.51 Q 160.19, 45.56, 170.54, 46.36 Q 180.88, 46.16, 191.23, 45.37 Q 201.58, 45.82, 211.92, 45.18 Q 222.27, 45.67,\
                        232.62, 45.20 Q 242.96, 45.21, 253.31, 45.10 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 45.80, 26.69, 45.78 Q 37.04, 45.63, 47.38, 45.92 Q 57.73,\
                        45.58, 68.08, 45.76 Q 78.42, 45.76, 88.77, 46.07 Q 99.12, 45.76, 109.46, 45.80 Q 119.81, 46.47, 130.15, 46.36 Q 140.50, 46.24,\
                        150.85, 46.27 Q 161.19, 46.38, 171.54, 46.46 Q 181.88, 46.30, 192.23, 45.83 Q 202.58, 46.07, 212.92, 46.43 Q 223.27, 46.77,\
                        233.62, 47.01 Q 243.96, 46.88, 254.31, 46.87 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page106900939-layer-1360226194button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page106900939-layer-1360226194button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page106900939-layer-1360226194button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Изменения<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-1360226194\', \'1029926925\', {"button":"left","id":"1505091817","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1938511453","options":"reloadOnly","target":"492670737","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-1734217449" style="position: absolute; left: 0px; top: 320px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1734217449" data-review-reference-id="1734217449">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 0.40, 22.62, 0.97 Q 32.92, 1.95, 43.23, 1.55 Q 53.54, 2.09,\
                        63.85, 2.39 Q 74.15, 1.90, 84.46, 1.33 Q 94.77, 1.68, 105.08, 1.85 Q 115.38, 1.67, 125.69, 1.55 Q 136.00, 1.46, 146.31, 1.15\
                        Q 156.62, 1.47, 166.92, 0.29 Q 177.23, 1.44, 187.54, 1.30 Q 197.85, 2.13, 208.15, 1.81 Q 218.46, 1.59, 228.77, 1.51 Q 239.08,\
                        1.21, 249.38, 0.90 Q 259.69, 1.92, 270.19, 1.81 Q 270.61, 12.55, 270.58, 23.42 Q 270.37, 34.23, 269.84, 44.84 Q 259.74, 45.14,\
                        249.62, 46.74 Q 239.12, 45.75, 228.79, 45.73 Q 218.46, 44.71, 208.16, 45.92 Q 197.85, 44.97, 187.54, 44.10 Q 177.23, 44.16,\
                        166.92, 45.26 Q 156.62, 45.38, 146.31, 45.50 Q 136.00, 45.81, 125.69, 45.61 Q 115.38, 44.63, 105.08, 45.40 Q 94.77, 45.75,\
                        84.46, 46.08 Q 74.15, 45.33, 63.85, 44.14 Q 53.54, 44.42, 43.23, 44.24 Q 32.92, 45.15, 22.62, 44.88 Q 12.31, 45.04, 1.67,\
                        45.33 Q 1.85, 34.30, 2.24, 23.47 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 272.91, 15.00, 273.37, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 272.30, 16.00, 272.46, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.06, 17.00, 273.01, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 45.08, 24.69, 44.96 Q 35.04, 44.87, 45.38, 45.03 Q 55.73,\
                        45.02, 66.08, 44.92 Q 76.42, 45.17, 86.77, 45.34 Q 97.12, 44.62, 107.46, 44.96 Q 117.81, 45.32, 128.15, 45.36 Q 138.50, 45.36,\
                        148.85, 44.98 Q 159.19, 45.41, 169.54, 45.24 Q 179.88, 44.35, 190.23, 44.82 Q 200.58, 45.67, 210.92, 45.14 Q 221.27, 45.89,\
                        231.62, 45.54 Q 241.96, 45.33, 252.31, 45.80 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 47.78, 25.69, 47.25 Q 36.04, 47.22, 46.38, 46.66 Q 56.73,\
                        46.93, 67.08, 46.98 Q 77.42, 46.96, 87.77, 46.89 Q 98.12, 47.21, 108.46, 46.54 Q 118.81, 46.77, 129.15, 46.50 Q 139.50, 46.51,\
                        149.85, 46.94 Q 160.19, 46.51, 170.54, 47.37 Q 180.88, 47.39, 191.23, 47.85 Q 201.58, 47.13, 211.92, 46.75 Q 222.27, 46.67,\
                        232.62, 46.53 Q 242.96, 46.77, 253.31, 46.18 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 46.02, 26.69, 46.45 Q 37.04, 46.56, 47.38, 47.08 Q 57.73,\
                        47.33, 68.08, 47.43 Q 78.42, 48.12, 88.77, 48.01 Q 99.12, 48.31, 109.46, 48.41 Q 119.81, 47.81, 130.15, 47.84 Q 140.50, 48.03,\
                        150.85, 48.01 Q 161.19, 47.87, 171.54, 47.29 Q 181.88, 46.60, 192.23, 46.44 Q 202.58, 47.16, 212.92, 47.01 Q 223.27, 46.80,\
                        233.62, 46.64 Q 243.96, 46.60, 254.31, 46.49 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page106900939-layer-1734217449button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page106900939-layer-1734217449button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page106900939-layer-1734217449button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Статистика<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-1734217449\', \'438952614\', {"button":"left","id":"39975667","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"527260149","options":"reloadOnly","target":"1497021823","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-683091116" style="position: absolute; left: 0px; top: 974px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="683091116" data-review-reference-id="683091116">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.55, 22.62, 0.74 Q 32.92, 0.42, 43.23, 0.70 Q 53.54, 0.92,\
                        63.85, 1.29 Q 74.15, 1.67, 84.46, 1.34 Q 94.77, 1.52, 105.08, 0.99 Q 115.38, 1.05, 125.69, 1.20 Q 136.00, 1.30, 146.31, 2.18\
                        Q 156.62, 2.22, 166.92, 2.22 Q 177.23, 1.75, 187.54, 1.73 Q 197.85, 1.83, 208.15, 1.47 Q 218.46, 0.83, 228.77, 1.53 Q 239.08,\
                        1.93, 249.38, 1.61 Q 259.69, 1.50, 270.40, 1.60 Q 269.85, 12.80, 269.64, 23.55 Q 269.62, 34.28, 269.87, 44.87 Q 259.61, 44.74,\
                        249.31, 44.44 Q 239.05, 44.57, 228.79, 45.54 Q 218.48, 45.99, 208.16, 46.34 Q 197.85, 45.87, 187.54, 45.70 Q 177.23, 45.87,\
                        166.92, 45.45 Q 156.62, 45.60, 146.31, 46.68 Q 136.00, 46.79, 125.69, 46.57 Q 115.38, 45.90, 105.08, 46.41 Q 94.77, 46.46,\
                        84.46, 46.44 Q 74.15, 46.47, 63.85, 46.39 Q 53.54, 46.89, 43.23, 47.14 Q 32.92, 46.71, 22.62, 45.59 Q 12.31, 45.64, 1.83,\
                        45.17 Q 1.25, 34.50, 1.53, 23.57 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 272.14, 15.00, 272.28, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 273.27, 16.00, 273.36, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 275.02, 17.00, 275.04, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 44.25, 24.69, 44.24 Q 35.04, 44.58, 45.38, 44.49 Q 55.73,\
                        44.75, 66.08, 44.88 Q 76.42, 44.84, 86.77, 44.93 Q 97.12, 44.98, 107.46, 45.66 Q 117.81, 46.22, 128.15, 45.40 Q 138.50, 45.48,\
                        148.85, 45.41 Q 159.19, 45.53, 169.54, 45.44 Q 179.88, 45.76, 190.23, 45.43 Q 200.58, 46.39, 210.92, 46.34 Q 221.27, 46.12,\
                        231.62, 46.30 Q 241.96, 46.80, 252.31, 45.33 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 45.66, 25.69, 46.53 Q 36.04, 46.82, 46.38, 46.63 Q 56.73,\
                        47.01, 67.08, 46.65 Q 77.42, 45.81, 87.77, 45.76 Q 98.12, 46.02, 108.46, 45.93 Q 118.81, 45.63, 129.15, 45.59 Q 139.50, 45.24,\
                        149.85, 44.69 Q 160.19, 44.53, 170.54, 44.57 Q 180.88, 46.03, 191.23, 45.09 Q 201.58, 45.54, 211.92, 45.30 Q 222.27, 45.04,\
                        232.62, 44.97 Q 242.96, 44.88, 253.31, 44.98 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 48.87, 26.69, 49.18 Q 37.04, 48.74, 47.38, 49.18 Q 57.73,\
                        47.69, 68.08, 47.56 Q 78.42, 47.92, 88.77, 47.71 Q 99.12, 46.71, 109.46, 46.41 Q 119.81, 46.63, 130.15, 46.91 Q 140.50, 47.39,\
                        150.85, 47.10 Q 161.19, 47.06, 171.54, 46.96 Q 181.88, 48.10, 192.23, 46.66 Q 202.58, 46.03, 212.92, 46.34 Q 223.27, 46.35,\
                        233.62, 46.20 Q 243.96, 46.02, 254.31, 45.87 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page106900939-layer-683091116button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page106900939-layer-683091116button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page106900939-layer-683091116button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Выход<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-683091116\', \'1331777761\', {"button":"left","id":"1263484831","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1763273653","options":"reloadOnly","target":"1168452877","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-2018754584" style="position: absolute; left: 0px; top: 928px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="2018754584" data-review-reference-id="2018754584">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.61, 22.62, 1.77 Q 32.92, 2.49, 43.23, 2.08 Q 53.54, 2.27,\
                        63.85, 2.06 Q 74.15, 1.95, 84.46, 1.73 Q 94.77, 1.38, 105.08, 1.82 Q 115.38, 1.82, 125.69, 1.15 Q 136.00, 1.11, 146.31, 1.41\
                        Q 156.62, 2.04, 166.92, 1.92 Q 177.23, 1.59, 187.54, 0.95 Q 197.85, 0.41, 208.15, 0.14 Q 218.46, 0.04, 228.77, -0.14 Q 239.08,\
                        -0.22, 249.38, -0.35 Q 259.69, 0.09, 270.96, 1.04 Q 271.53, 12.24, 271.90, 23.23 Q 271.77, 34.13, 270.65, 45.65 Q 260.08,\
                        46.21, 249.55, 46.23 Q 239.07, 44.91, 228.77, 44.93 Q 218.47, 45.25, 208.15, 44.94 Q 197.85, 45.04, 187.54, 45.59 Q 177.23,\
                        45.63, 166.92, 46.54 Q 156.62, 46.15, 146.31, 46.38 Q 136.00, 46.54, 125.69, 46.63 Q 115.38, 46.72, 105.08, 46.34 Q 94.77,\
                        46.30, 84.46, 46.56 Q 74.15, 46.61, 63.85, 46.58 Q 53.54, 46.32, 43.23, 46.61 Q 32.92, 46.43, 22.62, 46.29 Q 12.31, 46.05,\
                        1.22, 45.78 Q 1.43, 34.44, 1.39, 23.59 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 273.27, 15.00, 273.46, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 271.43, 16.00, 271.92, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 275.06, 17.00, 275.27, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 46.35, 24.69, 47.33 Q 35.04, 47.69, 45.38, 47.02 Q 55.73,\
                        46.63, 66.08, 46.87 Q 76.42, 46.41, 86.77, 47.34 Q 97.12, 46.78, 107.46, 46.78 Q 117.81, 45.71, 128.15, 45.40 Q 138.50, 45.02,\
                        148.85, 44.68 Q 159.19, 44.72, 169.54, 46.38 Q 179.88, 45.97, 190.23, 45.96 Q 200.58, 46.31, 210.92, 46.18 Q 221.27, 44.33,\
                        231.62, 43.73 Q 241.96, 43.70, 252.31, 43.60 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 46.63, 25.69, 47.06 Q 36.04, 46.80, 46.38, 47.74 Q 56.73,\
                        46.24, 67.08, 45.96 Q 77.42, 46.63, 87.77, 47.07 Q 98.12, 46.29, 108.46, 46.18 Q 118.81, 45.84, 129.15, 46.04 Q 139.50, 46.36,\
                        149.85, 46.42 Q 160.19, 46.32, 170.54, 46.86 Q 180.88, 47.13, 191.23, 46.62 Q 201.58, 46.51, 211.92, 47.02 Q 222.27, 45.74,\
                        232.62, 46.39 Q 242.96, 46.37, 253.31, 46.43 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 47.25, 26.69, 47.21 Q 37.04, 47.13, 47.38, 47.22 Q 57.73,\
                        47.38, 68.08, 47.47 Q 78.42, 48.16, 88.77, 47.69 Q 99.12, 48.01, 109.46, 47.53 Q 119.81, 47.62, 130.15, 47.89 Q 140.50, 47.92,\
                        150.85, 48.82 Q 161.19, 48.04, 171.54, 48.61 Q 181.88, 48.12, 192.23, 48.78 Q 202.58, 48.07, 212.92, 48.18 Q 223.27, 48.11,\
                        233.62, 48.61 Q 243.96, 49.11, 254.31, 48.98 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page106900939-layer-2018754584button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page106900939-layer-2018754584button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page106900939-layer-2018754584button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Настройки<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-2018754584\', \'1248403003\', {"button":"left","id":"1081421981","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1576978564","options":"reloadOnly","target":"1985076138","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-1791868487" style="position: absolute; left: 0px; top: 880px; width: 275px; height: 48px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1791868487" data-review-reference-id="1791868487">\
            <div class="stencil-wrapper" style="width: 275px; height: 48px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 48px;width:275px;" width="275" height="48">\
                     <svg:g width="275" height="48"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, -0.09, 22.62, -0.15 Q 32.92, -0.30, 43.23, 0.34 Q 53.54,\
                        0.19, 63.85, 0.02 Q 74.15, 0.19, 84.46, 0.90 Q 94.77, 0.58, 105.08, 0.29 Q 115.38, -0.35, 125.69, 0.10 Q 136.00, -0.08, 146.31,\
                        0.92 Q 156.62, 1.31, 166.92, 1.59 Q 177.23, 1.32, 187.54, 1.01 Q 197.85, 1.25, 208.15, 1.09 Q 218.46, 0.30, 228.77, 0.18 Q\
                        239.08, 0.60, 249.38, 0.80 Q 259.69, 1.20, 270.88, 1.12 Q 270.78, 11.99, 270.35, 22.45 Q 270.77, 32.70, 270.18, 43.18 Q 259.86,\
                        43.52, 249.49, 43.76 Q 239.13, 43.86, 228.81, 44.22 Q 218.48, 43.96, 208.17, 44.91 Q 197.85, 45.02, 187.54, 44.73 Q 177.23,\
                        44.35, 166.92, 44.52 Q 156.62, 44.23, 146.31, 44.08 Q 136.00, 43.90, 125.69, 44.13 Q 115.38, 44.57, 105.08, 44.58 Q 94.77,\
                        44.73, 84.46, 44.79 Q 74.15, 44.47, 63.85, 43.99 Q 53.54, 43.95, 43.23, 44.47 Q 32.92, 44.17, 22.62, 44.52 Q 12.31, 43.86,\
                        1.37, 43.63 Q 0.86, 33.13, 0.41, 22.73 Q 2.00, 12.25, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 270.96, 14.50, 271.56, 25.00 Q 271.00, 35.50, 271.00, 46.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 273.69, 15.50, 273.40, 26.00 Q 272.00, 36.50, 272.00, 47.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 272.24, 16.50, 272.13, 27.00 Q 273.00, 37.50, 273.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 44.00 Q 14.35, 45.21, 24.69, 45.07 Q 35.04, 44.60, 45.38, 44.54 Q 55.73,\
                        43.71, 66.08, 43.64 Q 76.42, 43.84, 86.77, 43.86 Q 97.12, 43.63, 107.46, 43.79 Q 117.81, 43.64, 128.15, 43.80 Q 138.50, 44.12,\
                        148.85, 43.49 Q 159.19, 43.77, 169.54, 43.46 Q 179.88, 43.87, 190.23, 43.48 Q 200.58, 44.04, 210.92, 44.22 Q 221.27, 44.35,\
                        231.62, 43.91 Q 241.96, 43.77, 252.31, 43.58 Q 262.65, 44.00, 273.00, 44.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 45.00 Q 15.35, 44.71, 25.69, 45.23 Q 36.04, 45.00, 46.38, 45.85 Q 56.73,\
                        44.55, 67.08, 43.93 Q 77.42, 44.46, 87.77, 44.87 Q 98.12, 44.20, 108.46, 43.93 Q 118.81, 44.79, 129.15, 43.61 Q 139.50, 43.95,\
                        149.85, 44.08 Q 160.19, 43.81, 170.54, 43.95 Q 180.88, 44.17, 191.23, 43.68 Q 201.58, 44.29, 211.92, 44.50 Q 222.27, 44.50,\
                        232.62, 45.23 Q 242.96, 43.92, 253.31, 43.20 Q 263.65, 45.00, 274.00, 45.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 46.00 Q 16.35, 44.33, 26.69, 44.05 Q 37.04, 44.60, 47.38, 43.96 Q 57.73,\
                        44.70, 68.08, 44.69 Q 78.42, 44.82, 88.77, 44.13 Q 99.12, 44.55, 109.46, 44.40 Q 119.81, 43.85, 130.15, 43.54 Q 140.50, 44.99,\
                        150.85, 45.11 Q 161.19, 44.17, 171.54, 44.27 Q 181.88, 44.18, 192.23, 44.61 Q 202.58, 44.37, 212.92, 44.46 Q 223.27, 45.09,\
                        233.62, 44.27 Q 243.96, 44.16, 254.31, 43.46 Q 264.65, 46.00, 275.00, 46.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page106900939-layer-1791868487button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page106900939-layer-1791868487button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page106900939-layer-1791868487button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:44px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Контакты<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 48px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-1791868487\', \'503721507\', {"button":"left","id":"307307194","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"640307367","options":"reloadOnly","target":"132438759","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page106900939"] .border-wrapper, body[data-current-page-id="page106900939"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page106900939"] .border-wrapper, body.has-frame[data-current-page-id="page106900939"]\
         .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="page106900939"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page106900939"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page106900939",\
      			"name": "cf - menu",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
   <div id="border-wrapper">\
      <div xmlns="http://www.w3.org/1999/xhtml" xmlns:json="http://json.org/" class="svg-border svg-border-600-1024" style="display: none;">\
         <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" class="svg_border" style="position:absolute;left:-43px;top:-13px;width:657px;height:1048px;"><svg:path class=" svg_unselected_element" d="M 32.00, 3.00 Q 42.17, 2.84, 52.33, 3.44 Q 62.50, 2.89, 72.67, 3.03 Q 82.83,\
            2.91, 93.00, 1.99 Q 103.17, 1.77, 113.33, 1.21 Q 123.50, 0.93, 133.67, 1.46 Q 143.83, 1.30, 154.00, 1.35 Q 164.17, 1.56, 174.33,\
            2.21 Q 184.50, 1.81, 194.67, 1.48 Q 204.83, 1.80, 215.00, 1.19 Q 225.17, 1.11, 235.33, 1.64 Q 245.50, 1.96, 255.67, 1.88 Q\
            265.83, 1.70, 276.00, 1.87 Q 286.17, 2.27, 296.33, 2.27 Q 306.50, 3.40, 316.67, 3.14 Q 326.83, 2.70, 337.00, 2.90 Q 347.17,\
            2.77, 357.33, 2.25 Q 367.50, 2.10, 377.67, 2.17 Q 387.83, 1.67, 398.00, 1.18 Q 408.17, 0.72, 418.33, 0.76 Q 428.50, 0.84,\
            438.67, 0.88 Q 448.83, 0.87, 459.00, 0.87 Q 469.17, 0.66, 479.33, 1.95 Q 489.50, 1.24, 499.67, 1.81 Q 509.83, 1.43, 520.00,\
            1.14 Q 530.17, 1.07, 540.33, 1.50 Q 550.50, 1.48, 560.67, 1.51 Q 570.83, 2.17, 581.00, 3.00 Q 591.17, 3.21, 601.33, 2.70 Q\
            611.50, 2.26, 621.67, 1.97 Q 631.83, 1.84, 642.10, 2.90 Q 641.96, 13.10, 641.98, 23.18 Q 642.34, 33.24, 642.15, 43.35 Q 642.15,\
            53.44, 641.96, 63.53 Q 641.99, 73.62, 641.71, 83.71 Q 642.03, 93.79, 642.27, 103.88 Q 642.39, 113.97, 643.18, 124.06 Q 644.23,\
            134.15, 643.85, 144.24 Q 643.87, 154.32, 644.00, 164.41 Q 643.98, 174.50, 643.39, 184.59 Q 643.39, 194.68, 643.19, 204.76\
            Q 643.60, 214.85, 643.59, 224.94 Q 643.72, 235.03, 643.53, 245.12 Q 644.07, 255.21, 644.20, 265.29 Q 643.84, 275.38, 641.50,\
            285.47 Q 641.53, 295.56, 643.04, 305.65 Q 643.00, 315.74, 642.51, 325.82 Q 642.59, 335.91, 643.13, 346.00 Q 643.26, 356.09,\
            642.61, 366.18 Q 642.62, 376.26, 642.56, 386.35 Q 642.74, 396.44, 642.39, 406.53 Q 643.28, 416.62, 642.96, 426.71 Q 643.15,\
            436.79, 642.85, 446.88 Q 642.35, 456.97, 642.56, 467.06 Q 642.72, 477.15, 643.24, 487.24 Q 643.45, 497.32, 643.57, 507.41\
            Q 643.29, 517.50, 643.35, 527.59 Q 643.30, 537.68, 643.25, 547.76 Q 642.89, 557.85, 642.57, 567.94 Q 642.82, 578.03, 642.38,\
            588.12 Q 642.48, 598.21, 641.62, 608.29 Q 642.43, 618.38, 642.56, 628.47 Q 642.41, 638.56, 642.64, 648.65 Q 642.07, 658.74,\
            642.93, 668.82 Q 642.75, 678.91, 642.76, 689.00 Q 642.22, 699.09, 643.10, 709.18 Q 642.83, 719.27, 642.31, 729.35 Q 642.78,\
            739.44, 643.06, 749.53 Q 643.45, 759.62, 642.88, 769.71 Q 643.50, 779.79, 642.94, 789.88 Q 643.14, 799.97, 643.02, 810.06\
            Q 643.56, 820.15, 642.53, 830.24 Q 642.57, 840.32, 643.03, 850.41 Q 642.98, 860.50, 643.99, 870.59 Q 642.90, 880.68, 643.36,\
            890.77 Q 643.19, 900.85, 642.82, 910.94 Q 643.14, 921.03, 642.96, 931.12 Q 643.70, 941.21, 643.01, 951.29 Q 643.98, 961.38,\
            642.59, 971.47 Q 642.33, 981.56, 642.14, 991.65 Q 642.04, 1001.74, 641.99, 1011.82 Q 641.96, 1021.91, 642.26, 1032.26 Q 631.82,\
            1031.97, 621.75, 1032.59 Q 611.53, 1032.49, 601.37, 1033.09 Q 591.19, 1033.45, 581.00, 1032.47 Q 570.84, 1032.92, 560.67,\
            1033.34 Q 550.50, 1033.67, 540.33, 1033.89 Q 530.17, 1033.72, 520.00, 1033.83 Q 509.83, 1033.10, 499.67, 1031.40 Q 489.50,\
            1030.27, 479.33, 1030.75 Q 469.17, 1031.01, 459.00, 1032.41 Q 448.83, 1032.72, 438.67, 1032.77 Q 428.50, 1033.07, 418.33,\
            1032.98 Q 408.17, 1033.08, 398.00, 1033.28 Q 387.83, 1032.91, 377.67, 1033.44 Q 367.50, 1033.48, 357.33, 1033.40 Q 347.17,\
            1032.98, 337.00, 1032.22 Q 326.83, 1032.49, 316.67, 1032.27 Q 306.50, 1032.30, 296.33, 1032.21 Q 286.17, 1032.22, 276.00,\
            1032.59 Q 265.83, 1031.68, 255.67, 1031.54 Q 245.50, 1032.42, 235.33, 1032.92 Q 225.17, 1033.10, 215.00, 1032.71 Q 204.83,\
            1032.51, 194.67, 1033.01 Q 184.50, 1033.28, 174.33, 1032.21 Q 164.17, 1031.29, 154.00, 1031.32 Q 143.83, 1031.83, 133.67,\
            1031.81 Q 123.50, 1031.89, 113.33, 1031.64 Q 103.17, 1031.81, 93.00, 1032.57 Q 82.83, 1032.89, 72.67, 1033.00 Q 62.50, 1033.12,\
            52.33, 1033.13 Q 42.17, 1032.80, 31.60, 1032.40 Q 31.45, 1022.10, 31.54, 1011.89 Q 31.52, 1001.77, 31.40, 991.67 Q 31.17,\
            981.57, 31.35, 971.48 Q 31.18, 961.39, 31.27, 951.30 Q 31.20, 941.21, 31.59, 931.12 Q 31.37, 921.03, 31.47, 910.94 Q 31.30,\
            900.85, 32.25, 890.77 Q 31.40, 880.68, 31.65, 870.59 Q 31.30, 860.50, 32.77, 850.41 Q 33.42, 840.32, 31.80, 830.24 Q 31.34,\
            820.15, 31.95, 810.06 Q 32.26, 799.97, 31.95, 789.88 Q 31.43, 779.79, 31.48, 769.71 Q 32.28, 759.62, 32.40, 749.53 Q 31.89,\
            739.44, 31.40, 729.35 Q 31.74, 719.27, 31.25, 709.18 Q 30.94, 699.09, 30.92, 689.00 Q 31.09, 678.91, 32.50, 668.82 Q 31.97,\
            658.74, 31.36, 648.65 Q 31.05, 638.56, 31.94, 628.47 Q 33.26, 618.38, 32.49, 608.29 Q 30.96, 598.21, 31.75, 588.12 Q 31.64,\
            578.03, 31.32, 567.94 Q 31.36, 557.85, 30.73, 547.76 Q 31.14, 537.68, 31.57, 527.59 Q 31.07, 517.50, 30.63, 507.41 Q 31.21,\
            497.32, 31.74, 487.24 Q 31.92, 477.15, 31.42, 467.06 Q 31.18, 456.97, 32.09, 446.88 Q 31.84, 436.79, 31.65, 426.71 Q 31.98,\
            416.62, 32.62, 406.53 Q 32.09, 396.44, 32.34, 386.35 Q 31.87, 376.26, 32.22, 366.18 Q 32.65, 356.09, 31.89, 346.00 Q 30.69,\
            335.91, 30.48, 325.82 Q 31.43, 315.74, 31.88, 305.65 Q 33.15, 295.56, 31.58, 285.47 Q 30.95, 275.38, 31.69, 265.29 Q 32.18,\
            255.21, 31.54, 245.12 Q 30.62, 235.03, 30.59, 224.94 Q 30.55, 214.85, 31.62, 204.76 Q 31.17, 194.68, 31.18, 184.59 Q 31.60,\
            174.50, 31.98, 164.41 Q 31.50, 154.32, 30.38, 144.24 Q 31.25, 134.15, 32.04, 124.06 Q 32.02, 113.97, 31.99, 103.88 Q 32.04,\
            93.79, 31.93, 83.71 Q 32.97, 73.62, 32.84, 63.53 Q 32.68, 53.44, 32.78, 43.35 Q 32.63, 33.26, 32.63, 23.18 Q 32.00, 13.09,\
            32.00, 3.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 23.00, 7.00 Q 33.17, 7.20, 43.33, 6.42 Q 53.50, 6.35, 63.67, 6.51 Q 73.83,\
            5.90, 84.00, 6.13 Q 94.17, 6.38, 104.33, 6.59 Q 114.50, 6.79, 124.67, 6.65 Q 134.83, 6.43, 145.00, 6.46 Q 155.17, 6.30, 165.33,\
            5.83 Q 175.50, 6.55, 185.67, 6.30 Q 195.83, 5.95, 206.00, 5.96 Q 216.17, 5.49, 226.33, 5.40 Q 236.50, 4.82, 246.67, 4.77 Q\
            256.83, 4.60, 267.00, 5.21 Q 277.17, 5.27, 287.33, 5.17 Q 297.50, 4.76, 307.67, 4.62 Q 317.83, 5.07, 328.00, 5.82 Q 338.17,\
            6.55, 348.33, 5.78 Q 358.50, 5.36, 368.67, 5.26 Q 378.83, 5.29, 389.00, 5.50 Q 399.17, 5.58, 409.33, 5.61 Q 419.50, 5.78,\
            429.67, 5.85 Q 439.83, 5.41, 450.00, 5.42 Q 460.17, 5.36, 470.33, 5.27 Q 480.50, 5.10, 490.67, 4.89 Q 500.83, 5.58, 511.00,\
            5.88 Q 521.17, 6.37, 531.33, 5.18 Q 541.50, 5.96, 551.67, 5.60 Q 561.83, 5.58, 572.00, 5.36 Q 582.17, 5.49, 592.33, 5.72 Q\
            602.50, 5.62, 612.67, 5.36 Q 622.83, 5.22, 633.73, 6.27 Q 633.00, 17.09, 633.06, 27.17 Q 632.93, 37.27, 633.45, 47.34 Q 633.39,\
            57.44, 633.33, 67.53 Q 633.25, 77.62, 633.63, 87.70 Q 633.86, 97.79, 634.42, 107.88 Q 634.56, 117.97, 634.70, 128.06 Q 633.50,\
            138.15, 634.14, 148.24 Q 634.12, 158.32, 633.67, 168.41 Q 634.14, 178.50, 634.27, 188.59 Q 634.00, 198.68, 634.30, 208.76\
            Q 634.49, 218.85, 634.46, 228.94 Q 634.09, 239.03, 633.97, 249.12 Q 633.72, 259.21, 633.98, 269.29 Q 635.04, 279.38, 634.96,\
            289.47 Q 634.98, 299.56, 635.17, 309.65 Q 634.60, 319.74, 634.86, 329.82 Q 634.68, 339.91, 634.13, 350.00 Q 634.66, 360.09,\
            633.90, 370.18 Q 632.96, 380.26, 633.31, 390.35 Q 634.00, 400.44, 634.93, 410.53 Q 634.43, 420.62, 633.19, 430.71 Q 632.74,\
            440.79, 633.45, 450.88 Q 632.79, 460.97, 633.65, 471.06 Q 633.66, 481.15, 634.48, 491.24 Q 634.38, 501.32, 634.34, 511.41\
            Q 634.15, 521.50, 633.94, 531.59 Q 633.98, 541.68, 633.70, 551.76 Q 634.15, 561.85, 634.18, 571.94 Q 634.34, 582.03, 634.67,\
            592.12 Q 634.44, 602.21, 634.69, 612.29 Q 633.51, 622.38, 632.76, 632.47 Q 632.47, 642.56, 633.47, 652.65 Q 632.69, 662.74,\
            633.12, 672.82 Q 633.71, 682.91, 633.81, 693.00 Q 633.48, 703.09, 633.23, 713.18 Q 633.45, 723.26, 633.03, 733.35 Q 632.85,\
            743.44, 632.52, 753.53 Q 632.59, 763.62, 632.84, 773.71 Q 632.97, 783.79, 633.02, 793.88 Q 633.10, 803.97, 633.79, 814.06\
            Q 633.23, 824.15, 634.01, 834.24 Q 634.68, 844.32, 634.93, 854.41 Q 634.42, 864.50, 633.87, 874.59 Q 634.24, 884.68, 634.43,\
            894.77 Q 633.31, 904.85, 633.66, 914.94 Q 633.47, 925.03, 633.22, 935.12 Q 633.34, 945.21, 633.46, 955.29 Q 633.50, 965.38,\
            633.72, 975.47 Q 634.13, 985.56, 633.61, 995.65 Q 633.19, 1005.74, 634.26, 1015.82 Q 634.08, 1025.91, 633.66, 1036.66 Q 623.04,\
            1036.63, 612.77, 1036.71 Q 602.52, 1036.27, 592.33, 1035.84 Q 582.17, 1035.97, 572.00, 1035.55 Q 561.84, 1036.80, 551.66,\
            1035.22 Q 541.50, 1036.15, 531.33, 1035.92 Q 521.17, 1036.15, 511.00, 1037.53 Q 500.83, 1037.70, 490.67, 1037.73 Q 480.50,\
            1037.86, 470.33, 1037.63 Q 460.17, 1038.14, 450.00, 1037.51 Q 439.83, 1037.29, 429.67, 1036.75 Q 419.50, 1036.52, 409.33,\
            1036.65 Q 399.17, 1037.69, 389.00, 1036.68 Q 378.83, 1036.46, 368.67, 1035.47 Q 358.50, 1036.72, 348.33, 1037.05 Q 338.17,\
            1036.60, 328.00, 1036.60 Q 317.83, 1036.78, 307.67, 1036.27 Q 297.50, 1035.83, 287.33, 1036.41 Q 277.17, 1036.07, 267.00,\
            1036.37 Q 256.83, 1036.83, 246.67, 1037.40 Q 236.50, 1037.24, 226.33, 1037.06 Q 216.17, 1036.54, 206.00, 1034.95 Q 195.83,\
            1035.48, 185.67, 1035.23 Q 175.50, 1036.54, 165.33, 1036.33 Q 155.17, 1035.64, 145.00, 1035.07 Q 134.83, 1036.09, 124.67,\
            1036.29 Q 114.50, 1035.94, 104.33, 1036.29 Q 94.17, 1037.23, 84.00, 1037.25 Q 73.83, 1036.82, 63.67, 1036.92 Q 53.50, 1036.59,\
            43.33, 1037.32 Q 33.17, 1037.23, 22.52, 1036.48 Q 22.55, 1026.06, 22.40, 1015.91 Q 22.34, 1005.78, 22.79, 995.65 Q 23.18,\
            985.56, 23.02, 975.47 Q 22.72, 965.38, 22.39, 955.30 Q 22.02, 945.21, 21.89, 935.12 Q 21.99, 925.03, 21.99, 914.94 Q 22.19,\
            904.85, 22.14, 894.77 Q 22.45, 884.68, 22.25, 874.59 Q 21.73, 864.50, 21.39, 854.41 Q 21.88, 844.32, 22.63, 834.24 Q 22.38,\
            824.15, 23.27, 814.06 Q 23.98, 803.97, 23.22, 793.88 Q 22.50, 783.79, 21.37, 773.71 Q 21.80, 763.62, 22.11, 753.53 Q 21.62,\
            743.44, 21.51, 733.35 Q 22.10, 723.26, 22.09, 713.18 Q 22.26, 703.09, 21.67, 693.00 Q 21.67, 682.91, 22.08, 672.82 Q 22.54,\
            662.74, 22.24, 652.65 Q 22.31, 642.56, 22.11, 632.47 Q 22.70, 622.38, 22.71, 612.29 Q 22.54, 602.21, 23.29, 592.12 Q 23.51,\
            582.03, 24.25, 571.94 Q 24.99, 561.85, 23.89, 551.76 Q 23.34, 541.68, 22.89, 531.59 Q 23.37, 521.50, 23.02, 511.41 Q 23.17,\
            501.32, 23.58, 491.24 Q 23.44, 481.15, 22.96, 471.06 Q 21.88, 460.97, 21.86, 450.88 Q 22.53, 440.79, 22.67, 430.71 Q 22.36,\
            420.62, 21.91, 410.53 Q 22.09, 400.44, 21.74, 390.35 Q 21.54, 380.26, 20.74, 370.18 Q 21.23, 360.09, 21.42, 350.00 Q 22.43,\
            339.91, 22.54, 329.82 Q 22.42, 319.74, 22.00, 309.65 Q 21.62, 299.56, 21.74, 289.47 Q 22.47, 279.38, 23.74, 269.29 Q 23.48,\
            259.21, 23.16, 249.12 Q 22.76, 239.03, 23.19, 228.94 Q 23.90, 218.85, 23.51, 208.76 Q 24.00, 198.68, 23.33, 188.59 Q 23.06,\
            178.50, 23.61, 168.41 Q 23.63, 158.32, 23.40, 148.24 Q 22.87, 138.15, 23.06, 128.06 Q 23.26, 117.97, 23.31, 107.88 Q 23.20,\
            97.79, 23.62, 87.71 Q 23.64, 77.62, 24.19, 67.53 Q 23.54, 57.44, 22.66, 47.35 Q 22.90, 37.26, 23.59, 27.18 Q 23.00, 17.09,\
            23.00, 7.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 40.00, 11.00 Q 50.17, 8.51, 60.33, 8.65 Q 70.50, 8.65, 80.67, 8.83 Q 90.83,\
            10.04, 101.00, 9.08 Q 111.17, 9.27, 121.33, 9.94 Q 131.50, 9.97, 141.67, 11.40 Q 151.83, 11.82, 162.00, 9.75 Q 172.17, 9.01,\
            182.33, 9.90 Q 192.50, 10.72, 202.67, 10.15 Q 212.83, 8.80, 223.00, 8.77 Q 233.17, 9.51, 243.33, 10.48 Q 253.50, 11.71, 263.67,\
            11.61 Q 273.83, 11.47, 284.00, 9.71 Q 294.17, 9.50, 304.33, 8.80 Q 314.50, 8.70, 324.67, 9.92 Q 334.83, 9.96, 345.00, 10.66\
            Q 355.17, 10.99, 365.33, 10.47 Q 375.50, 9.72, 385.67, 9.09 Q 395.83, 8.98, 406.00, 8.85 Q 416.17, 8.92, 426.33, 9.31 Q 436.50,\
            9.89, 446.67, 10.58 Q 456.83, 10.91, 467.00, 10.93 Q 477.17, 10.79, 487.33, 10.44 Q 497.50, 10.18, 507.67, 10.43 Q 517.83,\
            10.23, 528.00, 9.45 Q 538.17, 9.34, 548.33, 10.12 Q 558.50, 10.11, 568.67, 9.29 Q 578.83, 9.01, 589.00, 8.82 Q 599.17, 9.43,\
            609.33, 10.71 Q 619.50, 10.54, 629.67, 9.91 Q 639.83, 9.64, 650.62, 10.38 Q 651.23, 20.68, 651.02, 31.03 Q 651.02, 41.20,\
            651.12, 51.32 Q 651.24, 61.42, 651.70, 71.52 Q 651.90, 81.61, 651.60, 91.70 Q 651.12, 101.79, 651.38, 111.88 Q 651.26, 121.97,\
            651.03, 132.06 Q 651.11, 142.15, 650.76, 152.24 Q 650.70, 162.32, 651.27, 172.41 Q 650.66, 182.50, 650.29, 192.59 Q 650.09,\
            202.68, 650.40, 212.76 Q 650.42, 222.85, 651.16, 232.94 Q 651.42, 243.03, 651.35, 253.12 Q 651.23, 263.21, 650.38, 273.29\
            Q 650.98, 283.38, 650.27, 293.47 Q 650.51, 303.56, 650.53, 313.65 Q 651.46, 323.74, 651.63, 333.82 Q 650.78, 343.91, 651.70,\
            354.00 Q 651.17, 364.09, 651.72, 374.18 Q 651.95, 384.26, 651.92, 394.35 Q 651.31, 404.44, 651.82, 414.53 Q 651.50, 424.62,\
            651.49, 434.71 Q 651.89, 444.79, 651.56, 454.88 Q 651.42, 464.97, 651.48, 475.06 Q 651.79, 485.15, 651.93, 495.24 Q 652.01,\
            505.32, 651.54, 515.41 Q 650.59, 525.50, 649.47, 535.59 Q 649.09, 545.68, 649.86, 555.76 Q 649.69, 565.85, 650.45, 575.94\
            Q 650.27, 586.03, 650.91, 596.12 Q 650.81, 606.21, 650.63, 616.29 Q 651.10, 626.38, 650.76, 636.47 Q 651.23, 646.56, 650.32,\
            656.65 Q 650.72, 666.74, 650.48, 676.82 Q 650.56, 686.91, 650.48, 697.00 Q 650.14, 707.09, 650.56, 717.18 Q 650.71, 727.27,\
            650.99, 737.35 Q 650.48, 747.44, 651.11, 757.53 Q 651.08, 767.62, 650.85, 777.71 Q 651.04, 787.79, 650.33, 797.88 Q 650.76,\
            807.97, 650.29, 818.06 Q 650.42, 828.15, 649.99, 838.24 Q 650.53, 848.32, 649.87, 858.41 Q 650.49, 868.50, 650.23, 878.59\
            Q 650.01, 888.68, 649.99, 898.77 Q 649.41, 908.85, 649.96, 918.94 Q 650.17, 929.03, 650.96, 939.12 Q 650.06, 949.21, 650.55,\
            959.29 Q 650.69, 969.38, 649.99, 979.47 Q 649.94, 989.56, 649.44, 999.65 Q 649.75, 1009.74, 649.60, 1019.82 Q 650.15, 1029.91,\
            650.00, 1040.00 Q 639.99, 1040.47, 629.86, 1041.34 Q 619.62, 1041.80, 609.40, 1042.00 Q 599.20, 1041.92, 589.01, 1041.08 Q\
            578.84, 1041.05, 568.67, 1040.70 Q 558.50, 1039.86, 548.33, 1039.74 Q 538.17, 1040.61, 528.00, 1040.96 Q 517.83, 1040.92,\
            507.67, 1039.40 Q 497.50, 1039.24, 487.33, 1040.35 Q 477.17, 1041.19, 467.00, 1040.41 Q 456.83, 1041.18, 446.67, 1041.66 Q\
            436.50, 1041.34, 426.33, 1041.51 Q 416.17, 1041.48, 406.00, 1040.40 Q 395.83, 1040.59, 385.67, 1041.97 Q 375.50, 1040.79,\
            365.33, 1041.12 Q 355.17, 1041.53, 345.00, 1041.13 Q 334.83, 1041.45, 324.67, 1041.49 Q 314.50, 1041.52, 304.33, 1041.58 Q\
            294.17, 1041.40, 284.00, 1040.61 Q 273.83, 1040.70, 263.67, 1041.11 Q 253.50, 1041.21, 243.33, 1040.66 Q 233.17, 1040.33,\
            223.00, 1040.70 Q 212.83, 1040.90, 202.67, 1041.28 Q 192.50, 1041.43, 182.33, 1040.63 Q 172.17, 1040.60, 162.00, 1040.68 Q\
            151.83, 1040.67, 141.67, 1040.93 Q 131.50, 1041.00, 121.33, 1041.21 Q 111.17, 1041.41, 101.00, 1040.75 Q 90.83, 1041.01, 80.67,\
            1041.09 Q 70.50, 1040.99, 60.33, 1040.41 Q 50.17, 1040.06, 40.03, 1039.97 Q 40.01, 1029.91, 40.29, 1019.78 Q 40.14, 1009.73,\
            40.54, 999.63 Q 40.09, 989.56, 39.71, 979.47 Q 39.79, 969.38, 40.26, 959.29 Q 39.55, 949.21, 39.44, 939.12 Q 40.26, 929.03,\
            40.27, 918.94 Q 40.54, 908.85, 39.60, 898.77 Q 38.65, 888.68, 39.70, 878.59 Q 39.78, 868.50, 39.57, 858.41 Q 39.15, 848.32,\
            39.15, 838.24 Q 39.03, 828.15, 39.58, 818.06 Q 38.68, 807.97, 38.85, 797.88 Q 39.27, 787.79, 39.97, 777.71 Q 40.07, 767.62,\
            39.57, 757.53 Q 39.66, 747.44, 40.06, 737.35 Q 40.23, 727.27, 40.23, 717.18 Q 39.56, 707.09, 39.95, 697.00 Q 40.43, 686.91,\
            40.56, 676.82 Q 40.35, 666.74, 40.06, 656.65 Q 39.83, 646.56, 39.81, 636.47 Q 39.93, 626.38, 39.49, 616.29 Q 40.35, 606.21,\
            40.66, 596.12 Q 40.30, 586.03, 39.62, 575.94 Q 39.44, 565.85, 39.38, 555.76 Q 39.86, 545.68, 39.36, 535.59 Q 39.21, 525.50,\
            38.38, 515.41 Q 39.30, 505.32, 39.47, 495.24 Q 38.97, 485.15, 38.76, 475.06 Q 38.16, 464.97, 39.47, 454.88 Q 39.30, 444.79,\
            39.80, 434.71 Q 39.67, 424.62, 40.19, 414.53 Q 40.39, 404.44, 39.79, 394.35 Q 39.82, 384.26, 40.50, 374.18 Q 40.96, 364.09,\
            39.75, 354.00 Q 39.82, 343.91, 39.90, 333.82 Q 40.37, 323.74, 40.23, 313.65 Q 40.13, 303.56, 40.48, 293.47 Q 40.52, 283.38,\
            41.09, 273.29 Q 40.67, 263.21, 40.86, 253.12 Q 41.18, 243.03, 40.33, 232.94 Q 39.71, 222.85, 39.47, 212.76 Q 40.04, 202.68,\
            39.95, 192.59 Q 41.02, 182.50, 39.89, 172.41 Q 40.43, 162.32, 39.18, 152.24 Q 40.08, 142.15, 40.11, 132.06 Q 39.59, 121.97,\
            39.40, 111.88 Q 39.15, 101.79, 40.33, 91.71 Q 39.82, 81.62, 40.11, 71.53 Q 40.15, 61.44, 40.75, 51.35 Q 39.35, 41.26, 39.95,\
            31.18 Q 40.00, 21.09, 40.00, 11.00" style=" fill:white;"/>\
         </svg:svg>\
      </div>\
   </div>\
</div>');