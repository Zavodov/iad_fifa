rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__492670737-layer" class="layer" name="__containerId__pageLayer" data-layer-id="492670737" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-492670737-layer-1339902317" style="position: absolute; left: -1px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="1339902317" data-review-reference-id="1339902317">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024" viewBox="0 0 601 1024">\
                     <svg:g width="601" height="1024">\
                        <svg:rect x="0" y="0" width="601" height="1024" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="601" y2="1024" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="1024" x2="601" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-1488392406" style="position: absolute; left: 0px; top: 240px; width: 600px; height: 80px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1488392406" data-review-reference-id="1488392406">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div id="1488392406-34549728" style="position: absolute; left: 0px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="34549728" data-review-reference-id="34549728">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1488392406-34549728_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1488392406-34549728div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1488392406-34549728\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1488392406-34549728\', \'result\');">\
                           				RFPL\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1488392406-1063968397" style="position: absolute; left: 100px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1063968397" data-review-reference-id="1063968397">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1488392406-1063968397_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1488392406-1063968397div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1488392406-1063968397\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1488392406-1063968397\', \'result\');">\
                           				Premier League\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1488392406-555069017" style="position: absolute; left: 200px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="555069017" data-review-reference-id="555069017">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1488392406-555069017_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1488392406-555069017div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1488392406-555069017\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1488392406-555069017\', \'result\');">\
                           				BundesLiga\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1488392406-228247813" style="position: absolute; left: 400px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="228247813" data-review-reference-id="228247813">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1488392406-228247813_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1488392406-228247813div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1488392406-228247813\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1488392406-228247813\', \'result\');">\
                           				Ligue 1\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1488392406-1638266604" style="position: absolute; left: 300px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1638266604" data-review-reference-id="1638266604">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1488392406-1638266604_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1488392406-1638266604div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1488392406-1638266604\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1488392406-1638266604\', \'result\');">\
                           				Ligue BBVA\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1488392406-232978909" style="position: absolute; left: 500px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="232978909" data-review-reference-id="232978909">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1488392406-232978909_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1488392406-232978909div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1488392406-232978909\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1488392406-232978909\', \'result\');">\
                           				Serie A\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-1750759685" style="position: absolute; left: 0px; top: 320px; width: 600px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1750759685" data-review-reference-id="1750759685">\
            <div class="stencil-wrapper" style="width: 600px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px; width:600px;" width="600" height="465" viewBox="0 0 600 465">\
                     <svg:g width="600" height="465">\
                        <svg:rect x="0" y="0" width="600" height="465" style="stroke-width:1;stroke:black;fill:#a7a77c;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-64528672" style="position: absolute; left: 1px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="64528672" data-review-reference-id="64528672">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:599px;" width="599" height="80" viewBox="0 0 599 80">\
                     <svg:g width="599" height="80">\
                        <svg:rect x="0" y="0" width="599" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-1938931723" style="position: absolute; left: 0px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1938931723" data-review-reference-id="1938931723">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:600px;" width="600" height="80" viewBox="0 0 600 80">\
                     <svg:g width="600" height="80">\
                        <svg:rect x="0" y="0" width="600" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-984392688" style="position: absolute; left: 150px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="984392688" data-review-reference-id="984392688">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60" viewBox="0 0 200 60">\
                     <svg:g width="200" height="60">\
                        <svg:rect x="0" y="0" width="200" height="60" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="200" y2="60" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="60" x2="200" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-766132793" style="position: absolute; left: 408px; top: 20px; width: 172px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="766132793" data-review-reference-id="766132793">\
            <div class="stencil-wrapper" style="width: 172px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Изменения</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-1198888294" style="position: absolute; left: 95px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1198888294" data-review-reference-id="1198888294">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-600342421" style="position: absolute; left: 0px; top: 355px; width: 600px; height: 400px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="600342421" data-review-reference-id="600342421">\
            <div class="stencil-wrapper" style="width: 600px; height: 400px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-492670737-layer-600342421-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 400px;width:600px;" width="600" height="400">\
                     <svg:g id="__containerId__-492670737-layer-600342421svg" width="600" height="400"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 0.26, 22.55, 0.28 Q 32.83, 1.35, 43.10, 0.75 Q 53.38, 0.75,\
                        63.66, 1.52 Q 73.93, 0.86, 84.21, 1.18 Q 94.48, 0.86, 104.76, 0.59 Q 115.03, 1.49, 125.31, 1.31 Q 135.59, 0.78, 145.86, 1.29\
                        Q 156.14, 2.09, 166.41, 2.50 Q 176.69, 1.63, 186.97, 1.31 Q 197.24, 1.15, 207.52, 1.10 Q 217.79, 0.53, 228.07, 2.41 Q 238.34,\
                        1.77, 248.62, 1.91 Q 258.90, 1.02, 269.17, 1.44 Q 279.45, 1.57, 289.72, 1.58 Q 300.00, 1.24, 310.28, 1.54 Q 320.55, 1.50,\
                        330.83, 1.49 Q 341.10, 1.93, 351.38, 1.27 Q 361.66, 1.17, 371.93, 1.31 Q 382.21, 1.51, 392.48, 1.04 Q 402.76, 1.26, 413.03,\
                        0.81 Q 423.31, -0.28, 433.59, -0.19 Q 443.86, 0.20, 454.14, 2.67 Q 464.41, 2.30, 474.69, 2.28 Q 484.97, 2.06, 495.24, 2.38\
                        Q 505.52, 1.98, 515.79, 0.58 Q 526.07, 0.69, 536.34, 1.34 Q 546.62, 1.45, 556.90, 1.82 Q 567.17, 1.78, 577.45, 1.39 Q 587.72,\
                        1.31, 598.42, 1.58 Q 598.82, 12.15, 598.33, 22.80 Q 598.16, 33.25, 598.52, 43.67 Q 598.04, 54.10, 598.57, 64.52 Q 598.60,\
                        74.95, 597.35, 85.37 Q 597.95, 95.79, 598.62, 106.21 Q 598.52, 116.63, 598.71, 127.05 Q 599.08, 137.47, 598.73, 147.89 Q 599.41,\
                        158.32, 598.59, 168.74 Q 598.80, 179.16, 599.32, 189.58 Q 599.10, 200.00, 598.62, 210.42 Q 598.14, 220.84, 598.84, 231.26\
                        Q 598.96, 241.68, 599.54, 252.11 Q 599.97, 262.53, 599.66, 272.95 Q 599.75, 283.37, 599.91, 293.79 Q 599.62, 304.21, 598.34,\
                        314.63 Q 598.72, 325.05, 598.94, 335.47 Q 598.24, 345.89, 597.65, 356.32 Q 598.03, 366.74, 598.51, 377.16 Q 598.57, 387.58,\
                        597.92, 397.92 Q 587.56, 397.50, 577.34, 397.27 Q 567.15, 397.74, 556.91, 398.32 Q 546.62, 398.13, 536.35, 398.39 Q 526.07,\
                        398.06, 515.79, 398.24 Q 505.52, 398.88, 495.24, 398.63 Q 484.97, 399.47, 474.69, 398.14 Q 464.41, 398.72, 454.14, 398.52\
                        Q 443.86, 398.76, 433.59, 398.82 Q 423.31, 399.06, 413.03, 399.28 Q 402.76, 399.49, 392.48, 399.30 Q 382.21, 398.04, 371.93,\
                        398.69 Q 361.66, 398.04, 351.38, 397.96 Q 341.10, 397.88, 330.83, 398.40 Q 320.55, 399.15, 310.28, 399.47 Q 300.00, 398.67,\
                        289.72, 398.34 Q 279.45, 398.56, 269.17, 399.07 Q 258.90, 399.45, 248.62, 399.07 Q 238.34, 398.55, 228.07, 397.68 Q 217.79,\
                        397.81, 207.52, 397.80 Q 197.24, 397.24, 186.97, 398.01 Q 176.69, 397.84, 166.41, 398.92 Q 156.14, 399.99, 145.86, 399.61\
                        Q 135.59, 400.17, 125.31, 399.87 Q 115.03, 398.47, 104.76, 398.37 Q 94.48, 399.01, 84.21, 399.61 Q 73.93, 399.79, 63.66, 399.60\
                        Q 53.38, 399.07, 43.10, 399.74 Q 32.83, 399.30, 22.55, 398.59 Q 12.28, 398.22, 1.87, 398.13 Q 1.70, 387.68, 1.44, 377.24 Q\
                        1.15, 366.79, 1.36, 356.34 Q 1.33, 345.91, 1.09, 335.48 Q 1.43, 325.05, 1.70, 314.63 Q 1.24, 304.21, 1.08, 293.79 Q 1.38,\
                        283.37, 0.84, 272.95 Q 1.71, 262.53, 1.33, 252.11 Q 0.94, 241.68, 0.84, 231.26 Q 0.54, 220.84, 0.40, 210.42 Q 0.11, 200.00,\
                        0.08, 189.58 Q 0.70, 179.16, 1.05, 168.74 Q 0.86, 158.32, 0.47, 147.89 Q 0.76, 137.47, 1.40, 127.05 Q 2.10, 116.63, 2.00,\
                        106.21 Q 2.72, 95.79, 2.17, 85.37 Q 1.58, 74.95, 1.52, 64.53 Q 1.30, 54.11, 1.44, 43.68 Q 1.31, 33.26, 1.32, 22.84 Q 2.00,\
                        12.42, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 596px; height:396px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-492670737-layer-600342421-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-492670737-layer-600342421-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-492670737-layer-600342421-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-492670737-layer-600342421-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-492670737-layer-600342421-accordion", "596", "396", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-492670737-layer-1575802654" style="position: absolute; left: 0px; top: 385px; width: 600px; height: 280px" data-interactive-element-type="static.iphoneListBackground" class="iphoneListBackground stencil mobile-interaction-potential-trigger " data-stencil-id="1575802654" data-review-reference-id="1575802654">\
            <div class="stencil-wrapper" style="width: 600px; height: 280px">\
               <div title="" style="height: 280px; width:600px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="600" height="280" viewBox="0 0 600 280">\
                     <svg:rect x="1" y="1" width="598" rx="10" height="278" style="stroke-width:1;fill:white;;stroke:black;"></svg:rect>\
                     <svg:line x1="1" y1="40" x2="598" y2="40" style="stroke-width:1;stroke:black;"></svg:line>\
                     <svg:line x1="1" y1="80" x2="598" y2="80" style="stroke-width:1;stroke:black;"></svg:line>\
                     <svg:line x1="1" y1="120" x2="598" y2="120" style="stroke-width:1;stroke:black;"></svg:line>\
                     <svg:line x1="1" y1="160" x2="598" y2="160" style="stroke-width:1;stroke:black;"></svg:line>\
                     <svg:line x1="1" y1="200" x2="598" y2="200" style="stroke-width:1;stroke:black;"></svg:line>\
                     <svg:line x1="1" y1="240" x2="598" y2="240" style="stroke-width:1;stroke:black;"></svg:line>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-492670737-layer-771123316" style="position: absolute; left: 0px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="771123316" data-review-reference-id="771123316">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:86px;height:80px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">=<br /></button></div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="492670737"] .border-wrapper, body[data-current-page-id="492670737"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="492670737"] .border-wrapper, body.has-frame[data-current-page-id="492670737"] .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="492670737"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="492670737"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "492670737",\
      			"name": "changes",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
</div>');