rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__1497021823-layer" class="layer" name="__containerId__pageLayer" data-layer-id="1497021823" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-1497021823-layer-1971788974" style="position: absolute; left: -1px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="1971788974" data-review-reference-id="1971788974">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024" viewBox="0 0 601 1024">\
                     <svg:g width="601" height="1024">\
                        <svg:rect x="0" y="0" width="601" height="1024" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="601" y2="1024" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="1024" x2="601" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1942985619" style="position: absolute; left: 0px; top: 240px; width: 600px; height: 80px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1942985619" data-review-reference-id="1942985619">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div id="1942985619-1853931572" style="position: absolute; left: 0px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1853931572" data-review-reference-id="1853931572">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-1853931572_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-1853931572div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-1853931572\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-1853931572\', \'result\');">\
                           				RFPL\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-165592102" style="position: absolute; left: 100px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="165592102" data-review-reference-id="165592102">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-165592102_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-165592102div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-165592102\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-165592102\', \'result\');">\
                           				Premier League\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-468989596" style="position: absolute; left: 200px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="468989596" data-review-reference-id="468989596">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-468989596_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-468989596div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-468989596\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-468989596\', \'result\');">\
                           				BundesLiga\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-266350341" style="position: absolute; left: 400px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="266350341" data-review-reference-id="266350341">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-266350341_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-266350341div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-266350341\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-266350341\', \'result\');">\
                           				Ligue 1\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-2065274472" style="position: absolute; left: 300px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="2065274472" data-review-reference-id="2065274472">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-2065274472_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-2065274472div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-2065274472\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-2065274472\', \'result\');">\
                           				Ligue BBVA\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-1052746445" style="position: absolute; left: 500px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1052746445" data-review-reference-id="1052746445">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1942985619-1052746445_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1942985619-1052746445div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1942985619-1052746445\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1942985619-1052746445\', \'result\');">\
                           				Serie A\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1506471845" style="position: absolute; left: 0px; top: 320px; width: 600px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1506471845" data-review-reference-id="1506471845">\
            <div class="stencil-wrapper" style="width: 600px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px; width:600px;" width="600" height="465" viewBox="0 0 600 465">\
                     <svg:g width="600" height="465">\
                        <svg:rect x="0" y="0" width="600" height="465" style="stroke-width:1;stroke:black;fill:#a7a77c;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1561898673" style="position: absolute; left: 1px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1561898673" data-review-reference-id="1561898673">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:599px;" width="599" height="80" viewBox="0 0 599 80">\
                     <svg:g width="599" height="80">\
                        <svg:rect x="0" y="0" width="599" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1877215721" style="position: absolute; left: 0px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1877215721" data-review-reference-id="1877215721">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:600px;" width="600" height="80" viewBox="0 0 600 80">\
                     <svg:g width="600" height="80">\
                        <svg:rect x="0" y="0" width="600" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-648635862" style="position: absolute; left: 150px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="648635862" data-review-reference-id="648635862">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60" viewBox="0 0 200 60">\
                     <svg:g width="200" height="60">\
                        <svg:rect x="0" y="0" width="200" height="60" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="200" y2="60" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="60" x2="200" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1061433121" style="position: absolute; left: 95px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1061433121" data-review-reference-id="1061433121">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1795695749" style="position: absolute; left: 0px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="1795695749" data-review-reference-id="1795695749">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:86px;height:80px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">=<br /></button></div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-538977365" style="position: absolute; left: 417px; top: 20px; width: 174px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="538977365" data-review-reference-id="538977365">\
            <div class="stencil-wrapper" style="width: 174px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Статистика</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-656263660" style="position: absolute; left: 0px; top: 355px; width: 600px; height: 395px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="656263660" data-review-reference-id="656263660">\
            <div class="stencil-wrapper" style="width: 600px; height: 395px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-1497021823-layer-656263660-accordion" title="">\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; width: 600px; height:395px; font-size: 1em; line-height: 1.2em;border: 2px solid black; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="overflow:auto;border:1px solid black;background-color:white;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="overflow:auto;border:1px solid black;background-color:white;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="overflow:auto;border:1px solid black;background-color:white;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="overflow:auto;border:1px solid black;background-color:white;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-1497021823-layer-656263660-accordion", "600", "395", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-1497021823-layer-200451967" style="position: absolute; left: 5px; top: 390px; width: 595px; height: 265px" data-interactive-element-type="static.chart" class="chart stencil mobile-interaction-potential-trigger " data-stencil-id="200451967" data-review-reference-id="200451967">\
            <div class="stencil-wrapper" style="width: 595px; height: 265px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 265px;width:595px;" viewBox="0 0 595 265" width="595" height="265">\
                     <svg:g width="595" height="265">\
                        <svg:g xmlns:xi="http://www.w3.org/2001/XInclude" transform="scale(3.3055555555555554, 1.7666666666666666)">\
                           <svg:rect x="0" y="0" width="180" height="150" style="fill:white;stroke:none;"></svg:rect>\
                           <svg:g name="line" style="fill:none;stroke:black;stroke-width:1px;">\
                              <svg:path d="M 9,3 L 9,140 L 168,140 L 176,140"></svg:path>\
                              <svg:path d="M 167,135 L 177,140 L 167,145"></svg:path>\
                              <svg:path d="M 4,13 L 9,3 L 14,13"></svg:path>\
                              <svg:path d="M 9,140 L 30,112 L 46,98 L 54,106 L 68,89 L 74,82 L 76,74 L 82,67 L 92,96 L 97,76 L 106,66 L 111,50 L 122,37 L 127,45 L 129,51 L 139,51 L 151,29 L 159,26 L 161,16 L 166,12"></svg:path>\
                              <svg:path d="M 10,140 L 18,107 L 29,91 L 34,73 L 44,80 L 51,55 L 62,52 L 74,34 L 86,51 L 99,43 L 111,59 L 121,70 L 128,98 L 139,119 L 147,103 L 154,125 L 165,136"></svg:path>\
                           </svg:g>\
                        </svg:g>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="1497021823"] .border-wrapper, body[data-current-page-id="1497021823"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="1497021823"] .border-wrapper, body.has-frame[data-current-page-id="1497021823"] .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="1497021823"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="1497021823"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "1497021823",\
      			"name": "stats",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
</div>');