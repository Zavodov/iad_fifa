rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page47785501-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page47785501" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page47785501-layer-676081495" style="position: absolute; left: 274px; top: 0px; width: 1092px; height: 768px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="676081495" data-review-reference-id="676081495">\
            <div class="stencil-wrapper" style="width: 1092px; height: 768px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 768px;width:1092px;" width="1092" height="768" viewBox="0 0 1092 768">\
                     <svg:g width="1092" height="768">\
                        <svg:rect x="0" y="0" width="1092" height="768" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="1092" y2="768" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="768" x2="1092" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-1977956842" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 768px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1977956842" data-review-reference-id="1977956842">\
            <div class="stencil-wrapper" style="width: 275px; height: 768px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 768px; width:275px;" width="275" height="768" viewBox="0 0 275 768">\
                     <svg:g width="275" height="768">\
                        <svg:rect x="0" y="0" width="275" height="768" style="stroke-width:1;stroke:black;fill:#C1C1C1;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-638084121" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 160px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="638084121" data-review-reference-id="638084121">\
            <div class="stencil-wrapper" style="width: 275px; height: 160px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 160px; width:275px;" width="275" height="160" viewBox="0 0 275 160">\
                     <svg:g width="275" height="160">\
                        <svg:rect x="0" y="0" width="275" height="160" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-970830623" style="position: absolute; left: 15px; top: 35px; width: 240px; height: 87px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="970830623" data-review-reference-id="970830623">\
            <div class="stencil-wrapper" style="width: 240px; height: 87px">\
               <div id="970830623-1049911846" style="position: absolute; left: 0px; top: 0px; width: 85px; height: 85px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="1049911846" data-review-reference-id="1049911846">\
                  <div class="stencil-wrapper" style="width: 85px; height: 85px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 85px;width:85px;" width="85" height="85" viewBox="0 0 85 85">\
                           <svg:g width="85" height="85">\
                              <svg:ellipse cx="42.5" cy="42.5" rx="42.5" ry="42.5" style="stroke-width:1;stroke:black;fill:white;"></svg:ellipse>\
                           </svg:g>\
                        </svg:svg>\
                     </div>\
                  </div>\
               </div>\
               <div id="970830623-1147167391" style="position: absolute; left: 100px; top: 0px; width: 91px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1147167391" data-review-reference-id="1147167391">\
                  <div class="stencil-wrapper" style="width: 91px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Name</span></p></span></span></div>\
                  </div>\
               </div>\
               <div id="970830623-1597161878" style="position: absolute; left: 100px; top: 50px; width: 145px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1597161878" data-review-reference-id="1597161878">\
                  <div class="stencil-wrapper" style="width: 145px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Surname </p></span></span></div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-331846161" style="position: absolute; left: 276px; top: 0px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="331846161" data-review-reference-id="331846161">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:1090px;" width="1090" height="80" viewBox="0 0 1090 80">\
                     <svg:g width="1090" height="80">\
                        <svg:rect x="0" y="0" width="1090" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-1565882285" style="position: absolute; left: 355px; top: 10px; width: 230px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="1565882285" data-review-reference-id="1565882285">\
            <div class="stencil-wrapper" style="width: 230px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:230px;" width="230" height="60" viewBox="0 0 230 60">\
                     <svg:g width="230" height="60">\
                        <svg:rect x="0" y="0" width="230" height="60" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="230" y2="60" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="60" x2="230" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-1812536979" style="position: absolute; left: 0px; top: 180px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1812536979" data-review-reference-id="1812536979">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Коэффициенты<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page47785501-layer-1812536979\', \'977747619\', {"button":"left","id":"1569491427","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1726676677","options":"reloadOnly","target":"page606870902","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page47785501-layer-49371509" style="position: absolute; left: 1122px; top: 20px; width: 163px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="49371509" data-review-reference-id="49371509">\
            <div class="stencil-wrapper" style="width: 163px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Настройки</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-2035359404" style="position: absolute; left: 0px; top: 250px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="2035359404" data-review-reference-id="2035359404">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Изменения<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page47785501-layer-2035359404\', \'707029151\', {"button":"left","id":"1663322542","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"292462211","options":"reloadOnly","target":"page189476043","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page47785501-layer-1407149555" style="position: absolute; left: 0px; top: 320px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="1407149555" data-review-reference-id="1407149555">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Статистика<br /></button></div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-934316144" style="position: absolute; left: 0px; top: 718px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="934316144" data-review-reference-id="934316144">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Выход<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page47785501-layer-934316144\', \'953946000\', {"button":"left","id":"894317582","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1019853815","options":"reloadOnly","target":"page940099653","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page47785501-layer-638582979" style="position: absolute; left: 0px; top: 673px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="638582979" data-review-reference-id="638582979">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#a7a77c;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Настройки<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page47785501-layer-638582979\', \'141879811\', {"button":"left","id":"2031781733","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"685545331","options":"reloadOnly","target":"page47785501","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page47785501-layer-1773230734" style="position: absolute; left: 0px; top: 625px; width: 275px; height: 48px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1773230734" data-review-reference-id="1773230734">\
            <div class="stencil-wrapper" style="width: 275px; height: 48px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:48px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Контакты<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 48px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page47785501-layer-1773230734\', \'1453327347\', {"button":"left","id":"126705478","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"117479166","options":"reloadOnly","target":"page607113527","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page47785501-layer-1454435622" style="position: absolute; left: 355px; top: 80px; width: 925px; height: 610px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1454435622" data-review-reference-id="1454435622">\
            <div class="stencil-wrapper" style="width: 925px; height: 610px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 610px; width:925px;" width="925" height="610" viewBox="0 0 925 610">\
                     <svg:g width="925" height="610">\
                        <svg:rect x="0" y="0" width="925" height="610" style="stroke-width:1;stroke:black;fill:#a7a77c;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-1234641621" style="position: absolute; left: 645px; top: 122px; width: 104px; height: 103px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="1234641621" data-review-reference-id="1234641621">\
            <div class="stencil-wrapper" style="width: 104px; height: 103px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 103px;width:104px;" width="104" height="103" viewBox="0 0 104 103">\
                     <svg:g width="104" height="103">\
                        <svg:ellipse cx="52" cy="51.5" rx="52" ry="51.5" style="stroke-width:1;stroke:black;fill:white;"></svg:ellipse>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-iphoneButton153020553" style="position: absolute; left: 645px; top: 245px; width: 345px; height: 40px" data-interactive-element-type="default.iphoneButton" class="iphoneButton stencil mobile-interaction-potential-trigger " data-stencil-id="iphoneButton153020553" data-review-reference-id="iphoneButton153020553">\
            <div class="stencil-wrapper" style="width: 345px; height: 40px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="" style="height: 40px;width:345px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="345" height="40" viewBox="0 0 345 40">\
                     <svg:a>\
                        <svg:path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" stroke="#666666" d="M5,39.5 l-2,-0.5 -1,-1 -1,-1.5 -0.5,-1.5 0,-30 0.5,-2 1,-1 1,-1 2,-0.5 335,0 1.5,0.5 1.5,1 1,1.5 0.5,1.5 0,30 -0.5,1.5 -1,1.5 -1.5,1 -1.5,0.5 z"></svg:path>\
                        <svg:text x="172.5" y="20" dy="0.3em" fill="#FFFFFF" style="font-size:1.8333333333333333em;stroke-width:0pt;" font-family="\'HelveticaNeue-Bold\'" text-anchor="middle" xml:space="preserve">изменить</svg:text>\
                     </svg:a>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-textinput455896375" style="position: absolute; left: 780px; top: 120px; width: 210px; height: 40px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="textinput455896375" data-review-reference-id="textinput455896375">\
            <div class="stencil-wrapper" style="width: 210px; height: 40px">\
               <div title=""><textarea id="__containerId__-page47785501-layer-textinput455896375input" rows="" cols="" style="width:208px;height:36px;padding: 0px;border-width:1px;">NAME</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-textinput946317262" style="position: absolute; left: 780px; top: 185px; width: 210px; height: 40px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="textinput946317262" data-review-reference-id="textinput946317262">\
            <div class="stencil-wrapper" style="width: 210px; height: 40px">\
               <div title=""><textarea id="__containerId__-page47785501-layer-textinput946317262input" rows="" cols="" style="width:208px;height:36px;padding: 0px;border-width:1px;">SURNAME</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-iphoneButton266164048" style="position: absolute; left: 645px; top: 610px; width: 345px; height: 50px" data-interactive-element-type="default.iphoneButton" class="iphoneButton pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="iphoneButton266164048" data-review-reference-id="iphoneButton266164048">\
            <div class="stencil-wrapper" style="width: 345px; height: 50px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="" style="height: 50px;width:345px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="345" height="50" viewBox="0 0 345 50">\
                     <svg:a>\
                        <svg:path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" stroke="#666666" d="M5,49.5 l-2,-0.5 -1,-1 -1,-1.5 -0.5,-1.5 0,-40 0.5,-2 1,-1 1,-1 2,-0.5 335,0 1.5,0.5 1.5,1 1,1.5 0.5,1.5 0,40 -0.5,1.5 -1,1.5 -1.5,1 -1.5,0.5 z"></svg:path>\
                        <svg:text x="172.5" y="25" dy="0.3em" fill="#FFFFFF" style="font-size:1.8333333333333333em;stroke-width:0pt;" font-family="\'HelveticaNeue-Bold\'" text-anchor="middle" xml:space="preserve">сохранить</svg:text>\
                     </svg:a>\
                  </svg:svg>\
               </div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 345px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page47785501-layer-iphoneButton266164048\', \'interaction463287160\', {"button":"left","id":"action124542792","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction883553270","options":"reloadOnly","target":"page47785501","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page47785501-layer-1724475453" style="position: absolute; left: 276px; top: 688px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1724475453" data-review-reference-id="1724475453">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:1090px;" width="1090" height="80" viewBox="0 0 1090 80">\
                     <svg:g width="1090" height="80">\
                        <svg:rect x="0" y="0" width="1090" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-text641633702" style="position: absolute; left: 645px; top: 340px; width: 109px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text641633702" data-review-reference-id="text641633702">\
            <div class="stencil-wrapper" style="width: 109px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Язык - </span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-combobox652919372" style="position: absolute; left: 750px; top: 345px; width: 240px; height: 30px" data-interactive-element-type="default.combobox" class="combobox stencil mobile-interaction-potential-trigger " data-stencil-id="combobox652919372" data-review-reference-id="combobox652919372">\
            <div class="stencil-wrapper" style="width: 240px; height: 30px">\
               <div title=""><select id="__containerId__-page47785501-layer-combobox652919372select" style="width:240px;height:30px;" title="">\
                     <option title="">Русский</option>\
                     <option title="">English</option></select></div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-text689288641" style="position: absolute; left: 705px; top: 425px; width: 228px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text689288641" data-review-reference-id="text689288641">\
            <div class="stencil-wrapper" style="width: 228px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Предпочтения </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-text392957783" style="position: absolute; left: 645px; top: 485px; width: 106px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text392957783" data-review-reference-id="text392957783">\
            <div class="stencil-wrapper" style="width: 106px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Клуб - </span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-text340525627" style="position: absolute; left: 645px; top: 540px; width: 102px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text340525627" data-review-reference-id="text340525627">\
            <div class="stencil-wrapper" style="width: 102px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Лига - </span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-combobox452877969" style="position: absolute; left: 750px; top: 490px; width: 240px; height: 30px" data-interactive-element-type="default.combobox" class="combobox stencil mobile-interaction-potential-trigger " data-stencil-id="combobox452877969" data-review-reference-id="combobox452877969">\
            <div class="stencil-wrapper" style="width: 240px; height: 30px">\
               <div title=""><select id="__containerId__-page47785501-layer-combobox452877969select" style="width:240px;height:30px;" title="">\
                     <option title="">First entry</option>\
                     <option title="">Second entry</option>\
                     <option title="">Third entry</option></select></div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-combobox830037471" style="position: absolute; left: 750px; top: 545px; width: 240px; height: 30px" data-interactive-element-type="default.combobox" class="combobox stencil mobile-interaction-potential-trigger " data-stencil-id="combobox830037471" data-review-reference-id="combobox830037471">\
            <div class="stencil-wrapper" style="width: 240px; height: 30px">\
               <div title=""><select id="__containerId__-page47785501-layer-combobox830037471select" style="width:240px;height:30px;" title="">\
                     <option title="">First entry</option>\
                     <option title="">Second entry</option>\
                     <option title="">Third entry</option></select></div>\
            </div>\
         </div>\
         <div id="__containerId__-page47785501-layer-824963435" style="position: absolute; left: 355px; top: 710px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="824963435" data-review-reference-id="824963435">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page47785501"] .border-wrapper, body[data-current-page-id="page47785501"] .simulation-container{\
         			width:1366px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page47785501"] .border-wrapper, body.has-frame[data-current-page-id="page47785501"]\
         .simulation-container{\
         			height:768px;\
         		}\
         		\
         		body[data-current-page-id="page47785501"] .svg-border-1366-768{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page47785501"] .border-wrapper .border-div{\
         			width:1366px;\
         			height:768px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page47785501",\
      			"name": "settings",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":1366,\
      			"height":768,\
      			"parentFolder": "",\
      			"frame": "desktop",\
      			"frameOrientation": "landscape"\
      		}\
      	\
   </div>\
</div>');