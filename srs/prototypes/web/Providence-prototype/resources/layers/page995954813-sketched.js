rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page995954813-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page995954813" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page995954813-layer-277972261" style="position: absolute; left: 274px; top: 0px; width: 1092px; height: 768px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="277972261" data-review-reference-id="277972261">\
            <div class="stencil-wrapper" style="width: 1092px; height: 768px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 768px;width:1092px;" width="1092" height="768">\
                     <svg:g width="1092" height="768"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.07, 2.94, 22.15, 2.27 Q 32.22, 1.89, 42.30, 1.18 Q\
                        52.37, 1.95, 62.44, 1.80 Q 72.52, 1.50, 82.59, 1.61 Q 92.67, 1.67, 102.74, 2.46 Q 112.81, 1.60, 122.89, 1.74 Q 132.96, 0.57,\
                        143.04, 0.90 Q 153.11, 0.79, 163.19, 1.16 Q 173.26, 1.06, 183.33, 1.80 Q 193.41, 2.12, 203.48, 1.84 Q 213.56, 1.93, 223.63,\
                        0.89 Q 233.70, 1.43, 243.78, 1.78 Q 253.85, 1.52, 263.93, 1.41 Q 274.00, 1.53, 284.07, 1.48 Q 294.15, 1.44, 304.22, 1.11 Q\
                        314.30, 0.79, 324.37, 1.34 Q 334.44, 1.82, 344.52, 1.40 Q 354.59, 0.70, 364.67, 2.21 Q 374.74, 3.07, 384.81, 2.26 Q 394.89,\
                        2.25, 404.96, 1.45 Q 415.04, 2.50, 425.11, 2.25 Q 435.18, 1.58, 445.26, 1.36 Q 455.33, 1.05, 465.41, 1.33 Q 475.48, 1.20,\
                        485.56, 0.61 Q 495.63, 0.62, 505.70, 0.58 Q 515.78, 0.42, 525.85, 1.39 Q 535.93, 1.69, 546.00, 1.23 Q 556.07, 1.94, 566.15,\
                        0.93 Q 576.22, 0.32, 586.30, 0.75 Q 596.37, 0.40, 606.44, 0.39 Q 616.52, 0.27, 626.59, 0.03 Q 636.67, -0.05, 646.74, -0.32\
                        Q 656.81, 0.44, 666.89, 0.45 Q 676.96, 0.51, 687.04, 0.54 Q 697.11, 0.53, 707.19, 0.17 Q 717.26, 0.95, 727.33, 1.63 Q 737.41,\
                        2.59, 747.48, 2.65 Q 757.56, 1.86, 767.63, 1.79 Q 777.70, 1.49, 787.78, 2.07 Q 797.85, 1.46, 807.93, 0.45 Q 818.00, 1.50,\
                        828.07, 1.39 Q 838.15, 1.86, 848.22, 1.04 Q 858.30, 0.22, 868.37, 0.93 Q 878.44, 1.58, 888.52, 1.30 Q 898.59, 1.36, 908.67,\
                        0.46 Q 918.74, 0.35, 928.82, -0.06 Q 938.89, 0.22, 948.96, 0.43 Q 959.04, -0.04, 969.11, -0.10 Q 979.19, -0.07, 989.26, 0.10\
                        Q 999.33, -0.08, 1009.41, 0.07 Q 1019.48, 0.72, 1029.56, 0.80 Q 1039.63, 1.30, 1049.70, 2.73 Q 1059.78, 2.51, 1069.85, 2.34\
                        Q 1079.93, 2.20, 1090.24, 1.76 Q 1089.91, 12.08, 1089.28, 22.21 Q 1089.11, 32.22, 1090.39, 42.20 Q 1091.80, 52.23, 1091.31,\
                        62.31 Q 1090.84, 72.37, 1090.58, 82.42 Q 1090.56, 92.47, 1091.00, 102.53 Q 1091.10, 112.58, 1090.90, 122.63 Q 1091.36, 132.68,\
                        1090.99, 142.74 Q 1091.41, 152.79, 1091.06, 162.84 Q 1090.96, 172.89, 1091.73, 182.95 Q 1091.81, 193.00, 1092.28, 203.05 Q\
                        1091.14, 213.11, 1091.28, 223.16 Q 1090.52, 233.21, 1090.83, 243.26 Q 1090.64, 253.32, 1090.64, 263.37 Q 1090.71, 273.42,\
                        1090.64, 283.47 Q 1090.38, 293.53, 1090.12, 303.58 Q 1090.46, 313.63, 1091.01, 323.68 Q 1090.73, 333.74, 1090.84, 343.79 Q\
                        1090.07, 353.84, 1091.39, 363.89 Q 1091.74, 373.95, 1090.57, 384.00 Q 1090.09, 394.05, 1090.44, 404.11 Q 1091.38, 414.16,\
                        1090.99, 424.21 Q 1090.73, 434.26, 1090.28, 444.32 Q 1090.96, 454.37, 1091.37, 464.42 Q 1091.03, 474.47, 1090.69, 484.53 Q\
                        1090.70, 494.58, 1090.94, 504.63 Q 1090.96, 514.68, 1091.46, 524.74 Q 1090.98, 534.79, 1090.86, 544.84 Q 1090.38, 554.89,\
                        1091.72, 564.95 Q 1091.66, 575.00, 1090.98, 585.05 Q 1090.45, 595.11, 1090.72, 605.16 Q 1091.18, 615.21, 1091.49, 625.26 Q\
                        1091.54, 635.32, 1091.21, 645.37 Q 1091.28, 655.42, 1091.03, 665.47 Q 1090.55, 675.53, 1090.64, 685.58 Q 1090.85, 695.63,\
                        1090.78, 705.68 Q 1090.98, 715.74, 1091.66, 725.79 Q 1091.65, 735.84, 1091.73, 745.89 Q 1091.70, 755.95, 1090.82, 766.82 Q\
                        1080.28, 767.04, 1069.96, 766.73 Q 1059.85, 767.08, 1049.73, 766.69 Q 1039.65, 766.95, 1029.57, 767.49 Q 1019.49, 767.59,\
                        1009.41, 767.82 Q 999.34, 767.80, 989.26, 767.66 Q 979.19, 767.53, 969.11, 766.98 Q 959.04, 767.53, 948.96, 767.31 Q 938.89,\
                        767.31, 928.82, 767.30 Q 918.74, 767.19, 908.67, 766.51 Q 898.59, 766.18, 888.52, 766.77 Q 878.44, 766.72, 868.37, 766.09\
                        Q 858.30, 766.31, 848.22, 766.10 Q 838.15, 766.44, 828.07, 767.23 Q 818.00, 767.49, 807.93, 767.54 Q 797.85, 767.19, 787.78,\
                        767.15 Q 777.70, 767.41, 767.63, 767.95 Q 757.56, 767.99, 747.48, 766.86 Q 737.41, 767.69, 727.33, 765.70 Q 717.26, 764.97,\
                        707.19, 765.21 Q 697.11, 765.70, 687.04, 766.51 Q 676.96, 767.50, 666.89, 768.06 Q 656.81, 767.85, 646.74, 767.03 Q 636.67,\
                        766.58, 626.59, 766.31 Q 616.52, 765.38, 606.44, 766.09 Q 596.37, 765.64, 586.30, 765.65 Q 576.22, 766.10, 566.15, 766.37\
                        Q 556.07, 766.45, 546.00, 766.50 Q 535.93, 766.09, 525.85, 766.70 Q 515.78, 765.17, 505.70, 766.58 Q 495.63, 766.74, 485.56,\
                        766.36 Q 475.48, 766.57, 465.41, 766.76 Q 455.33, 766.87, 445.26, 766.24 Q 435.18, 766.21, 425.11, 765.71 Q 415.04, 766.45,\
                        404.96, 767.03 Q 394.89, 767.26, 384.81, 767.12 Q 374.74, 767.67, 364.67, 767.83 Q 354.59, 767.14, 344.52, 766.66 Q 334.44,\
                        766.46, 324.37, 766.87 Q 314.30, 766.21, 304.22, 765.86 Q 294.15, 766.13, 284.07, 765.98 Q 274.00, 766.45, 263.93, 766.13\
                        Q 253.85, 766.25, 243.78, 766.28 Q 233.70, 767.36, 223.63, 767.02 Q 213.56, 767.28, 203.48, 766.51 Q 193.41, 766.76, 183.33,\
                        766.60 Q 173.26, 766.21, 163.19, 766.19 Q 153.11, 766.73, 143.04, 766.74 Q 132.96, 766.68, 122.89, 767.13 Q 112.81, 767.23,\
                        102.74, 766.91 Q 92.67, 766.91, 82.59, 766.94 Q 72.52, 766.11, 62.44, 765.60 Q 52.37, 766.66, 42.30, 767.50 Q 32.22, 767.65,\
                        22.15, 768.45 Q 12.07, 767.21, 2.02, 765.98 Q 1.77, 756.02, 2.49, 745.82 Q 2.35, 735.82, 1.58, 725.80 Q 0.66, 715.76, 0.31,\
                        705.70 Q 0.43, 695.64, 1.25, 685.58 Q 1.20, 675.53, 1.46, 665.47 Q 2.15, 655.42, 2.02, 645.37 Q 2.08, 635.32, 0.97, 625.26\
                        Q 1.35, 615.21, 0.65, 605.16 Q 1.75, 595.11, 1.62, 585.05 Q 1.93, 575.00, 2.02, 564.95 Q 1.45, 554.89, 1.86, 544.84 Q 1.37,\
                        534.79, 1.72, 524.74 Q 2.35, 514.68, 1.98, 504.63 Q 1.42, 494.58, 1.10, 484.53 Q 1.63, 474.47, 2.12, 464.42 Q 2.43, 454.37,\
                        2.42, 444.32 Q 2.62, 434.26, 2.87, 424.21 Q 1.78, 414.16, 0.93, 404.11 Q 0.37, 394.05, 0.98, 384.00 Q 1.95, 373.95, 2.54,\
                        363.89 Q 2.20, 353.84, 2.97, 343.79 Q 2.50, 333.74, 2.04, 323.68 Q 1.68, 313.63, 0.92, 303.58 Q 0.99, 293.53, 1.38, 283.47\
                        Q 2.21, 273.42, 1.83, 263.37 Q 2.90, 253.32, 2.12, 243.26 Q 1.68, 233.21, 1.32, 223.16 Q 0.85, 213.11, 1.59, 203.05 Q 1.20,\
                        193.00, 1.93, 182.95 Q 1.13, 172.89, 2.00, 162.84 Q 1.90, 152.79, 1.14, 142.74 Q 1.33, 132.68, 0.90, 122.63 Q 0.55, 112.58,\
                        1.03, 102.53 Q 1.76, 92.47, 0.67, 82.42 Q 0.93, 72.37, 0.08, 62.32 Q -0.06, 52.26, -0.21, 42.21 Q -0.56, 32.16, -0.02, 22.11\
                        Q 2.00, 12.05, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 10.85, 6.92, 19.15, 12.63 Q 27.45, 18.33, 35.79, 23.99 Q 43.79,\
                        30.12, 51.86, 36.15 Q 60.38, 41.54, 68.69, 47.23 Q 76.93, 53.03, 84.79, 59.35 Q 92.36, 66.10, 100.90, 71.46 Q 109.48, 76.78,\
                        117.80, 82.45 Q 126.46, 87.65, 134.72, 93.41 Q 142.99, 99.16, 151.20, 105.00 Q 159.07, 111.30, 167.08, 117.43 Q 174.32, 124.65,\
                        182.47, 130.57 Q 190.81, 136.21, 199.45, 141.43 Q 207.88, 146.96, 216.30, 152.49 Q 224.75, 157.98, 232.75, 164.11 Q 241.31,\
                        169.45, 249.81, 174.87 Q 258.24, 180.40, 266.49, 186.17 Q 274.82, 191.84, 283.08, 197.60 Q 291.41, 203.25, 299.49, 209.28\
                        Q 307.74, 215.06, 315.80, 221.10 Q 324.34, 226.47, 332.40, 232.52 Q 340.04, 239.16, 347.62, 245.89 Q 356.11, 251.33, 364.54,\
                        256.84 Q 372.75, 262.68, 381.16, 268.23 Q 389.26, 274.22, 398.10, 279.16 Q 406.30, 285.01, 414.04, 291.51 Q 421.82, 297.95,\
                        430.56, 303.03 Q 439.21, 308.24, 447.38, 314.13 Q 455.26, 320.43, 463.58, 326.12 Q 472.16, 331.42, 480.10, 337.64 Q 488.99,\
                        342.51, 496.98, 348.65 Q 505.58, 353.93, 513.95, 359.53 Q 521.92, 365.71, 530.44, 371.11 Q 538.79, 376.74, 547.19, 382.31\
                        Q 555.59, 387.86, 563.84, 393.65 Q 571.93, 399.65, 579.96, 405.74 Q 588.03, 411.78, 596.33, 417.49 Q 604.52, 423.35, 612.76,\
                        429.13 Q 621.01, 434.92, 628.74, 441.43 Q 637.68, 446.22, 645.72, 452.30 Q 654.22, 457.72, 662.18, 463.91 Q 670.32, 469.85,\
                        678.80, 475.30 Q 686.77, 481.46, 694.70, 487.70 Q 703.14, 493.21, 711.36, 499.03 Q 719.59, 504.83, 728.00, 510.38 Q 736.24,\
                        516.17, 744.41, 522.06 Q 752.71, 527.77, 761.19, 533.22 Q 769.73, 538.59, 776.96, 545.82 Q 785.37, 551.36, 794.40, 556.03\
                        Q 802.56, 561.93, 810.74, 567.81 Q 818.59, 574.16, 826.98, 579.74 Q 834.80, 586.13, 843.03, 591.93 Q 851.19, 597.84, 859.67,\
                        603.29 Q 868.04, 608.90, 876.14, 614.88 Q 884.20, 620.94, 892.28, 626.95 Q 900.45, 632.84, 908.87, 638.38 Q 917.18, 644.07,\
                        925.48, 649.77 Q 934.16, 654.94, 942.16, 661.08 Q 949.74, 667.80, 958.10, 673.43 Q 967.11, 678.12, 975.20, 684.12 Q 983.12,\
                        690.37, 991.13, 696.49 Q 999.56, 702.01, 1008.02, 707.50 Q 1016.29, 713.24, 1024.62, 718.91 Q 1032.49, 725.21, 1040.63, 731.15\
                        Q 1048.71, 737.18, 1057.41, 742.31 Q 1066.05, 747.53, 1074.27, 753.36 Q 1081.76, 760.21, 1090.00, 766.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 766.00 Q 10.34, 760.33, 18.27, 754.07 Q 26.51, 748.25, 34.62, 742.26\
                        Q 42.82, 736.39, 50.83, 730.25 Q 59.03, 724.39, 67.14, 718.38 Q 75.37, 712.56, 83.60, 706.74 Q 91.76, 700.80, 99.86, 694.80\
                        Q 108.17, 689.08, 116.64, 683.59 Q 124.89, 677.79, 133.24, 672.14 Q 141.54, 666.41, 149.75, 660.55 Q 157.92, 654.64, 166.15,\
                        648.81 Q 174.36, 642.95, 182.58, 637.11 Q 191.00, 631.56, 199.09, 625.54 Q 207.29, 619.67, 215.64, 614.01 Q 224.08, 608.48,\
                        232.86, 603.43 Q 240.99, 597.46, 249.35, 591.82 Q 257.29, 585.59, 265.68, 579.99 Q 273.75, 573.93, 282.05, 568.20 Q 290.22,\
                        562.29, 298.62, 556.71 Q 306.67, 550.61, 315.08, 545.06 Q 323.27, 539.17, 331.48, 533.30 Q 339.78, 527.58, 347.91, 521.61\
                        Q 356.35, 516.08, 364.64, 510.35 Q 373.15, 504.92, 381.40, 499.12 Q 389.50, 493.11, 397.66, 487.18 Q 405.86, 481.31, 414.37,\
                        475.88 Q 422.65, 470.12, 430.91, 464.33 Q 438.70, 457.89, 446.99, 452.14 Q 455.28, 446.40, 464.03, 441.31 Q 471.95, 435.04,\
                        480.36, 429.47 Q 488.89, 424.08, 497.36, 418.59 Q 505.24, 412.27, 513.16, 405.99 Q 521.98, 401.01, 529.93, 394.78 Q 537.77,\
                        388.40, 546.22, 382.89 Q 554.67, 377.38, 563.05, 371.76 Q 571.22, 365.85, 579.35, 359.87 Q 587.71, 354.23, 595.80, 348.20\
                        Q 604.38, 342.88, 612.05, 336.25 Q 620.42, 330.63, 628.95, 325.23 Q 637.37, 319.67, 645.67, 313.95 Q 653.84, 308.03, 662.14,\
                        302.31 Q 670.23, 296.27, 678.44, 290.42 Q 686.42, 284.23, 694.48, 278.17 Q 702.91, 272.63, 711.68, 267.57 Q 720.15, 262.08,\
                        728.21, 256.01 Q 736.47, 250.23, 744.82, 244.57 Q 753.04, 238.73, 761.28, 232.92 Q 769.43, 226.97, 777.87, 221.45 Q 786.33,\
                        215.95, 794.83, 210.51 Q 803.07, 204.70, 811.34, 198.92 Q 818.94, 192.21, 827.56, 186.94 Q 835.46, 180.63, 844.19, 175.52\
                        Q 852.51, 169.82, 860.73, 163.98 Q 868.91, 158.08, 876.69, 151.61 Q 884.98, 145.87, 893.19, 140.01 Q 901.39, 134.14, 909.99,\
                        128.84 Q 918.62, 123.59, 927.12, 118.14 Q 935.19, 112.09, 943.13, 105.85 Q 951.37, 100.04, 959.80, 94.50 Q 968.16, 88.86,\
                        976.71, 83.48 Q 984.73, 77.36, 993.73, 72.63 Q 1001.67, 66.39, 1009.63, 60.17 Q 1017.42, 53.71, 1025.48, 47.65 Q 1034.68,\
                        43.21, 1042.76, 37.16 Q 1050.91, 31.22, 1058.74, 24.82 Q 1066.98, 19.02, 1074.88, 12.71 Q 1083.74, 7.79, 1092.00, 2.00" style="\
                        fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1281359082" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 768px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1281359082" data-review-reference-id="1281359082">\
            <div class="stencil-wrapper" style="width: 275px; height: 768px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 768px;width:275px;" width="275" height="768">\
                     <svg:g width="275" height="768"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.42, -0.45, 22.85, -0.34 Q 33.27, -0.58, 43.69, -0.11 Q 54.12,\
                        0.67, 64.54, -0.39 Q 74.96, 0.16, 85.38, 0.51 Q 95.81, 1.60, 106.23, 1.61 Q 116.65, 0.92, 127.08, 0.63 Q 137.50, 0.97, 147.92,\
                        1.37 Q 158.35, 2.25, 168.77, 0.83 Q 179.19, 1.30, 189.62, 1.79 Q 200.04, 2.68, 210.46, 2.51 Q 220.88, 2.26, 231.31, 1.44 Q\
                        241.73, 0.86, 252.15, 2.05 Q 262.58, 2.27, 272.99, 2.01 Q 273.45, 11.90, 273.23, 22.07 Q 273.20, 32.14, 272.37, 42.23 Q 273.94,\
                        52.25, 273.31, 62.31 Q 273.01, 72.37, 272.87, 82.42 Q 272.93, 92.47, 273.89, 102.53 Q 273.03, 112.58, 273.50, 122.63 Q 273.30,\
                        132.68, 273.85, 142.74 Q 274.04, 152.79, 273.26, 162.84 Q 273.04, 172.89, 273.45, 182.95 Q 273.86, 193.00, 273.98, 203.05\
                        Q 273.82, 213.11, 272.64, 223.16 Q 272.79, 233.21, 271.89, 243.26 Q 272.46, 253.32, 272.80, 263.37 Q 273.06, 273.42, 273.56,\
                        283.47 Q 272.77, 293.53, 272.82, 303.58 Q 272.24, 313.63, 272.74, 323.68 Q 272.76, 333.74, 271.97, 343.79 Q 273.20, 353.84,\
                        274.06, 363.89 Q 273.80, 373.95, 272.88, 384.00 Q 273.71, 394.05, 273.50, 404.11 Q 273.49, 414.16, 273.48, 424.21 Q 273.10,\
                        434.26, 273.23, 444.32 Q 273.99, 454.37, 274.09, 464.42 Q 273.45, 474.47, 272.74, 484.53 Q 272.87, 494.58, 273.62, 504.63\
                        Q 274.12, 514.68, 272.74, 524.74 Q 273.28, 534.79, 273.42, 544.84 Q 273.99, 554.89, 274.08, 564.95 Q 274.07, 575.00, 274.64,\
                        585.05 Q 274.67, 595.11, 274.94, 605.16 Q 275.03, 615.21, 274.67, 625.26 Q 274.55, 635.32, 274.60, 645.37 Q 274.63, 655.42,\
                        274.85, 665.47 Q 275.27, 675.53, 275.00, 685.58 Q 274.87, 695.63, 273.59, 705.68 Q 273.53, 715.74, 274.34, 725.79 Q 273.63,\
                        735.84, 272.67, 745.89 Q 272.60, 755.95, 273.59, 766.59 Q 262.95, 767.11, 252.34, 767.30 Q 241.82, 767.40, 231.35, 767.35\
                        Q 220.91, 767.44, 210.47, 766.93 Q 200.04, 766.43, 189.62, 766.16 Q 179.19, 766.26, 168.77, 766.83 Q 158.35, 766.81, 147.92,\
                        765.98 Q 137.50, 765.52, 127.08, 766.08 Q 116.65, 765.71, 106.23, 766.04 Q 95.81, 765.81, 85.38, 766.04 Q 74.96, 766.56, 64.54,\
                        767.65 Q 54.12, 767.47, 43.69, 767.37 Q 33.27, 767.18, 22.85, 766.59 Q 12.42, 765.35, 2.23, 765.77 Q 1.59, 756.08, 1.35, 745.99\
                        Q 1.36, 735.88, 1.26, 725.81 Q 2.33, 715.73, 1.48, 705.69 Q 0.95, 695.64, 1.26, 685.58 Q 1.67, 675.53, 1.88, 665.47 Q 1.25,\
                        655.42, 1.13, 645.37 Q 1.14, 635.32, 0.78, 625.26 Q 0.90, 615.21, 1.55, 605.16 Q 1.34, 595.11, 1.77, 585.05 Q 2.14, 575.00,\
                        0.99, 564.95 Q 0.99, 554.89, 0.93, 544.84 Q 0.81, 534.79, 0.73, 524.74 Q 0.29, 514.68, 0.59, 504.63 Q 1.11, 494.58, 1.61,\
                        484.53 Q 1.26, 474.47, 0.31, 464.42 Q 0.61, 454.37, 0.71, 444.32 Q 1.12, 434.26, 1.65, 424.21 Q 2.61, 414.16, 2.03, 404.11\
                        Q 1.68, 394.05, 1.82, 384.00 Q 1.26, 373.95, 0.89, 363.89 Q 1.20, 353.84, 0.93, 343.79 Q 0.80, 333.74, 0.60, 323.68 Q 1.05,\
                        313.63, 0.31, 303.58 Q 1.17, 293.53, 1.52, 283.47 Q 2.21, 273.42, 1.42, 263.37 Q 1.39, 253.32, 1.17, 243.26 Q 1.23, 233.21,\
                        1.10, 223.16 Q 0.77, 213.11, 1.09, 203.05 Q 0.04, 193.00, 1.00, 182.95 Q 0.98, 172.89, 1.15, 162.84 Q 0.16, 152.79, -0.17,\
                        142.74 Q 0.32, 132.68, 0.17, 122.63 Q 1.24, 112.58, 0.49, 102.53 Q 0.31, 92.47, 0.26, 82.42 Q 1.43, 72.37, 1.53, 62.32 Q 2.50,\
                        52.26, 2.50, 42.21 Q 2.04, 32.16, 2.30, 22.11 Q 2.00, 12.05, 2.00, 2.00" style=" fill:#C1C1C1;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1108730242" style="position: absolute; left: 355px; top: 130px; width: 930px; height: 30px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1108730242" data-review-reference-id="1108730242">\
            <div class="stencil-wrapper" style="width: 930px; height: 30px">\
               <div id="1108730242-95509338" style="position: absolute; left: 0px; top: 0px; width: 1335px; height: 665px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="95509338" data-review-reference-id="95509338">\
                  <div class="stencil-wrapper" style="width: 1335px; height: 665px">\
                     <div id="95509338-949097759" style="position: absolute; left: 0px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="949097759" data-review-reference-id="949097759">\
                        <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                           <div title="">\
                              <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                                 <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                                    <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 6.17, 25.50, 6.13, 19.00 Q 6.31, 17.50, 6.50, 15.88 Q 7.24, 14.90,\
                                       7.18, 13.65 Q 7.79, 12.67, 8.61, 11.62 Q 9.34, 10.58, 11.18, 11.29 Q 11.84, 10.21, 12.88, 9.73 Q 14.35, 9.12, 15.90, 8.45\
                                       Q 27.17, 9.00, 38.32, 8.79 Q 49.50, 8.79, 60.65, 7.35 Q 71.83, 8.52, 83.00, 9.73 Q 94.17, 9.18, 105.33, 8.73 Q 116.50, 8.53,\
                                       127.67, 8.36 Q 138.83, 7.55, 150.12, 8.28 Q 151.79, 8.32, 153.12, 9.68 Q 154.13, 10.21, 155.32, 10.31 Q 156.07, 11.36, 157.44,\
                                       11.55 Q 158.17, 12.52, 158.57, 13.66 Q 159.27, 14.58, 160.54, 15.33 Q 160.19, 17.23, 159.69, 19.06 Q 159.23, 25.57, 159.59,\
                                       31.62 Q 148.92, 31.96, 137.87, 32.07 Q 126.82, 32.46, 115.71, 31.87 Q 104.65, 32.69, 93.57, 32.01 Q 82.50, 32.29, 71.43, 32.29\
                                       Q 60.36, 32.52, 49.29, 33.29 Q 38.21, 33.05, 27.14, 32.96 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                                    </svg:g>\
                                 </svg:g>\
                              </svg:svg>\
                              <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'949097759\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'949097759\', \'result\');" class="selected">\
                                 <div class="smallSkechtedTab">\
                                    <div id="949097759_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">RFPL\
                                       							\
                                       <addMouseOverListener></addMouseOverListener>\
                                       							\
                                       <addMouseOutListener></addMouseOutListener>\
                                       						\
                                    </div>\
                                 </div>\
                                 <div class="bigSkechtedTab">\
                                    <div id="949097759_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">RFPL\
                                       							\
                                       <addMouseOverListener></addMouseOverListener>\
                                       							\
                                       <addMouseOutListener></addMouseOutListener>\
                                       						\
                                    </div>\
                                 </div>\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1108730242-171458935" style="position: absolute; left: 155px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="171458935" data-review-reference-id="171458935">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 8.74, 25.50, 7.74, 19.00 Q 6.49, 17.50, 6.64, 15.92 Q 6.07, 14.48,\
                                 6.20, 13.22 Q 7.14, 12.37, 8.62, 11.63 Q 9.65, 11.02, 10.44, 10.07 Q 11.41, 9.43, 12.35, 8.52 Q 13.81, 7.70, 15.66, 7.19 Q\
                                 27.04, 7.65, 38.31, 8.38 Q 49.49, 8.50, 60.66, 8.75 Q 71.82, 7.43, 83.00, 8.11 Q 94.17, 8.24, 105.33, 7.81 Q 116.50, 8.80,\
                                 127.67, 7.42 Q 138.83, 7.23, 150.29, 7.19 Q 151.95, 7.66, 153.66, 8.21 Q 154.60, 9.12, 155.49, 9.94 Q 156.33, 10.83, 157.18,\
                                 11.82 Q 156.93, 13.41, 157.75, 14.15 Q 158.19, 15.17, 158.47, 16.23 Q 159.56, 17.48, 161.09, 18.80 Q 160.73, 25.43, 159.92,\
                                 31.93 Q 148.82, 31.67, 137.81, 31.68 Q 126.81, 32.31, 115.74, 32.90 Q 104.64, 31.93, 93.57, 31.44 Q 82.50, 32.20, 71.43, 32.65\
                                 Q 60.36, 32.67, 49.29, 33.03 Q 38.21, 33.31, 27.14, 33.24 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'171458935\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'171458935\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="171458935_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="171458935_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1108730242-105188535" style="position: absolute; left: 310px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="105188535" data-review-reference-id="105188535">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 5.82, 25.50, 6.39, 19.00 Q 5.89, 17.50, 6.61, 15.91 Q 5.52, 14.28,\
                                 5.93, 13.11 Q 7.00, 12.30, 8.63, 11.64 Q 9.77, 11.18, 9.94, 9.24 Q 11.03, 8.74, 12.26, 8.31 Q 13.85, 7.80, 15.63, 7.02 Q 27.06,\
                                 7.79, 38.29, 8.03 Q 49.50, 9.05, 60.67, 8.88 Q 71.82, 7.28, 82.99, 7.09 Q 94.16, 7.60, 105.33, 7.27 Q 116.50, 7.27, 127.67,\
                                 7.26 Q 138.83, 6.60, 150.37, 6.71 Q 152.08, 7.16, 153.72, 8.04 Q 154.42, 9.53, 155.00, 11.00 Q 156.09, 11.32, 157.46, 11.53\
                                 Q 158.35, 12.39, 158.38, 13.77 Q 158.60, 14.95, 159.20, 15.91 Q 160.54, 17.10, 161.44, 18.73 Q 161.27, 25.39, 160.66, 32.61\
                                 Q 149.15, 32.65, 137.97, 32.81 Q 126.86, 33.14, 115.76, 33.40 Q 104.66, 33.34, 93.58, 33.04 Q 82.51, 33.39, 71.43, 32.99 Q\
                                 60.36, 33.81, 49.29, 34.02 Q 38.21, 33.45, 27.14, 33.72 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'105188535\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'105188535\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="105188535_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="105188535_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1108730242-1827932383" style="position: absolute; left: 465px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1827932383" data-review-reference-id="1827932383">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 6.54, 25.50, 6.69, 19.00 Q 6.64, 17.50, 6.69, 15.93 Q 7.57, 15.02,\
                                 8.01, 14.00 Q 9.07, 13.27, 9.05, 12.05 Q 10.08, 11.61, 11.07, 11.11 Q 12.09, 10.66, 13.07, 10.17 Q 14.80, 10.29, 16.25, 10.35\
                                 Q 27.26, 10.08, 38.37, 9.73 Q 49.50, 9.07, 60.68, 9.76 Q 71.84, 10.18, 83.00, 9.29 Q 94.17, 9.15, 105.33, 9.49 Q 116.50, 10.42,\
                                 127.67, 9.42 Q 138.83, 9.34, 149.74, 10.59 Q 151.26, 10.48, 152.64, 10.99 Q 153.72, 11.15, 154.79, 11.44 Q 155.74, 12.05,\
                                 156.49, 12.52 Q 157.49, 13.01, 157.52, 14.29 Q 158.38, 15.07, 158.78, 16.10 Q 159.33, 17.57, 160.12, 18.98 Q 160.97, 25.41,\
                                 160.63, 32.59 Q 149.08, 32.45, 137.95, 32.63 Q 126.78, 31.92, 115.73, 32.44 Q 104.64, 31.53, 93.57, 31.44 Q 82.50, 32.37,\
                                 71.43, 32.98 Q 60.36, 32.59, 49.29, 32.93 Q 38.21, 31.68, 27.14, 31.46 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1827932383\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1827932383\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1827932383_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1827932383_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1108730242-1559693441" style="position: absolute; left: 620px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1559693441" data-review-reference-id="1559693441">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 6.01, 25.50, 5.92, 19.00 Q 5.88, 17.50, 5.95, 15.75 Q 7.15, 14.87,\
                                 7.70, 13.87 Q 7.53, 12.55, 8.28, 11.30 Q 9.78, 11.20, 11.01, 11.02 Q 11.81, 10.16, 13.07, 10.16 Q 14.57, 9.68, 15.82, 8.02\
                                 Q 27.10, 8.27, 38.30, 8.21 Q 49.49, 8.38, 60.67, 9.05 Q 71.84, 9.89, 83.00, 9.71 Q 94.17, 8.85, 105.33, 9.39 Q 116.50, 10.33,\
                                 127.67, 10.38 Q 138.83, 10.90, 149.77, 10.41 Q 151.54, 9.35, 153.16, 9.55 Q 154.04, 10.41, 155.18, 10.62 Q 156.16, 11.16,\
                                 157.48, 11.52 Q 157.56, 12.96, 158.84, 13.49 Q 158.94, 14.76, 159.14, 15.94 Q 159.23, 17.60, 159.81, 19.04 Q 159.82, 25.52,\
                                 159.88, 31.89 Q 149.04, 32.34, 138.00, 33.00 Q 126.84, 32.82, 115.72, 32.20 Q 104.64, 31.95, 93.57, 31.77 Q 82.50, 31.36,\
                                 71.43, 32.10 Q 60.36, 31.33, 49.29, 31.10 Q 38.21, 31.28, 27.14, 31.03 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1559693441\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1559693441\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1559693441_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1559693441_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1108730242-1199591457" style="position: absolute; left: 775px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1199591457" data-review-reference-id="1199591457">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 6.01, 25.50, 5.85, 19.00 Q 5.77, 17.50, 5.67, 15.69 Q 6.61, 14.68,\
                                 7.10, 13.61 Q 7.43, 12.50, 8.14, 11.16 Q 9.19, 10.38, 10.59, 10.33 Q 11.76, 10.07, 12.72, 9.37 Q 14.27, 8.90, 15.85, 8.21\
                                 Q 27.11, 8.40, 38.29, 8.11 Q 49.48, 8.02, 60.65, 7.82 Q 71.83, 8.33, 83.00, 8.42 Q 94.17, 8.90, 105.33, 9.23 Q 116.50, 9.15,\
                                 127.67, 8.92 Q 138.83, 8.56, 150.00, 9.00 Q 151.57, 9.23, 153.06, 9.84 Q 154.05, 10.39, 154.90, 11.22 Q 155.69, 12.14, 157.01,\
                                 11.99 Q 157.08, 13.30, 157.97, 14.02 Q 158.52, 14.99, 159.83, 15.64 Q 160.52, 17.11, 160.60, 18.89 Q 160.74, 25.43, 160.82,\
                                 32.76 Q 149.30, 33.09, 137.97, 32.80 Q 126.84, 32.83, 115.73, 32.58 Q 104.66, 32.90, 93.58, 32.91 Q 82.50, 32.45, 71.43, 32.73\
                                 Q 60.36, 32.24, 49.29, 32.29 Q 38.21, 32.38, 27.14, 32.33 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1199591457\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1199591457\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1199591457_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1199591457_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1276864689" style="position: absolute; left: 275px; top: 160px; width: 1091px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1276864689" data-review-reference-id="1276864689">\
            <div class="stencil-wrapper" style="width: 1091px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px;width:1091px;" width="1091" height="465">\
                     <svg:g width="1091" height="465"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.06, -0.57, 22.13, -0.53 Q 32.19, -0.23, 42.26, 0.32 Q 52.32,\
                        0.05, 62.39, 0.23 Q 72.45, -0.02, 82.52, 0.98 Q 92.58, 0.46, 102.65, 0.92 Q 112.71, 1.31, 122.78, 1.12 Q 132.84, 0.50, 142.91,\
                        0.50 Q 152.97, 1.08, 163.04, 1.36 Q 173.10, 0.91, 183.17, 0.32 Q 193.23, 0.12, 203.30, 0.95 Q 213.36, 1.03, 223.43, 0.86 Q\
                        233.49, 0.71, 243.56, 0.57 Q 253.62, 1.02, 263.69, 0.74 Q 273.75, 0.80, 283.81, 0.62 Q 293.88, 0.43, 303.94, 0.27 Q 314.01,\
                        0.83, 324.07, 0.88 Q 334.14, 0.60, 344.20, 0.51 Q 354.27, -0.18, 364.33, 0.07 Q 374.40, 0.66, 384.46, 0.06 Q 394.53, 0.56,\
                        404.59, 1.21 Q 414.66, 1.84, 424.72, 1.97 Q 434.79, 1.37, 444.85, 1.17 Q 454.92, 1.17, 464.98, 1.14 Q 475.05, 0.97, 485.11,\
                        1.17 Q 495.18, 1.26, 505.24, 2.26 Q 515.31, 2.16, 525.37, 1.56 Q 535.44, 1.43, 545.50, 1.41 Q 555.56, 1.15, 565.63, 1.13 Q\
                        575.69, 1.14, 585.76, 1.39 Q 595.82, 1.22, 605.89, 1.31 Q 615.95, 1.33, 626.02, 2.00 Q 636.08, 0.84, 646.15, 1.64 Q 656.21,\
                        1.32, 666.28, 1.13 Q 676.34, 0.77, 686.41, 0.78 Q 696.47, 1.76, 706.54, 0.39 Q 716.60, 0.32, 726.67, 0.33 Q 736.73, 0.68,\
                        746.80, 1.01 Q 756.86, 0.45, 766.93, -0.05 Q 776.99, 0.76, 787.06, 1.11 Q 797.12, 1.31, 807.19, 1.58 Q 817.25, 1.06, 827.32,\
                        1.52 Q 837.38, 1.46, 847.44, 1.53 Q 857.51, 1.34, 867.57, 0.73 Q 877.64, 1.52, 887.70, 0.91 Q 897.77, 0.98, 907.83, 0.89 Q\
                        917.90, 1.09, 927.96, 1.18 Q 938.03, 0.97, 948.09, 0.78 Q 958.16, 0.93, 968.22, 1.08 Q 978.29, 0.78, 988.35, 1.11 Q 998.42,\
                        0.95, 1008.48, 2.51 Q 1018.55, 2.33, 1028.61, 2.67 Q 1038.68, 1.79, 1048.74, 1.75 Q 1058.81, 2.15, 1068.87, 1.49 Q 1078.94,\
                        2.45, 1089.06, 1.94 Q 1088.85, 12.07, 1089.38, 21.99 Q 1089.59, 32.03, 1089.29, 42.08 Q 1089.72, 52.10, 1089.90, 62.12 Q 1090.17,\
                        72.15, 1089.39, 82.17 Q 1089.38, 92.20, 1088.83, 102.22 Q 1089.25, 112.24, 1088.78, 122.26 Q 1088.89, 132.28, 1088.71, 142.30\
                        Q 1087.82, 152.33, 1088.19, 162.35 Q 1087.92, 172.37, 1089.60, 182.39 Q 1088.27, 192.41, 1087.76, 202.43 Q 1088.04, 212.46,\
                        1088.36, 222.48 Q 1088.78, 232.50, 1089.12, 242.52 Q 1089.69, 252.54, 1089.57, 262.57 Q 1090.06, 272.59, 1089.03, 282.61 Q\
                        1087.75, 292.63, 1087.52, 302.65 Q 1088.32, 312.67, 1089.88, 322.70 Q 1088.76, 332.72, 1090.12, 342.74 Q 1090.27, 352.76,\
                        1089.93, 362.78 Q 1089.90, 372.80, 1089.65, 382.83 Q 1089.87, 392.85, 1090.57, 402.87 Q 1090.49, 412.89, 1090.65, 422.91 Q\
                        1090.81, 432.93, 1090.37, 442.96 Q 1089.61, 452.98, 1089.11, 463.11 Q 1079.10, 463.49, 1068.89, 463.13 Q 1058.83, 463.37,\
                        1048.73, 462.71 Q 1038.68, 463.45, 1028.61, 462.96 Q 1018.55, 463.20, 1008.48, 463.61 Q 998.42, 463.29, 988.35, 463.58 Q 978.29,\
                        463.74, 968.22, 463.84 Q 958.16, 462.83, 948.09, 463.08 Q 938.03, 463.17, 927.96, 463.50 Q 917.90, 463.99, 907.83, 464.39\
                        Q 897.77, 463.48, 887.70, 462.93 Q 877.64, 463.55, 867.57, 463.95 Q 857.51, 463.82, 847.44, 463.83 Q 837.38, 464.03, 827.32,\
                        464.10 Q 817.25, 463.96, 807.19, 463.80 Q 797.12, 463.70, 787.06, 463.37 Q 776.99, 463.41, 766.93, 463.75 Q 756.86, 463.83,\
                        746.80, 464.08 Q 736.73, 463.85, 726.67, 463.50 Q 716.60, 463.23, 706.54, 463.17 Q 696.47, 463.65, 686.41, 463.93 Q 676.34,\
                        463.15, 666.28, 462.90 Q 656.21, 463.75, 646.15, 463.58 Q 636.08, 463.32, 626.02, 462.70 Q 615.95, 463.05, 605.89, 463.52\
                        Q 595.82, 463.27, 585.76, 462.65 Q 575.69, 462.25, 565.63, 462.64 Q 555.56, 463.37, 545.50, 462.89 Q 535.44, 462.62, 525.37,\
                        462.90 Q 515.31, 463.90, 505.24, 463.99 Q 495.18, 462.96, 485.11, 461.92 Q 475.05, 463.99, 464.98, 462.70 Q 454.92, 463.26,\
                        444.85, 462.73 Q 434.79, 463.46, 424.72, 463.85 Q 414.66, 463.37, 404.59, 463.86 Q 394.53, 463.46, 384.46, 462.88 Q 374.40,\
                        463.24, 364.33, 463.68 Q 354.27, 463.54, 344.20, 464.37 Q 334.14, 464.41, 324.07, 464.79 Q 314.01, 463.86, 303.94, 463.54\
                        Q 293.88, 463.84, 283.81, 462.70 Q 273.75, 463.08, 263.69, 462.63 Q 253.62, 463.14, 243.56, 463.40 Q 233.49, 463.03, 223.43,\
                        462.67 Q 213.36, 462.70, 203.30, 462.06 Q 193.23, 462.35, 183.17, 462.16 Q 173.10, 462.38, 163.04, 462.45 Q 152.97, 463.41,\
                        142.91, 463.48 Q 132.84, 463.29, 122.78, 462.43 Q 112.71, 463.43, 102.65, 464.19 Q 92.58, 464.14, 82.52, 463.18 Q 72.45, 463.71,\
                        62.39, 462.89 Q 52.32, 462.98, 42.26, 463.14 Q 32.19, 463.68, 22.13, 463.53 Q 12.06, 463.74, 1.41, 463.59 Q 1.72, 453.07,\
                        0.51, 443.17 Q -0.37, 433.09, -0.10, 422.98 Q -0.04, 412.92, 0.45, 402.88 Q 0.38, 392.85, -0.07, 382.83 Q 0.64, 372.81, 1.38,\
                        362.78 Q 0.62, 352.76, 2.10, 342.74 Q 2.91, 332.72, 3.07, 322.70 Q 3.64, 312.67, 2.95, 302.65 Q 3.01, 292.63, 1.21, 282.61\
                        Q 2.77, 272.59, 2.67, 262.57 Q 1.58, 252.54, 0.63, 242.52 Q 0.34, 232.50, 0.39, 222.48 Q 1.16, 212.46, 1.56, 202.43 Q 1.17,\
                        192.41, 1.45, 182.39 Q 0.51, 172.37, 1.51, 162.35 Q 0.89, 152.33, 1.29, 142.30 Q 1.04, 132.28, -0.03, 122.26 Q -0.21, 112.24,\
                        -0.28, 102.22 Q 0.48, 92.20, 0.05, 82.17 Q -0.08, 72.15, -0.00, 62.13 Q -0.03, 52.11, 0.86, 42.09 Q 1.54, 32.07, 2.40, 22.04\
                        Q 2.00, 12.02, 2.00, 2.00" style=" fill:#a7a77c;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-63566695" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 160px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="63566695" data-review-reference-id="63566695">\
            <div class="stencil-wrapper" style="width: 275px; height: 160px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 160px;width:275px;" width="275" height="160">\
                     <svg:g width="275" height="160"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.42, 1.86, 22.85, 2.04 Q 33.27, 2.01, 43.69, 1.82 Q 54.12, 1.74,\
                        64.54, 2.14 Q 74.96, 2.15, 85.38, 1.36 Q 95.81, 1.83, 106.23, 1.25 Q 116.65, 1.49, 127.08, 2.31 Q 137.50, 1.69, 147.92, 2.14\
                        Q 158.35, 1.09, 168.77, 0.70 Q 179.19, 1.47, 189.62, 1.33 Q 200.04, 1.33, 210.46, 1.05 Q 220.88, 1.76, 231.31, 1.29 Q 241.73,\
                        1.89, 252.15, 2.83 Q 262.58, 2.40, 273.25, 1.75 Q 272.78, 13.22, 272.75, 24.32 Q 274.02, 35.36, 273.76, 46.55 Q 274.68, 57.69,\
                        275.02, 68.84 Q 274.40, 79.99, 274.09, 91.14 Q 274.24, 102.28, 274.19, 113.43 Q 273.66, 124.57, 273.77, 135.71 Q 273.59, 146.86,\
                        273.60, 158.60 Q 262.72, 158.44, 252.21, 158.42 Q 241.75, 158.25, 231.34, 159.10 Q 220.89, 158.54, 210.46, 158.20 Q 200.04,\
                        158.37, 189.61, 157.66 Q 179.19, 158.66, 168.77, 158.44 Q 158.35, 159.25, 147.92, 158.88 Q 137.50, 159.06, 127.08, 158.54\
                        Q 116.65, 157.82, 106.23, 158.78 Q 95.81, 158.81, 85.38, 158.53 Q 74.96, 157.00, 64.54, 157.72 Q 54.12, 158.68, 43.69, 159.06\
                        Q 33.27, 159.76, 22.85, 160.00 Q 12.42, 160.23, 0.80, 159.20 Q 0.33, 147.41, 0.17, 135.98 Q 0.13, 124.70, 0.21, 113.49 Q 0.19,\
                        102.31, -0.27, 91.16 Q -0.20, 80.01, 0.43, 68.86 Q 0.72, 57.72, 1.51, 46.57 Q 2.08, 35.43, 2.48, 24.29 Q 2.00, 13.14, 2.00,\
                        2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1687614788" style="position: absolute; left: 15px; top: 35px; width: 240px; height: 87px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1687614788" data-review-reference-id="1687614788">\
            <div class="stencil-wrapper" style="width: 240px; height: 87px">\
               <div id="1687614788-1631949613" style="position: absolute; left: 0px; top: 0px; width: 85px; height: 85px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="1631949613" data-review-reference-id="1631949613">\
                  <div class="stencil-wrapper" style="width: 85px; height: 85px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 85px;width:85px;" width="85" height="85">\
                           <svg:g width="85" height="85"><svg:path class=" svg_unselected_element" d="M 82.00, 44.00 Q 83.84, 44.94, 82.18, 54.95 Q 77.97, 64.29, 71.67, 72.43 Q 63.42,\
                              78.65, 53.67, 82.13 Q 43.47, 83.36, 33.28, 82.01 Q 23.75, 78.06, 15.55, 71.75 Q 9.27, 63.49, 5.77, 53.70 Q 4.63, 43.47, 5.92,\
                              33.27 Q 10.33, 23.93, 16.39, 15.67 Q 24.80, 9.68, 34.30, 5.76 Q 44.51, 4.39, 54.76, 5.73 Q 64.24, 9.95, 72.15, 16.61 Q 78.44,\
                              24.71, 81.97, 34.40 Q 81.99, 44.66, 80.53, 54.47" style=" fill:white;"/>\
                           </svg:g>\
                        </svg:svg>\
                     </div>\
                  </div>\
               </div>\
               <div id="1687614788-2084429644" style="position: absolute; left: 100px; top: 0px; width: 91px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="2084429644" data-review-reference-id="2084429644">\
                  <div class="stencil-wrapper" style="width: 91px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Name</span></p></span></span></div>\
                  </div>\
               </div>\
               <div id="1687614788-1926687529" style="position: absolute; left: 100px; top: 50px; width: 145px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1926687529" data-review-reference-id="1926687529">\
                  <div class="stencil-wrapper" style="width: 145px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Surname </p></span></span></div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1665666048" style="position: absolute; left: 276px; top: 0px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1665666048" data-review-reference-id="1665666048">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:1090px;" width="1090" height="80">\
                     <svg:g width="1090" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.06, 1.63, 22.11, 2.05 Q 32.17, 2.73, 42.22, 1.74 Q 52.28, 1.44,\
                        62.33, 2.09 Q 72.39, 0.78, 82.44, 0.80 Q 92.50, 1.60, 102.56, 1.68 Q 112.61, 2.74, 122.67, 3.04 Q 132.72, 3.07, 142.78, 1.51\
                        Q 152.83, 1.65, 162.89, 1.34 Q 172.94, 2.34, 183.00, 1.96 Q 193.06, 1.70, 203.11, 1.59 Q 213.17, 2.20, 223.22, 1.62 Q 233.28,\
                        1.23, 243.33, 1.63 Q 253.39, 2.40, 263.44, 2.74 Q 273.50, 1.74, 283.56, 0.89 Q 293.61, 1.36, 303.67, 2.29 Q 313.72, 2.27,\
                        323.78, 2.29 Q 333.83, 3.36, 343.89, 3.14 Q 353.94, 2.23, 364.00, 2.10 Q 374.06, 1.07, 384.11, 0.94 Q 394.17, 0.67, 404.22,\
                        1.12 Q 414.28, 1.02, 424.33, 2.36 Q 434.39, 1.43, 444.44, 0.63 Q 454.50, 0.37, 464.56, -0.16 Q 474.61, -0.33, 484.67, 0.24\
                        Q 494.72, 1.60, 504.78, 1.72 Q 514.83, 3.35, 524.89, 3.24 Q 534.94, 1.19, 545.00, 1.52 Q 555.06, 1.06, 565.11, 0.50 Q 575.17,\
                        0.44, 585.22, 1.31 Q 595.28, 1.55, 605.33, 1.68 Q 615.39, 2.01, 625.44, 1.61 Q 635.50, 3.26, 645.56, 2.02 Q 655.61, 2.09,\
                        665.67, 2.71 Q 675.72, 2.87, 685.78, 1.83 Q 695.83, 1.29, 705.89, 0.98 Q 715.94, 0.94, 726.00, 1.45 Q 736.05, 0.56, 746.11,\
                        0.77 Q 756.17, 1.08, 766.22, 0.45 Q 776.28, -0.19, 786.33, 0.40 Q 796.39, 0.79, 806.44, 0.55 Q 816.50, 1.73, 826.55, 0.91\
                        Q 836.61, 2.33, 846.67, 2.85 Q 856.72, 1.98, 866.78, 1.89 Q 876.83, 0.55, 886.89, 0.19 Q 896.94, 0.80, 907.00, 0.26 Q 917.05,\
                        0.23, 927.11, 0.04 Q 937.17, -0.06, 947.22, 0.52 Q 957.28, 0.43, 967.33, 0.59 Q 977.39, 1.43, 987.44, 1.60 Q 997.50, 2.41,\
                        1007.55, 0.88 Q 1017.61, 1.79, 1027.67, 2.99 Q 1037.72, 3.25, 1047.78, 1.94 Q 1057.83, 0.17, 1067.89, -0.25 Q 1077.94, 0.27,\
                        1088.60, 1.40 Q 1088.63, 14.46, 1089.27, 27.15 Q 1089.12, 39.93, 1089.42, 52.62 Q 1089.80, 65.30, 1088.91, 78.91 Q 1078.31,\
                        79.12, 1068.02, 78.96 Q 1057.83, 78.02, 1047.80, 78.81 Q 1037.74, 79.11, 1027.67, 79.02 Q 1017.61, 78.72, 1007.56, 78.86 Q\
                        997.50, 79.21, 987.44, 79.34 Q 977.39, 77.86, 967.33, 76.72 Q 957.28, 77.71, 947.22, 78.15 Q 937.17, 78.01, 927.11, 78.03\
                        Q 917.05, 77.65, 907.00, 77.33 Q 896.94, 77.77, 886.89, 77.43 Q 876.83, 77.63, 866.78, 78.36 Q 856.72, 78.76, 846.67, 78.56\
                        Q 836.61, 78.46, 826.55, 78.27 Q 816.50, 78.89, 806.44, 78.77 Q 796.39, 78.15, 786.33, 78.46 Q 776.28, 79.11, 766.22, 78.79\
                        Q 756.17, 78.50, 746.11, 78.68 Q 736.05, 79.32, 726.00, 79.57 Q 715.94, 79.65, 705.89, 79.67 Q 695.83, 79.76, 685.78, 80.04\
                        Q 675.72, 79.72, 665.67, 78.73 Q 655.61, 78.41, 645.56, 78.54 Q 635.50, 79.44, 625.44, 78.89 Q 615.39, 78.66, 605.33, 77.66\
                        Q 595.28, 78.54, 585.22, 78.34 Q 575.17, 76.83, 565.11, 76.33 Q 555.06, 76.23, 545.00, 77.00 Q 534.94, 78.23, 524.89, 79.00\
                        Q 514.83, 79.31, 504.78, 78.27 Q 494.72, 79.11, 484.67, 78.97 Q 474.61, 79.33, 464.56, 78.71 Q 454.50, 78.12, 444.44, 78.05\
                        Q 434.39, 79.18, 424.33, 79.01 Q 414.28, 78.98, 404.22, 78.92 Q 394.17, 78.82, 384.11, 78.55 Q 374.06, 78.93, 364.00, 79.69\
                        Q 353.94, 79.43, 343.89, 78.93 Q 333.83, 78.42, 323.78, 78.74 Q 313.72, 79.01, 303.67, 78.85 Q 293.61, 78.99, 283.56, 78.76\
                        Q 273.50, 79.63, 263.44, 78.91 Q 253.39, 77.18, 243.33, 76.80 Q 233.28, 77.02, 223.22, 77.98 Q 213.17, 77.32, 203.11, 77.34\
                        Q 193.06, 77.16, 183.00, 76.76 Q 172.94, 76.87, 162.89, 77.16 Q 152.83, 78.07, 142.78, 78.74 Q 132.72, 79.70, 122.67, 79.72\
                        Q 112.61, 80.31, 102.56, 79.80 Q 92.50, 79.77, 82.44, 79.44 Q 72.39, 79.15, 62.33, 79.17 Q 52.28, 79.67, 42.22, 78.58 Q 32.17,\
                        78.85, 22.11, 79.56 Q 12.06, 79.28, 1.30, 78.70 Q 0.97, 65.68, 0.53, 52.88 Q 0.56, 40.10, 0.05, 27.40 Q 2.00, 14.67, 2.00,\
                        2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1325471400" style="position: absolute; left: 276px; top: 688px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1325471400" data-review-reference-id="1325471400">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:1090px;" width="1090" height="80">\
                     <svg:g width="1090" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.06, 2.01, 22.11, 2.05 Q 32.17, 2.88, 42.22, 1.91 Q 52.28, 1.78,\
                        62.33, 2.87 Q 72.39, 3.08, 82.44, 2.04 Q 92.50, 1.29, 102.56, 1.63 Q 112.61, 1.96, 122.67, 1.84 Q 132.72, 1.78, 142.78, 1.80\
                        Q 152.83, 1.73, 162.89, 1.43 Q 172.94, 0.95, 183.00, 0.87 Q 193.06, 0.29, 203.11, 0.50 Q 213.17, 0.32, 223.22, 0.20 Q 233.28,\
                        0.04, 243.33, -0.12 Q 253.39, -0.11, 263.44, 0.28 Q 273.50, 0.13, 283.56, -0.01 Q 293.61, -0.09, 303.67, -0.24 Q 313.72, 0.24,\
                        323.78, -0.14 Q 333.83, 1.00, 343.89, 1.18 Q 353.94, 1.55, 364.00, 1.36 Q 374.06, 1.26, 384.11, 0.46 Q 394.17, 1.39, 404.22,\
                        0.84 Q 414.28, 0.91, 424.33, 0.87 Q 434.39, 0.72, 444.44, 0.62 Q 454.50, 0.44, 464.56, 0.80 Q 474.61, 0.85, 484.67, 0.23 Q\
                        494.72, 0.51, 504.78, 0.16 Q 514.83, 0.51, 524.89, 0.92 Q 534.94, 0.96, 545.00, 1.14 Q 555.06, 0.90, 565.11, 0.83 Q 575.17,\
                        0.93, 585.22, 0.24 Q 595.28, 0.58, 605.33, 0.46 Q 615.39, 0.15, 625.44, 1.25 Q 635.50, 0.13, 645.56, 0.87 Q 655.61, 2.05,\
                        665.67, 2.08 Q 675.72, 1.16, 685.78, 1.15 Q 695.83, 0.95, 705.89, 1.50 Q 715.94, 1.56, 726.00, 1.06 Q 736.05, 1.15, 746.11,\
                        1.17 Q 756.17, 1.53, 766.22, 0.93 Q 776.28, 0.80, 786.33, 1.71 Q 796.39, 1.60, 806.44, 1.13 Q 816.50, 1.07, 826.55, 0.54 Q\
                        836.61, 1.35, 846.67, 1.25 Q 856.72, 0.70, 866.78, 0.54 Q 876.83, 0.46, 886.89, 0.42 Q 896.94, 0.31, 907.00, 0.22 Q 917.05,\
                        0.54, 927.11, 1.04 Q 937.17, 0.71, 947.22, 1.00 Q 957.28, 0.18, 967.33, 0.04 Q 977.39, 0.29, 987.44, 0.97 Q 997.50, -0.06,\
                        1007.55, 0.07 Q 1017.61, 0.56, 1027.67, 0.63 Q 1037.72, 1.18, 1047.78, 1.74 Q 1057.83, 1.77, 1067.89, 1.88 Q 1077.94, 1.32,\
                        1088.20, 1.80 Q 1088.41, 14.53, 1088.70, 27.23 Q 1089.12, 39.93, 1088.83, 52.64 Q 1088.37, 65.33, 1088.26, 78.26 Q 1078.28,\
                        79.03, 1068.09, 79.45 Q 1057.91, 79.20, 1047.81, 79.16 Q 1037.74, 79.27, 1027.67, 78.96 Q 1017.61, 78.98, 1007.56, 78.50 Q\
                        997.50, 77.56, 987.44, 77.65 Q 977.39, 78.99, 967.33, 79.35 Q 957.28, 79.08, 947.22, 78.78 Q 937.17, 79.16, 927.11, 78.32\
                        Q 917.05, 78.61, 907.00, 78.11 Q 896.94, 78.16, 886.89, 78.29 Q 876.83, 78.61, 866.78, 78.65 Q 856.72, 78.73, 846.67, 79.07\
                        Q 836.61, 78.17, 826.55, 78.65 Q 816.50, 78.46, 806.44, 78.90 Q 796.39, 79.17, 786.33, 79.40 Q 776.28, 79.67, 766.22, 79.42\
                        Q 756.17, 79.60, 746.11, 79.68 Q 736.05, 79.29, 726.00, 78.06 Q 715.94, 78.59, 705.89, 78.43 Q 695.83, 78.56, 685.78, 79.61\
                        Q 675.72, 79.57, 665.67, 79.46 Q 655.61, 78.94, 645.56, 79.15 Q 635.50, 78.52, 625.44, 78.37 Q 615.39, 78.43, 605.33, 78.26\
                        Q 595.28, 78.38, 585.22, 79.05 Q 575.17, 78.90, 565.11, 77.80 Q 555.06, 78.32, 545.00, 77.41 Q 534.94, 78.77, 524.89, 78.94\
                        Q 514.83, 78.12, 504.78, 78.94 Q 494.72, 79.05, 484.67, 79.33 Q 474.61, 79.33, 464.56, 79.48 Q 454.50, 79.58, 444.44, 79.71\
                        Q 434.39, 78.77, 424.33, 78.69 Q 414.28, 79.03, 404.22, 78.68 Q 394.17, 78.58, 384.11, 78.46 Q 374.06, 78.74, 364.00, 79.01\
                        Q 353.94, 78.02, 343.89, 78.27 Q 333.83, 78.86, 323.78, 79.31 Q 313.72, 79.70, 303.67, 79.56 Q 293.61, 79.20, 283.56, 78.97\
                        Q 273.50, 79.06, 263.44, 78.00 Q 253.39, 79.37, 243.33, 79.63 Q 233.28, 79.45, 223.22, 79.90 Q 213.17, 79.56, 203.11, 79.89\
                        Q 193.06, 79.31, 183.00, 80.04 Q 172.94, 79.84, 162.89, 79.06 Q 152.83, 77.96, 142.78, 78.35 Q 132.72, 78.59, 122.67, 77.72\
                        Q 112.61, 77.94, 102.56, 77.80 Q 92.50, 78.80, 82.44, 78.52 Q 72.39, 78.62, 62.33, 78.45 Q 52.28, 78.21, 42.22, 77.96 Q 32.17,\
                        78.38, 22.11, 78.63 Q 12.06, 78.80, 1.43, 78.57 Q 1.28, 65.57, 1.36, 52.76 Q 2.52, 39.97, 1.91, 27.34 Q 2.00, 14.67, 2.00,\
                        2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-743554012" style="position: absolute; left: 355px; top: 10px; width: 230px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="743554012" data-review-reference-id="743554012">\
            <div class="stencil-wrapper" style="width: 230px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:230px;" width="230" height="60">\
                     <svg:g width="230" height="60"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.27, 1.09, 22.55, 1.28 Q 32.82, 1.29, 43.09, 1.15 Q\
                        53.36, 0.77, 63.64, 1.47 Q 73.91, 1.17, 84.18, 1.66 Q 94.45, 1.31, 104.73, 1.03 Q 115.00, 1.81, 125.27, 1.48 Q 135.55, 1.11,\
                        145.82, 0.45 Q 156.09, 1.15, 166.36, 1.19 Q 176.64, 1.76, 186.91, 1.84 Q 197.18, 1.41, 207.45, 1.74 Q 217.73, 1.74, 228.05,\
                        1.95 Q 228.81, 15.73, 228.73, 29.90 Q 228.77, 43.95, 228.19, 58.19 Q 217.86, 58.41, 207.59, 59.01 Q 197.21, 58.44, 186.90,\
                        57.71 Q 176.63, 57.85, 166.36, 57.96 Q 156.09, 57.05, 145.82, 57.63 Q 135.54, 57.41, 125.27, 57.68 Q 115.00, 58.22, 104.73,\
                        58.11 Q 94.45, 58.30, 84.18, 57.51 Q 73.91, 58.55, 63.64, 58.28 Q 53.36, 58.09, 43.09, 58.34 Q 32.82, 59.11, 22.55, 59.40\
                        Q 12.27, 59.07, 1.42, 58.58 Q 1.18, 44.27, 0.47, 30.22 Q 2.00, 16.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.21, 4.79, 22.32, 8.00 Q 32.46, 11.07, 42.75, 13.54 Q 53.02,\
                        16.09, 63.42, 18.16 Q 73.70, 20.66, 84.07, 22.82 Q 94.28, 25.62, 104.37, 28.89 Q 114.69, 31.24, 124.94, 33.87 Q 135.48, 35.37,\
                        145.68, 38.19 Q 156.06, 40.30, 166.54, 42.00 Q 176.88, 44.27, 187.01, 47.39 Q 197.29, 49.92, 207.38, 53.22 Q 217.73, 55.45,\
                        228.00, 58.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 58.00 Q 11.84, 53.32, 22.19, 50.73 Q 32.52, 48.04, 43.00, 45.95 Q 53.26,\
                        43.02, 63.64, 40.52 Q 74.06, 38.21, 84.51, 36.01 Q 94.72, 32.84, 105.14, 30.54 Q 115.60, 28.39, 125.96, 25.81 Q 136.29, 23.12,\
                        146.74, 20.94 Q 157.24, 18.95, 167.58, 16.32 Q 177.84, 13.35, 188.13, 10.50 Q 198.61, 8.41, 209.02, 6.05 Q 219.64, 4.55, 230.00,\
                        2.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-411069666" style="position: absolute; left: 0px; top: 180px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="411069666" data-review-reference-id="411069666">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 2.89, 22.62, 3.10 Q 32.92, 2.58, 43.23, 2.62 Q 53.54, 1.48,\
                        63.85, 1.47 Q 74.15, 1.08, 84.46, 1.45 Q 94.77, 1.26, 105.08, 1.37 Q 115.38, 1.57, 125.69, 1.39 Q 136.00, 1.93, 146.31, 0.24\
                        Q 156.62, -0.19, 166.92, 0.12 Q 177.23, 0.68, 187.54, 1.08 Q 197.85, 1.82, 208.15, 2.00 Q 218.46, 1.58, 228.77, 1.39 Q 239.08,\
                        0.33, 249.38, 0.03 Q 259.69, 0.50, 270.59, 1.41 Q 271.30, 12.32, 271.66, 23.26 Q 271.90, 34.12, 270.96, 45.96 Q 260.17, 46.50,\
                        249.65, 46.95 Q 239.19, 46.84, 228.82, 46.72 Q 218.48, 46.21, 208.16, 45.98 Q 197.85, 45.53, 187.54, 44.69 Q 177.23, 44.95,\
                        166.92, 44.48 Q 156.62, 45.36, 146.31, 45.19 Q 136.00, 44.88, 125.69, 44.88 Q 115.38, 45.28, 105.08, 46.20 Q 94.77, 46.76,\
                        84.46, 46.00 Q 74.15, 46.56, 63.85, 46.62 Q 53.54, 46.43, 43.23, 46.59 Q 32.92, 46.64, 22.62, 46.16 Q 12.31, 46.06, 1.75,\
                        45.25 Q 0.95, 34.60, 0.88, 23.66 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 270.78, 15.00, 270.94, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 272.21, 16.00, 271.94, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 274.77, 17.00, 274.73, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 46.92, 24.69, 47.15 Q 35.04, 47.49, 45.38, 46.59 Q 55.73,\
                        46.08, 66.08, 46.83 Q 76.42, 46.13, 86.77, 45.67 Q 97.12, 45.79, 107.46, 45.22 Q 117.81, 45.99, 128.15, 46.50 Q 138.50, 47.00,\
                        148.85, 45.71 Q 159.19, 45.04, 169.54, 45.60 Q 179.88, 46.26, 190.23, 45.92 Q 200.58, 44.56, 210.92, 44.74 Q 221.27, 46.33,\
                        231.62, 46.05 Q 241.96, 45.64, 252.31, 46.32 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 46.08, 25.69, 46.03 Q 36.04, 46.51, 46.38, 46.63 Q 56.73,\
                        47.08, 67.08, 46.85 Q 77.42, 47.04, 87.77, 47.14 Q 98.12, 46.78, 108.46, 47.95 Q 118.81, 47.80, 129.15, 48.09 Q 139.50, 47.95,\
                        149.85, 47.75 Q 160.19, 47.81, 170.54, 47.76 Q 180.88, 47.55, 191.23, 46.64 Q 201.58, 47.79, 211.92, 48.04 Q 222.27, 48.14,\
                        232.62, 47.79 Q 242.96, 47.79, 253.31, 48.11 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 49.71, 26.69, 48.86 Q 37.04, 47.87, 47.38, 47.33 Q 57.73,\
                        46.44, 68.08, 45.87 Q 78.42, 46.27, 88.77, 46.27 Q 99.12, 46.69, 109.46, 46.41 Q 119.81, 46.36, 130.15, 45.61 Q 140.50, 45.92,\
                        150.85, 46.28 Q 161.19, 47.01, 171.54, 46.58 Q 181.88, 47.12, 192.23, 47.02 Q 202.58, 46.90, 212.92, 46.56 Q 223.27, 46.58,\
                        233.62, 46.84 Q 243.96, 45.99, 254.31, 45.89 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page995954813-layer-411069666button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page995954813-layer-411069666button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page995954813-layer-411069666button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Коэффициенты<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page995954813-layer-411069666\', \'276992336\', {"button":"left","id":"2141500830","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"598465367","options":"reloadOnly","target":"page606870902","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page995954813-layer-479308961" style="position: absolute; left: 1117px; top: 20px; width: 174px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="479308961" data-review-reference-id="479308961">\
            <div class="stencil-wrapper" style="width: 174px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Статистика</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-647396613" style="position: absolute; left: 0px; top: 250px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="647396613" data-review-reference-id="647396613">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.86, 22.62, 1.80 Q 32.92, 1.68, 43.23, 1.91 Q 53.54, 1.47,\
                        63.85, 1.32 Q 74.15, 1.48, 84.46, 1.58 Q 94.77, 1.61, 105.08, 1.57 Q 115.38, 1.15, 125.69, 1.53 Q 136.00, 2.55, 146.31, 1.43\
                        Q 156.62, 1.93, 166.92, 1.01 Q 177.23, 2.94, 187.54, 1.65 Q 197.85, 1.93, 208.15, 1.93 Q 218.46, 1.64, 228.77, 1.40 Q 239.08,\
                        1.09, 249.38, 1.42 Q 259.69, 1.34, 269.63, 2.37 Q 270.33, 12.64, 270.77, 23.39 Q 270.39, 34.22, 270.39, 45.39 Q 260.19, 46.57,\
                        249.67, 47.11 Q 239.12, 45.76, 228.77, 44.99 Q 218.46, 44.72, 208.16, 46.46 Q 197.85, 45.92, 187.54, 44.61 Q 177.23, 44.03,\
                        166.92, 45.21 Q 156.62, 45.91, 146.31, 45.42 Q 136.00, 45.72, 125.69, 44.98 Q 115.38, 44.87, 105.08, 45.18 Q 94.77, 45.31,\
                        84.46, 45.36 Q 74.15, 45.98, 63.85, 46.44 Q 53.54, 45.84, 43.23, 45.54 Q 32.92, 45.59, 22.62, 46.79 Q 12.31, 45.96, 1.58,\
                        45.42 Q 1.90, 34.28, 2.41, 23.44 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 271.45, 15.00, 271.41, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 269.76, 16.00, 270.82, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.84, 17.00, 273.81, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 44.96, 24.69, 44.94 Q 35.04, 44.81, 45.38, 44.72 Q 55.73,\
                        44.77, 66.08, 45.29 Q 76.42, 44.87, 86.77, 44.74 Q 97.12, 44.63, 107.46, 44.88 Q 117.81, 45.24, 128.15, 45.87 Q 138.50, 45.08,\
                        148.85, 45.35 Q 159.19, 45.35, 169.54, 44.87 Q 179.88, 44.82, 190.23, 44.66 Q 200.58, 44.86, 210.92, 45.35 Q 221.27, 46.16,\
                        231.62, 45.85 Q 241.96, 47.11, 252.31, 46.90 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 46.31, 25.69, 45.70 Q 36.04, 45.22, 46.38, 45.59 Q 56.73,\
                        45.70, 67.08, 45.43 Q 77.42, 45.80, 87.77, 45.69 Q 98.12, 45.66, 108.46, 45.59 Q 118.81, 45.33, 129.15, 45.13 Q 139.50, 45.52,\
                        149.85, 45.67 Q 160.19, 45.64, 170.54, 45.36 Q 180.88, 45.74, 191.23, 45.70 Q 201.58, 45.65, 211.92, 45.60 Q 222.27, 45.31,\
                        232.62, 45.68 Q 242.96, 45.29, 253.31, 45.27 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 48.05, 26.69, 48.47 Q 37.04, 49.04, 47.38, 48.60 Q 57.73,\
                        48.47, 68.08, 49.49 Q 78.42, 49.67, 88.77, 49.33 Q 99.12, 47.78, 109.46, 48.43 Q 119.81, 48.21, 130.15, 48.59 Q 140.50, 49.13,\
                        150.85, 48.60 Q 161.19, 48.92, 171.54, 48.66 Q 181.88, 47.66, 192.23, 46.94 Q 202.58, 48.00, 212.92, 48.26 Q 223.27, 47.32,\
                        233.62, 47.10 Q 243.96, 47.92, 254.31, 47.94 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page995954813-layer-647396613button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page995954813-layer-647396613button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page995954813-layer-647396613button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Изменения<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page995954813-layer-647396613\', \'interaction366609757\', {"button":"left","id":"action466528516","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction3821626","options":"reloadOnly","target":"page189476043","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page995954813-layer-1504055364" style="position: absolute; left: 0px; top: 320px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="1504055364" data-review-reference-id="1504055364">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.02, 22.62, 1.16 Q 32.92, 1.12, 43.23, 1.79 Q 53.54, 1.97,\
                        63.85, 1.78 Q 74.15, 1.86, 84.46, 2.00 Q 94.77, 2.23, 105.08, 1.65 Q 115.38, 1.41, 125.69, 1.95 Q 136.00, 2.33, 146.31, 3.23\
                        Q 156.62, 2.20, 166.92, 2.65 Q 177.23, 1.87, 187.54, 1.83 Q 197.85, 1.76, 208.15, 2.04 Q 218.46, 1.85, 228.77, 1.83 Q 239.08,\
                        2.46, 249.38, 1.80 Q 259.69, 2.11, 270.26, 1.74 Q 269.83, 12.81, 269.53, 23.57 Q 270.06, 34.25, 270.39, 45.39 Q 259.92, 45.72,\
                        249.38, 44.95 Q 239.14, 46.01, 228.82, 46.79 Q 218.49, 47.04, 208.17, 47.04 Q 197.85, 47.35, 187.54, 47.01 Q 177.23, 46.83,\
                        166.92, 46.16 Q 156.62, 44.95, 146.31, 45.67 Q 136.00, 45.04, 125.69, 45.71 Q 115.38, 45.46, 105.08, 45.44 Q 94.77, 46.22,\
                        84.46, 46.38 Q 74.15, 45.71, 63.85, 45.26 Q 53.54, 45.51, 43.23, 46.12 Q 32.92, 45.60, 22.62, 45.35 Q 12.31, 45.16, 2.08,\
                        44.92 Q 2.30, 34.15, 2.31, 23.46 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#a7a77c;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 272.53, 15.00, 271.92, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 274.12, 16.00, 274.20, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.85, 17.00, 274.34, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 44.93, 24.69, 45.12 Q 35.04, 44.97, 45.38, 44.95 Q 55.73,\
                        44.91, 66.08, 45.31 Q 76.42, 45.04, 86.77, 44.91 Q 97.12, 44.87, 107.46, 44.90 Q 117.81, 45.31, 128.15, 45.33 Q 138.50, 45.03,\
                        148.85, 44.92 Q 159.19, 44.59, 169.54, 45.06 Q 179.88, 45.00, 190.23, 44.89 Q 200.58, 44.86, 210.92, 44.95 Q 221.27, 45.14,\
                        231.62, 45.67 Q 241.96, 46.32, 252.31, 46.24 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 45.70, 25.69, 46.34 Q 36.04, 46.53, 46.38, 46.84 Q 56.73,\
                        46.68, 67.08, 46.62 Q 77.42, 46.94, 87.77, 47.19 Q 98.12, 47.32, 108.46, 47.47 Q 118.81, 47.65, 129.15, 47.22 Q 139.50, 46.67,\
                        149.85, 46.28 Q 160.19, 46.20, 170.54, 46.06 Q 180.88, 46.22, 191.23, 46.40 Q 201.58, 45.85, 211.92, 46.23 Q 222.27, 46.41,\
                        232.62, 46.09 Q 242.96, 46.32, 253.31, 45.92 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 47.45, 26.69, 47.41 Q 37.04, 47.29, 47.38, 47.16 Q 57.73,\
                        46.85, 68.08, 46.83 Q 78.42, 46.81, 88.77, 46.68 Q 99.12, 46.82, 109.46, 46.56 Q 119.81, 46.59, 130.15, 46.56 Q 140.50, 46.43,\
                        150.85, 46.35 Q 161.19, 46.27, 171.54, 46.06 Q 181.88, 46.59, 192.23, 46.70 Q 202.58, 46.41, 212.92, 46.39 Q 223.27, 47.07,\
                        233.62, 46.94 Q 243.96, 47.01, 254.31, 46.37 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page995954813-layer-1504055364button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page995954813-layer-1504055364button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page995954813-layer-1504055364button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Статистика<br />  \
                     			</button></div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-4427567" style="position: absolute; left: 0px; top: 718px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="4427567" data-review-reference-id="4427567">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 0.04, 22.62, 1.06 Q 32.92, 1.79, 43.23, 2.33 Q 53.54, 2.60,\
                        63.85, 2.13 Q 74.15, 2.64, 84.46, 2.53 Q 94.77, 2.12, 105.08, 2.34 Q 115.38, 0.67, 125.69, 1.07 Q 136.00, 2.04, 146.31, 1.06\
                        Q 156.62, 1.89, 166.92, 1.51 Q 177.23, 1.65, 187.54, 1.14 Q 197.85, 1.44, 208.15, 2.86 Q 218.46, 0.68, 228.77, 0.20 Q 239.08,\
                        0.36, 249.38, 0.35 Q 259.69, 0.99, 270.15, 1.85 Q 270.25, 12.67, 271.43, 23.30 Q 271.38, 34.16, 270.31, 45.31 Q 259.75, 45.18,\
                        249.51, 45.89 Q 239.16, 46.37, 228.81, 46.30 Q 218.47, 45.83, 208.17, 47.23 Q 197.85, 47.36, 187.54, 47.05 Q 177.23, 45.83,\
                        166.92, 45.46 Q 156.62, 46.54, 146.31, 47.20 Q 136.00, 46.94, 125.69, 46.53 Q 115.38, 46.31, 105.08, 45.80 Q 94.77, 44.50,\
                        84.46, 45.26 Q 74.15, 44.82, 63.85, 45.37 Q 53.54, 45.85, 43.23, 45.85 Q 32.92, 45.90, 22.62, 46.03 Q 12.31, 46.41, 1.18,\
                        45.82 Q 1.36, 34.46, 1.57, 23.56 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 271.49, 15.00, 271.58, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 272.02, 16.00, 272.03, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.56, 17.00, 273.41, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 48.06, 24.69, 47.95 Q 35.04, 46.83, 45.38, 47.43 Q 55.73,\
                        45.97, 66.08, 45.53 Q 76.42, 46.77, 86.77, 46.64 Q 97.12, 45.46, 107.46, 44.86 Q 117.81, 44.35, 128.15, 44.27 Q 138.50, 44.54,\
                        148.85, 45.43 Q 159.19, 45.35, 169.54, 45.27 Q 179.88, 44.98, 190.23, 44.11 Q 200.58, 44.11, 210.92, 44.34 Q 221.27, 44.35,\
                        231.62, 44.70 Q 241.96, 44.66, 252.31, 44.28 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 46.75, 25.69, 45.67 Q 36.04, 45.25, 46.38, 45.18 Q 56.73,\
                        45.19, 67.08, 45.48 Q 77.42, 45.31, 87.77, 45.29 Q 98.12, 45.16, 108.46, 45.13 Q 118.81, 45.06, 129.15, 45.34 Q 139.50, 45.29,\
                        149.85, 45.18 Q 160.19, 45.80, 170.54, 45.46 Q 180.88, 45.42, 191.23, 45.30 Q 201.58, 45.13, 211.92, 45.07 Q 222.27, 45.36,\
                        232.62, 45.36 Q 242.96, 45.79, 253.31, 45.85 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 48.02, 26.69, 48.50 Q 37.04, 48.49, 47.38, 48.17 Q 57.73,\
                        48.15, 68.08, 47.50 Q 78.42, 47.47, 88.77, 47.23 Q 99.12, 47.37, 109.46, 48.24 Q 119.81, 47.89, 130.15, 47.75 Q 140.50, 48.12,\
                        150.85, 47.52 Q 161.19, 46.85, 171.54, 46.43 Q 181.88, 47.09, 192.23, 46.96 Q 202.58, 48.24, 212.92, 49.05 Q 223.27, 49.14,\
                        233.62, 48.42 Q 243.96, 47.74, 254.31, 47.67 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page995954813-layer-4427567button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page995954813-layer-4427567button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page995954813-layer-4427567button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Выход<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page995954813-layer-4427567\', \'interaction576353665\', {"button":"left","id":"action1641582","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction70262354","options":"reloadOnly","target":"page940099653","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page995954813-layer-1667912423" style="position: absolute; left: 0px; top: 673px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1667912423" data-review-reference-id="1667912423">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.52, 22.62, 1.43 Q 32.92, 1.39, 43.23, 1.29 Q 53.54, 1.22,\
                        63.85, 0.99 Q 74.15, 0.82, 84.46, 0.68 Q 94.77, 0.59, 105.08, 0.95 Q 115.38, 0.74, 125.69, 0.56 Q 136.00, 0.53, 146.31, 0.45\
                        Q 156.62, 1.12, 166.92, 0.74 Q 177.23, 0.22, 187.54, 0.38 Q 197.85, 0.77, 208.15, 0.77 Q 218.46, 0.48, 228.77, 0.75 Q 239.08,\
                        1.07, 249.38, 1.14 Q 259.69, 0.56, 270.77, 1.23 Q 271.30, 12.32, 271.55, 23.28 Q 271.46, 34.15, 270.40, 45.40 Q 259.94, 45.77,\
                        249.47, 45.62 Q 239.11, 45.57, 228.81, 46.41 Q 218.48, 46.11, 208.16, 46.05 Q 197.85, 46.07, 187.54, 45.65 Q 177.23, 45.42,\
                        166.92, 45.46 Q 156.62, 46.12, 146.31, 45.34 Q 136.00, 45.21, 125.69, 45.15 Q 115.38, 45.59, 105.08, 45.10 Q 94.77, 45.86,\
                        84.46, 45.02 Q 74.15, 45.27, 63.85, 45.38 Q 53.54, 45.29, 43.23, 45.27 Q 32.92, 45.53, 22.62, 45.53 Q 12.31, 46.21, 1.26,\
                        45.74 Q 0.80, 34.65, 1.28, 23.60 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 272.98, 15.00, 272.52, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 274.54, 16.00, 274.51, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.46, 17.00, 273.53, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 45.31, 24.69, 45.79 Q 35.04, 45.01, 45.38, 46.73 Q 55.73,\
                        45.27, 66.08, 45.62 Q 76.42, 47.17, 86.77, 47.28 Q 97.12, 45.35, 107.46, 44.74 Q 117.81, 44.91, 128.15, 45.45 Q 138.50, 46.28,\
                        148.85, 46.98 Q 159.19, 46.24, 169.54, 46.55 Q 179.88, 45.39, 190.23, 44.76 Q 200.58, 44.48, 210.92, 44.38 Q 221.27, 44.31,\
                        231.62, 46.23 Q 241.96, 45.50, 252.31, 46.71 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 45.96, 25.69, 45.80 Q 36.04, 45.82, 46.38, 46.05 Q 56.73,\
                        46.01, 67.08, 46.04 Q 77.42, 46.38, 87.77, 46.38 Q 98.12, 46.12, 108.46, 46.03 Q 118.81, 45.68, 129.15, 46.00 Q 139.50, 46.37,\
                        149.85, 46.90 Q 160.19, 46.65, 170.54, 46.92 Q 180.88, 46.93, 191.23, 46.53 Q 201.58, 46.80, 211.92, 46.89 Q 222.27, 46.62,\
                        232.62, 46.21 Q 242.96, 46.14, 253.31, 46.50 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 47.10, 26.69, 47.59 Q 37.04, 47.74, 47.38, 47.66 Q 57.73,\
                        47.65, 68.08, 47.32 Q 78.42, 47.15, 88.77, 47.07 Q 99.12, 47.02, 109.46, 46.98 Q 119.81, 47.13, 130.15, 46.92 Q 140.50, 46.89,\
                        150.85, 46.76 Q 161.19, 46.73, 171.54, 46.86 Q 181.88, 46.85, 192.23, 47.27 Q 202.58, 46.71, 212.92, 46.66 Q 223.27, 47.16,\
                        233.62, 47.17 Q 243.96, 46.78, 254.31, 46.88 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page995954813-layer-1667912423button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page995954813-layer-1667912423button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page995954813-layer-1667912423button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Настройки<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page995954813-layer-1667912423\', \'interaction60967251\', {"button":"left","id":"action743983144","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction367706269","options":"reloadOnly","target":"page47785501","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page995954813-layer-449336035" style="position: absolute; left: 0px; top: 625px; width: 275px; height: 48px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="449336035" data-review-reference-id="449336035">\
            <div class="stencil-wrapper" style="width: 275px; height: 48px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 48px;width:275px;" width="275" height="48">\
                     <svg:g width="275" height="48"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 0.22, 22.62, 0.13 Q 32.92, 0.01, 43.23, 0.13 Q 53.54, 0.47,\
                        63.85, 0.19 Q 74.15, 0.07, 84.46, 0.20 Q 94.77, 0.69, 105.08, 0.83 Q 115.38, 0.60, 125.69, 0.74 Q 136.00, 0.84, 146.31, 0.63\
                        Q 156.62, 0.93, 166.92, 1.09 Q 177.23, 1.21, 187.54, 1.21 Q 197.85, 1.33, 208.15, 1.27 Q 218.46, 1.04, 228.77, 0.90 Q 239.08,\
                        1.60, 249.38, 1.20 Q 259.69, 2.00, 270.13, 1.87 Q 270.49, 12.09, 270.59, 22.42 Q 270.36, 32.73, 269.95, 42.95 Q 259.81, 43.38,\
                        249.43, 43.36 Q 239.15, 44.20, 228.79, 43.53 Q 218.47, 43.83, 208.16, 43.43 Q 197.85, 43.74, 187.54, 43.40 Q 177.23, 43.52,\
                        166.92, 43.95 Q 156.62, 43.91, 146.31, 44.09 Q 136.00, 44.55, 125.69, 44.31 Q 115.38, 43.27, 105.08, 42.89 Q 94.77, 43.57,\
                        84.46, 43.49 Q 74.15, 43.23, 63.85, 42.87 Q 53.54, 42.71, 43.23, 42.97 Q 32.92, 43.37, 22.62, 43.19 Q 12.31, 43.23, 1.56,\
                        43.44 Q 1.34, 32.97, 2.11, 22.48 Q 2.00, 12.25, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 273.26, 14.50, 273.13, 25.00 Q 271.00, 35.50, 271.00, 46.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 272.48, 15.50, 272.51, 26.00 Q 272.00, 36.50, 272.00, 47.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 274.91, 16.50, 275.03, 27.00 Q 273.00, 37.50, 273.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 44.00 Q 14.35, 45.20, 24.69, 44.77 Q 35.04, 44.37, 45.38, 43.36 Q 55.73,\
                        43.21, 66.08, 42.61 Q 76.42, 41.93, 86.77, 41.83 Q 97.12, 42.57, 107.46, 41.92 Q 117.81, 42.42, 128.15, 41.96 Q 138.50, 41.87,\
                        148.85, 42.12 Q 159.19, 42.08, 169.54, 42.28 Q 179.88, 43.30, 190.23, 43.92 Q 200.58, 42.70, 210.92, 43.11 Q 221.27, 42.48,\
                        231.62, 43.11 Q 241.96, 42.84, 252.31, 42.28 Q 262.65, 44.00, 273.00, 44.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 45.00 Q 15.35, 43.64, 25.69, 44.37 Q 36.04, 44.35, 46.38, 44.70 Q 56.73,\
                        43.65, 67.08, 42.97 Q 77.42, 43.42, 87.77, 44.30 Q 98.12, 44.00, 108.46, 43.96 Q 118.81, 43.88, 129.15, 43.97 Q 139.50, 44.00,\
                        149.85, 44.28 Q 160.19, 44.80, 170.54, 45.16 Q 180.88, 43.86, 191.23, 44.13 Q 201.58, 43.95, 211.92, 43.88 Q 222.27, 43.86,\
                        232.62, 43.72 Q 242.96, 44.04, 253.31, 44.12 Q 263.65, 45.00, 274.00, 45.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 46.00 Q 16.35, 46.48, 26.69, 46.15 Q 37.04, 45.81, 47.38, 45.46 Q 57.73,\
                        45.42, 68.08, 45.30 Q 78.42, 44.99, 88.77, 45.01 Q 99.12, 45.23, 109.46, 45.15 Q 119.81, 45.10, 130.15, 44.90 Q 140.50, 44.65,\
                        150.85, 44.94 Q 161.19, 44.89, 171.54, 44.96 Q 181.88, 44.97, 192.23, 45.17 Q 202.58, 44.94, 212.92, 45.01 Q 223.27, 45.49,\
                        233.62, 45.33 Q 243.96, 45.20, 254.31, 45.02 Q 264.65, 46.00, 275.00, 46.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page995954813-layer-449336035button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page995954813-layer-449336035button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page995954813-layer-449336035button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:44px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Контакты<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 48px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page995954813-layer-449336035\', \'interaction898740289\', {"button":"left","id":"action602857057","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction981232967","options":"reloadOnly","target":"page607113527","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page995954813-layer-53444741" style="position: absolute; left: 355px; top: 710px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="53444741" data-review-reference-id="53444741">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-514781659" style="position: absolute; left: 355px; top: 195px; width: 930px; height: 395px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="514781659" data-review-reference-id="514781659">\
            <div class="stencil-wrapper" style="width: 930px; height: 395px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-page995954813-layer-514781659-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 395px;width:930px;" width="930" height="395">\
                     <svg:g id="__containerId__-page995954813-layer-514781659svg" width="930" height="395"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.07, 0.34, 22.13, 0.50 Q 32.20, 0.77, 42.26, 0.88 Q 52.33, 1.47,\
                        62.39, 1.20 Q 72.46, 0.80, 82.52, 1.36 Q 92.59, 1.58, 102.65, 1.06 Q 112.72, 0.36, 122.78, 0.07 Q 132.85, -0.16, 142.91, -0.20\
                        Q 152.98, -0.23, 163.04, 0.47 Q 173.11, 1.05, 183.17, 0.85 Q 193.24, 0.46, 203.30, 0.39 Q 213.37, 0.12, 223.43, 0.02 Q 233.50,\
                        0.13, 243.57, 0.40 Q 253.63, 0.32, 263.70, -0.09 Q 273.76, -0.24, 283.83, -0.20 Q 293.89, -0.18, 303.96, -0.22 Q 314.02, 0.82,\
                        324.09, 1.26 Q 334.15, 1.20, 344.22, 1.49 Q 354.28, 0.65, 364.35, 1.75 Q 374.41, 0.65, 384.48, 0.98 Q 394.54, 0.76, 404.61,\
                        0.73 Q 414.67, 0.65, 424.74, 0.43 Q 434.80, 0.36, 444.87, 0.71 Q 454.93, 0.67, 465.00, 0.77 Q 475.07, 1.06, 485.13, 0.98 Q\
                        495.20, 1.27, 505.26, 0.68 Q 515.33, 0.88, 525.39, 0.50 Q 535.46, 0.57, 545.52, 0.47 Q 555.59, 0.38, 565.65, 0.35 Q 575.72,\
                        0.06, 585.78, 0.18 Q 595.85, 0.74, 605.91, 0.09 Q 615.98, 1.58, 626.04, 1.61 Q 636.11, 2.13, 646.17, 1.76 Q 656.24, 1.27,\
                        666.30, 1.40 Q 676.37, 1.36, 686.44, 1.70 Q 696.50, 1.29, 706.57, 0.34 Q 716.63, 0.41, 726.70, 2.11 Q 736.76, 1.78, 746.83,\
                        0.85 Q 756.89, 1.42, 766.96, 1.27 Q 777.02, 0.83, 787.09, 0.48 Q 797.15, 0.92, 807.22, 0.54 Q 817.28, 0.65, 827.35, 0.98 Q\
                        837.41, 0.78, 847.48, 0.72 Q 857.54, 0.68, 867.61, 0.46 Q 877.67, 0.37, 887.74, 0.80 Q 897.81, 0.88, 907.87, 0.46 Q 917.94,\
                        0.41, 928.70, 1.31 Q 928.84, 12.01, 928.85, 22.46 Q 928.48, 32.84, 928.84, 43.13 Q 928.37, 53.44, 928.31, 63.73 Q 929.01,\
                        74.02, 929.26, 84.31 Q 929.24, 94.60, 929.07, 104.89 Q 928.55, 115.18, 928.76, 125.47 Q 928.24, 135.76, 928.76, 146.05 Q 928.47,\
                        156.34, 929.05, 166.63 Q 929.25, 176.92, 929.15, 187.21 Q 928.88, 197.50, 928.92, 207.79 Q 929.14, 218.08, 929.26, 228.37\
                        Q 929.29, 238.66, 929.26, 248.95 Q 928.78, 259.24, 928.59, 269.53 Q 929.99, 279.82, 929.62, 290.11 Q 927.81, 300.39, 927.64,\
                        310.68 Q 927.83, 320.97, 928.44, 331.26 Q 928.19, 341.55, 927.96, 351.84 Q 927.63, 362.13, 927.75, 372.42 Q 927.45, 382.71,\
                        927.87, 392.87 Q 917.87, 392.81, 907.78, 392.37 Q 897.85, 393.65, 887.77, 393.95 Q 877.70, 394.54, 867.62, 394.07 Q 857.55,\
                        394.58, 847.48, 394.64 Q 837.42, 394.96, 827.35, 394.72 Q 817.28, 394.65, 807.22, 394.45 Q 797.15, 392.98, 787.09, 393.25\
                        Q 777.02, 392.68, 766.96, 393.21 Q 756.89, 393.92, 746.83, 393.24 Q 736.76, 393.56, 726.70, 392.68 Q 716.63, 393.62, 706.57,\
                        393.39 Q 696.50, 393.94, 686.44, 393.95 Q 676.37, 394.24, 666.30, 394.74 Q 656.24, 393.76, 646.17, 393.10 Q 636.11, 393.71,\
                        626.04, 394.22 Q 615.98, 393.25, 605.91, 394.17 Q 595.85, 393.09, 585.78, 393.37 Q 575.72, 393.86, 565.65, 393.06 Q 555.59,\
                        394.34, 545.52, 393.57 Q 535.46, 394.45, 525.39, 393.82 Q 515.33, 394.39, 505.26, 394.35 Q 495.20, 394.56, 485.13, 394.49\
                        Q 475.07, 394.24, 465.00, 393.88 Q 454.93, 393.54, 444.87, 393.24 Q 434.80, 392.66, 424.74, 393.94 Q 414.67, 393.89, 404.61,\
                        394.11 Q 394.54, 394.37, 384.48, 394.44 Q 374.41, 394.71, 364.35, 393.91 Q 354.28, 394.47, 344.22, 394.42 Q 334.15, 394.45,\
                        324.09, 393.90 Q 314.02, 394.48, 303.96, 394.38 Q 293.89, 393.99, 283.83, 393.40 Q 273.76, 393.02, 263.70, 393.28 Q 253.63,\
                        393.22, 243.57, 393.61 Q 233.50, 394.06, 223.43, 394.18 Q 213.37, 394.56, 203.30, 394.66 Q 193.24, 394.92, 183.17, 394.69\
                        Q 173.11, 393.47, 163.04, 393.82 Q 152.98, 395.33, 142.91, 393.74 Q 132.85, 393.11, 122.78, 393.07 Q 112.72, 393.76, 102.65,\
                        393.97 Q 92.59, 393.25, 82.52, 393.30 Q 72.46, 393.42, 62.39, 393.91 Q 52.33, 394.14, 42.26, 394.41 Q 32.20, 394.45, 22.13,\
                        394.33 Q 12.07, 394.04, 1.44, 393.56 Q 1.33, 382.93, 1.31, 372.52 Q 1.20, 362.18, 0.91, 351.88 Q 0.84, 341.57, 0.93, 331.27\
                        Q 1.36, 320.98, 1.13, 310.69 Q 0.76, 300.40, 0.43, 290.11 Q 0.48, 279.82, 0.33, 269.53 Q 0.86, 259.24, 0.93, 248.95 Q 0.56,\
                        238.66, 0.25, 228.37 Q 0.04, 218.08, 0.29, 207.79 Q 1.14, 197.50, 1.54, 187.21 Q 1.34, 176.92, 1.12, 166.63 Q 1.53, 156.34,\
                        1.60, 146.05 Q 1.75, 135.76, 1.46, 125.47 Q 1.53, 115.18, 1.50, 104.89 Q 1.50, 94.61, 1.07, 84.32 Q 1.25, 74.03, 1.35, 63.74\
                        Q 1.37, 53.45, 1.62, 43.16 Q 2.67, 32.87, 2.88, 22.58 Q 2.00, 12.29, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 926px; height:391px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page995954813-layer-514781659-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page995954813-layer-514781659-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page995954813-layer-514781659-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page995954813-layer-514781659-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-page995954813-layer-514781659-accordion", "926", "391", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-page995954813-layer-chart230116452" style="position: absolute; left: 360px; top: 230px; width: 920px; height: 265px" data-interactive-element-type="static.chart" class="chart stencil mobile-interaction-potential-trigger " data-stencil-id="chart230116452" data-review-reference-id="chart230116452">\
            <div class="stencil-wrapper" style="width: 920px; height: 265px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 265px;width:920px;" viewBox="0 0 920 265" width="920" height="265">\
                     <svg:g width="920" height="265">\
                        <svg:g transform="scale(5.111111111111111, 1.7666666666666666)"><svg:path id="defaultID" class=" svg_unselected_element" d="M 0.00, 0.00 Q 10.00, 0.78, 20.00, 1.05 Q 30.00, 0.63, 40.00,\
                           0.34 Q 50.00, -0.85, 60.00, -0.68 Q 70.00, -0.41, 80.00, -0.45 Q 90.00, -0.89, 100.00, -0.63 Q 110.00, -0.34, 120.00, -0.60\
                           Q 130.00, -0.57, 140.00, -1.09 Q 150.00, -1.21, 160.00, -1.04 Q 170.00, -0.57, 180.37, -0.37 Q 179.97, 10.72, 179.98, 21.43\
                           Q 180.43, 32.11, 180.29, 42.85 Q 180.93, 53.56, 180.80, 64.28 Q 180.72, 75.00, 180.29, 85.71 Q 180.48, 96.43, 180.58, 107.14\
                           Q 180.95, 117.86, 180.73, 128.57 Q 180.38, 139.29, 180.42, 150.42 Q 170.03, 150.08, 160.04, 150.29 Q 150.01, 150.08, 140.03,\
                           150.79 Q 129.99, 149.64, 119.99, 149.13 Q 110.00, 149.64, 100.00, 149.80 Q 90.00, 151.02, 80.00, 150.69 Q 70.00, 150.92, 60.00,\
                           150.48 Q 50.00, 150.49, 40.00, 150.49 Q 30.00, 150.56, 20.00, 150.45 Q 10.00, 149.82, -0.26, 150.26 Q -0.22, 139.36, -0.35,\
                           128.62 Q 0.22, 117.84, -0.05, 107.14 Q 0.16, 96.43, -0.30, 85.72 Q -0.90, 75.00, -0.85, 64.29 Q -1.16, 53.57, -0.68, 42.86\
                           Q 0.08, 32.14, 0.18, 21.43 Q 0.00, 10.71, 0.00, -0.00" style="fill:white;stroke:none;"/>\
                           <svg:g name="line" style="fill:none;stroke:black;stroke-width:1px;"><svg:path class=" svg_unselected_element" d="M 9.00, 3.00 Q 9.26, 14.42, 9.78, 25.83 Q 10.10, 37.25, 9.84, 48.67 Q 9.58, 60.08,\
                              10.04, 71.50 Q 9.79, 82.92, 10.60, 94.33 Q 9.48, 105.75, 10.11, 117.17 Q 9.80, 128.58, 9.47, 139.53 Q 20.45, 139.71, 31.72,\
                              139.94 Q 43.09, 139.73, 54.45, 139.30 Q 65.79, 139.71, 77.14, 140.96 Q 88.50, 139.95, 99.86, 139.33 Q 111.22, 138.91, 122.57,\
                              139.32 Q 133.93, 139.93, 145.29, 139.04 Q 156.64, 138.86, 168.00, 139.34 Q 172.00, 140.00, 176.00, 140.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 167.00, 135.00 Q 172.09, 137.31, 176.86, 140.00 Q 172.00, 142.50, 167.00, 145.00"\
                              style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 13.00 Q 4.91, 7.21, 9.00, 2.46 Q 11.50, 8.00, 14.00, 13.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 9.00, 140.00 Q 18.12, 124.96, 29.37, 111.42 Q 37.48, 104.46, 46.00, 98.36 Q\
                              50.03, 101.93, 53.89, 105.66 Q 60.88, 97.33, 68.03, 89.03 Q 71.47, 85.94, 73.73, 81.86 Q 75.67, 78.26, 75.81, 73.88 Q 78.80,\
                              70.36, 82.02, 66.87 Q 87.26, 81.30, 91.81, 95.74 Q 94.25, 85.89, 96.91, 75.93 Q 101.49, 70.99, 105.58, 65.78 Q 108.64, 58.06,\
                              111.69, 50.42 Q 116.34, 43.38, 122.04, 36.24 Q 125.34, 39.96, 128.56, 44.04 Q 129.63, 47.26, 129.77, 49.71 Q 134.41, 49.30,\
                              138.30, 49.63 Q 143.58, 38.67, 150.19, 27.73 Q 154.35, 26.19, 158.02, 25.27 Q 158.78, 20.48, 160.18, 15.36 Q 163.50, 14.00,\
                              166.00, 12.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 10.00, 140.00 Q 12.43, 123.12, 16.49, 106.33 Q 21.93, 98.12, 27.64, 90.44 Q\
                              30.12, 81.53, 33.69, 72.03 Q 39.35, 75.27, 43.47, 79.33 Q 46.66, 67.03, 50.57, 54.38 Q 56.13, 52.67, 61.34, 51.24 Q 67.12,\
                              42.24, 74.07, 33.14 Q 80.53, 41.78, 85.96, 50.31 Q 92.34, 46.55, 99.23, 42.39 Q 105.30, 50.63, 111.03, 58.97 Q 116.53, 63.99,\
                              121.10, 69.95 Q 124.63, 83.95, 128.24, 97.89 Q 133.67, 108.41, 139.00, 118.74 Q 142.80, 110.80, 147.18, 102.73 Q 150.41, 114.05,\
                              154.34, 124.73 Q 159.50, 130.50, 165.00, 136.00" style=" fill:none;"/>\
                           </svg:g>\
                        </svg:g>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page995954813"] .border-wrapper, body[data-current-page-id="page995954813"] .simulation-container{\
         			width:1366px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page995954813"] .border-wrapper, body.has-frame[data-current-page-id="page995954813"]\
         .simulation-container{\
         			height:768px;\
         		}\
         		\
         		body[data-current-page-id="page995954813"] .svg-border-1366-768{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page995954813"] .border-wrapper .border-div{\
         			width:1366px;\
         			height:768px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page995954813",\
      			"name": "stats",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":1366,\
      			"height":768,\
      			"parentFolder": "",\
      			"frame": "desktop",\
      			"frameOrientation": "landscape"\
      		}\
      	\
   </div>\
   <div id="border-wrapper">\
      <div xmlns="http://www.w3.org/1999/xhtml" xmlns:json="http://json.org/" class="svg-border svg-border-1366-768" style="display: none;">\
         <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" class="svg_border" style="position:absolute;left:-43px;top:-13px;width:1423px;height:792px;"><svg:path class=" svg_unselected_element" d="M 32.00, 3.00 Q 42.12, 4.59, 52.24, 4.04 Q 62.35, 3.76, 72.47, 3.79 Q 82.59,\
            3.51, 92.71, 3.00 Q 102.82, 3.23, 112.94, 2.22 Q 123.06, 3.33, 133.18, 2.75 Q 143.29, 2.20, 153.41, 2.60 Q 163.53, 3.38, 173.65,\
            3.13 Q 183.76, 2.00, 193.88, 2.12 Q 204.00, 3.76, 214.12, 3.31 Q 224.24, 3.53, 234.35, 2.34 Q 244.47, 2.69, 254.59, 2.05 Q\
            264.71, 1.84, 274.82, 2.66 Q 284.94, 3.43, 295.06, 3.11 Q 305.18, 3.50, 315.29, 2.97 Q 325.41, 2.16, 335.53, 1.80 Q 345.65,\
            0.97, 355.76, 1.96 Q 365.88, 2.74, 376.00, 4.16 Q 386.12, 5.14, 396.24, 3.13 Q 406.35, 4.73, 416.47, 3.64 Q 426.59, 3.16,\
            436.71, 2.39 Q 446.82, 2.05, 456.94, 2.48 Q 467.06, 3.12, 477.18, 2.49 Q 487.29, 2.12, 497.41, 2.33 Q 507.53, 2.12, 517.65,\
            2.52 Q 527.76, 2.99, 537.88, 2.55 Q 548.00, 1.38, 558.12, 1.76 Q 568.24, 3.13, 578.35, 1.56 Q 588.47, 3.20, 598.59, 1.42 Q\
            608.71, 1.96, 618.82, 1.48 Q 628.94, 2.07, 639.06, 1.94 Q 649.18, 1.76, 659.29, 1.54 Q 669.41, 2.15, 679.53, 2.97 Q 689.65,\
            3.50, 699.77, 3.23 Q 709.88, 3.51, 720.00, 4.07 Q 730.12, 4.50, 740.24, 4.23 Q 750.35, 1.74, 760.47, 1.31 Q 770.59, 2.79,\
            780.71, 3.82 Q 790.82, 3.62, 800.94, 3.08 Q 811.06, 3.42, 821.18, 3.16 Q 831.29, 2.88, 841.41, 2.08 Q 851.53, 1.32, 861.65,\
            1.50 Q 871.77, 1.59, 881.88, 2.25 Q 892.00, 3.01, 902.12, 3.57 Q 912.24, 2.66, 922.35, 2.95 Q 932.47, 3.19, 942.59, 1.91 Q\
            952.71, 1.93, 962.82, 2.03 Q 972.94, 2.25, 983.06, 2.14 Q 993.18, 2.35, 1003.30, 2.49 Q 1013.41, 2.99, 1023.53, 3.66 Q 1033.65,\
            3.18, 1043.77, 2.64 Q 1053.88, 1.37, 1064.00, 1.09 Q 1074.12, 1.10, 1084.24, 2.11 Q 1094.35, 3.32, 1104.47, 4.14 Q 1114.59,\
            1.31, 1124.71, 1.02 Q 1134.83, 2.31, 1144.94, 3.85 Q 1155.06, 2.10, 1165.18, 1.74 Q 1175.30, 1.92, 1185.41, 2.54 Q 1195.53,\
            2.21, 1205.65, 1.65 Q 1215.77, 1.23, 1225.88, 1.92 Q 1236.00, 2.18, 1246.12, 1.83 Q 1256.24, 1.68, 1266.35, 2.57 Q 1276.47,\
            1.98, 1286.59, 1.88 Q 1296.71, 1.90, 1306.83, 2.11 Q 1316.94, 2.05, 1327.06, 2.35 Q 1337.18, 2.53, 1347.30, 2.66 Q 1357.41,\
            2.29, 1367.53, 1.84 Q 1377.65, 2.47, 1387.77, 2.23 Q 1397.88, 1.96, 1408.17, 2.83 Q 1408.00, 13.17, 1407.96, 23.35 Q 1407.53,\
            33.54, 1406.97, 43.72 Q 1407.59, 53.86, 1408.67, 64.02 Q 1410.08, 74.19, 1409.92, 84.36 Q 1409.79, 94.54, 1409.12, 104.71\
            Q 1408.29, 114.88, 1407.65, 125.05 Q 1407.70, 135.22, 1408.36, 145.39 Q 1409.15, 155.57, 1409.73, 165.74 Q 1409.33, 175.91,\
            1408.90, 186.08 Q 1408.90, 196.25, 1408.60, 206.42 Q 1408.04, 216.59, 1407.61, 226.76 Q 1408.25, 236.93, 1408.44, 247.11 Q\
            1408.94, 257.28, 1409.56, 267.45 Q 1409.92, 277.62, 1409.50, 287.79 Q 1409.30, 297.96, 1409.86, 308.13 Q 1409.17, 318.30,\
            1409.57, 328.47 Q 1408.78, 338.64, 1408.41, 348.82 Q 1408.63, 358.99, 1407.80, 369.16 Q 1408.17, 379.33, 1408.84, 389.50 Q\
            1409.36, 399.67, 1408.48, 409.84 Q 1408.25, 420.01, 1408.25, 430.18 Q 1408.50, 440.36, 1409.44, 450.53 Q 1409.81, 460.70,\
            1409.28, 470.87 Q 1408.73, 481.04, 1409.25, 491.21 Q 1408.39, 501.38, 1407.96, 511.55 Q 1408.20, 521.72, 1407.96, 531.89 Q\
            1408.55, 542.07, 1408.67, 552.24 Q 1409.06, 562.41, 1408.97, 572.58 Q 1409.06, 582.75, 1408.88, 592.92 Q 1408.72, 603.09,\
            1407.99, 613.26 Q 1408.88, 623.43, 1409.59, 633.61 Q 1409.65, 643.78, 1409.42, 653.95 Q 1409.33, 664.12, 1409.39, 674.29 Q\
            1408.37, 684.46, 1408.44, 694.63 Q 1409.17, 704.80, 1408.30, 714.97 Q 1408.23, 725.15, 1407.37, 735.32 Q 1407.19, 745.49,\
            1407.08, 755.66 Q 1407.16, 765.83, 1407.45, 775.45 Q 1398.06, 776.52, 1388.03, 777.83 Q 1377.75, 777.47, 1367.58, 777.36 Q\
            1357.45, 778.11, 1347.31, 777.54 Q 1337.18, 776.00, 1327.06, 775.55 Q 1316.94, 776.29, 1306.83, 776.72 Q 1296.71, 776.42,\
            1286.59, 776.12 Q 1276.47, 777.01, 1266.35, 776.89 Q 1256.24, 776.65, 1246.12, 776.68 Q 1236.00, 777.54, 1225.88, 776.80 Q\
            1215.77, 777.27, 1205.65, 776.67 Q 1195.53, 777.49, 1185.41, 777.63 Q 1175.30, 777.54, 1165.18, 777.98 Q 1155.06, 778.13,\
            1144.94, 778.20 Q 1134.83, 777.60, 1124.71, 778.01 Q 1114.59, 777.07, 1104.47, 777.24 Q 1094.35, 776.92, 1084.24, 776.21 Q\
            1074.12, 777.11, 1064.00, 776.81 Q 1053.88, 776.99, 1043.77, 777.18 Q 1033.65, 777.22, 1023.53, 777.38 Q 1013.41, 777.83,\
            1003.30, 777.66 Q 993.18, 777.77, 983.06, 777.38 Q 972.94, 776.98, 962.82, 776.99 Q 952.71, 777.03, 942.59, 777.20 Q 932.47,\
            777.24, 922.35, 777.46 Q 912.24, 777.51, 902.12, 777.47 Q 892.00, 777.35, 881.88, 776.99 Q 871.77, 776.85, 861.65, 775.61\
            Q 851.53, 776.08, 841.41, 775.80 Q 831.29, 776.41, 821.18, 776.60 Q 811.06, 776.63, 800.94, 776.68 Q 790.82, 776.30, 780.71,\
            775.98 Q 770.59, 776.49, 760.47, 776.94 Q 750.35, 777.41, 740.24, 777.34 Q 730.12, 776.42, 720.00, 776.52 Q 709.88, 776.42,\
            699.77, 775.74 Q 689.65, 775.70, 679.53, 775.66 Q 669.41, 777.00, 659.29, 777.71 Q 649.18, 777.69, 639.06, 777.78 Q 628.94,\
            777.77, 618.82, 777.45 Q 608.71, 777.24, 598.59, 777.77 Q 588.47, 777.85, 578.35, 778.01 Q 568.24, 778.09, 558.12, 777.74\
            Q 548.00, 777.11, 537.88, 776.88 Q 527.76, 777.32, 517.65, 777.55 Q 507.53, 777.69, 497.41, 777.84 Q 487.29, 777.79, 477.18,\
            777.18 Q 467.06, 776.74, 456.94, 776.41 Q 446.82, 777.45, 436.71, 777.42 Q 426.59, 777.33, 416.47, 777.48 Q 406.35, 777.54,\
            396.24, 777.43 Q 386.12, 777.44, 376.00, 777.32 Q 365.88, 776.51, 355.76, 776.68 Q 345.65, 776.82, 335.53, 776.99 Q 325.41,\
            777.37, 315.29, 777.29 Q 305.18, 777.55, 295.06, 777.37 Q 284.94, 777.46, 274.82, 777.61 Q 264.71, 777.74, 254.59, 777.81\
            Q 244.47, 777.29, 234.35, 776.80 Q 224.24, 776.72, 214.12, 776.69 Q 204.00, 777.03, 193.88, 777.41 Q 183.76, 776.44, 173.65,\
            776.04 Q 163.53, 776.01, 153.41, 776.60 Q 143.29, 776.09, 133.18, 775.73 Q 123.06, 775.81, 112.94, 775.36 Q 102.82, 776.52,\
            92.71, 776.23 Q 82.59, 776.02, 72.47, 775.54 Q 62.35, 775.55, 52.24, 775.92 Q 42.12, 776.04, 32.06, 775.94 Q 32.00, 765.83,\
            32.21, 755.63 Q 32.60, 745.45, 32.46, 735.30 Q 32.41, 725.14, 31.76, 714.98 Q 30.62, 704.81, 31.02, 694.63 Q 31.49, 684.46,\
            32.32, 674.29 Q 31.62, 664.12, 31.69, 653.95 Q 31.10, 643.78, 30.73, 633.61 Q 31.47, 623.43, 31.30, 613.26 Q 31.21, 603.09,\
            31.69, 592.92 Q 31.37, 582.75, 31.44, 572.58 Q 32.35, 562.41, 32.22, 552.24 Q 33.06, 542.07, 32.96, 531.89 Q 32.24, 521.72,\
            31.98, 511.55 Q 31.79, 501.38, 33.04, 491.21 Q 32.24, 481.04, 31.98, 470.87 Q 32.39, 460.70, 32.56, 450.53 Q 32.10, 440.36,\
            31.47, 430.18 Q 31.36, 420.01, 31.57, 409.84 Q 31.79, 399.67, 31.79, 389.50 Q 32.90, 379.33, 31.61, 369.16 Q 31.85, 358.99,\
            31.60, 348.82 Q 31.81, 338.64, 32.58, 328.47 Q 31.89, 318.30, 31.70, 308.13 Q 30.84, 297.96, 30.76, 287.79 Q 30.46, 277.62,\
            30.58, 267.45 Q 30.90, 257.28, 31.02, 247.11 Q 31.00, 236.93, 31.42, 226.76 Q 31.51, 216.59, 30.91, 206.42 Q 31.64, 196.25,\
            31.92, 186.08 Q 32.41, 175.91, 31.89, 165.74 Q 31.50, 155.57, 31.38, 145.39 Q 31.93, 135.22, 32.83, 125.05 Q 31.80, 114.88,\
            32.82, 104.71 Q 32.89, 94.54, 32.62, 84.37 Q 32.86, 74.20, 32.46, 64.03 Q 32.09, 53.86, 31.75, 43.68 Q 31.39, 33.51, 31.14,\
            23.34 Q 32.00, 13.17, 32.00, 3.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 23.00, 7.00 Q 33.12, 4.71, 43.24, 4.91 Q 53.35, 4.95, 63.47, 5.30 Q 73.59,\
            5.38, 83.71, 5.33 Q 93.82, 5.67, 103.94, 6.24 Q 114.06, 6.20, 124.18, 6.30 Q 134.29, 6.42, 144.41, 6.20 Q 154.53, 6.72, 164.65,\
            5.81 Q 174.76, 7.66, 184.88, 6.27 Q 195.00, 6.92, 205.12, 6.27 Q 215.24, 6.12, 225.35, 6.28 Q 235.47, 6.50, 245.59, 6.35 Q\
            255.71, 6.73, 265.82, 5.98 Q 275.94, 7.00, 286.06, 7.04 Q 296.18, 6.29, 306.29, 6.01 Q 316.41, 7.27, 326.53, 8.14 Q 336.65,\
            8.23, 346.76, 5.99 Q 356.88, 6.01, 367.00, 6.99 Q 377.12, 6.56, 387.24, 6.86 Q 397.35, 6.59, 407.47, 6.37 Q 417.59, 6.58,\
            427.71, 6.55 Q 437.82, 6.50, 447.94, 6.67 Q 458.06, 6.87, 468.18, 7.17 Q 478.29, 7.32, 488.41, 7.48 Q 498.53, 7.06, 508.65,\
            6.32 Q 518.76, 7.57, 528.88, 7.94 Q 539.00, 8.19, 549.12, 8.07 Q 559.24, 8.34, 569.35, 6.87 Q 579.47, 6.50, 589.59, 5.63 Q\
            599.71, 6.53, 609.82, 6.77 Q 619.94, 7.18, 630.06, 7.42 Q 640.18, 7.88, 650.29, 8.71 Q 660.41, 7.53, 670.53, 5.47 Q 680.65,\
            5.43, 690.77, 5.32 Q 700.88, 5.79, 711.00, 6.19 Q 721.12, 5.79, 731.24, 6.21 Q 741.35, 7.55, 751.47, 6.95 Q 761.59, 7.32,\
            771.71, 6.54 Q 781.82, 6.41, 791.94, 6.10 Q 802.06, 6.12, 812.18, 6.04 Q 822.29, 6.27, 832.41, 6.27 Q 842.53, 6.80, 852.65,\
            6.01 Q 862.77, 6.82, 872.88, 5.78 Q 883.00, 5.60, 893.12, 5.60 Q 903.24, 6.01, 913.35, 7.17 Q 923.47, 6.64, 933.59, 7.42 Q\
            943.71, 7.57, 953.82, 7.52 Q 963.94, 7.47, 974.06, 6.95 Q 984.18, 6.19, 994.30, 6.01 Q 1004.41, 8.01, 1014.53, 7.43 Q 1024.65,\
            7.42, 1034.77, 7.31 Q 1044.88, 7.35, 1055.00, 6.91 Q 1065.12, 6.70, 1075.24, 6.34 Q 1085.35, 6.73, 1095.47, 7.42 Q 1105.59,\
            6.51, 1115.71, 6.49 Q 1125.83, 6.44, 1135.94, 7.06 Q 1146.06, 7.31, 1156.18, 6.77 Q 1166.30, 6.93, 1176.41, 7.94 Q 1186.53,\
            8.29, 1196.65, 7.56 Q 1206.77, 6.98, 1216.88, 6.01 Q 1227.00, 5.30, 1237.12, 5.90 Q 1247.24, 6.75, 1257.35, 6.51 Q 1267.47,\
            6.48, 1277.59, 7.83 Q 1287.71, 7.93, 1297.83, 7.25 Q 1307.94, 5.84, 1318.06, 5.54 Q 1328.18, 6.63, 1338.30, 6.31 Q 1348.41,\
            6.21, 1358.53, 6.26 Q 1368.65, 5.99, 1378.77, 6.07 Q 1388.88, 5.97, 1399.84, 6.16 Q 1400.39, 16.71, 1400.52, 27.13 Q 1400.34,\
            37.42, 1399.70, 47.66 Q 1399.27, 57.85, 1397.87, 68.04 Q 1398.72, 78.20, 1398.88, 88.37 Q 1398.98, 98.54, 1398.81, 108.71\
            Q 1399.30, 118.88, 1399.72, 129.05 Q 1399.44, 139.22, 1399.26, 149.39 Q 1400.42, 159.57, 1400.63, 169.74 Q 1400.01, 179.91,\
            1400.27, 190.08 Q 1400.42, 200.25, 1400.46, 210.42 Q 1400.42, 220.59, 1400.12, 230.76 Q 1400.26, 240.93, 1400.11, 251.11 Q\
            1399.13, 261.28, 1398.62, 271.45 Q 1399.62, 281.62, 1400.30, 291.79 Q 1399.64, 301.96, 1399.32, 312.13 Q 1399.63, 322.30,\
            1400.77, 332.47 Q 1400.96, 342.64, 1401.04, 352.82 Q 1400.77, 362.99, 1400.60, 373.16 Q 1400.67, 383.33, 1400.15, 393.50 Q\
            1399.48, 403.67, 1398.29, 413.84 Q 1398.19, 424.01, 1399.27, 434.18 Q 1399.05, 444.36, 1398.84, 454.53 Q 1399.15, 464.70,\
            1399.67, 474.87 Q 1399.98, 485.04, 1400.13, 495.21 Q 1400.11, 505.38, 1400.13, 515.55 Q 1399.90, 525.72, 1399.89, 535.89 Q\
            1399.48, 546.07, 1399.43, 556.24 Q 1399.84, 566.41, 1399.59, 576.58 Q 1398.85, 586.75, 1398.34, 596.92 Q 1398.86, 607.09,\
            1399.71, 617.26 Q 1398.97, 627.43, 1399.11, 637.61 Q 1399.46, 647.78, 1399.55, 657.95 Q 1399.80, 668.12, 1400.23, 678.29 Q\
            1400.43, 688.46, 1400.56, 698.63 Q 1400.56, 708.80, 1400.33, 718.97 Q 1399.55, 729.15, 1399.74, 739.32 Q 1399.57, 749.49,\
            1399.21, 759.66 Q 1398.88, 769.83, 1398.69, 779.69 Q 1388.97, 780.26, 1378.88, 780.80 Q 1368.71, 780.90, 1358.55, 780.42 Q\
            1348.43, 780.81, 1338.30, 780.60 Q 1328.17, 778.78, 1318.06, 779.92 Q 1307.94, 779.09, 1297.83, 779.37 Q 1287.71, 781.23,\
            1277.59, 780.56 Q 1267.47, 780.16, 1257.35, 780.56 Q 1247.24, 780.41, 1237.12, 778.77 Q 1227.00, 778.60, 1216.88, 778.73 Q\
            1206.77, 779.73, 1196.65, 779.43 Q 1186.53, 779.06, 1176.41, 779.84 Q 1166.30, 780.03, 1156.18, 781.16 Q 1146.06, 780.05,\
            1135.94, 781.84 Q 1125.83, 781.61, 1115.71, 780.93 Q 1105.59, 780.64, 1095.47, 779.96 Q 1085.35, 780.22, 1075.24, 780.35 Q\
            1065.12, 780.55, 1055.00, 780.01 Q 1044.88, 779.06, 1034.77, 779.82 Q 1024.65, 779.74, 1014.53, 779.97 Q 1004.41, 779.49,\
            994.30, 779.57 Q 984.18, 779.69, 974.06, 780.97 Q 963.94, 780.54, 953.82, 781.13 Q 943.71, 780.56, 933.59, 779.21 Q 923.47,\
            779.08, 913.35, 779.53 Q 903.24, 779.78, 893.12, 779.76 Q 883.00, 780.38, 872.88, 779.80 Q 862.77, 780.69, 852.65, 780.21\
            Q 842.53, 779.21, 832.41, 779.61 Q 822.29, 779.46, 812.18, 780.22 Q 802.06, 780.35, 791.94, 781.50 Q 781.82, 780.94, 771.71,\
            781.39 Q 761.59, 781.57, 751.47, 781.59 Q 741.35, 781.69, 731.24, 781.45 Q 721.12, 780.42, 711.00, 779.35 Q 700.88, 780.35,\
            690.77, 779.76 Q 680.65, 779.33, 670.53, 779.16 Q 660.41, 779.89, 650.29, 780.65 Q 640.18, 781.26, 630.06, 780.73 Q 619.94,\
            779.47, 609.82, 780.24 Q 599.71, 779.38, 589.59, 779.35 Q 579.47, 779.63, 569.35, 780.44 Q 559.24, 780.82, 549.12, 780.70\
            Q 539.00, 780.84, 528.88, 780.43 Q 518.76, 781.05, 508.65, 780.53 Q 498.53, 781.86, 488.41, 782.00 Q 478.29, 782.14, 468.18,\
            782.01 Q 458.06, 781.78, 447.94, 780.70 Q 437.82, 780.87, 427.71, 781.57 Q 417.59, 780.76, 407.47, 782.00 Q 397.35, 782.24,\
            387.24, 781.94 Q 377.12, 781.87, 367.00, 781.21 Q 356.88, 780.79, 346.76, 780.58 Q 336.65, 780.99, 326.53, 781.30 Q 316.41,\
            781.82, 306.29, 781.50 Q 296.18, 781.08, 286.06, 780.13 Q 275.94, 779.69, 265.82, 779.05 Q 255.71, 779.08, 245.59, 779.76\
            Q 235.47, 780.08, 225.35, 781.37 Q 215.24, 781.07, 205.12, 780.76 Q 195.00, 779.99, 184.88, 779.92 Q 174.76, 779.35, 164.65,\
            779.55 Q 154.53, 781.23, 144.41, 781.96 Q 134.29, 782.28, 124.18, 782.32 Q 114.06, 782.41, 103.94, 782.22 Q 93.82, 782.17,\
            83.71, 781.81 Q 73.59, 781.72, 63.47, 781.79 Q 53.35, 782.01, 43.24, 782.07 Q 33.12, 782.02, 22.05, 780.95 Q 21.54, 770.32,\
            21.68, 759.85 Q 22.13, 749.55, 22.71, 739.33 Q 22.77, 729.15, 23.58, 718.97 Q 23.25, 708.80, 22.57, 698.63 Q 22.03, 688.46,\
            22.12, 678.29 Q 22.19, 668.12, 22.04, 657.95 Q 22.65, 647.78, 21.71, 637.61 Q 22.18, 627.43, 21.71, 617.26 Q 21.65, 607.09,\
            21.23, 596.92 Q 21.05, 586.75, 20.90, 576.58 Q 20.97, 566.41, 21.17, 556.24 Q 21.17, 546.07, 21.20, 535.89 Q 21.57, 525.72,\
            21.61, 515.55 Q 21.53, 505.38, 22.04, 495.21 Q 22.37, 485.04, 22.53, 474.87 Q 22.52, 464.70, 22.43, 454.53 Q 22.32, 444.36,\
            22.58, 434.18 Q 23.48, 424.01, 22.47, 413.84 Q 23.17, 403.67, 22.39, 393.50 Q 22.50, 383.33, 22.66, 373.16 Q 22.48, 362.99,\
            22.83, 352.82 Q 22.28, 342.64, 23.13, 332.47 Q 23.19, 322.30, 23.23, 312.13 Q 22.84, 301.96, 22.28, 291.79 Q 21.39, 281.62,\
            22.14, 271.45 Q 22.55, 261.28, 22.14, 251.11 Q 22.19, 240.93, 21.37, 230.76 Q 22.31, 220.59, 21.13, 210.42 Q 21.69, 200.25,\
            21.59, 190.08 Q 21.12, 179.91, 21.27, 169.74 Q 21.21, 159.57, 21.77, 149.39 Q 21.72, 139.22, 21.50, 129.05 Q 21.58, 118.88,\
            21.80, 108.71 Q 21.56, 98.54, 21.69, 88.37 Q 21.18, 78.20, 22.19, 68.03 Q 22.81, 57.86, 21.82, 47.68 Q 22.95, 37.51, 21.84,\
            27.34 Q 23.00, 17.17, 23.00, 7.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 40.00, 11.00 Q 50.12, 10.39, 60.24, 10.26 Q 70.35, 10.46, 80.47, 10.61 Q 90.59,\
            10.51, 100.71, 10.97 Q 110.82, 10.91, 120.94, 10.83 Q 131.06, 9.78, 141.18, 10.52 Q 151.29, 10.59, 161.41, 10.54 Q 171.53,\
            11.07, 181.65, 10.68 Q 191.76, 10.86, 201.88, 11.29 Q 212.00, 11.09, 222.12, 10.19 Q 232.24, 10.39, 242.35, 11.18 Q 252.47,\
            11.52, 262.59, 11.26 Q 272.71, 11.11, 282.82, 11.17 Q 292.94, 11.54, 303.06, 11.81 Q 313.18, 11.05, 323.29, 10.97 Q 333.41,\
            10.90, 343.53, 10.59 Q 353.65, 11.45, 363.76, 10.92 Q 373.88, 10.83, 384.00, 10.93 Q 394.12, 11.82, 404.24, 10.36 Q 414.35,\
            10.88, 424.47, 9.92 Q 434.59, 10.26, 444.71, 9.83 Q 454.82, 10.37, 464.94, 10.55 Q 475.06, 9.80, 485.18, 10.10 Q 495.29, 9.56,\
            505.41, 10.44 Q 515.53, 9.97, 525.65, 10.24 Q 535.76, 10.79, 545.88, 10.98 Q 556.00, 11.76, 566.12, 11.54 Q 576.24, 11.18,\
            586.35, 10.05 Q 596.47, 10.52, 606.59, 10.42 Q 616.71, 10.95, 626.82, 10.66 Q 636.94, 10.48, 647.06, 10.43 Q 657.18, 10.71,\
            667.29, 11.53 Q 677.41, 10.94, 687.53, 11.19 Q 697.65, 10.57, 707.77, 11.09 Q 717.88, 11.23, 728.00, 11.27 Q 738.12, 11.83,\
            748.24, 12.26 Q 758.35, 11.78, 768.47, 10.07 Q 778.59, 10.28, 788.71, 11.22 Q 798.82, 10.90, 808.94, 9.87 Q 819.06, 9.53,\
            829.18, 10.34 Q 839.29, 10.13, 849.41, 10.32 Q 859.53, 10.13, 869.65, 10.74 Q 879.77, 10.53, 889.88, 10.97 Q 900.00, 11.23,\
            910.12, 9.88 Q 920.24, 10.05, 930.35, 10.17 Q 940.47, 10.33, 950.59, 9.78 Q 960.71, 10.04, 970.82, 10.91 Q 980.94, 10.96,\
            991.06, 10.34 Q 1001.18, 10.02, 1011.30, 11.47 Q 1021.41, 11.00, 1031.53, 12.48 Q 1041.65, 11.18, 1051.77, 12.21 Q 1061.88,\
            11.98, 1072.00, 12.64 Q 1082.12, 12.43, 1092.24, 11.16 Q 1102.35, 11.33, 1112.47, 11.13 Q 1122.59, 10.78, 1132.71, 10.01 Q\
            1142.83, 10.18, 1152.94, 10.63 Q 1163.06, 10.84, 1173.18, 9.76 Q 1183.30, 9.04, 1193.41, 10.65 Q 1203.53, 10.37, 1213.65,\
            9.99 Q 1223.77, 9.76, 1233.88, 9.95 Q 1244.00, 9.98, 1254.12, 9.37 Q 1264.24, 9.22, 1274.35, 9.04 Q 1284.47, 9.43, 1294.59,\
            10.23 Q 1304.71, 10.02, 1314.83, 9.80 Q 1324.94, 10.38, 1335.06, 10.79 Q 1345.18, 10.72, 1355.30, 11.00 Q 1365.41, 10.01,\
            1375.53, 11.13 Q 1385.65, 12.08, 1395.77, 11.31 Q 1405.88, 11.13, 1416.07, 10.93 Q 1416.17, 21.11, 1415.64, 31.39 Q 1415.98,\
            41.51, 1416.13, 51.68 Q 1416.14, 61.85, 1416.28, 72.02 Q 1416.68, 82.19, 1416.95, 92.37 Q 1417.41, 102.54, 1416.73, 112.71\
            Q 1416.21, 122.88, 1416.57, 133.05 Q 1416.87, 143.22, 1417.33, 153.39 Q 1417.94, 163.57, 1417.68, 173.74 Q 1417.46, 183.91,\
            1417.82, 194.08 Q 1417.70, 204.25, 1417.25, 214.42 Q 1417.24, 224.59, 1418.14, 234.76 Q 1418.59, 244.93, 1417.24, 255.11 Q\
            1416.85, 265.28, 1416.46, 275.45 Q 1416.43, 285.62, 1415.61, 295.79 Q 1415.61, 305.96, 1416.03, 316.13 Q 1416.35, 326.30,\
            1415.48, 336.47 Q 1415.19, 346.64, 1415.36, 356.82 Q 1416.38, 366.99, 1417.41, 377.16 Q 1417.08, 387.33, 1417.59, 397.50 Q\
            1417.13, 407.67, 1417.60, 417.84 Q 1417.31, 428.01, 1416.79, 438.18 Q 1417.01, 448.36, 1417.14, 458.53 Q 1416.77, 468.70,\
            1417.08, 478.87 Q 1417.27, 489.04, 1417.32, 499.21 Q 1417.31, 509.38, 1417.26, 519.55 Q 1417.00, 529.72, 1416.91, 539.89 Q\
            1416.80, 550.07, 1416.96, 560.24 Q 1416.62, 570.41, 1416.32, 580.58 Q 1416.48, 590.75, 1417.32, 600.92 Q 1416.88, 611.09,\
            1415.23, 621.26 Q 1415.25, 631.43, 1416.75, 641.61 Q 1416.27, 651.78, 1415.64, 661.95 Q 1416.43, 672.12, 1416.29, 682.29 Q\
            1416.02, 692.46, 1415.57, 702.63 Q 1414.71, 712.80, 1414.78, 722.97 Q 1415.86, 733.15, 1416.67, 743.32 Q 1416.48, 753.49,\
            1416.82, 763.66 Q 1415.43, 773.83, 1416.59, 784.59 Q 1406.33, 785.34, 1395.92, 785.08 Q 1385.73, 785.16, 1375.58, 785.42 Q\
            1365.44, 785.40, 1355.31, 785.63 Q 1345.19, 785.84, 1335.06, 785.30 Q 1324.94, 784.88, 1314.83, 785.21 Q 1304.71, 785.47,\
            1294.59, 785.72 Q 1284.47, 785.76, 1274.36, 786.19 Q 1264.24, 785.01, 1254.12, 783.89 Q 1244.00, 784.07, 1233.88, 785.10 Q\
            1223.77, 785.77, 1213.65, 785.59 Q 1203.53, 785.69, 1193.41, 785.75 Q 1183.30, 785.37, 1173.18, 784.57 Q 1163.06, 784.44,\
            1152.94, 784.88 Q 1142.83, 785.49, 1132.71, 785.77 Q 1122.59, 785.74, 1112.47, 785.93 Q 1102.35, 785.59, 1092.24, 785.71 Q\
            1082.12, 785.82, 1072.00, 785.79 Q 1061.88, 784.91, 1051.77, 785.38 Q 1041.65, 785.48, 1031.53, 784.91 Q 1021.41, 785.54,\
            1011.30, 785.38 Q 1001.18, 784.92, 991.06, 784.76 Q 980.94, 784.90, 970.82, 784.97 Q 960.71, 785.15, 950.59, 785.02 Q 940.47,\
            784.62, 930.35, 783.84 Q 920.24, 783.78, 910.12, 785.08 Q 900.00, 785.45, 889.88, 784.79 Q 879.77, 783.41, 869.65, 783.96\
            Q 859.53, 784.33, 849.41, 784.43 Q 839.29, 784.58, 829.18, 784.55 Q 819.06, 784.34, 808.94, 785.25 Q 798.82, 784.57, 788.71,\
            785.43 Q 778.59, 784.77, 768.47, 784.58 Q 758.35, 785.39, 748.24, 785.20 Q 738.12, 785.90, 728.00, 786.05 Q 717.88, 785.82,\
            707.77, 785.85 Q 697.65, 785.70, 687.53, 785.77 Q 677.41, 785.73, 667.29, 785.72 Q 657.18, 785.94, 647.06, 785.46 Q 636.94,\
            785.36, 626.82, 786.05 Q 616.71, 786.08, 606.59, 786.02 Q 596.47, 785.96, 586.35, 784.48 Q 576.24, 785.06, 566.12, 784.89\
            Q 556.00, 784.62, 545.88, 784.93 Q 535.76, 785.02, 525.65, 785.03 Q 515.53, 784.59, 505.41, 785.69 Q 495.29, 785.46, 485.18,\
            785.22 Q 475.06, 785.06, 464.94, 784.42 Q 454.82, 784.09, 444.71, 784.89 Q 434.59, 784.84, 424.47, 785.52 Q 414.35, 785.76,\
            404.24, 785.73 Q 394.12, 785.78, 384.00, 785.43 Q 373.88, 785.15, 363.76, 785.68 Q 353.65, 785.79, 343.53, 785.72 Q 333.41,\
            785.78, 323.29, 784.76 Q 313.18, 784.41, 303.06, 784.66 Q 292.94, 784.40, 282.82, 783.99 Q 272.71, 784.62, 262.59, 784.81\
            Q 252.47, 784.86, 242.35, 784.85 Q 232.24, 785.08, 222.12, 784.71 Q 212.00, 784.50, 201.88, 784.55 Q 191.76, 784.91, 181.65,\
            784.95 Q 171.53, 785.18, 161.41, 784.58 Q 151.29, 784.57, 141.18, 784.60 Q 131.06, 784.59, 120.94, 783.99 Q 110.82, 784.02,\
            100.71, 784.35 Q 90.59, 784.79, 80.47, 785.35 Q 70.35, 784.72, 60.24, 784.59 Q 50.12, 784.87, 39.33, 784.67 Q 39.04, 774.15,\
            39.30, 763.76 Q 39.49, 753.52, 39.15, 743.34 Q 39.04, 733.16, 39.62, 722.98 Q 40.20, 712.80, 40.03, 702.63 Q 39.55, 692.46,\
            39.51, 682.29 Q 39.46, 672.12, 39.31, 661.95 Q 39.72, 651.78, 40.41, 641.61 Q 40.82, 631.43, 39.98, 621.26 Q 40.32, 611.09,\
            39.54, 600.92 Q 40.13, 590.75, 39.22, 580.58 Q 39.38, 570.41, 39.70, 560.24 Q 39.15, 550.07, 39.95, 539.89 Q 39.49, 529.72,\
            40.32, 519.55 Q 39.82, 509.38, 40.12, 499.21 Q 39.95, 489.04, 40.40, 478.87 Q 40.66, 468.70, 40.35, 458.53 Q 40.85, 448.36,\
            40.93, 438.18 Q 41.21, 428.01, 40.56, 417.84 Q 40.80, 407.67, 39.58, 397.50 Q 39.17, 387.33, 38.35, 377.16 Q 38.32, 366.99,\
            39.53, 356.82 Q 39.88, 346.64, 40.49, 336.47 Q 40.41, 326.30, 40.61, 316.13 Q 39.38, 305.96, 39.01, 295.79 Q 39.67, 285.62,\
            39.93, 275.45 Q 40.43, 265.28, 40.34, 255.11 Q 40.10, 244.93, 38.44, 234.76 Q 39.20, 224.59, 39.35, 214.42 Q 40.25, 204.25,\
            39.59, 194.08 Q 39.23, 183.91, 39.30, 173.74 Q 39.39, 163.57, 40.03, 153.39 Q 39.56, 143.22, 40.78, 133.05 Q 40.65, 122.88,\
            39.62, 112.71 Q 39.14, 102.54, 38.14, 92.37 Q 37.53, 82.20, 37.89, 72.03 Q 37.78, 61.86, 37.38, 51.68 Q 39.02, 41.51, 40.25,\
            31.34 Q 40.00, 21.17, 40.00, 11.00" style=" fill:white;"/>\
         </svg:svg>\
      </div>\
   </div>\
</div>');