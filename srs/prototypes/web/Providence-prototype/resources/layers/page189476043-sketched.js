rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page189476043-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page189476043" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page189476043-layer-1564809249" style="position: absolute; left: 274px; top: 0px; width: 1092px; height: 768px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="1564809249" data-review-reference-id="1564809249">\
            <div class="stencil-wrapper" style="width: 1092px; height: 768px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 768px;width:1092px;" width="1092" height="768">\
                     <svg:g width="1092" height="768"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.07, 0.75, 22.15, 0.56 Q 32.22, 0.71, 42.30, 0.61 Q\
                        52.37, 0.61, 62.44, 0.90 Q 72.52, 0.74, 82.59, 0.70 Q 92.67, 0.56, 102.74, 0.56 Q 112.81, 0.76, 122.89, 0.76 Q 132.96, 0.33,\
                        143.04, 0.60 Q 153.11, 0.56, 163.19, 0.92 Q 173.26, 0.76, 183.33, 0.69 Q 193.41, 0.55, 203.48, 0.51 Q 213.56, 0.48, 223.63,\
                        1.18 Q 233.70, 1.39, 243.78, 1.27 Q 253.85, 0.93, 263.93, 0.97 Q 274.00, 0.41, 284.07, 0.78 Q 294.15, 0.51, 304.22, 0.81 Q\
                        314.30, 1.29, 324.37, 1.55 Q 334.44, 1.42, 344.52, 1.32 Q 354.59, 1.21, 364.67, 1.38 Q 374.74, 0.73, 384.81, 0.77 Q 394.89,\
                        0.72, 404.96, 1.28 Q 415.04, 1.20, 425.11, 1.07 Q 435.18, 1.48, 445.26, 1.36 Q 455.33, 0.77, 465.41, 0.87 Q 475.48, 0.56,\
                        485.56, 0.74 Q 495.63, 1.05, 505.70, 1.32 Q 515.78, 1.22, 525.85, 1.59 Q 535.93, 0.39, 546.00, -0.07 Q 556.07, -0.32, 566.15,\
                        0.03 Q 576.22, 0.15, 586.30, -0.05 Q 596.37, 1.15, 606.44, 0.84 Q 616.52, 1.08, 626.59, 0.66 Q 636.67, 0.86, 646.74, 1.03\
                        Q 656.81, 0.98, 666.89, 1.32 Q 676.96, 1.42, 687.04, 1.15 Q 697.11, 1.03, 707.19, 0.84 Q 717.26, 0.80, 727.33, 1.22 Q 737.41,\
                        1.24, 747.48, 0.31 Q 757.56, 0.32, 767.63, 0.66 Q 777.70, 1.44, 787.78, 0.36 Q 797.85, 0.93, 807.93, 0.98 Q 818.00, 0.91,\
                        828.07, 0.84 Q 838.15, 1.36, 848.22, 1.13 Q 858.30, 0.71, 868.37, 1.96 Q 878.44, 1.62, 888.52, 2.78 Q 898.59, 1.21, 908.67,\
                        1.33 Q 918.74, 1.73, 928.82, 1.41 Q 938.89, 0.98, 948.96, 0.74 Q 959.04, 0.81, 969.11, 1.07 Q 979.19, 1.63, 989.26, 1.90 Q\
                        999.33, 1.95, 1009.41, 2.23 Q 1019.48, 1.97, 1029.56, 1.92 Q 1039.63, 1.71, 1049.70, 3.13 Q 1059.78, 1.91, 1069.85, 1.76 Q\
                        1079.93, 0.55, 1090.97, 1.03 Q 1091.17, 11.66, 1091.59, 21.88 Q 1091.48, 32.06, 1091.31, 42.17 Q 1090.41, 52.26, 1090.61,\
                        62.31 Q 1090.66, 72.37, 1091.37, 82.42 Q 1090.68, 92.47, 1089.45, 102.53 Q 1089.71, 112.58, 1090.06, 122.63 Q 1090.91, 132.68,\
                        1089.56, 142.74 Q 1089.54, 152.79, 1090.88, 162.84 Q 1090.24, 172.89, 1090.69, 182.95 Q 1090.91, 193.00, 1090.88, 203.05 Q\
                        1091.37, 213.11, 1091.61, 223.16 Q 1091.16, 233.21, 1090.98, 243.26 Q 1089.76, 253.32, 1089.88, 263.37 Q 1090.48, 273.42,\
                        1090.67, 283.47 Q 1091.09, 293.53, 1090.81, 303.58 Q 1091.03, 313.63, 1091.15, 323.68 Q 1090.66, 333.74, 1091.44, 343.79 Q\
                        1090.58, 353.84, 1091.02, 363.89 Q 1090.43, 373.95, 1090.93, 384.00 Q 1091.75, 394.05, 1092.27, 404.11 Q 1091.56, 414.16,\
                        1091.81, 424.21 Q 1091.08, 434.26, 1091.48, 444.32 Q 1090.96, 454.37, 1090.19, 464.42 Q 1090.42, 474.47, 1090.92, 484.53 Q\
                        1091.02, 494.58, 1090.18, 504.63 Q 1090.24, 514.68, 1090.66, 524.74 Q 1090.81, 534.79, 1090.59, 544.84 Q 1090.65, 554.89,\
                        1091.31, 564.95 Q 1090.64, 575.00, 1090.28, 585.05 Q 1090.17, 595.11, 1090.27, 605.16 Q 1090.78, 615.21, 1090.94, 625.26 Q\
                        1090.69, 635.32, 1090.43, 645.37 Q 1090.88, 655.42, 1091.43, 665.47 Q 1090.47, 675.53, 1089.57, 685.58 Q 1090.07, 695.63,\
                        1091.01, 705.68 Q 1091.29, 715.74, 1091.10, 725.79 Q 1090.66, 735.84, 1090.83, 745.89 Q 1091.34, 755.95, 1090.70, 766.70 Q\
                        1080.22, 766.87, 1070.06, 767.45 Q 1059.88, 767.59, 1049.77, 767.87 Q 1039.66, 768.01, 1029.57, 767.45 Q 1019.49, 767.73,\
                        1009.41, 767.63 Q 999.34, 767.61, 989.26, 766.49 Q 979.19, 766.17, 969.11, 766.26 Q 959.04, 766.55, 948.96, 766.17 Q 938.89,\
                        765.75, 928.82, 765.60 Q 918.74, 765.62, 908.67, 765.78 Q 898.59, 765.94, 888.52, 766.01 Q 878.44, 766.39, 868.37, 766.25\
                        Q 858.30, 765.17, 848.22, 764.71 Q 838.15, 765.87, 828.07, 766.60 Q 818.00, 766.59, 807.93, 765.59 Q 797.85, 766.29, 787.78,\
                        766.39 Q 777.70, 766.75, 767.63, 767.10 Q 757.56, 766.66, 747.48, 766.81 Q 737.41, 767.25, 727.33, 767.29 Q 717.26, 767.73,\
                        707.19, 767.35 Q 697.11, 767.32, 687.04, 766.64 Q 676.96, 765.94, 666.89, 765.56 Q 656.81, 766.24, 646.74, 766.45 Q 636.67,\
                        767.00, 626.59, 766.12 Q 616.52, 766.01, 606.44, 766.92 Q 596.37, 767.11, 586.30, 766.74 Q 576.22, 766.48, 566.15, 765.49\
                        Q 556.07, 766.02, 546.00, 766.28 Q 535.93, 766.04, 525.85, 765.81 Q 515.78, 765.53, 505.70, 767.22 Q 495.63, 766.14, 485.56,\
                        765.06 Q 475.48, 765.32, 465.41, 765.99 Q 455.33, 766.34, 445.26, 767.85 Q 435.18, 767.73, 425.11, 766.82 Q 415.04, 767.28,\
                        404.96, 766.95 Q 394.89, 767.67, 384.81, 767.40 Q 374.74, 767.14, 364.67, 767.43 Q 354.59, 768.15, 344.52, 768.35 Q 334.44,\
                        768.48, 324.37, 768.51 Q 314.30, 768.49, 304.22, 767.58 Q 294.15, 767.63, 284.07, 767.39 Q 274.00, 766.01, 263.93, 765.78\
                        Q 253.85, 766.37, 243.78, 765.42 Q 233.70, 766.66, 223.63, 766.35 Q 213.56, 766.59, 203.48, 766.17 Q 193.41, 766.70, 183.33,\
                        767.05 Q 173.26, 766.99, 163.19, 766.69 Q 153.11, 766.45, 143.04, 767.12 Q 132.96, 766.90, 122.89, 767.26 Q 112.81, 766.58,\
                        102.74, 765.87 Q 92.67, 767.25, 82.59, 767.43 Q 72.52, 766.63, 62.44, 766.13 Q 52.37, 766.89, 42.30, 766.89 Q 32.22, 766.33,\
                        22.15, 767.61 Q 12.07, 767.60, 0.94, 767.06 Q 0.49, 756.45, 0.62, 746.09 Q 0.70, 735.93, 0.11, 725.85 Q 1.24, 715.75, 1.87,\
                        705.69 Q 1.54, 695.63, 1.18, 685.58 Q 0.93, 675.53, 1.72, 665.47 Q 1.69, 655.42, 1.23, 645.37 Q 1.49, 635.32, 1.21, 625.26\
                        Q 2.32, 615.21, 1.24, 605.16 Q 2.14, 595.11, 1.31, 585.05 Q 1.75, 575.00, 2.10, 564.95 Q 0.93, 554.89, 1.25, 544.84 Q 0.72,\
                        534.79, 1.11, 524.74 Q 0.51, 514.68, 0.71, 504.63 Q 1.35, 494.58, 1.16, 484.53 Q 0.91, 474.47, 1.26, 464.42 Q 0.74, 454.37,\
                        0.78, 444.32 Q 1.17, 434.26, 0.78, 424.21 Q 0.91, 414.16, -0.00, 404.11 Q -0.20, 394.05, 0.19, 384.00 Q 1.53, 373.95, 0.55,\
                        363.89 Q 0.85, 353.84, 0.50, 343.79 Q 0.34, 333.74, 0.22, 323.68 Q 0.18, 313.63, 0.18, 303.58 Q 1.58, 293.53, 0.85, 283.47\
                        Q 1.46, 273.42, 0.87, 263.37 Q 0.90, 253.32, 0.46, 243.26 Q 0.28, 233.21, 0.21, 223.16 Q 0.15, 213.11, 0.20, 203.05 Q 0.79,\
                        193.00, 1.63, 182.95 Q 2.06, 172.89, 1.82, 162.84 Q 1.38, 152.79, 1.56, 142.74 Q 1.86, 132.68, 1.44, 122.63 Q 1.66, 112.58,\
                        1.51, 102.53 Q 1.78, 92.47, 1.00, 82.42 Q 0.98, 72.37, 0.85, 62.32 Q 0.81, 52.26, 1.13, 42.21 Q 1.09, 32.16, 0.96, 22.11 Q\
                        2.00, 12.05, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 11.25, 6.36, 19.22, 12.53 Q 27.38, 18.44, 35.39, 24.56 Q 43.40,\
                        30.68, 51.79, 36.25 Q 60.23, 41.76, 68.16, 47.99 Q 76.36, 53.84, 84.56, 59.69 Q 92.90, 65.34, 101.04, 71.27 Q 109.03, 77.41,\
                        117.47, 82.92 Q 126.14, 88.10, 134.25, 94.08 Q 142.80, 99.43, 151.14, 105.07 Q 159.22, 111.09, 167.02, 117.51 Q 175.60, 122.82,\
                        183.47, 129.14 Q 191.89, 134.68, 199.92, 140.76 Q 208.10, 146.64, 216.20, 152.63 Q 224.50, 158.33, 232.87, 163.94 Q 241.34,\
                        169.41, 249.78, 174.92 Q 257.61, 181.30, 266.28, 186.48 Q 274.81, 191.84, 283.31, 197.27 Q 291.30, 203.41, 299.85, 208.76\
                        Q 308.25, 214.33, 315.86, 221.02 Q 323.84, 227.17, 331.66, 233.56 Q 340.02, 239.19, 348.80, 244.21 Q 357.19, 249.79, 364.68,\
                        256.65 Q 372.80, 262.61, 381.48, 267.77 Q 389.55, 273.80, 397.74, 279.67 Q 405.49, 286.16, 414.37, 291.05 Q 422.93, 296.38,\
                        431.04, 302.36 Q 438.97, 308.59, 447.29, 314.26 Q 455.47, 320.15, 463.32, 326.48 Q 471.79, 331.95, 479.87, 337.96 Q 488.60,\
                        343.06, 496.94, 348.71 Q 505.31, 354.32, 512.94, 360.98 Q 521.13, 366.83, 530.07, 371.63 Q 538.62, 376.99, 546.80, 382.86\
                        Q 555.11, 388.55, 563.43, 394.22 Q 571.51, 400.24, 579.45, 406.46 Q 588.06, 411.73, 596.40, 417.39 Q 604.70, 423.09, 612.77,\
                        429.13 Q 620.77, 435.25, 628.20, 442.20 Q 636.63, 447.72, 644.87, 453.51 Q 653.26, 459.09, 661.33, 465.12 Q 669.45, 471.09,\
                        677.89, 476.60 Q 686.26, 482.20, 694.49, 488.00 Q 703.01, 493.39, 711.54, 498.78 Q 719.29, 505.26, 727.46, 511.15 Q 735.44,\
                        517.32, 743.62, 523.19 Q 752.14, 528.59, 760.14, 534.72 Q 768.42, 540.45, 776.77, 546.09 Q 784.92, 552.00, 793.74, 556.98\
                        Q 801.63, 563.26, 809.66, 569.36 Q 818.33, 574.53, 827.07, 579.60 Q 835.56, 585.05, 843.69, 590.99 Q 851.80, 596.96, 859.89,\
                        602.97 Q 868.13, 608.76, 876.06, 615.00 Q 884.44, 620.59, 892.90, 626.07 Q 900.88, 632.23, 909.37, 637.66 Q 917.58, 643.50,\
                        925.31, 650.02 Q 933.78, 655.48, 942.18, 661.04 Q 950.17, 667.20, 958.73, 672.53 Q 966.46, 679.04, 975.60, 683.56 Q 983.41,\
                        689.96, 991.17, 696.44 Q 999.42, 702.22, 1007.81, 707.79 Q 1015.85, 713.87, 1023.87, 719.97 Q 1032.08, 725.81, 1040.61, 731.18\
                        Q 1048.70, 737.18, 1057.82, 741.72 Q 1066.13, 747.41, 1074.69, 752.76 Q 1081.76, 760.21, 1090.00, 766.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 766.00 Q 9.06, 758.51, 17.79, 753.39 Q 26.42, 748.13, 34.94, 742.72 Q\
                        43.03, 736.69, 51.58, 731.32 Q 60.15, 725.98, 68.26, 719.97 Q 76.16, 713.69, 84.39, 707.86 Q 92.14, 701.35, 100.37, 695.52\
                        Q 109.04, 690.31, 117.38, 684.64 Q 125.13, 678.14, 134.07, 673.33 Q 142.22, 667.38, 150.39, 661.47 Q 158.88, 656.02, 167.41,\
                        650.62 Q 175.49, 644.57, 183.15, 637.93 Q 191.30, 631.98, 199.48, 626.10 Q 207.59, 620.09, 216.41, 615.11 Q 224.55, 609.15,\
                        232.75, 603.28 Q 240.93, 597.38, 248.52, 590.64 Q 257.64, 586.09, 265.79, 580.14 Q 273.87, 574.11, 281.99, 568.11 Q 289.99,\
                        561.96, 298.58, 556.65 Q 307.10, 551.23, 315.07, 545.03 Q 323.45, 539.42, 331.87, 533.87 Q 339.95, 527.82, 348.33, 522.21\
                        Q 356.55, 516.37, 364.64, 510.35 Q 372.53, 504.03, 380.84, 498.32 Q 389.76, 493.47, 397.60, 487.09 Q 405.28, 480.49, 413.85,\
                        475.13 Q 422.32, 469.65, 430.87, 464.29 Q 439.03, 458.36, 447.14, 452.36 Q 455.66, 446.95, 464.19, 441.54 Q 472.93, 436.45,\
                        480.87, 430.20 Q 488.30, 423.24, 496.17, 416.89 Q 504.74, 411.55, 513.71, 406.78 Q 522.15, 401.26, 530.09, 395.01 Q 538.17,\
                        388.97, 546.54, 383.34 Q 555.05, 377.91, 563.05, 371.77 Q 571.28, 365.94, 579.32, 359.83 Q 587.54, 353.99, 596.60, 349.34\
                        Q 605.19, 344.04, 613.44, 338.24 Q 621.38, 332.00, 629.50, 326.01 Q 637.80, 320.29, 645.81, 314.15 Q 654.55, 309.04, 662.12,\
                        302.27 Q 670.25, 296.31, 678.88, 291.04 Q 687.14, 285.27, 695.63, 279.81 Q 704.11, 274.34, 712.15, 268.24 Q 720.08, 261.99,\
                        728.53, 256.47 Q 736.46, 250.21, 744.61, 244.28 Q 753.05, 238.75, 761.19, 232.79 Q 769.93, 227.69, 778.67, 222.59 Q 786.71,\
                        216.49, 794.48, 210.02 Q 802.30, 203.60, 810.32, 197.47 Q 818.68, 191.83, 827.36, 186.65 Q 835.55, 180.76, 844.04, 175.30\
                        Q 852.78, 170.20, 861.05, 164.44 Q 869.02, 158.23, 877.49, 152.74 Q 885.69, 146.88, 893.33, 140.21 Q 902.11, 135.17, 910.33,\
                        129.33 Q 918.21, 123.00, 926.38, 117.09 Q 934.50, 111.10, 942.99, 105.65 Q 951.37, 100.04, 959.42, 93.95 Q 967.23, 87.52,\
                        975.97, 82.43 Q 984.36, 76.82, 992.22, 70.47 Q 1000.20, 64.28, 1008.60, 58.70 Q 1016.59, 52.54, 1024.91, 46.83 Q 1033.13,\
                        41.00, 1041.32, 35.11 Q 1050.12, 30.09, 1058.04, 23.82 Q 1066.16, 17.84, 1074.39, 12.02 Q 1083.74, 7.79, 1092.00, 2.00" style="\
                        fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-598702032" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 768px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="598702032" data-review-reference-id="598702032">\
            <div class="stencil-wrapper" style="width: 275px; height: 768px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 768px;width:275px;" width="275" height="768">\
                     <svg:g width="275" height="768"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.42, 0.42, 22.85, 0.25 Q 33.27, 0.35, 43.69, 0.29 Q 54.12, 0.67,\
                        64.54, 0.42 Q 74.96, 1.76, 85.38, 1.01 Q 95.81, 0.69, 106.23, 0.63 Q 116.65, 0.07, 127.08, 0.07 Q 137.50, 1.84, 147.92, 2.36\
                        Q 158.35, 1.08, 168.77, 0.98 Q 179.19, 0.02, 189.62, 0.03 Q 200.04, 0.04, 210.46, 0.36 Q 220.88, 0.63, 231.31, 0.03 Q 241.73,\
                        -0.11, 252.15, -0.48 Q 262.58, 0.28, 273.50, 1.50 Q 273.37, 11.93, 273.22, 22.07 Q 273.62, 32.12, 273.17, 42.21 Q 272.80,\
                        52.27, 272.80, 62.32 Q 273.64, 72.37, 274.28, 82.42 Q 273.72, 92.47, 274.01, 102.53 Q 274.22, 112.58, 273.96, 122.63 Q 274.56,\
                        132.68, 274.52, 142.74 Q 274.73, 152.79, 274.81, 162.84 Q 274.85, 172.89, 274.99, 182.95 Q 274.39, 193.00, 274.55, 203.05\
                        Q 273.84, 213.11, 274.60, 223.16 Q 274.37, 233.21, 274.14, 243.26 Q 274.25, 253.32, 274.39, 263.37 Q 274.09, 273.42, 275.01,\
                        283.47 Q 274.62, 293.53, 273.85, 303.58 Q 273.99, 313.63, 273.90, 323.68 Q 273.36, 333.74, 273.36, 343.79 Q 272.58, 353.84,\
                        273.66, 363.89 Q 273.02, 373.95, 273.83, 384.00 Q 273.56, 394.05, 273.36, 404.11 Q 273.96, 414.16, 273.55, 424.21 Q 272.67,\
                        434.26, 273.32, 444.32 Q 274.21, 454.37, 273.92, 464.42 Q 273.52, 474.47, 273.63, 484.53 Q 273.68, 494.58, 273.77, 504.63\
                        Q 274.29, 514.68, 274.73, 524.74 Q 275.16, 534.79, 274.68, 544.84 Q 273.71, 554.89, 274.64, 564.95 Q 274.44, 575.00, 274.20,\
                        585.05 Q 274.68, 595.11, 274.97, 605.16 Q 275.17, 615.21, 275.25, 625.26 Q 274.96, 635.32, 274.17, 645.37 Q 274.33, 655.42,\
                        274.15, 665.47 Q 273.03, 675.53, 273.19, 685.58 Q 274.26, 695.63, 274.05, 705.68 Q 273.52, 715.74, 273.70, 725.79 Q 273.71,\
                        735.84, 273.49, 745.89 Q 272.93, 755.95, 273.32, 766.32 Q 263.06, 767.44, 252.27, 766.80 Q 241.77, 766.53, 231.32, 766.44\
                        Q 220.90, 766.91, 210.47, 767.57 Q 200.04, 767.19, 189.62, 767.45 Q 179.19, 766.66, 168.77, 766.44 Q 158.35, 766.86, 147.92,\
                        767.19 Q 137.50, 766.88, 127.08, 767.04 Q 116.65, 767.32, 106.23, 766.35 Q 95.81, 766.68, 85.38, 765.80 Q 74.96, 764.53, 64.54,\
                        765.29 Q 54.12, 765.63, 43.69, 766.81 Q 33.27, 766.59, 22.85, 766.03 Q 12.42, 766.27, 1.56, 766.44 Q 1.49, 756.12, 1.48, 745.97\
                        Q 1.63, 735.87, 2.19, 725.78 Q 1.76, 715.74, 0.89, 705.69 Q 0.97, 695.64, 1.84, 685.58 Q 2.64, 675.53, 3.25, 665.47 Q 2.05,\
                        655.42, 2.12, 645.37 Q 1.49, 635.32, 0.83, 625.26 Q 1.38, 615.21, 1.45, 605.16 Q 1.49, 595.11, 1.93, 585.05 Q 1.96, 575.00,\
                        2.15, 564.95 Q 1.75, 554.89, 2.11, 544.84 Q 1.30, 534.79, 1.36, 524.74 Q 1.57, 514.68, 1.09, 504.63 Q 0.94, 494.58, 1.11,\
                        484.53 Q 0.32, 474.47, -0.28, 464.42 Q 0.19, 454.37, 0.85, 444.32 Q 0.93, 434.26, 2.16, 424.21 Q 2.35, 414.16, 0.78, 404.11\
                        Q 1.53, 394.05, 1.83, 384.00 Q 1.72, 373.95, 1.91, 363.89 Q 1.46, 353.84, 0.68, 343.79 Q 0.27, 333.74, 0.47, 323.68 Q 0.29,\
                        313.63, 0.09, 303.58 Q 0.01, 293.53, -0.03, 283.47 Q 0.44, 273.42, 0.60, 263.37 Q 1.61, 253.32, 1.77, 243.26 Q 1.25, 233.21,\
                        1.16, 223.16 Q 0.73, 213.11, 0.44, 203.05 Q 0.38, 193.00, 1.15, 182.95 Q 0.89, 172.89, 0.83, 162.84 Q 0.99, 152.79, 0.90,\
                        142.74 Q 0.86, 132.68, 1.54, 122.63 Q 2.30, 112.58, 2.16, 102.53 Q 1.77, 92.47, 1.48, 82.42 Q 1.45, 72.37, 1.41, 62.32 Q 1.65,\
                        52.26, 1.93, 42.21 Q 2.68, 32.16, 3.13, 22.11 Q 2.00, 12.05, 2.00, 2.00" style=" fill:#C1C1C1;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-606212298" style="position: absolute; left: 355px; top: 130px; width: 930px; height: 30px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="606212298" data-review-reference-id="606212298">\
            <div class="stencil-wrapper" style="width: 930px; height: 30px">\
               <div id="606212298-1510572249" style="position: absolute; left: 0px; top: 0px; width: 1335px; height: 665px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1510572249" data-review-reference-id="1510572249">\
                  <div class="stencil-wrapper" style="width: 1335px; height: 665px">\
                     <div id="1510572249-1186813712" style="position: absolute; left: 0px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1186813712" data-review-reference-id="1186813712">\
                        <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                           <div title="">\
                              <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                                 <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                                    <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 5.43, 25.50, 5.73, 19.00 Q 5.52, 17.50, 5.74, 15.70 Q 7.04, 14.83,\
                                       6.65, 13.42 Q 6.88, 12.25, 8.38, 11.40 Q 9.92, 11.40, 10.71, 10.52 Q 11.44, 9.48, 12.38, 8.58 Q 13.92, 8.00, 15.68, 7.26 Q\
                                       27.02, 7.42, 38.27, 7.65 Q 49.46, 7.06, 60.65, 7.85 Q 71.82, 7.27, 83.00, 7.35 Q 94.16, 7.26, 105.33, 7.42 Q 116.50, 7.87,\
                                       127.67, 7.93 Q 138.83, 8.56, 150.24, 7.54 Q 151.93, 7.76, 153.66, 8.22 Q 154.94, 8.34, 155.39, 10.17 Q 156.55, 10.36, 157.61,\
                                       11.38 Q 158.39, 12.36, 159.72, 12.97 Q 159.97, 14.19, 159.96, 15.58 Q 160.90, 16.96, 161.89, 18.65 Q 161.55, 25.36, 160.72,\
                                       32.67 Q 149.29, 33.06, 138.04, 33.31 Q 126.88, 33.41, 115.74, 32.87 Q 104.65, 32.63, 93.58, 32.71 Q 82.50, 32.20, 71.43, 32.37\
                                       Q 60.36, 32.10, 49.29, 31.67 Q 38.21, 32.19, 27.14, 33.11 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                                    </svg:g>\
                                 </svg:g>\
                              </svg:svg>\
                              <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1186813712\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1186813712\', \'result\');" class="selected">\
                                 <div class="smallSkechtedTab">\
                                    <div id="1186813712_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">RFPL\
                                       							\
                                       <addMouseOverListener></addMouseOverListener>\
                                       							\
                                       <addMouseOutListener></addMouseOutListener>\
                                       						\
                                    </div>\
                                 </div>\
                                 <div class="bigSkechtedTab">\
                                    <div id="1186813712_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">RFPL\
                                       							\
                                       <addMouseOverListener></addMouseOverListener>\
                                       							\
                                       <addMouseOutListener></addMouseOutListener>\
                                       						\
                                    </div>\
                                 </div>\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="606212298-882477332" style="position: absolute; left: 155px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="882477332" data-review-reference-id="882477332">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 7.82, 25.50, 7.56, 19.00 Q 7.34, 17.50, 6.54, 15.89 Q 7.56, 15.02,\
                                 8.10, 14.04 Q 8.03, 12.78, 8.84, 11.85 Q 9.46, 10.75, 10.87, 10.78 Q 11.76, 10.06, 12.77, 9.47 Q 14.12, 8.50, 15.73, 7.54\
                                 Q 27.05, 7.76, 38.30, 8.24 Q 49.46, 7.33, 60.65, 7.52 Q 71.83, 8.67, 83.00, 8.47 Q 94.17, 8.73, 105.33, 8.54 Q 116.50, 8.29,\
                                 127.67, 8.11 Q 138.83, 7.87, 150.20, 7.79 Q 151.84, 8.13, 153.40, 8.90 Q 154.15, 10.16, 155.01, 10.97 Q 155.96, 11.58, 156.66,\
                                 12.35 Q 157.36, 13.10, 158.61, 13.63 Q 159.69, 14.34, 160.33, 15.42 Q 160.54, 17.10, 161.34, 18.75 Q 161.31, 25.38, 160.68,\
                                 32.63 Q 149.28, 33.03, 137.93, 32.54 Q 126.82, 32.47, 115.75, 33.01 Q 104.66, 33.08, 93.58, 33.50 Q 82.50, 33.24, 71.43, 33.33\
                                 Q 60.36, 33.48, 49.29, 33.45 Q 38.21, 33.42, 27.14, 33.28 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'882477332\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'882477332\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="882477332_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="882477332_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="606212298-746476258" style="position: absolute; left: 310px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="746476258" data-review-reference-id="746476258">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 6.25, 25.50, 6.06, 19.00 Q 6.28, 17.50, 6.52, 15.89 Q 7.06, 14.84,\
                                 8.12, 14.05 Q 7.98, 12.76, 9.12, 12.11 Q 10.13, 11.68, 11.01, 11.01 Q 12.10, 10.69, 13.38, 10.86 Q 14.74, 10.12, 15.98, 8.91\
                                 Q 27.16, 8.95, 38.36, 9.59 Q 49.53, 10.20, 60.67, 9.64 Q 71.83, 8.47, 83.00, 8.61 Q 94.17, 8.71, 105.33, 8.48 Q 116.50, 9.20,\
                                 127.67, 9.80 Q 138.83, 8.73, 149.95, 9.33 Q 151.46, 9.65, 152.84, 10.43 Q 154.16, 10.12, 155.00, 11.01 Q 155.68, 12.15, 156.72,\
                                 12.28 Q 156.82, 13.49, 157.90, 14.06 Q 157.86, 15.35, 159.45, 15.80 Q 160.82, 16.99, 161.53, 18.72 Q 160.97, 25.41, 160.47,\
                                 32.44 Q 149.27, 33.02, 138.01, 33.07 Q 126.84, 32.79, 115.76, 33.54 Q 104.66, 33.29, 93.58, 32.66 Q 82.50, 31.59, 71.43, 30.63\
                                 Q 60.36, 30.69, 49.29, 32.47 Q 38.21, 31.83, 27.14, 31.76 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'746476258\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'746476258\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="746476258_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="746476258_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="606212298-1085355756" style="position: absolute; left: 465px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1085355756" data-review-reference-id="1085355756">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 7.09, 25.50, 7.28, 19.00 Q 6.52, 17.50, 7.58, 16.14 Q 7.24, 14.90,\
                                 7.31, 13.70 Q 8.56, 13.03, 9.92, 12.90 Q 10.91, 12.76, 11.08, 11.14 Q 11.28, 9.19, 12.27, 8.33 Q 14.00, 8.21, 15.81, 7.95\
                                 Q 27.08, 8.09, 38.36, 9.67 Q 49.52, 9.85, 60.68, 9.85 Q 71.84, 9.46, 83.00, 8.61 Q 94.16, 7.45, 105.33, 6.87 Q 116.50, 6.75,\
                                 127.67, 7.31 Q 138.83, 8.14, 150.06, 8.63 Q 151.56, 9.26, 153.26, 9.31 Q 154.54, 9.25, 155.84, 9.21 Q 156.86, 9.71, 157.45,\
                                 11.55 Q 157.83, 12.76, 158.50, 13.70 Q 159.09, 14.68, 159.66, 15.71 Q 159.44, 17.52, 160.28, 18.95 Q 160.08, 25.49, 159.94,\
                                 31.94 Q 149.04, 32.33, 137.94, 32.59 Q 126.86, 33.10, 115.76, 33.39 Q 104.68, 34.26, 93.59, 34.43 Q 82.51, 33.61, 71.43, 33.85\
                                 Q 60.36, 33.94, 49.29, 33.25 Q 38.21, 34.16, 27.14, 33.92 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1085355756\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1085355756\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1085355756_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1085355756_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="606212298-1128455295" style="position: absolute; left: 620px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1128455295" data-review-reference-id="1128455295">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 7.93, 25.50, 7.08, 19.00 Q 6.51, 17.50, 6.69, 15.93 Q 7.48, 14.99,\
                                 7.44, 13.76 Q 8.30, 12.91, 8.84, 11.85 Q 9.67, 11.04, 10.74, 10.57 Q 11.69, 9.94, 12.54, 8.95 Q 14.21, 8.73, 15.96, 8.79 Q\
                                 27.09, 8.19, 38.32, 8.68 Q 49.47, 7.77, 60.66, 8.13 Q 71.83, 8.58, 83.00, 8.42 Q 94.17, 8.87, 105.33, 8.83 Q 116.50, 8.83,\
                                 127.67, 8.69 Q 138.83, 8.41, 150.05, 8.70 Q 151.53, 9.36, 152.96, 10.11 Q 154.07, 10.33, 155.53, 9.86 Q 156.50, 10.46, 157.43,\
                                 11.56 Q 157.99, 12.65, 158.41, 13.75 Q 159.94, 14.21, 160.02, 15.56 Q 160.43, 17.14, 161.08, 18.80 Q 160.64, 25.44, 160.55,\
                                 32.51 Q 149.40, 33.38, 137.97, 32.79 Q 126.85, 32.95, 115.74, 32.89 Q 104.66, 33.18, 93.57, 32.33 Q 82.50, 31.75, 71.43, 32.33\
                                 Q 60.36, 32.89, 49.29, 32.89 Q 38.21, 32.16, 27.14, 32.47 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1128455295\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1128455295\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1128455295_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1128455295_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="606212298-1377395495" style="position: absolute; left: 775px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1377395495" data-review-reference-id="1377395495">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 5.00, 25.50, 4.84, 19.00 Q 4.94, 17.50, 5.42, 15.63 Q 5.58, 14.30,\
                                 6.87, 13.51 Q 7.75, 12.65, 8.49, 11.50 Q 9.32, 10.56, 9.95, 9.26 Q 11.00, 8.67, 12.67, 9.26 Q 14.63, 9.85, 16.00, 8.98 Q 27.06,\
                                 7.81, 38.34, 9.17 Q 49.50, 8.86, 60.66, 8.50 Q 71.83, 8.22, 83.00, 7.57 Q 94.16, 7.14, 105.33, 7.42 Q 116.50, 8.22, 127.67,\
                                 8.51 Q 138.83, 9.33, 149.93, 9.41 Q 151.50, 9.52, 153.19, 9.48 Q 154.52, 9.29, 155.78, 9.33 Q 156.53, 10.40, 157.32, 11.68\
                                 Q 158.28, 12.44, 159.23, 13.26 Q 160.34, 13.99, 160.72, 15.25 Q 160.92, 16.95, 161.13, 18.79 Q 161.11, 25.40, 160.56, 32.52\
                                 Q 149.23, 32.91, 137.99, 32.94 Q 126.87, 33.32, 115.77, 33.61 Q 104.67, 33.58, 93.58, 33.69 Q 82.51, 33.76, 71.43, 33.68 Q\
                                 60.36, 33.12, 49.29, 32.64 Q 38.21, 32.78, 27.14, 33.02 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1377395495\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1377395495\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1377395495_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1377395495_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-1840063710" style="position: absolute; left: 275px; top: 160px; width: 1091px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1840063710" data-review-reference-id="1840063710">\
            <div class="stencil-wrapper" style="width: 1091px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px;width:1091px;" width="1091" height="465">\
                     <svg:g width="1091" height="465"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.06, 1.07, 22.13, 1.06 Q 32.19, 0.91, 42.26, 0.87 Q 52.32, 0.84,\
                        62.39, 1.07 Q 72.45, 1.01, 82.52, 0.88 Q 92.58, 0.87, 102.65, 0.83 Q 112.71, 0.78, 122.78, 1.46 Q 132.84, 1.57, 142.91, 0.87\
                        Q 152.97, 1.99, 163.04, 0.75 Q 173.10, 0.68, 183.17, 0.58 Q 193.23, 0.55, 203.30, 0.52 Q 213.36, 0.73, 223.43, 0.93 Q 233.49,\
                        1.84, 243.56, 1.92 Q 253.62, 1.68, 263.69, 2.20 Q 273.75, 1.36, 283.81, 1.54 Q 293.88, 1.46, 303.94, 2.84 Q 314.01, 1.53,\
                        324.07, 0.96 Q 334.14, 0.95, 344.20, 0.84 Q 354.27, 0.76, 364.33, 0.90 Q 374.40, 0.94, 384.46, 0.90 Q 394.53, 0.76, 404.59,\
                        0.48 Q 414.66, 0.69, 424.72, 0.26 Q 434.79, 0.36, 444.85, 0.34 Q 454.92, 0.93, 464.98, 2.16 Q 475.05, 2.19, 485.11, 1.96 Q\
                        495.18, 1.02, 505.24, 1.13 Q 515.31, 1.67, 525.37, 1.84 Q 535.44, 1.57, 545.50, 1.09 Q 555.56, 1.77, 565.63, 1.32 Q 575.69,\
                        1.01, 585.76, 1.08 Q 595.82, 1.41, 605.89, 2.11 Q 615.95, 1.97, 626.02, 1.05 Q 636.08, 0.73, 646.15, 0.97 Q 656.21, 1.25,\
                        666.28, 1.11 Q 676.34, 0.01, 686.41, -0.02 Q 696.47, 0.12, 706.54, -0.16 Q 716.60, 0.46, 726.67, 0.75 Q 736.73, 1.01, 746.80,\
                        1.38 Q 756.86, 1.07, 766.93, 1.11 Q 776.99, 0.74, 787.06, 0.07 Q 797.12, 0.54, 807.19, 0.61 Q 817.25, 0.81, 827.32, 1.44 Q\
                        837.38, 0.95, 847.44, 0.92 Q 857.51, 0.65, 867.57, 0.89 Q 877.64, 1.12, 887.70, 1.27 Q 897.77, 1.84, 907.83, 1.30 Q 917.90,\
                        2.01, 927.96, 1.80 Q 938.03, 1.50, 948.09, 1.16 Q 958.16, 2.26, 968.22, 2.11 Q 978.29, 1.07, 988.35, 1.51 Q 998.42, 0.80,\
                        1008.48, 1.25 Q 1018.55, 0.56, 1028.61, 0.59 Q 1038.68, 1.08, 1048.74, 1.49 Q 1058.81, 2.29, 1068.87, 2.17 Q 1078.94, 2.39,\
                        1089.08, 1.92 Q 1088.61, 12.15, 1088.41, 22.13 Q 1088.16, 32.12, 1088.73, 42.10 Q 1088.75, 52.11, 1088.97, 62.13 Q 1089.36,\
                        72.15, 1088.93, 82.17 Q 1089.95, 92.19, 1088.91, 102.22 Q 1088.98, 112.24, 1089.78, 122.26 Q 1089.65, 132.28, 1089.60, 142.30\
                        Q 1089.68, 152.33, 1089.49, 162.35 Q 1089.36, 172.37, 1089.72, 182.39 Q 1088.93, 192.41, 1089.49, 202.43 Q 1089.64, 212.46,\
                        1089.24, 222.48 Q 1089.22, 232.50, 1089.25, 242.52 Q 1090.17, 252.54, 1090.37, 262.57 Q 1090.73, 272.59, 1090.34, 282.61 Q\
                        1090.43, 292.63, 1090.82, 302.65 Q 1091.01, 312.67, 1091.19, 322.70 Q 1090.55, 332.72, 1089.74, 342.74 Q 1089.19, 352.76,\
                        1089.27, 362.78 Q 1088.61, 372.80, 1089.01, 382.83 Q 1088.86, 392.85, 1088.68, 402.87 Q 1089.18, 412.89, 1089.40, 422.91 Q\
                        1089.17, 432.93, 1089.32, 442.96 Q 1090.21, 452.98, 1089.09, 463.09 Q 1079.25, 463.96, 1069.07, 464.39 Q 1058.85, 463.69,\
                        1048.77, 463.75 Q 1038.69, 463.90, 1028.62, 463.97 Q 1018.55, 463.47, 1008.48, 463.34 Q 998.42, 462.29, 988.35, 463.42 Q 978.29,\
                        464.01, 968.22, 464.40 Q 958.16, 464.57, 948.09, 464.61 Q 938.03, 464.53, 927.96, 464.62 Q 917.90, 464.67, 907.83, 464.36\
                        Q 897.77, 463.81, 887.70, 463.87 Q 877.64, 463.82, 867.57, 463.06 Q 857.51, 462.94, 847.44, 462.78 Q 837.38, 463.68, 827.32,\
                        463.99 Q 817.25, 463.48, 807.19, 462.75 Q 797.12, 462.90, 787.06, 462.58 Q 776.99, 463.30, 766.93, 463.33 Q 756.86, 463.46,\
                        746.80, 463.85 Q 736.73, 463.68, 726.67, 461.97 Q 716.60, 462.75, 706.54, 462.33 Q 696.47, 463.71, 686.41, 463.80 Q 676.34,\
                        463.42, 666.28, 463.28 Q 656.21, 463.84, 646.15, 464.31 Q 636.08, 464.03, 626.02, 463.39 Q 615.95, 463.91, 605.89, 464.45\
                        Q 595.82, 464.58, 585.76, 464.58 Q 575.69, 463.75, 565.63, 463.78 Q 555.56, 463.70, 545.50, 463.59 Q 535.44, 463.18, 525.37,\
                        463.10 Q 515.31, 463.10, 505.24, 463.24 Q 495.18, 462.78, 485.11, 462.43 Q 475.05, 463.03, 464.98, 463.30 Q 454.92, 463.67,\
                        444.85, 463.27 Q 434.79, 463.97, 424.72, 463.53 Q 414.66, 463.49, 404.59, 462.74 Q 394.53, 463.14, 384.46, 463.57 Q 374.40,\
                        463.09, 364.33, 462.85 Q 354.27, 462.67, 344.20, 462.72 Q 334.14, 463.02, 324.07, 463.11 Q 314.01, 462.58, 303.94, 463.08\
                        Q 293.88, 463.53, 283.81, 464.32 Q 273.75, 464.03, 263.69, 462.75 Q 253.62, 462.18, 243.56, 462.30 Q 233.49, 463.26, 223.43,\
                        463.57 Q 213.36, 463.38, 203.30, 463.52 Q 193.23, 464.05, 183.17, 463.51 Q 173.10, 462.99, 163.04, 463.88 Q 152.97, 463.17,\
                        142.91, 463.46 Q 132.84, 462.04, 122.78, 462.23 Q 112.71, 463.42, 102.65, 463.59 Q 92.58, 463.40, 82.52, 462.54 Q 72.45, 463.37,\
                        62.39, 462.80 Q 52.32, 462.83, 42.26, 462.51 Q 32.19, 462.45, 22.13, 462.17 Q 12.06, 462.31, 2.09, 462.91 Q 2.75, 452.73,\
                        2.02, 442.95 Q 1.82, 432.95, 0.95, 422.95 Q 1.02, 412.91, 0.96, 402.88 Q 0.83, 392.85, 0.35, 382.83 Q -0.01, 372.81, 1.50,\
                        362.78 Q 1.63, 352.76, 0.94, 342.74 Q 0.70, 332.72, 0.40, 322.70 Q 1.00, 312.67, -0.01, 302.65 Q -0.10, 292.63, -0.24, 282.61\
                        Q 0.54, 272.59, 0.95, 262.57 Q 1.84, 252.54, 2.02, 242.52 Q 2.67, 232.50, 2.42, 222.48 Q 2.36, 212.46, 1.86, 202.43 Q 1.76,\
                        192.41, 2.62, 182.39 Q 1.74, 172.37, 2.02, 162.35 Q 1.28, 152.33, 1.96, 142.30 Q 2.06, 132.28, 2.32, 122.26 Q 2.38, 112.24,\
                        2.03, 102.22 Q 1.94, 92.20, 1.58, 82.17 Q 1.47, 72.15, 2.05, 62.13 Q 2.34, 52.11, 1.46, 42.09 Q 0.58, 32.07, 0.40, 22.04 Q\
                        2.00, 12.02, 2.00, 2.00" style=" fill:#a7a77c;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-784305066" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 160px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="784305066" data-review-reference-id="784305066">\
            <div class="stencil-wrapper" style="width: 275px; height: 160px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 160px;width:275px;" width="275" height="160">\
                     <svg:g width="275" height="160"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.42, 0.71, 22.85, 0.56 Q 33.27, 0.82, 43.69, 1.00 Q 54.12, 1.98,\
                        64.54, 1.72 Q 74.96, 2.13, 85.38, 1.80 Q 95.81, 1.78, 106.23, 2.17 Q 116.65, 1.58, 127.08, 0.74 Q 137.50, 0.90, 147.92, 1.72\
                        Q 158.35, 1.08, 168.77, 1.76 Q 179.19, 1.20, 189.62, 1.49 Q 200.04, 1.12, 210.46, 1.47 Q 220.88, 2.42, 231.31, 1.32 Q 241.73,\
                        1.22, 252.15, 0.78 Q 262.58, 0.47, 273.81, 1.19 Q 273.12, 13.10, 273.62, 24.20 Q 274.26, 35.34, 274.69, 46.52 Q 274.69, 57.69,\
                        273.55, 68.85 Q 274.12, 80.00, 274.79, 91.14 Q 274.78, 102.28, 274.46, 113.43 Q 274.27, 124.57, 274.57, 135.71 Q 274.92, 146.86,\
                        273.75, 158.75 Q 262.75, 158.52, 252.25, 158.69 Q 241.80, 159.02, 231.37, 159.90 Q 220.90, 159.21, 210.47, 159.60 Q 200.04,\
                        159.35, 189.62, 159.53 Q 179.19, 159.85, 168.77, 159.39 Q 158.35, 160.34, 147.92, 159.34 Q 137.50, 158.62, 127.08, 158.69\
                        Q 116.65, 158.62, 106.23, 158.93 Q 95.81, 159.22, 85.38, 158.51 Q 74.96, 157.62, 64.54, 158.48 Q 54.12, 159.23, 43.69, 159.81\
                        Q 33.27, 159.26, 22.85, 158.95 Q 12.42, 159.09, 1.44, 158.56 Q 1.22, 147.12, 1.28, 135.82 Q 1.10, 124.63, 0.92, 113.46 Q 1.01,\
                        102.30, 0.52, 91.15 Q 0.93, 80.00, 0.94, 68.86 Q 1.24, 57.72, 1.25, 46.57 Q 1.74, 35.43, 0.43, 24.29 Q 2.00, 13.14, 2.00,\
                        2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-991307636" style="position: absolute; left: 15px; top: 35px; width: 240px; height: 87px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="991307636" data-review-reference-id="991307636">\
            <div class="stencil-wrapper" style="width: 240px; height: 87px">\
               <div id="991307636-832991126" style="position: absolute; left: 0px; top: 0px; width: 85px; height: 85px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="832991126" data-review-reference-id="832991126">\
                  <div class="stencil-wrapper" style="width: 85px; height: 85px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 85px;width:85px;" width="85" height="85">\
                           <svg:g width="85" height="85"><svg:path class=" svg_unselected_element" d="M 82.00, 44.00 Q 82.09, 44.68, 81.13, 54.65 Q 76.63, 63.60, 71.68, 72.43 Q 63.22,\
                              78.36, 53.25, 81.02 Q 43.47, 83.33, 33.29, 81.95 Q 23.85, 77.84, 15.92, 71.30 Q 11.14, 62.15, 7.20, 53.17 Q 5.01, 43.43, 5.81,\
                              33.25 Q 10.21, 23.87, 17.07, 16.23 Q 25.14, 10.16, 34.50, 6.32 Q 44.65, 5.88, 54.66, 6.38 Q 63.88, 10.75, 71.43, 17.49 Q 77.21,\
                              25.60, 81.24, 34.67 Q 81.99, 44.66, 80.53, 54.47" style=" fill:white;"/>\
                           </svg:g>\
                        </svg:svg>\
                     </div>\
                  </div>\
               </div>\
               <div id="991307636-1982813214" style="position: absolute; left: 100px; top: 0px; width: 91px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1982813214" data-review-reference-id="1982813214">\
                  <div class="stencil-wrapper" style="width: 91px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Name</span></p></span></span></div>\
                  </div>\
               </div>\
               <div id="991307636-477098393" style="position: absolute; left: 100px; top: 50px; width: 145px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="477098393" data-review-reference-id="477098393">\
                  <div class="stencil-wrapper" style="width: 145px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Surname </p></span></span></div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-1191584763" style="position: absolute; left: 276px; top: 0px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1191584763" data-review-reference-id="1191584763">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:1090px;" width="1090" height="80">\
                     <svg:g width="1090" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.06, 1.96, 22.11, 2.06 Q 32.17, 2.48, 42.22, 2.16 Q 52.28, 3.12,\
                        62.33, 2.72 Q 72.39, 1.56, 82.44, 0.97 Q 92.50, 1.36, 102.56, 1.90 Q 112.61, 1.99, 122.67, 1.83 Q 132.72, 1.49, 142.78, 1.35\
                        Q 152.83, 1.64, 162.89, 1.45 Q 172.94, 1.84, 183.00, 1.91 Q 193.06, 2.40, 203.11, 1.65 Q 213.17, 1.33, 223.22, 1.67 Q 233.28,\
                        2.78, 243.33, 2.32 Q 253.39, 1.25, 263.44, 2.05 Q 273.50, 2.30, 283.56, 1.75 Q 293.61, 1.68, 303.67, 1.89 Q 313.72, 1.64,\
                        323.78, 1.67 Q 333.83, 1.95, 343.89, 2.95 Q 353.94, 2.91, 364.00, 2.59 Q 374.06, 1.47, 384.11, 1.64 Q 394.17, 1.46, 404.22,\
                        1.21 Q 414.28, 1.80, 424.33, 1.19 Q 434.39, 0.61, 444.44, 1.07 Q 454.50, 1.49, 464.56, 1.91 Q 474.61, 1.60, 484.67, 1.48 Q\
                        494.72, 0.84, 504.78, 0.63 Q 514.83, 1.89, 524.89, 2.51 Q 534.94, 1.86, 545.00, 2.11 Q 555.06, 1.78, 565.11, 1.27 Q 575.17,\
                        0.95, 585.22, 1.59 Q 595.28, 2.78, 605.33, 1.88 Q 615.39, 1.96, 625.44, 1.52 Q 635.50, 1.63, 645.56, 1.89 Q 655.61, 2.07,\
                        665.67, 2.23 Q 675.72, 2.62, 685.78, 3.42 Q 695.83, 2.79, 705.89, 3.43 Q 715.94, 3.19, 726.00, 2.92 Q 736.05, 3.00, 746.11,\
                        1.67 Q 756.17, 1.42, 766.22, 1.83 Q 776.28, 1.99, 786.33, 1.73 Q 796.39, 1.73, 806.44, 1.91 Q 816.50, 1.53, 826.55, 2.46 Q\
                        836.61, 1.82, 846.67, 1.56 Q 856.72, 1.33, 866.78, 0.81 Q 876.83, 0.99, 886.89, 0.39 Q 896.94, -0.16, 907.00, 0.37 Q 917.05,\
                        0.97, 927.11, 1.05 Q 937.17, 0.98, 947.22, 1.93 Q 957.28, 3.47, 967.33, 2.22 Q 977.39, 2.15, 987.44, 1.05 Q 997.50, 0.39,\
                        1007.55, 0.52 Q 1017.61, 1.93, 1027.67, 2.60 Q 1037.72, 3.08, 1047.78, 2.75 Q 1057.83, 1.88, 1067.89, 2.29 Q 1077.94, 2.13,\
                        1088.07, 1.92 Q 1088.15, 14.62, 1088.70, 27.23 Q 1088.70, 39.95, 1088.41, 52.65 Q 1087.99, 65.33, 1088.20, 78.20 Q 1078.07,\
                        78.38, 1067.97, 78.59 Q 1057.83, 78.00, 1047.79, 78.32 Q 1037.74, 79.13, 1027.68, 79.61 Q 1017.61, 79.27, 1007.56, 78.88 Q\
                        997.50, 78.98, 987.44, 79.24 Q 977.39, 78.86, 967.33, 79.36 Q 957.28, 79.34, 947.22, 79.07 Q 937.17, 79.10, 927.11, 78.72\
                        Q 917.05, 78.09, 907.00, 77.75 Q 896.94, 78.33, 886.89, 78.68 Q 876.83, 78.00, 866.78, 78.04 Q 856.72, 78.51, 846.67, 78.93\
                        Q 836.61, 78.26, 826.55, 77.48 Q 816.50, 77.64, 806.44, 77.83 Q 796.39, 78.10, 786.33, 78.15 Q 776.28, 77.80, 766.22, 77.88\
                        Q 756.17, 78.26, 746.11, 78.87 Q 736.05, 79.43, 726.00, 79.94 Q 715.94, 79.85, 705.89, 80.05 Q 695.83, 79.24, 685.78, 79.52\
                        Q 675.72, 79.96, 665.67, 79.73 Q 655.61, 79.09, 645.56, 78.63 Q 635.50, 78.44, 625.44, 78.05 Q 615.39, 78.71, 605.33, 79.53\
                        Q 595.28, 79.06, 585.22, 78.32 Q 575.17, 77.79, 565.11, 78.36 Q 555.06, 78.23, 545.00, 78.45 Q 534.94, 79.30, 524.89, 78.51\
                        Q 514.83, 79.00, 504.78, 78.83 Q 494.72, 78.85, 484.67, 78.87 Q 474.61, 78.96, 464.56, 79.73 Q 454.50, 79.00, 444.44, 79.65\
                        Q 434.39, 79.34, 424.33, 79.14 Q 414.28, 78.44, 404.22, 78.66 Q 394.17, 78.52, 384.11, 79.35 Q 374.06, 79.35, 364.00, 79.50\
                        Q 353.94, 79.46, 343.89, 78.46 Q 333.83, 77.27, 323.78, 76.54 Q 313.72, 77.36, 303.67, 78.25 Q 293.61, 78.39, 283.56, 77.90\
                        Q 273.50, 77.96, 263.44, 78.75 Q 253.39, 78.04, 243.33, 78.60 Q 233.28, 78.47, 223.22, 78.83 Q 213.17, 79.08, 203.11, 78.15\
                        Q 193.06, 78.08, 183.00, 78.02 Q 172.94, 78.56, 162.89, 77.82 Q 152.83, 78.44, 142.78, 78.92 Q 132.72, 79.54, 122.67, 78.93\
                        Q 112.61, 79.56, 102.56, 78.97 Q 92.50, 78.98, 82.44, 79.45 Q 72.39, 79.00, 62.33, 79.52 Q 52.28, 80.29, 42.22, 79.22 Q 32.17,\
                        79.90, 22.11, 79.53 Q 12.06, 79.81, 1.27, 78.73 Q 0.51, 65.83, 0.58, 52.87 Q -0.00, 40.13, 0.57, 27.38 Q 2.00, 14.67, 2.00,\
                        2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-63224330" style="position: absolute; left: 276px; top: 688px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="63224330" data-review-reference-id="63224330">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:1090px;" width="1090" height="80">\
                     <svg:g width="1090" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.06, 0.84, 22.11, 1.16 Q 32.17, 1.05, 42.22, 1.71 Q 52.28, 1.11,\
                        62.33, 1.30 Q 72.39, 1.82, 82.44, 2.10 Q 92.50, 2.29, 102.56, 1.61 Q 112.61, 1.92, 122.67, 2.03 Q 132.72, 2.89, 142.78, 2.50\
                        Q 152.83, 2.29, 162.89, 1.92 Q 172.94, 1.63, 183.00, 1.83 Q 193.06, 1.45, 203.11, 1.40 Q 213.17, 0.86, 223.22, 2.06 Q 233.28,\
                        1.93, 243.33, 1.81 Q 253.39, 0.80, 263.44, 2.36 Q 273.50, 3.26, 283.56, 1.65 Q 293.61, 1.65, 303.67, 1.43 Q 313.72, 2.34,\
                        323.78, 1.07 Q 333.83, 0.14, 343.89, 1.16 Q 353.94, 1.38, 364.00, 1.59 Q 374.06, 1.32, 384.11, 0.89 Q 394.17, 1.19, 404.22,\
                        1.39 Q 414.28, 0.81, 424.33, 0.62 Q 434.39, 1.52, 444.44, 1.92 Q 454.50, 2.09, 464.56, 1.29 Q 474.61, 1.11, 484.67, 1.32 Q\
                        494.72, -0.01, 504.78, 0.48 Q 514.83, 0.99, 524.89, 2.53 Q 534.94, 2.75, 545.00, 3.02 Q 555.06, 2.70, 565.11, 1.45 Q 575.17,\
                        2.08, 585.22, 2.13 Q 595.28, 2.42, 605.33, 1.50 Q 615.39, 1.69, 625.44, 2.49 Q 635.50, 1.71, 645.56, 0.80 Q 655.61, 0.10,\
                        665.67, 0.13 Q 675.72, 0.74, 685.78, 1.82 Q 695.83, 1.82, 705.89, 1.99 Q 715.94, 2.22, 726.00, 1.84 Q 736.05, 2.26, 746.11,\
                        1.80 Q 756.17, 2.19, 766.22, 1.17 Q 776.28, 1.49, 786.33, 1.15 Q 796.39, 1.01, 806.44, 0.85 Q 816.50, 0.41, 826.55, 0.51 Q\
                        836.61, 0.54, 846.67, 1.26 Q 856.72, 0.68, 866.78, 1.82 Q 876.83, 1.50, 886.89, 1.11 Q 896.94, 0.67, 907.00, 1.71 Q 917.05,\
                        1.91, 927.11, 1.13 Q 937.17, 1.50, 947.22, 0.78 Q 957.28, 1.03, 967.33, 1.25 Q 977.39, 0.84, 987.44, 0.80 Q 997.50, 0.85,\
                        1007.55, 1.77 Q 1017.61, 1.95, 1027.67, 1.56 Q 1037.72, 1.69, 1047.78, 2.37 Q 1057.83, 1.99, 1067.89, 2.01 Q 1077.94, 2.00,\
                        1087.80, 2.20 Q 1088.07, 14.64, 1087.90, 27.35 Q 1088.10, 39.99, 1088.94, 52.64 Q 1088.32, 65.33, 1088.32, 78.32 Q 1078.16,\
                        78.64, 1068.06, 79.19 Q 1057.92, 79.34, 1047.76, 77.59 Q 1037.73, 78.29, 1027.67, 78.90 Q 1017.62, 79.37, 1007.56, 78.26 Q\
                        997.50, 78.21, 987.44, 77.91 Q 977.39, 79.00, 967.33, 78.80 Q 957.28, 79.60, 947.22, 79.71 Q 937.17, 79.44, 927.11, 80.12\
                        Q 917.05, 80.52, 907.00, 80.38 Q 896.94, 79.80, 886.89, 80.00 Q 876.83, 79.29, 866.78, 78.35 Q 856.72, 77.96, 846.67, 78.43\
                        Q 836.61, 78.84, 826.55, 78.47 Q 816.50, 77.57, 806.44, 77.96 Q 796.39, 78.05, 786.33, 78.06 Q 776.28, 78.24, 766.22, 77.66\
                        Q 756.17, 77.75, 746.11, 78.38 Q 736.05, 78.29, 726.00, 77.52 Q 715.94, 78.06, 705.89, 78.96 Q 695.83, 79.07, 685.78, 78.52\
                        Q 675.72, 78.38, 665.67, 78.78 Q 655.61, 79.14, 645.56, 78.82 Q 635.50, 78.71, 625.44, 79.34 Q 615.39, 79.90, 605.33, 80.03\
                        Q 595.28, 79.94, 585.22, 79.86 Q 575.17, 79.71, 565.11, 79.35 Q 555.06, 78.69, 545.00, 78.45 Q 534.94, 78.94, 524.89, 79.26\
                        Q 514.83, 78.97, 504.78, 78.27 Q 494.72, 78.19, 484.67, 78.68 Q 474.61, 78.57, 464.56, 78.03 Q 454.50, 77.55, 444.44, 77.91\
                        Q 434.39, 77.29, 424.33, 77.06 Q 414.28, 76.81, 404.22, 77.07 Q 394.17, 78.35, 384.11, 78.39 Q 374.06, 78.33, 364.00, 77.74\
                        Q 353.94, 78.35, 343.89, 78.31 Q 333.83, 78.66, 323.78, 78.83 Q 313.72, 79.39, 303.67, 79.40 Q 293.61, 79.41, 283.56, 79.58\
                        Q 273.50, 78.85, 263.44, 78.48 Q 253.39, 78.89, 243.33, 79.09 Q 233.28, 78.48, 223.22, 79.01 Q 213.17, 79.27, 203.11, 78.99\
                        Q 193.06, 79.14, 183.00, 79.40 Q 172.94, 78.84, 162.89, 78.26 Q 152.83, 78.43, 142.78, 77.68 Q 132.72, 78.27, 122.67, 78.91\
                        Q 112.61, 78.53, 102.56, 78.55 Q 92.50, 78.87, 82.44, 78.50 Q 72.39, 78.67, 62.33, 78.58 Q 52.28, 78.41, 42.22, 79.71 Q 32.17,\
                        80.07, 22.11, 79.48 Q 12.06, 78.66, 1.67, 78.33 Q 0.92, 65.69, 1.15, 52.79 Q 1.15, 40.06, 1.03, 27.36 Q 2.00, 14.67, 2.00,\
                        2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-352178416" style="position: absolute; left: 355px; top: 10px; width: 230px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="352178416" data-review-reference-id="352178416">\
            <div class="stencil-wrapper" style="width: 230px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:230px;" width="230" height="60">\
                     <svg:g width="230" height="60"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.27, 1.66, 22.55, 1.39 Q 32.82, 1.15, 43.09, 1.26 Q\
                        53.36, 1.30, 63.64, 1.32 Q 73.91, 1.29, 84.18, 1.11 Q 94.45, 0.97, 104.73, 0.99 Q 115.00, 1.09, 125.27, 1.05 Q 135.55, 0.91,\
                        145.82, 0.81 Q 156.09, 0.88, 166.36, 1.09 Q 176.64, 0.95, 186.91, 0.85 Q 197.18, 0.87, 207.45, 0.99 Q 217.73, 0.43, 228.68,\
                        1.32 Q 229.21, 15.60, 229.11, 29.84 Q 229.04, 43.93, 228.58, 58.58 Q 218.04, 58.97, 207.63, 59.28 Q 197.27, 59.39, 186.95,\
                        59.23 Q 176.64, 57.96, 166.37, 59.19 Q 156.10, 59.46, 145.82, 58.96 Q 135.55, 59.12, 125.27, 58.95 Q 115.00, 59.01, 104.73,\
                        59.23 Q 94.45, 59.59, 84.18, 58.84 Q 73.91, 58.85, 63.64, 59.51 Q 53.36, 58.89, 43.09, 58.58 Q 32.82, 58.84, 22.55, 59.14\
                        Q 12.27, 58.77, 1.43, 58.57 Q 1.16, 44.28, 1.32, 30.10 Q 2.00, 16.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.87, 2.15, 23.02, 5.17 Q 33.21, 8.06, 43.44, 10.79 Q 53.63,\
                        13.64, 63.87, 16.32 Q 74.00, 19.44, 84.25, 22.08 Q 94.66, 24.09, 104.93, 26.62 Q 115.25, 28.99, 125.56, 31.39 Q 135.79, 34.12,\
                        145.93, 37.18 Q 156.23, 39.63, 166.47, 42.29 Q 176.73, 44.91, 187.11, 47.01 Q 197.35, 49.70, 207.64, 52.15 Q 217.73, 55.45,\
                        228.00, 58.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 58.00 Q 12.55, 56.22, 22.89, 53.56 Q 33.15, 50.62, 43.40, 47.61 Q 53.60,\
                        44.38, 64.12, 42.47 Q 74.56, 40.23, 84.87, 37.46 Q 95.20, 34.81, 105.56, 32.25 Q 115.82, 29.28, 126.44, 27.76 Q 136.91, 25.66,\
                        147.20, 22.79 Q 157.69, 20.76, 167.64, 16.56 Q 178.19, 14.78, 188.37, 11.46 Q 198.80, 9.18, 209.32, 7.30 Q 219.64, 4.55, 230.00,\
                        2.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-987791261" style="position: absolute; left: 0px; top: 180px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="987791261" data-review-reference-id="987791261">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 3.27, 22.62, 2.31 Q 32.92, 1.95, 43.23, 1.45 Q 53.54, 2.50,\
                        63.85, 2.56 Q 74.15, 2.11, 84.46, 1.22 Q 94.77, 1.53, 105.08, 1.69 Q 115.38, 1.31, 125.69, 1.50 Q 136.00, 1.28, 146.31, 2.11\
                        Q 156.62, 1.57, 166.92, 1.72 Q 177.23, 1.56, 187.54, 0.69 Q 197.85, 1.70, 208.15, 0.76 Q 218.46, 0.52, 228.77, 1.01 Q 239.08,\
                        2.41, 249.38, 1.57 Q 259.69, 1.05, 270.37, 1.63 Q 270.11, 12.71, 269.46, 23.58 Q 269.71, 34.27, 270.19, 45.19 Q 259.99, 45.93,\
                        249.45, 45.47 Q 239.14, 45.95, 228.80, 45.90 Q 218.48, 46.39, 208.17, 47.55 Q 197.85, 46.69, 187.54, 46.18 Q 177.23, 46.13,\
                        166.92, 46.93 Q 156.62, 46.09, 146.31, 46.90 Q 136.00, 46.86, 125.69, 46.62 Q 115.38, 45.54, 105.08, 45.51 Q 94.77, 46.36,\
                        84.46, 46.97 Q 74.15, 46.86, 63.85, 46.48 Q 53.54, 46.58, 43.23, 46.52 Q 32.92, 46.87, 22.62, 46.49 Q 12.31, 46.05, 1.59,\
                        45.41 Q 1.27, 34.49, 1.32, 23.60 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 270.80, 15.00, 270.82, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 273.22, 16.00, 273.22, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.80, 17.00, 274.38, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 44.24, 24.69, 44.15 Q 35.04, 44.04, 45.38, 44.16 Q 55.73,\
                        44.49, 66.08, 44.22 Q 76.42, 44.09, 86.77, 44.22 Q 97.12, 44.73, 107.46, 45.61 Q 117.81, 45.30, 128.15, 44.75 Q 138.50, 44.55,\
                        148.85, 44.64 Q 159.19, 44.14, 169.54, 44.18 Q 179.88, 45.20, 190.23, 45.34 Q 200.58, 45.07, 210.92, 46.36 Q 221.27, 46.14,\
                        231.62, 45.89 Q 241.96, 45.22, 252.31, 45.20 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 45.19, 25.69, 45.89 Q 36.04, 46.80, 46.38, 46.31 Q 56.73,\
                        46.57, 67.08, 46.88 Q 77.42, 46.45, 87.77, 45.79 Q 98.12, 45.91, 108.46, 46.15 Q 118.81, 45.89, 129.15, 45.77 Q 139.50, 45.56,\
                        149.85, 45.22 Q 160.19, 44.71, 170.54, 44.45 Q 180.88, 45.74, 191.23, 45.29 Q 201.58, 45.75, 211.92, 45.52 Q 222.27, 45.24,\
                        232.62, 45.14 Q 242.96, 45.12, 253.31, 45.03 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 48.61, 26.69, 47.65 Q 37.04, 47.23, 47.38, 46.73 Q 57.73,\
                        47.91, 68.08, 47.40 Q 78.42, 46.90, 88.77, 46.81 Q 99.12, 47.10, 109.46, 47.99 Q 119.81, 46.81, 130.15, 46.57 Q 140.50, 46.69,\
                        150.85, 46.72 Q 161.19, 46.78, 171.54, 47.43 Q 181.88, 47.26, 192.23, 47.54 Q 202.58, 47.56, 212.92, 48.00 Q 223.27, 47.41,\
                        233.62, 46.74 Q 243.96, 47.20, 254.31, 47.47 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page189476043-layer-987791261button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page189476043-layer-987791261button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page189476043-layer-987791261button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Коэффициенты<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page189476043-layer-987791261\', \'interaction277522265\', {"button":"left","id":"action389694926","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction508902595","options":"reloadOnly","target":"page606870902","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page189476043-layer-68618477" style="position: absolute; left: 1117px; top: 20px; width: 172px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="68618477" data-review-reference-id="68618477">\
            <div class="stencil-wrapper" style="width: 172px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Изменения</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-35730108" style="position: absolute; left: 0px; top: 250px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="35730108" data-review-reference-id="35730108">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.04, 22.62, 1.33 Q 32.92, 1.38, 43.23, 0.94 Q 53.54, 0.21,\
                        63.85, 0.38 Q 74.15, -0.17, 84.46, 0.85 Q 94.77, 0.35, 105.08, 0.62 Q 115.38, 0.79, 125.69, 0.76 Q 136.00, 0.38, 146.31, -0.04\
                        Q 156.62, 1.07, 166.92, 1.81 Q 177.23, 1.40, 187.54, 0.60 Q 197.85, 0.62, 208.15, 0.60 Q 218.46, 0.57, 228.77, 0.42 Q 239.08,\
                        0.33, 249.38, 0.13 Q 259.69, 0.44, 270.70, 1.30 Q 271.19, 12.35, 271.51, 23.28 Q 271.64, 34.14, 270.36, 45.36 Q 259.78, 45.28,\
                        249.43, 45.35 Q 239.08, 45.07, 228.80, 46.15 Q 218.48, 46.14, 208.16, 46.09 Q 197.85, 46.37, 187.54, 45.58 Q 177.23, 45.69,\
                        166.92, 46.22 Q 156.62, 45.56, 146.31, 45.90 Q 136.00, 46.09, 125.69, 46.17 Q 115.38, 46.24, 105.08, 45.95 Q 94.77, 46.09,\
                        84.46, 46.39 Q 74.15, 46.16, 63.85, 45.51 Q 53.54, 45.30, 43.23, 45.81 Q 32.92, 45.54, 22.62, 45.84 Q 12.31, 46.21, 1.25,\
                        45.75 Q 0.84, 34.64, 0.81, 23.67 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#a7a77c;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 272.51, 15.00, 272.54, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 272.24, 16.00, 272.20, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 274.07, 17.00, 274.23, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 46.63, 24.69, 46.45 Q 35.04, 46.45, 45.38, 46.90 Q 55.73,\
                        47.48, 66.08, 46.72 Q 76.42, 46.30, 86.77, 46.14 Q 97.12, 46.06, 107.46, 46.75 Q 117.81, 46.38, 128.15, 45.92 Q 138.50, 45.58,\
                        148.85, 45.75 Q 159.19, 46.07, 169.54, 45.26 Q 179.88, 45.10, 190.23, 45.29 Q 200.58, 45.72, 210.92, 46.25 Q 221.27, 45.65,\
                        231.62, 45.82 Q 241.96, 46.12, 252.31, 45.60 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 46.94, 25.69, 47.31 Q 36.04, 46.91, 46.38, 46.89 Q 56.73,\
                        46.60, 67.08, 46.50 Q 77.42, 47.10, 87.77, 47.33 Q 98.12, 47.38, 108.46, 47.56 Q 118.81, 46.87, 129.15, 47.01 Q 139.50, 47.56,\
                        149.85, 47.08 Q 160.19, 47.10, 170.54, 46.41 Q 180.88, 45.83, 191.23, 46.45 Q 201.58, 46.20, 211.92, 47.37 Q 222.27, 46.08,\
                        232.62, 46.91 Q 242.96, 46.93, 253.31, 47.17 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 47.26, 26.69, 47.86 Q 37.04, 47.92, 47.38, 48.28 Q 57.73,\
                        47.68, 68.08, 47.13 Q 78.42, 47.26, 88.77, 47.40 Q 99.12, 47.26, 109.46, 47.05 Q 119.81, 47.27, 130.15, 47.06 Q 140.50, 47.14,\
                        150.85, 47.15 Q 161.19, 47.08, 171.54, 47.32 Q 181.88, 47.60, 192.23, 47.45 Q 202.58, 47.23, 212.92, 46.98 Q 223.27, 46.81,\
                        233.62, 47.47 Q 243.96, 47.05, 254.31, 47.30 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page189476043-layer-35730108button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page189476043-layer-35730108button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page189476043-layer-35730108button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Изменения<br />  \
                     			</button></div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-421256168" style="position: absolute; left: 0px; top: 320px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="421256168" data-review-reference-id="421256168">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 0.19, 22.62, 0.63 Q 32.92, 1.45, 43.23, 1.47 Q 53.54, 2.34,\
                        63.85, 1.73 Q 74.15, 0.42, 84.46, -0.15 Q 94.77, 1.31, 105.08, 1.00 Q 115.38, 0.93, 125.69, 0.63 Q 136.00, 0.51, 146.31, 0.38\
                        Q 156.62, 0.04, 166.92, 0.04 Q 177.23, 0.38, 187.54, 1.91 Q 197.85, 1.31, 208.15, 1.13 Q 218.46, 1.19, 228.77, 0.89 Q 239.08,\
                        1.74, 249.38, 0.77 Q 259.69, 0.17, 270.63, 1.37 Q 271.09, 12.39, 271.47, 23.29 Q 271.70, 34.14, 271.02, 46.02 Q 260.24, 46.73,\
                        249.66, 47.04 Q 239.20, 47.00, 228.81, 46.22 Q 218.47, 45.33, 208.15, 44.40 Q 197.84, 44.46, 187.54, 44.96 Q 177.23, 45.51,\
                        166.92, 45.22 Q 156.62, 44.68, 146.31, 45.00 Q 136.00, 46.12, 125.69, 44.96 Q 115.38, 45.22, 105.08, 44.12 Q 94.77, 45.50,\
                        84.46, 45.69 Q 74.15, 46.14, 63.85, 46.58 Q 53.54, 45.72, 43.23, 45.61 Q 32.92, 45.32, 22.62, 45.36 Q 12.31, 46.19, 1.59,\
                        45.41 Q 1.01, 34.58, 0.85, 23.66 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 270.26, 15.00, 270.08, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 274.37, 16.00, 273.40, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.94, 17.00, 274.10, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 45.30, 24.69, 44.94 Q 35.04, 44.91, 45.38, 45.20 Q 55.73,\
                        44.83, 66.08, 45.82 Q 76.42, 45.15, 86.77, 46.00 Q 97.12, 45.02, 107.46, 45.60 Q 117.81, 46.23, 128.15, 46.19 Q 138.50, 45.12,\
                        148.85, 44.34 Q 159.19, 45.75, 169.54, 46.50 Q 179.88, 46.02, 190.23, 45.19 Q 200.58, 45.11, 210.92, 45.91 Q 221.27, 46.24,\
                        231.62, 45.65 Q 241.96, 45.52, 252.31, 46.61 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 47.29, 25.69, 46.33 Q 36.04, 45.72, 46.38, 45.55 Q 56.73,\
                        45.26, 67.08, 45.17 Q 77.42, 45.35, 87.77, 45.10 Q 98.12, 45.40, 108.46, 45.40 Q 118.81, 45.67, 129.15, 45.61 Q 139.50, 45.95,\
                        149.85, 45.74 Q 160.19, 45.73, 170.54, 44.88 Q 180.88, 45.62, 191.23, 45.26 Q 201.58, 45.57, 211.92, 46.16 Q 222.27, 46.56,\
                        232.62, 45.97 Q 242.96, 45.92, 253.31, 45.83 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 46.44, 26.69, 46.94 Q 37.04, 47.09, 47.38, 47.02 Q 57.73,\
                        47.01, 68.08, 46.67 Q 78.42, 46.49, 88.77, 46.91 Q 99.12, 46.47, 109.46, 46.79 Q 119.81, 46.47, 130.15, 46.26 Q 140.50, 46.22,\
                        150.85, 46.09 Q 161.19, 46.06, 171.54, 47.08 Q 181.88, 46.34, 192.23, 46.35 Q 202.58, 46.48, 212.92, 46.66 Q 223.27, 47.04,\
                        233.62, 46.94 Q 243.96, 46.62, 254.31, 46.66 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page189476043-layer-421256168button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page189476043-layer-421256168button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page189476043-layer-421256168button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Статистика<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page189476043-layer-421256168\', \'interaction494869075\', {"button":"left","id":"action378196588","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction685374820","options":"reloadOnly","target":"page995954813","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page189476043-layer-685009201" style="position: absolute; left: 0px; top: 718px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="685009201" data-review-reference-id="685009201">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.67, 22.62, 2.50 Q 32.92, 2.46, 43.23, 3.43 Q 53.54, 2.03,\
                        63.85, 2.27 Q 74.15, 2.88, 84.46, 3.58 Q 94.77, 2.70, 105.08, 2.05 Q 115.38, 2.16, 125.69, 2.07 Q 136.00, 3.51, 146.31, 2.77\
                        Q 156.62, 2.50, 166.92, 2.75 Q 177.23, 2.74, 187.54, 1.99 Q 197.85, 1.67, 208.15, 1.11 Q 218.46, 2.30, 228.77, 2.22 Q 239.08,\
                        1.37, 249.38, 1.07 Q 259.69, 1.92, 269.57, 2.43 Q 270.20, 12.68, 270.49, 23.43 Q 270.68, 34.20, 270.43, 45.43 Q 259.79, 45.31,\
                        249.48, 45.71 Q 239.09, 45.27, 228.79, 45.74 Q 218.47, 45.82, 208.16, 46.50 Q 197.85, 46.40, 187.54, 46.27 Q 177.23, 46.35,\
                        166.92, 47.24 Q 156.62, 46.67, 146.31, 45.94 Q 136.00, 45.98, 125.69, 45.78 Q 115.38, 45.76, 105.08, 46.00 Q 94.77, 46.06,\
                        84.46, 46.71 Q 74.15, 47.09, 63.85, 46.79 Q 53.54, 45.05, 43.23, 44.49 Q 32.92, 44.74, 22.62, 44.98 Q 12.31, 45.28, 1.86,\
                        45.14 Q 1.92, 34.28, 1.45, 23.58 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 270.54, 15.00, 270.87, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 271.57, 16.00, 271.79, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 274.00, 17.00, 273.57, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 46.38, 24.69, 45.91 Q 35.04, 45.75, 45.38, 45.26 Q 55.73,\
                        45.58, 66.08, 45.79 Q 76.42, 46.06, 86.77, 45.58 Q 97.12, 45.26, 107.46, 45.73 Q 117.81, 45.61, 128.15, 45.64 Q 138.50, 44.83,\
                        148.85, 45.72 Q 159.19, 46.39, 169.54, 45.85 Q 179.88, 45.28, 190.23, 45.67 Q 200.58, 46.00, 210.92, 45.23 Q 221.27, 45.80,\
                        231.62, 45.10 Q 241.96, 45.71, 252.31, 45.50 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 47.04, 25.69, 46.75 Q 36.04, 47.48, 46.38, 46.67 Q 56.73,\
                        48.78, 67.08, 48.99 Q 77.42, 48.17, 87.77, 46.56 Q 98.12, 47.86, 108.46, 48.46 Q 118.81, 48.12, 129.15, 48.76 Q 139.50, 48.27,\
                        149.85, 48.69 Q 160.19, 47.69, 170.54, 46.39 Q 180.88, 45.97, 191.23, 46.96 Q 201.58, 47.98, 211.92, 47.81 Q 222.27, 47.54,\
                        232.62, 46.93 Q 242.96, 47.66, 253.31, 47.67 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 46.47, 26.69, 46.38 Q 37.04, 46.97, 47.38, 46.72 Q 57.73,\
                        46.96, 68.08, 47.41 Q 78.42, 46.95, 88.77, 46.77 Q 99.12, 46.62, 109.46, 46.80 Q 119.81, 46.83, 130.15, 47.56 Q 140.50, 47.58,\
                        150.85, 47.23 Q 161.19, 47.26, 171.54, 47.23 Q 181.88, 47.42, 192.23, 47.46 Q 202.58, 46.75, 212.92, 46.94 Q 223.27, 46.99,\
                        233.62, 46.85 Q 243.96, 47.20, 254.31, 48.00 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page189476043-layer-685009201button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page189476043-layer-685009201button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page189476043-layer-685009201button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Выход<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page189476043-layer-685009201\', \'interaction173339785\', {"button":"left","id":"action78889276","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction170441484","options":"reloadOnly","target":"page940099653","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page189476043-layer-1207690582" style="position: absolute; left: 0px; top: 673px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1207690582" data-review-reference-id="1207690582">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 0.55, 22.62, 0.39 Q 32.92, 0.43, 43.23, 0.73 Q 53.54, 0.53,\
                        63.85, 0.68 Q 74.15, 1.05, 84.46, 1.13 Q 94.77, 1.08, 105.08, 1.00 Q 115.38, 0.82, 125.69, 1.32 Q 136.00, 1.79, 146.31, 1.59\
                        Q 156.62, 1.46, 166.92, 1.72 Q 177.23, 2.63, 187.54, 1.65 Q 197.85, 1.41, 208.15, 1.58 Q 218.46, 1.70, 228.77, 1.53 Q 239.08,\
                        1.29, 249.38, 2.28 Q 259.69, 1.96, 269.87, 2.13 Q 270.24, 12.67, 270.44, 23.44 Q 270.75, 34.20, 270.25, 45.25 Q 259.77, 45.25,\
                        249.49, 45.80 Q 239.08, 45.06, 228.75, 44.24 Q 218.46, 44.79, 208.16, 45.70 Q 197.85, 46.02, 187.54, 45.73 Q 177.23, 45.43,\
                        166.92, 45.00 Q 156.62, 45.25, 146.31, 44.89 Q 136.00, 45.02, 125.69, 45.65 Q 115.38, 46.30, 105.08, 45.76 Q 94.77, 45.76,\
                        84.46, 44.26 Q 74.15, 43.83, 63.85, 45.02 Q 53.54, 45.04, 43.23, 44.78 Q 32.92, 44.31, 22.62, 45.70 Q 12.31, 45.63, 1.86,\
                        45.14 Q 1.53, 34.41, 1.66, 23.55 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 271.76, 15.00, 271.44, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 270.64, 16.00, 271.51, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 272.73, 17.00, 272.85, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 43.29, 24.69, 43.60 Q 35.04, 43.68, 45.38, 44.10 Q 55.73,\
                        44.10, 66.08, 44.15 Q 76.42, 45.21, 86.77, 45.13 Q 97.12, 45.03, 107.46, 45.03 Q 117.81, 44.85, 128.15, 44.94 Q 138.50, 45.32,\
                        148.85, 45.24 Q 159.19, 46.27, 169.54, 44.84 Q 179.88, 45.20, 190.23, 45.16 Q 200.58, 45.02, 210.92, 44.99 Q 221.27, 44.90,\
                        231.62, 44.79 Q 241.96, 45.44, 252.31, 45.27 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 46.80, 25.69, 46.75 Q 36.04, 46.98, 46.38, 47.00 Q 56.73,\
                        47.88, 67.08, 46.93 Q 77.42, 47.08, 87.77, 46.46 Q 98.12, 46.30, 108.46, 47.61 Q 118.81, 47.08, 129.15, 47.01 Q 139.50, 47.73,\
                        149.85, 47.91 Q 160.19, 47.28, 170.54, 47.32 Q 180.88, 46.73, 191.23, 46.82 Q 201.58, 47.11, 211.92, 47.25 Q 222.27, 47.08,\
                        232.62, 46.70 Q 242.96, 47.38, 253.31, 47.10 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 48.25, 26.69, 48.84 Q 37.04, 48.14, 47.38, 49.06 Q 57.73,\
                        49.34, 68.08, 47.93 Q 78.42, 48.84, 88.77, 48.67 Q 99.12, 48.83, 109.46, 49.46 Q 119.81, 48.74, 130.15, 48.08 Q 140.50, 48.64,\
                        150.85, 48.55 Q 161.19, 47.55, 171.54, 47.47 Q 181.88, 47.86, 192.23, 48.18 Q 202.58, 48.94, 212.92, 49.08 Q 223.27, 48.34,\
                        233.62, 47.68 Q 243.96, 47.43, 254.31, 47.35 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page189476043-layer-1207690582button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page189476043-layer-1207690582button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page189476043-layer-1207690582button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Настройки<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page189476043-layer-1207690582\', \'interaction638595480\', {"button":"left","id":"action293296677","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction374237093","options":"reloadOnly","target":"page47785501","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page189476043-layer-1634000025" style="position: absolute; left: 0px; top: 625px; width: 275px; height: 48px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1634000025" data-review-reference-id="1634000025">\
            <div class="stencil-wrapper" style="width: 275px; height: 48px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 48px;width:275px;" width="275" height="48">\
                     <svg:g width="275" height="48"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, -0.60, 22.62, -0.44 Q 32.92, -0.29, 43.23, -0.12 Q 53.54,\
                        0.57, 63.85, 0.07 Q 74.15, -0.06, 84.46, 0.33 Q 94.77, 1.58, 105.08, 1.74 Q 115.38, 1.46, 125.69, 0.96 Q 136.00, 0.60, 146.31,\
                        0.80 Q 156.62, 0.66, 166.92, 1.10 Q 177.23, 1.38, 187.54, 1.75 Q 197.85, 1.11, 208.15, 0.70 Q 218.46, 0.61, 228.77, 0.57 Q\
                        239.08, 1.27, 249.38, 0.98 Q 259.69, 0.66, 270.72, 1.28 Q 270.87, 11.96, 271.14, 22.34 Q 271.39, 32.66, 270.47, 43.47 Q 259.97,\
                        43.86, 249.46, 43.53 Q 239.15, 44.16, 228.77, 43.14 Q 218.46, 42.74, 208.15, 42.72 Q 197.85, 42.69, 187.54, 43.38 Q 177.23,\
                        43.41, 166.92, 44.04 Q 156.62, 44.23, 146.31, 44.26 Q 136.00, 44.33, 125.69, 44.42 Q 115.38, 44.22, 105.08, 43.30 Q 94.77,\
                        43.02, 84.46, 43.49 Q 74.15, 43.79, 63.85, 43.52 Q 53.54, 43.46, 43.23, 43.81 Q 32.92, 43.90, 22.62, 43.61 Q 12.31, 43.03,\
                        1.55, 43.45 Q 0.85, 33.13, 1.49, 22.57 Q 2.00, 12.25, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 269.42, 14.50, 269.64, 25.00 Q 271.00, 35.50, 271.00, 46.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 270.59, 15.50, 270.77, 26.00 Q 272.00, 36.50, 272.00, 47.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 274.86, 16.50, 274.37, 27.00 Q 273.00, 37.50, 273.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 44.00 Q 14.35, 43.78, 24.69, 43.75 Q 35.04, 43.49, 45.38, 43.38 Q 55.73,\
                        43.01, 66.08, 42.92 Q 76.42, 42.88, 86.77, 43.21 Q 97.12, 43.04, 107.46, 42.76 Q 117.81, 42.61, 128.15, 42.56 Q 138.50, 42.93,\
                        148.85, 42.60 Q 159.19, 42.75, 169.54, 42.86 Q 179.88, 43.04, 190.23, 42.52 Q 200.58, 42.91, 210.92, 43.14 Q 221.27, 43.18,\
                        231.62, 43.07 Q 241.96, 42.82, 252.31, 43.25 Q 262.65, 44.00, 273.00, 44.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 45.00 Q 15.35, 44.94, 25.69, 44.59 Q 36.04, 44.18, 46.38, 44.52 Q 56.73,\
                        45.20, 67.08, 44.99 Q 77.42, 45.05, 87.77, 45.82 Q 98.12, 45.55, 108.46, 45.56 Q 118.81, 44.60, 129.15, 45.03 Q 139.50, 45.66,\
                        149.85, 44.50 Q 160.19, 45.61, 170.54, 45.23 Q 180.88, 43.90, 191.23, 45.47 Q 201.58, 44.85, 211.92, 45.66 Q 222.27, 45.19,\
                        232.62, 43.58 Q 242.96, 43.72, 253.31, 44.38 Q 263.65, 45.00, 274.00, 45.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 46.00 Q 16.35, 45.24, 26.69, 45.07 Q 37.04, 45.67, 47.38, 45.37 Q 57.73,\
                        45.58, 68.08, 46.21 Q 78.42, 45.29, 88.77, 45.62 Q 99.12, 45.25, 109.46, 45.75 Q 119.81, 46.27, 130.15, 46.57 Q 140.50, 46.77,\
                        150.85, 45.68 Q 161.19, 45.81, 171.54, 46.18 Q 181.88, 45.97, 192.23, 45.65 Q 202.58, 46.07, 212.92, 46.12 Q 223.27, 46.65,\
                        233.62, 46.07 Q 243.96, 45.74, 254.31, 45.79 Q 264.65, 46.00, 275.00, 46.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page189476043-layer-1634000025button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page189476043-layer-1634000025button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page189476043-layer-1634000025button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:44px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Контакты<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 48px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page189476043-layer-1634000025\', \'interaction327886842\', {"button":"left","id":"action131477880","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction656799605","options":"reloadOnly","target":"page607113527","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page189476043-layer-1832345745" style="position: absolute; left: 355px; top: 710px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1832345745" data-review-reference-id="1832345745">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-816793362" style="position: absolute; left: 355px; top: 195px; width: 930px; height: 400px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="816793362" data-review-reference-id="816793362">\
            <div class="stencil-wrapper" style="width: 930px; height: 400px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-page189476043-layer-816793362-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 400px;width:930px;" width="930" height="400">\
                     <svg:g id="__containerId__-page189476043-layer-816793362svg" width="930" height="400"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.07, 3.70, 22.13, 2.70 Q 32.20, 2.10, 42.26, 1.69 Q 52.33, 1.48,\
                        62.39, 1.41 Q 72.46, 1.25, 82.52, 1.21 Q 92.59, 0.88, 102.65, 1.22 Q 112.72, 1.19, 122.78, 1.04 Q 132.85, 0.96, 142.91, 0.75\
                        Q 152.98, 1.15, 163.04, 1.23 Q 173.11, 0.55, 183.17, 0.99 Q 193.24, 0.67, 203.30, 0.67 Q 213.37, 1.21, 223.43, 1.10 Q 233.50,\
                        1.21, 243.57, 0.98 Q 253.63, 0.76, 263.70, 1.30 Q 273.76, 0.75, 283.83, 0.56 Q 293.89, 0.60, 303.96, 1.26 Q 314.02, 1.54,\
                        324.09, 1.29 Q 334.15, 0.92, 344.22, 0.62 Q 354.28, 0.85, 364.35, 1.10 Q 374.41, 0.67, 384.48, 1.14 Q 394.54, 1.29, 404.61,\
                        1.19 Q 414.67, 0.71, 424.74, 1.39 Q 434.80, 1.38, 444.87, 1.35 Q 454.93, 0.95, 465.00, 2.29 Q 475.07, 1.41, 485.13, 1.59 Q\
                        495.20, 1.43, 505.26, 1.43 Q 515.33, 1.98, 525.39, 2.01 Q 535.46, 1.15, 545.52, 0.79 Q 555.59, 0.87, 565.65, 0.78 Q 575.72,\
                        0.77, 585.78, 0.86 Q 595.85, 1.28, 605.91, 2.24 Q 615.98, 2.63, 626.04, 2.41 Q 636.11, 2.04, 646.17, 1.91 Q 656.24, 1.39,\
                        666.30, 1.24 Q 676.37, 0.94, 686.44, 1.39 Q 696.50, 1.88, 706.57, 1.94 Q 716.63, 3.03, 726.70, 1.86 Q 736.76, 1.27, 746.83,\
                        1.06 Q 756.89, 0.99, 766.96, 1.51 Q 777.02, 1.27, 787.09, 1.42 Q 797.15, 0.91, 807.22, 0.84 Q 817.28, 0.03, 827.35, 0.11 Q\
                        837.41, 1.30, 847.48, 1.44 Q 857.54, 0.78, 867.61, 1.38 Q 877.67, 1.78, 887.74, 1.19 Q 897.81, 1.42, 907.87, 0.60 Q 917.94,\
                        1.38, 927.58, 2.42 Q 927.42, 12.62, 928.43, 22.78 Q 928.46, 33.23, 928.28, 43.68 Q 926.79, 54.12, 928.02, 64.53 Q 929.05,\
                        74.94, 928.51, 85.37 Q 928.06, 95.79, 927.69, 106.21 Q 927.03, 116.63, 926.94, 127.05 Q 927.24, 137.47, 928.11, 147.89 Q 928.30,\
                        158.32, 929.20, 168.74 Q 928.71, 179.16, 929.15, 189.58 Q 929.36, 200.00, 929.04, 210.42 Q 929.08, 220.84, 929.03, 231.26\
                        Q 929.23, 241.68, 929.33, 252.11 Q 929.42, 262.53, 928.88, 272.95 Q 928.19, 283.37, 927.70, 293.79 Q 927.59, 304.21, 927.48,\
                        314.63 Q 927.87, 325.05, 928.32, 335.47 Q 928.67, 345.89, 928.71, 356.32 Q 928.19, 366.74, 928.26, 377.16 Q 928.21, 387.58,\
                        928.38, 398.38 Q 918.08, 398.42, 907.94, 398.48 Q 897.85, 398.71, 887.76, 398.63 Q 877.67, 397.73, 867.61, 397.51 Q 857.54,\
                        398.00, 847.48, 397.64 Q 837.41, 398.20, 827.35, 398.73 Q 817.28, 398.06, 807.22, 398.11 Q 797.15, 398.56, 787.09, 399.64\
                        Q 777.02, 400.02, 766.96, 399.58 Q 756.89, 398.90, 746.83, 399.57 Q 736.76, 399.64, 726.70, 400.05 Q 716.63, 399.34, 706.57,\
                        398.85 Q 696.50, 399.14, 686.44, 399.45 Q 676.37, 398.24, 666.30, 398.38 Q 656.24, 398.65, 646.17, 399.32 Q 636.11, 398.77,\
                        626.04, 398.67 Q 615.98, 399.18, 605.91, 399.57 Q 595.85, 399.30, 585.78, 399.74 Q 575.72, 399.81, 565.65, 399.35 Q 555.59,\
                        398.86, 545.52, 399.13 Q 535.46, 399.78, 525.39, 399.37 Q 515.33, 399.54, 505.26, 398.46 Q 495.20, 397.41, 485.13, 398.40\
                        Q 475.07, 399.39, 465.00, 399.59 Q 454.93, 398.85, 444.87, 398.83 Q 434.80, 398.98, 424.74, 399.36 Q 414.67, 399.25, 404.61,\
                        399.11 Q 394.54, 398.65, 384.48, 397.76 Q 374.41, 397.34, 364.35, 397.54 Q 354.28, 398.33, 344.22, 399.11 Q 334.15, 399.47,\
                        324.09, 399.16 Q 314.02, 399.04, 303.96, 398.87 Q 293.89, 397.80, 283.83, 395.91 Q 273.76, 397.03, 263.70, 397.79 Q 253.63,\
                        398.46, 243.57, 398.97 Q 233.50, 397.75, 223.43, 398.72 Q 213.37, 398.27, 203.30, 398.81 Q 193.24, 398.31, 183.17, 398.12\
                        Q 173.11, 397.32, 163.04, 397.70 Q 152.98, 398.18, 142.91, 397.66 Q 132.85, 397.94, 122.78, 396.86 Q 112.72, 398.36, 102.65,\
                        397.51 Q 92.59, 397.45, 82.52, 398.29 Q 72.46, 398.86, 62.39, 399.53 Q 52.33, 399.28, 42.26, 399.27 Q 32.20, 399.51, 22.13,\
                        399.96 Q 12.07, 399.82, 1.19, 398.81 Q 1.07, 387.89, 1.14, 377.28 Q 0.60, 366.83, 0.35, 356.37 Q 0.46, 345.92, 0.36, 335.49\
                        Q 0.22, 325.06, 0.74, 314.63 Q 0.53, 304.21, 0.74, 293.79 Q 0.81, 283.37, 0.58, 272.95 Q 0.16, 262.53, 0.68, 252.11 Q 0.42,\
                        241.68, 1.55, 231.26 Q 2.26, 220.84, 2.14, 210.42 Q 1.03, 200.00, 1.05, 189.58 Q 1.24, 179.16, 0.83, 168.74 Q 1.46, 158.32,\
                        1.33, 147.89 Q 1.74, 137.47, 0.93, 127.05 Q 0.95, 116.63, 0.95, 106.21 Q 1.07, 95.79, 1.27, 85.37 Q 0.76, 74.95, 1.59, 64.53\
                        Q 0.75, 54.11, 1.49, 43.68 Q 1.40, 33.26, 1.57, 22.84 Q 2.00, 12.42, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 926px; height:396px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page189476043-layer-816793362-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page189476043-layer-816793362-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page189476043-layer-816793362-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page189476043-layer-816793362-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-page189476043-layer-816793362-accordion", "926", "396", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-page189476043-layer-iphoneListBackground528657481" style="position: absolute; left: 355px; top: 225px; width: 930px; height: 280px" data-interactive-element-type="static.iphoneListBackground" class="iphoneListBackground stencil mobile-interaction-potential-trigger " data-stencil-id="iphoneListBackground528657481" data-review-reference-id="iphoneListBackground528657481">\
            <div class="stencil-wrapper" style="width: 930px; height: 280px">\
               <div title="" style="height: 280px;width:930px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="930" height="280" viewBox="0 0 930 280"><svg:path class=" svg_unselected_element" d="M 2.00, 268.00 Q 0.40, 257.33, 1.06, 246.67 Q 1.70, 236.00, 1.45, 225.33 Q 1.28,\
                     214.67, 2.06, 204.00 Q 1.84, 193.33, 1.68, 182.67 Q 1.40, 172.00, 1.77, 161.33 Q 2.33, 150.67, 2.37, 140.00 Q 1.88, 129.33,\
                     1.62, 118.67 Q 1.24, 108.00, 0.81, 97.33 Q 1.27, 86.67, 0.64, 76.00 Q 0.95, 65.33, 1.25, 54.67 Q 1.93, 44.00, 1.86, 33.33\
                     Q 1.80, 22.67, 1.33, 11.89 Q 1.50, 10.25, 1.75, 8.54 Q 2.60, 7.61, 3.68, 6.85 Q 3.77, 5.65, 4.74, 4.75 Q 5.96, 4.44, 6.54,\
                     3.24 Q 8.09, 3.66, 8.71, 2.34 Q 10.24, 1.83, 11.71, 0.42 Q 21.95, 0.67, 32.09, 1.12 Q 42.20, 1.91, 52.26, 1.27 Q 62.33, 1.20,\
                     72.40, 2.07 Q 82.47, 1.59, 92.53, 1.93 Q 102.60, 2.07, 112.67, 2.12 Q 122.73, 2.22, 132.80, 2.00 Q 142.87, 2.38, 152.93, 0.90\
                     Q 163.00, 1.26, 173.07, 1.14 Q 183.13, 1.65, 193.20, 1.23 Q 203.27, 1.55, 213.33, 2.07 Q 223.40, 2.08, 233.47, 1.48 Q 243.53,\
                     1.25, 253.60, 2.49 Q 263.67, 2.36, 273.73, 2.85 Q 283.80, 1.42, 293.87, 1.51 Q 303.93, 3.06, 314.00, 2.64 Q 324.07, 1.93,\
                     334.13, 0.75 Q 344.20, 1.86, 354.27, 1.16 Q 364.33, 1.20, 374.40, 0.09 Q 384.47, 1.16, 394.53, 1.42 Q 404.60, 1.22, 414.67,\
                     1.19 Q 424.73, 1.01, 434.80, 1.75 Q 444.87, 0.97, 454.93, 0.68 Q 465.00, 0.49, 475.07, 0.79 Q 485.13, 1.62, 495.20, 1.62 Q\
                     505.27, 0.93, 515.33, 0.53 Q 525.40, 0.95, 535.47, 1.29 Q 545.53, 2.11, 555.60, 1.77 Q 565.67, 2.19, 575.73, 2.15 Q 585.80,\
                     2.41, 595.87, 2.18 Q 605.93, 1.47, 616.00, 1.99 Q 626.07, 2.12, 636.13, 2.75 Q 646.20, 1.11, 656.27, 0.67 Q 666.33, 0.91,\
                     676.40, 1.81 Q 686.47, 0.49, 696.53, 1.07 Q 706.60, 1.66, 716.67, 1.73 Q 726.73, 1.91, 736.80, 0.87 Q 746.87, 0.63, 756.93,\
                     0.66 Q 767.00, 1.41, 777.07, 1.37 Q 787.13, 1.39, 797.20, 1.22 Q 807.27, 1.15, 817.33, 0.98 Q 827.40, 0.44, 837.47, 1.26 Q\
                     847.53, 1.07, 857.60, 2.49 Q 867.67, 1.36, 877.73, 1.30 Q 887.80, 2.30, 897.87, 2.63 Q 907.93, 2.27, 918.10, 1.37 Q 919.61,\
                     2.04, 920.80, 3.54 Q 921.81, 3.93, 922.47, 5.14 Q 923.47, 5.59, 924.74, 5.27 Q 924.63, 6.62, 925.74, 7.15 Q 927.10, 7.67,\
                     927.38, 8.83 Q 928.35, 10.17, 927.89, 12.02 Q 928.97, 22.54, 929.30, 33.19 Q 928.90, 43.85, 928.96, 54.49 Q 929.50, 65.12,\
                     929.90, 75.74 Q 929.37, 86.37, 928.49, 97.00 Q 928.27, 107.62, 928.02, 118.25 Q 927.98, 128.87, 928.52, 139.50 Q 928.28, 150.12,\
                     928.91, 160.75 Q 929.62, 171.37, 929.34, 182.00 Q 928.62, 192.62, 928.39, 203.25 Q 929.41, 213.87, 929.86, 224.50 Q 929.90,\
                     235.12, 929.80, 245.75 Q 929.46, 256.37, 929.36, 267.22 Q 928.93, 268.85, 928.19, 270.44 Q 927.72, 271.53, 927.35, 272.63\
                     Q 926.96, 273.71, 926.28, 275.26 Q 925.11, 276.04, 924.04, 276.72 Q 923.13, 277.55, 921.95, 278.18 Q 920.24, 278.43, 918.39,\
                     279.14 Q 908.13, 279.15, 897.97, 279.26 Q 887.84, 278.89, 877.74, 277.67 Q 867.67, 277.59, 857.60, 278.43 Q 847.53, 278.03,\
                     837.47, 277.95 Q 827.40, 278.30, 817.33, 278.37 Q 807.27, 277.86, 797.20, 277.61 Q 787.13, 276.96, 777.07, 277.67 Q 767.00,\
                     278.17, 756.93, 277.80 Q 746.87, 277.51, 736.80, 277.29 Q 726.73, 277.85, 716.67, 277.29 Q 706.60, 276.07, 696.53, 275.93\
                     Q 686.47, 276.39, 676.40, 277.64 Q 666.33, 278.43, 656.27, 278.74 Q 646.20, 278.70, 636.13, 279.32 Q 626.07, 279.28, 616.00,\
                     278.28 Q 605.93, 277.44, 595.87, 277.99 Q 585.80, 277.96, 575.73, 277.02 Q 565.67, 276.12, 555.60, 276.93 Q 545.53, 277.20,\
                     535.47, 277.30 Q 525.40, 276.09, 515.33, 276.30 Q 505.27, 277.02, 495.20, 277.62 Q 485.13, 277.88, 475.07, 278.31 Q 465.00,\
                     278.55, 454.93, 278.67 Q 444.87, 278.66, 434.80, 277.99 Q 424.73, 277.84, 414.67, 278.05 Q 404.60, 277.53, 394.53, 278.09\
                     Q 384.47, 278.14, 374.40, 278.47 Q 364.33, 277.68, 354.27, 277.94 Q 344.20, 278.09, 334.13, 278.41 Q 324.07, 278.54, 314.00,\
                     278.94 Q 303.93, 277.79, 293.87, 277.29 Q 283.80, 277.61, 273.73, 278.30 Q 263.67, 278.42, 253.60, 277.11 Q 243.53, 277.92,\
                     233.47, 277.86 Q 223.40, 277.77, 213.33, 277.66 Q 203.27, 277.97, 193.20, 277.33 Q 183.13, 277.50, 173.07, 277.68 Q 163.00,\
                     278.07, 152.93, 278.02 Q 142.87, 277.66, 132.80, 278.42 Q 122.73, 277.39, 112.67, 277.84 Q 102.60, 277.97, 92.53, 277.30 Q\
                     82.47, 277.65, 72.40, 277.64 Q 62.33, 277.71, 52.27, 276.35 Q 42.20, 275.86, 32.13, 276.72 Q 22.07, 278.09, 11.86, 277.89\
                     Q 10.52, 276.43, 9.33, 275.11 Q 8.04, 275.40, 6.88, 275.25 Q 5.75, 275.02, 4.46, 274.54 Q 4.24, 273.19, 3.77, 272.14 Q 2.05,\
                     271.79, 1.08, 270.84 Q 2.50, 268.50, 2.00, 267.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 1.00, 40.00 Q 11.09, 39.96, 21.17, 40.40 Q 31.26, 40.76, 41.35, 40.65 Q 51.43,\
                     39.60, 61.52, 40.19 Q 71.61, 39.19, 81.70, 39.23 Q 91.78, 38.99, 101.87, 38.47 Q 111.96, 38.74, 122.04, 39.75 Q 132.13, 40.05,\
                     142.22, 38.69 Q 152.30, 39.14, 162.39, 39.60 Q 172.48, 40.73, 182.57, 38.95 Q 192.65, 38.40, 202.74, 37.74 Q 212.83, 37.75,\
                     222.91, 38.03 Q 233.00, 38.67, 243.09, 40.10 Q 253.17, 39.89, 263.26, 39.68 Q 273.35, 38.26, 283.43, 37.53 Q 293.52, 37.74,\
                     303.61, 38.35 Q 313.70, 38.78, 323.78, 38.89 Q 333.87, 39.57, 343.96, 40.44 Q 354.04, 40.06, 364.13, 39.28 Q 374.22, 39.31,\
                     384.30, 39.37 Q 394.39, 39.27, 404.48, 38.79 Q 414.57, 38.42, 424.65, 38.32 Q 434.74, 38.16, 444.83, 38.07 Q 454.91, 37.92,\
                     465.00, 37.99 Q 475.09, 38.50, 485.17, 39.56 Q 495.26, 39.41, 505.35, 38.38 Q 515.43, 38.72, 525.52, 38.63 Q 535.61, 38.09,\
                     545.70, 38.30 Q 555.78, 38.08, 565.87, 38.26 Q 575.96, 38.45, 586.04, 38.95 Q 596.13, 39.09, 606.22, 39.48 Q 616.30, 39.55,\
                     626.39, 39.34 Q 636.48, 39.30, 646.57, 39.34 Q 656.65, 39.33, 666.74, 39.43 Q 676.83, 39.74, 686.91, 39.52 Q 697.00, 39.30,\
                     707.09, 39.35 Q 717.17, 38.73, 727.26, 38.65 Q 737.35, 39.10, 747.43, 39.08 Q 757.52, 39.78, 767.61, 39.36 Q 777.70, 39.36,\
                     787.78, 39.29 Q 797.87, 39.29, 807.96, 38.79 Q 818.04, 38.16, 828.13, 38.70 Q 838.22, 38.59, 848.30, 38.68 Q 858.39, 38.86,\
                     868.48, 38.68 Q 878.57, 37.96, 888.65, 37.88 Q 898.74, 38.25, 908.83, 37.76 Q 918.91, 40.00, 929.00, 40.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 1.00, 80.00 Q 11.09, 80.37, 21.17, 79.93 Q 31.26, 79.19, 41.35, 78.60 Q 51.43,\
                     78.30, 61.52, 77.74 Q 71.61, 78.10, 81.70, 77.97 Q 91.78, 77.93, 101.87, 78.19 Q 111.96, 77.94, 122.04, 78.15 Q 132.13, 78.46,\
                     142.22, 78.59 Q 152.30, 78.51, 162.39, 78.26 Q 172.48, 78.28, 182.57, 77.94 Q 192.65, 78.37, 202.74, 78.87 Q 212.83, 79.14,\
                     222.91, 78.52 Q 233.00, 78.16, 243.09, 78.72 Q 253.17, 78.85, 263.26, 79.00 Q 273.35, 79.15, 283.43, 79.33 Q 293.52, 79.50,\
                     303.61, 79.50 Q 313.70, 79.20, 323.78, 79.19 Q 333.87, 79.23, 343.96, 79.03 Q 354.04, 78.86, 364.13, 78.39 Q 374.22, 78.61,\
                     384.30, 79.49 Q 394.39, 79.08, 404.48, 79.66 Q 414.57, 79.79, 424.65, 79.32 Q 434.74, 79.55, 444.83, 78.90 Q 454.91, 78.69,\
                     465.00, 79.10 Q 475.09, 79.69, 485.17, 80.08 Q 495.26, 80.18, 505.35, 80.16 Q 515.43, 79.81, 525.52, 79.35 Q 535.61, 78.91,\
                     545.70, 78.99 Q 555.78, 78.86, 565.87, 79.89 Q 575.96, 79.35, 586.04, 79.55 Q 596.13, 79.49, 606.22, 79.53 Q 616.30, 79.55,\
                     626.39, 79.11 Q 636.48, 79.24, 646.57, 79.64 Q 656.65, 80.25, 666.74, 79.88 Q 676.83, 80.56, 686.91, 79.13 Q 697.00, 78.44,\
                     707.09, 78.65 Q 717.17, 78.33, 727.26, 78.52 Q 737.35, 78.02, 747.43, 78.85 Q 757.52, 78.95, 767.61, 79.91 Q 777.70, 79.11,\
                     787.78, 79.34 Q 797.87, 80.02, 807.96, 80.37 Q 818.04, 80.80, 828.13, 80.52 Q 838.22, 80.18, 848.30, 79.80 Q 858.39, 80.36,\
                     868.48, 80.05 Q 878.57, 79.68, 888.65, 79.02 Q 898.74, 78.89, 908.83, 79.03 Q 918.91, 80.00, 929.00, 80.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 1.00, 120.00 Q 11.09, 117.85, 21.17, 118.44 Q 31.26, 118.74, 41.35, 119.09\
                     Q 51.43, 119.13, 61.52, 119.37 Q 71.61, 119.59, 81.70, 119.16 Q 91.78, 119.36, 101.87, 119.14 Q 111.96, 119.17, 122.04, 119.70\
                     Q 132.13, 120.02, 142.22, 120.08 Q 152.30, 118.94, 162.39, 118.95 Q 172.48, 119.37, 182.57, 119.39 Q 192.65, 119.35, 202.74,\
                     119.16 Q 212.83, 119.05, 222.91, 119.79 Q 233.00, 119.81, 243.09, 120.02 Q 253.17, 121.09, 263.26, 120.33 Q 273.35, 120.34,\
                     283.43, 120.73 Q 293.52, 119.48, 303.61, 119.37 Q 313.70, 117.90, 323.78, 118.62 Q 333.87, 119.23, 343.96, 119.22 Q 354.04,\
                     119.93, 364.13, 119.80 Q 374.22, 119.92, 384.30, 120.09 Q 394.39, 119.31, 404.48, 120.12 Q 414.57, 119.08, 424.65, 119.26\
                     Q 434.74, 119.69, 444.83, 120.48 Q 454.91, 121.27, 465.00, 120.63 Q 475.09, 119.21, 485.17, 119.79 Q 495.26, 120.24, 505.35,\
                     119.91 Q 515.43, 118.87, 525.52, 118.77 Q 535.61, 118.82, 545.70, 119.06 Q 555.78, 119.26, 565.87, 120.31 Q 575.96, 120.16,\
                     586.04, 119.02 Q 596.13, 120.19, 606.22, 119.76 Q 616.30, 119.14, 626.39, 118.25 Q 636.48, 118.09, 646.57, 118.30 Q 656.65,\
                     118.54, 666.74, 118.79 Q 676.83, 118.73, 686.91, 118.97 Q 697.00, 118.88, 707.09, 119.80 Q 717.17, 119.65, 727.26, 119.43\
                     Q 737.35, 120.13, 747.43, 120.19 Q 757.52, 119.89, 767.61, 120.08 Q 777.70, 119.48, 787.78, 118.73 Q 797.87, 119.32, 807.96,\
                     119.98 Q 818.04, 119.81, 828.13, 118.48 Q 838.22, 118.57, 848.30, 119.13 Q 858.39, 120.08, 868.48, 119.57 Q 878.57, 119.32,\
                     888.65, 120.41 Q 898.74, 121.61, 908.83, 121.71 Q 918.91, 120.00, 929.00, 120.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 1.00, 160.00 Q 11.09, 159.78, 21.17, 159.68 Q 31.26, 159.59, 41.35, 159.40\
                     Q 51.43, 159.10, 61.52, 159.10 Q 71.61, 159.01, 81.70, 158.91 Q 91.78, 158.68, 101.87, 158.74 Q 111.96, 158.90, 122.04, 158.79\
                     Q 132.13, 158.66, 142.22, 158.50 Q 152.30, 158.34, 162.39, 158.85 Q 172.48, 158.86, 182.57, 158.51 Q 192.65, 158.27, 202.74,\
                     159.03 Q 212.83, 158.76, 222.91, 158.86 Q 233.00, 158.65, 243.09, 158.49 Q 253.17, 158.65, 263.26, 158.52 Q 273.35, 158.46,\
                     283.43, 158.29 Q 293.52, 158.17, 303.61, 158.23 Q 313.70, 158.00, 323.78, 158.84 Q 333.87, 158.47, 343.96, 159.05 Q 354.04,\
                     158.74, 364.13, 158.95 Q 374.22, 158.91, 384.30, 159.09 Q 394.39, 158.91, 404.48, 159.81 Q 414.57, 160.03, 424.65, 159.19\
                     Q 434.74, 159.91, 444.83, 159.96 Q 454.91, 159.75, 465.00, 159.54 Q 475.09, 159.30, 485.17, 159.24 Q 495.26, 159.04, 505.35,\
                     159.32 Q 515.43, 159.10, 525.52, 159.12 Q 535.61, 159.46, 545.70, 159.50 Q 555.78, 159.32, 565.87, 158.62 Q 575.96, 158.45,\
                     586.04, 158.33 Q 596.13, 158.82, 606.22, 159.17 Q 616.30, 159.36, 626.39, 158.43 Q 636.48, 158.67, 646.57, 159.46 Q 656.65,\
                     159.33, 666.74, 159.16 Q 676.83, 159.37, 686.91, 160.38 Q 697.00, 160.12, 707.09, 159.85 Q 717.17, 159.59, 727.26, 159.84\
                     Q 737.35, 159.78, 747.43, 159.59 Q 757.52, 159.61, 767.61, 159.78 Q 777.70, 160.19, 787.78, 159.58 Q 797.87, 160.21, 807.96,\
                     160.26 Q 818.04, 161.03, 828.13, 160.09 Q 838.22, 159.59, 848.30, 159.42 Q 858.39, 159.38, 868.48, 160.20 Q 878.57, 159.67,\
                     888.65, 160.09 Q 898.74, 159.31, 908.83, 159.87 Q 918.91, 160.00, 929.00, 160.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 1.00, 200.00 Q 11.09, 200.29, 21.17, 200.32 Q 31.26, 200.20, 41.35, 199.98\
                     Q 51.43, 200.25, 61.52, 199.73 Q 71.61, 199.61, 81.70, 198.99 Q 91.78, 199.11, 101.87, 199.30 Q 111.96, 198.90, 122.04, 198.50\
                     Q 132.13, 197.99, 142.22, 199.14 Q 152.30, 199.10, 162.39, 198.42 Q 172.48, 197.95, 182.57, 197.56 Q 192.65, 197.60, 202.74,\
                     197.94 Q 212.83, 198.23, 222.91, 198.14 Q 233.00, 198.13, 243.09, 198.43 Q 253.17, 198.21, 263.26, 198.22 Q 273.35, 198.50,\
                     283.43, 199.52 Q 293.52, 199.72, 303.61, 198.94 Q 313.70, 198.43, 323.78, 198.39 Q 333.87, 198.47, 343.96, 198.85 Q 354.04,\
                     198.79, 364.13, 198.43 Q 374.22, 198.24, 384.30, 198.06 Q 394.39, 197.87, 404.48, 198.02 Q 414.57, 198.63, 424.65, 198.12\
                     Q 434.74, 198.78, 444.83, 198.58 Q 454.91, 198.01, 465.00, 198.04 Q 475.09, 198.17, 485.17, 198.16 Q 495.26, 198.28, 505.35,\
                     198.42 Q 515.43, 198.63, 525.52, 199.09 Q 535.61, 199.32, 545.70, 199.23 Q 555.78, 199.42, 565.87, 199.06 Q 575.96, 199.49,\
                     586.04, 199.41 Q 596.13, 199.25, 606.22, 198.98 Q 616.30, 199.23, 626.39, 199.28 Q 636.48, 199.01, 646.57, 199.22 Q 656.65,\
                     199.52, 666.74, 199.03 Q 676.83, 199.07, 686.91, 199.60 Q 697.00, 198.95, 707.09, 199.83 Q 717.17, 199.81, 727.26, 199.55\
                     Q 737.35, 199.16, 747.43, 198.43 Q 757.52, 198.35, 767.61, 198.15 Q 777.70, 197.80, 787.78, 198.06 Q 797.87, 197.86, 807.96,\
                     199.03 Q 818.04, 199.91, 828.13, 200.01 Q 838.22, 200.03, 848.30, 198.83 Q 858.39, 198.49, 868.48, 199.73 Q 878.57, 199.80,\
                     888.65, 199.78 Q 898.74, 199.10, 908.83, 198.58 Q 918.91, 200.00, 929.00, 200.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 1.00, 240.00 Q 11.09, 238.64, 21.17, 239.32 Q 31.26, 240.54, 41.35, 239.29\
                     Q 51.43, 240.30, 61.52, 240.63 Q 71.61, 239.46, 81.70, 238.83 Q 91.78, 239.71, 101.87, 240.44 Q 111.96, 240.99, 122.04, 239.71\
                     Q 132.13, 238.82, 142.22, 238.22 Q 152.30, 238.39, 162.39, 238.47 Q 172.48, 239.84, 182.57, 239.98 Q 192.65, 238.98, 202.74,\
                     239.29 Q 212.83, 239.52, 222.91, 239.42 Q 233.00, 238.70, 243.09, 238.29 Q 253.17, 238.44, 263.26, 238.32 Q 273.35, 238.08,\
                     283.43, 237.94 Q 293.52, 239.45, 303.61, 238.82 Q 313.70, 239.17, 323.78, 238.45 Q 333.87, 239.77, 343.96, 240.50 Q 354.04,\
                     240.53, 364.13, 240.60 Q 374.22, 239.62, 384.30, 239.59 Q 394.39, 239.23, 404.48, 239.91 Q 414.57, 239.03, 424.65, 240.11\
                     Q 434.74, 239.92, 444.83, 240.07 Q 454.91, 239.77, 465.00, 239.42 Q 475.09, 239.16, 485.17, 238.78 Q 495.26, 239.15, 505.35,\
                     238.78 Q 515.43, 238.54, 525.52, 238.44 Q 535.61, 238.19, 545.70, 238.12 Q 555.78, 238.47, 565.87, 238.38 Q 575.96, 239.02,\
                     586.04, 240.51 Q 596.13, 239.36, 606.22, 239.72 Q 616.30, 239.81, 626.39, 240.45 Q 636.48, 239.56, 646.57, 239.13 Q 656.65,\
                     238.98, 666.74, 240.76 Q 676.83, 241.20, 686.91, 240.13 Q 697.00, 239.77, 707.09, 239.72 Q 717.17, 239.93, 727.26, 239.78\
                     Q 737.35, 239.23, 747.43, 238.25 Q 757.52, 237.75, 767.61, 238.69 Q 777.70, 238.39, 787.78, 238.30 Q 797.87, 239.00, 807.96,\
                     239.49 Q 818.04, 238.42, 828.13, 238.64 Q 838.22, 238.38, 848.30, 240.34 Q 858.39, 239.72, 868.48, 238.64 Q 878.57, 238.51,\
                     888.65, 239.18 Q 898.74, 238.89, 908.83, 238.78 Q 918.91, 240.00, 929.00, 240.00" style=" fill:none;"/>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page189476043"] .border-wrapper, body[data-current-page-id="page189476043"] .simulation-container{\
         			width:1366px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page189476043"] .border-wrapper, body.has-frame[data-current-page-id="page189476043"]\
         .simulation-container{\
         			height:768px;\
         		}\
         		\
         		body[data-current-page-id="page189476043"] .svg-border-1366-768{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page189476043"] .border-wrapper .border-div{\
         			width:1366px;\
         			height:768px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page189476043",\
      			"name": "changes",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":1366,\
      			"height":768,\
      			"parentFolder": "",\
      			"frame": "desktop",\
      			"frameOrientation": "landscape"\
      		}\
      	\
   </div>\
   <div id="border-wrapper">\
      <div xmlns="http://www.w3.org/1999/xhtml" xmlns:json="http://json.org/" class="svg-border svg-border-1366-768" style="display: none;">\
         <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" class="svg_border" style="position:absolute;left:-43px;top:-13px;width:1423px;height:792px;"><svg:path class=" svg_unselected_element" d="M 32.00, 3.00 Q 42.12, 3.66, 52.24, 3.28 Q 62.35, 3.78, 72.47, 2.26 Q 82.59,\
            2.65, 92.71, 2.85 Q 102.82, 1.80, 112.94, 1.17 Q 123.06, 2.20, 133.18, 2.72 Q 143.29, 3.04, 153.41, 2.49 Q 163.53, 3.69, 173.65,\
            1.74 Q 183.76, 3.09, 193.88, 1.36 Q 204.00, 1.46, 214.12, 2.11 Q 224.24, 2.03, 234.35, 2.73 Q 244.47, 3.44, 254.59, 2.90 Q\
            264.71, 2.34, 274.82, 0.78 Q 284.94, 1.22, 295.06, 2.42 Q 305.18, 1.94, 315.29, 0.77 Q 325.41, 1.49, 335.53, 3.75 Q 345.65,\
            3.39, 355.76, 1.40 Q 365.88, 1.36, 376.00, 2.07 Q 386.12, 2.53, 396.24, 3.55 Q 406.35, 3.03, 416.47, 2.91 Q 426.59, 2.89,\
            436.71, 3.01 Q 446.82, 2.82, 456.94, 2.57 Q 467.06, 1.86, 477.18, 1.21 Q 487.29, 1.59, 497.41, 1.49 Q 507.53, 1.53, 517.65,\
            0.95 Q 527.76, 0.87, 537.88, 2.19 Q 548.00, 3.09, 558.12, 3.68 Q 568.24, 3.39, 578.35, 2.86 Q 588.47, 1.67, 598.59, 1.01 Q\
            608.71, 1.13, 618.82, 1.54 Q 628.94, 2.29, 639.06, 3.22 Q 649.18, 3.40, 659.29, 3.11 Q 669.41, 2.99, 679.53, 2.39 Q 689.65,\
            1.57, 699.77, 2.24 Q 709.88, 1.46, 720.00, 2.53 Q 730.12, 1.96, 740.24, 2.58 Q 750.35, 2.85, 760.47, 4.13 Q 770.59, 3.51,\
            780.71, 2.95 Q 790.82, 2.26, 800.94, 2.35 Q 811.06, 2.25, 821.18, 1.98 Q 831.29, 1.76, 841.41, 2.64 Q 851.53, 2.80, 861.65,\
            2.54 Q 871.77, 1.88, 881.88, 2.59 Q 892.00, 2.64, 902.12, 2.26 Q 912.24, 1.46, 922.35, 1.51 Q 932.47, 1.18, 942.59, 1.09 Q\
            952.71, 0.90, 962.82, 1.41 Q 972.94, 1.66, 983.06, 2.10 Q 993.18, 1.68, 1003.30, 1.45 Q 1013.41, 1.89, 1023.53, 1.51 Q 1033.65,\
            1.00, 1043.77, 2.36 Q 1053.88, 2.11, 1064.00, 2.15 Q 1074.12, 1.80, 1084.24, 1.83 Q 1094.35, 1.45, 1104.47, 1.39 Q 1114.59,\
            1.51, 1124.71, 1.48 Q 1134.83, 1.89, 1144.94, 1.71 Q 1155.06, 1.60, 1165.18, 1.44 Q 1175.30, 1.41, 1185.41, 1.38 Q 1195.53,\
            1.72, 1205.65, 2.05 Q 1215.77, 1.48, 1225.88, 2.70 Q 1236.00, 1.85, 1246.12, 1.68 Q 1256.24, 1.59, 1266.35, 1.41 Q 1276.47,\
            1.34, 1286.59, 1.83 Q 1296.71, 1.89, 1306.83, 2.36 Q 1316.94, 2.06, 1327.06, 2.08 Q 1337.18, 2.54, 1347.30, 1.89 Q 1357.41,\
            2.69, 1367.53, 3.13 Q 1377.65, 2.80, 1387.77, 0.94 Q 1397.88, 2.00, 1408.43, 2.57 Q 1408.96, 12.85, 1409.27, 23.16 Q 1408.43,\
            33.48, 1407.81, 43.69 Q 1408.41, 53.85, 1408.26, 64.02 Q 1408.25, 74.20, 1406.87, 84.37 Q 1407.27, 94.54, 1408.22, 104.71\
            Q 1407.78, 114.88, 1408.81, 125.05 Q 1408.48, 135.22, 1409.01, 145.39 Q 1408.75, 155.57, 1408.84, 165.74 Q 1409.46, 175.91,\
            1409.45, 186.08 Q 1409.18, 196.25, 1409.37, 206.42 Q 1409.39, 216.59, 1409.57, 226.76 Q 1409.61, 236.93, 1408.76, 247.11 Q\
            1409.26, 257.28, 1408.35, 267.45 Q 1408.90, 277.62, 1408.58, 287.79 Q 1408.00, 297.96, 1408.27, 308.13 Q 1408.17, 318.30,\
            1407.82, 328.47 Q 1408.08, 338.64, 1408.62, 348.82 Q 1407.82, 358.99, 1408.68, 369.16 Q 1408.08, 379.33, 1409.36, 389.50 Q\
            1408.25, 399.67, 1408.71, 409.84 Q 1409.42, 420.01, 1409.37, 430.18 Q 1409.63, 440.36, 1409.18, 450.53 Q 1408.90, 460.70,\
            1409.12, 470.87 Q 1408.57, 481.04, 1408.22, 491.21 Q 1408.38, 501.38, 1408.95, 511.55 Q 1408.26, 521.72, 1407.65, 531.89 Q\
            1407.05, 542.07, 1408.80, 552.24 Q 1409.17, 562.41, 1408.91, 572.58 Q 1407.98, 582.75, 1408.31, 592.92 Q 1409.09, 603.09,\
            1408.39, 613.26 Q 1409.50, 623.43, 1408.96, 633.61 Q 1407.92, 643.78, 1406.93, 653.95 Q 1407.95, 664.12, 1408.26, 674.29 Q\
            1408.48, 684.46, 1408.21, 694.63 Q 1407.77, 704.80, 1408.81, 714.97 Q 1409.14, 725.15, 1409.36, 735.32 Q 1409.61, 745.49,\
            1409.68, 755.66 Q 1409.22, 765.83, 1408.64, 776.64 Q 1398.22, 777.01, 1387.96, 777.34 Q 1377.76, 777.62, 1367.59, 777.74 Q\
            1357.44, 777.39, 1347.31, 777.33 Q 1337.18, 777.53, 1327.06, 777.70 Q 1316.94, 777.34, 1306.83, 777.31 Q 1296.71, 777.42,\
            1286.59, 777.59 Q 1276.47, 777.75, 1266.35, 777.56 Q 1256.24, 778.18, 1246.12, 777.84 Q 1236.00, 777.09, 1225.88, 777.01 Q\
            1215.77, 777.04, 1205.65, 776.02 Q 1195.53, 776.31, 1185.41, 776.76 Q 1175.30, 776.77, 1165.18, 775.63 Q 1155.06, 775.72,\
            1144.94, 776.72 Q 1134.83, 776.83, 1124.71, 776.99 Q 1114.59, 777.02, 1104.47, 776.58 Q 1094.35, 776.84, 1084.24, 776.87 Q\
            1074.12, 777.65, 1064.00, 777.49 Q 1053.88, 777.39, 1043.77, 776.64 Q 1033.65, 776.49, 1023.53, 776.09 Q 1013.41, 775.76,\
            1003.30, 776.21 Q 993.18, 775.63, 983.06, 775.71 Q 972.94, 775.92, 962.82, 775.74 Q 952.71, 775.74, 942.59, 774.81 Q 932.47,\
            776.10, 922.35, 775.56 Q 912.24, 775.79, 902.12, 775.44 Q 892.00, 775.87, 881.88, 776.19 Q 871.77, 775.84, 861.65, 775.76\
            Q 851.53, 776.48, 841.41, 777.67 Q 831.29, 777.27, 821.18, 776.54 Q 811.06, 776.28, 800.94, 776.83 Q 790.82, 777.03, 780.71,\
            777.20 Q 770.59, 777.35, 760.47, 777.58 Q 750.35, 777.60, 740.24, 776.82 Q 730.12, 774.85, 720.00, 775.57 Q 709.88, 776.56,\
            699.77, 776.43 Q 689.65, 776.12, 679.53, 776.13 Q 669.41, 776.83, 659.29, 776.95 Q 649.18, 776.21, 639.06, 775.19 Q 628.94,\
            775.11, 618.82, 775.71 Q 608.71, 776.53, 598.59, 776.72 Q 588.47, 776.49, 578.35, 774.83 Q 568.24, 775.27, 558.12, 776.10\
            Q 548.00, 776.13, 537.88, 775.90 Q 527.76, 777.13, 517.65, 776.71 Q 507.53, 776.51, 497.41, 776.45 Q 487.29, 776.34, 477.18,\
            776.03 Q 467.06, 776.13, 456.94, 776.99 Q 446.82, 776.46, 436.71, 777.02 Q 426.59, 776.66, 416.47, 776.45 Q 406.35, 776.74,\
            396.24, 776.55 Q 386.12, 776.45, 376.00, 776.54 Q 365.88, 775.89, 355.76, 777.00 Q 345.65, 776.79, 335.53, 776.85 Q 325.41,\
            776.25, 315.29, 776.03 Q 305.18, 776.43, 295.06, 776.76 Q 284.94, 776.21, 274.82, 774.94 Q 264.71, 775.68, 254.59, 775.77\
            Q 244.47, 775.56, 234.35, 775.42 Q 224.24, 774.59, 214.12, 773.85 Q 204.00, 774.84, 193.88, 775.14 Q 183.76, 775.38, 173.65,\
            775.48 Q 163.53, 775.15, 153.41, 775.45 Q 143.29, 775.03, 133.18, 775.68 Q 123.06, 775.91, 112.94, 776.56 Q 102.82, 776.30,\
            92.71, 775.46 Q 82.59, 776.49, 72.47, 776.64 Q 62.35, 775.50, 52.24, 775.30 Q 42.12, 775.78, 31.31, 776.69 Q 32.04, 765.82,\
            32.06, 755.65 Q 32.25, 745.47, 31.53, 735.33 Q 31.48, 725.15, 30.62, 714.99 Q 30.67, 704.81, 30.18, 694.64 Q 30.66, 684.46,\
            29.89, 674.29 Q 30.89, 664.12, 30.99, 653.95 Q 30.69, 643.78, 31.37, 633.61 Q 31.18, 623.43, 30.85, 613.26 Q 31.63, 603.09,\
            31.50, 592.92 Q 32.88, 582.75, 32.49, 572.58 Q 32.14, 562.41, 31.94, 552.24 Q 30.94, 542.07, 30.99, 531.89 Q 31.36, 521.72,\
            31.49, 511.55 Q 31.17, 501.38, 31.85, 491.21 Q 32.61, 481.04, 32.28, 470.87 Q 30.91, 460.70, 30.01, 450.53 Q 30.16, 440.36,\
            30.37, 430.18 Q 31.16, 420.01, 30.35, 409.84 Q 30.79, 399.67, 30.91, 389.50 Q 30.97, 379.33, 30.45, 369.16 Q 30.03, 358.99,\
            30.86, 348.82 Q 32.70, 338.64, 32.50, 328.47 Q 31.41, 318.30, 30.44, 308.13 Q 30.38, 297.96, 31.28, 287.79 Q 31.19, 277.62,\
            32.52, 267.45 Q 32.59, 257.28, 33.53, 247.11 Q 32.08, 236.93, 31.98, 226.76 Q 31.84, 216.59, 31.61, 206.42 Q 31.58, 196.25,\
            31.92, 186.08 Q 31.43, 175.91, 31.89, 165.74 Q 32.38, 155.57, 32.43, 145.39 Q 31.09, 135.22, 30.71, 125.05 Q 30.57, 114.88,\
            30.88, 104.71 Q 30.88, 94.54, 31.09, 84.37 Q 30.66, 74.20, 30.43, 64.03 Q 29.98, 53.86, 30.19, 43.68 Q 30.06, 33.51, 30.78,\
            23.34 Q 32.00, 13.17, 32.00, 3.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 23.00, 7.00 Q 33.12, 5.37, 43.24, 5.29 Q 53.35, 5.64, 63.47, 5.72 Q 73.59,\
            5.70, 83.71, 6.00 Q 93.82, 6.09, 103.94, 5.88 Q 114.06, 5.64, 124.18, 5.81 Q 134.29, 6.05, 144.41, 6.23 Q 154.53, 6.46, 164.65,\
            6.83 Q 174.76, 7.07, 184.88, 6.39 Q 195.00, 5.97, 205.12, 6.07 Q 215.24, 6.42, 225.35, 6.04 Q 235.47, 6.02, 245.59, 6.05 Q\
            255.71, 6.10, 265.82, 6.41 Q 275.94, 5.51, 286.06, 6.40 Q 296.18, 6.14, 306.29, 6.39 Q 316.41, 6.31, 326.53, 6.28 Q 336.65,\
            6.03, 346.76, 5.76 Q 356.88, 5.62, 367.00, 6.14 Q 377.12, 5.36, 387.24, 6.19 Q 397.35, 6.17, 407.47, 6.14 Q 417.59, 6.37,\
            427.71, 5.82 Q 437.82, 6.08, 447.94, 6.10 Q 458.06, 5.77, 468.18, 5.45 Q 478.29, 5.56, 488.41, 6.22 Q 498.53, 5.83, 508.65,\
            6.40 Q 518.76, 5.38, 528.88, 5.41 Q 539.00, 5.81, 549.12, 6.59 Q 559.24, 6.69, 569.35, 6.49 Q 579.47, 6.44, 589.59, 6.56 Q\
            599.71, 6.49, 609.82, 6.40 Q 619.94, 6.73, 630.06, 6.97 Q 640.18, 6.88, 650.29, 6.58 Q 660.41, 6.42, 670.53, 5.88 Q 680.65,\
            5.61, 690.77, 5.54 Q 700.88, 7.05, 711.00, 6.79 Q 721.12, 5.78, 731.24, 5.94 Q 741.35, 6.50, 751.47, 7.00 Q 761.59, 6.69,\
            771.71, 6.97 Q 781.82, 7.01, 791.94, 6.77 Q 802.06, 7.66, 812.18, 7.85 Q 822.29, 7.15, 832.41, 6.10 Q 842.53, 5.50, 852.65,\
            6.47 Q 862.77, 7.17, 872.88, 6.54 Q 883.00, 6.22, 893.12, 6.45 Q 903.24, 6.91, 913.35, 5.90 Q 923.47, 5.55, 933.59, 5.75 Q\
            943.71, 6.37, 953.82, 6.90 Q 963.94, 7.73, 974.06, 6.95 Q 984.18, 7.27, 994.30, 7.43 Q 1004.41, 6.24, 1014.53, 5.82 Q 1024.65,\
            5.80, 1034.77, 6.12 Q 1044.88, 6.45, 1055.00, 6.45 Q 1065.12, 6.25, 1075.24, 6.86 Q 1085.35, 6.89, 1095.47, 7.20 Q 1105.59,\
            6.45, 1115.71, 6.48 Q 1125.83, 6.45, 1135.94, 6.50 Q 1146.06, 6.32, 1156.18, 6.49 Q 1166.30, 6.60, 1176.41, 6.46 Q 1186.53,\
            6.81, 1196.65, 6.49 Q 1206.77, 7.62, 1216.88, 7.11 Q 1227.00, 7.26, 1237.12, 7.35 Q 1247.24, 7.61, 1257.35, 7.31 Q 1267.47,\
            6.87, 1277.59, 6.64 Q 1287.71, 6.34, 1297.83, 6.67 Q 1307.94, 6.33, 1318.06, 6.33 Q 1328.18, 5.84, 1338.30, 5.75 Q 1348.41,\
            5.97, 1358.53, 5.42 Q 1368.65, 5.95, 1378.77, 8.03 Q 1388.88, 8.74, 1398.79, 7.21 Q 1398.69, 17.28, 1399.77, 27.23 Q 1399.34,\
            37.49, 1399.60, 47.66 Q 1399.06, 57.85, 1399.31, 68.02 Q 1399.48, 78.20, 1398.66, 88.37 Q 1398.97, 98.54, 1399.22, 108.71\
            Q 1399.77, 118.88, 1399.61, 129.05 Q 1399.60, 139.22, 1399.22, 149.39 Q 1399.44, 159.57, 1400.27, 169.74 Q 1399.47, 179.91,\
            1399.12, 190.08 Q 1398.31, 200.25, 1397.90, 210.42 Q 1399.25, 220.59, 1398.43, 230.76 Q 1399.42, 240.93, 1400.20, 251.11 Q\
            1400.72, 261.28, 1401.16, 271.45 Q 1401.18, 281.62, 1400.74, 291.79 Q 1399.68, 301.96, 1399.68, 312.13 Q 1399.69, 322.30,\
            1399.77, 332.47 Q 1399.84, 342.64, 1400.31, 352.82 Q 1400.61, 362.99, 1400.64, 373.16 Q 1400.26, 383.33, 1400.37, 393.50 Q\
            1401.10, 403.67, 1400.72, 413.84 Q 1401.09, 424.01, 1401.10, 434.18 Q 1401.08, 444.36, 1400.94, 454.53 Q 1400.39, 464.70,\
            1399.84, 474.87 Q 1399.46, 485.04, 1399.53, 495.21 Q 1398.82, 505.38, 1398.59, 515.55 Q 1398.02, 525.72, 1399.15, 535.89 Q\
            1399.35, 546.07, 1399.39, 556.24 Q 1399.23, 566.41, 1399.46, 576.58 Q 1400.38, 586.75, 1400.52, 596.92 Q 1400.34, 607.09,\
            1399.99, 617.26 Q 1399.61, 627.43, 1399.31, 637.61 Q 1399.18, 647.78, 1398.96, 657.95 Q 1399.20, 668.12, 1399.35, 678.29 Q\
            1398.94, 688.46, 1398.92, 698.63 Q 1399.17, 708.80, 1399.70, 718.97 Q 1399.46, 729.15, 1399.57, 739.32 Q 1399.45, 749.49,\
            1399.21, 759.66 Q 1400.66, 769.83, 1398.80, 779.80 Q 1389.11, 780.68, 1378.89, 780.83 Q 1368.67, 780.24, 1358.54, 780.35 Q\
            1348.42, 780.07, 1338.30, 780.12 Q 1328.18, 780.31, 1318.06, 780.79 Q 1307.94, 780.80, 1297.83, 781.03 Q 1287.71, 781.09,\
            1277.59, 780.33 Q 1267.47, 779.83, 1257.35, 780.22 Q 1247.24, 780.38, 1237.12, 780.40 Q 1227.00, 780.23, 1216.88, 780.43 Q\
            1206.77, 780.33, 1196.65, 780.50 Q 1186.53, 779.15, 1176.41, 778.90 Q 1166.30, 778.97, 1156.18, 779.48 Q 1146.06, 779.72,\
            1135.94, 779.63 Q 1125.83, 779.36, 1115.71, 779.29 Q 1105.59, 778.62, 1095.47, 778.46 Q 1085.35, 779.05, 1075.24, 779.96 Q\
            1065.12, 780.84, 1055.00, 779.68 Q 1044.88, 779.99, 1034.77, 780.39 Q 1024.65, 780.85, 1014.53, 780.95 Q 1004.41, 780.48,\
            994.30, 780.56 Q 984.18, 780.10, 974.06, 780.24 Q 963.94, 780.08, 953.82, 780.70 Q 943.71, 780.52, 933.59, 781.10 Q 923.47,\
            781.68, 913.35, 781.72 Q 903.24, 782.10, 893.12, 781.49 Q 883.00, 780.43, 872.88, 781.93 Q 862.77, 782.31, 852.65, 781.80\
            Q 842.53, 780.69, 832.41, 780.09 Q 822.29, 779.44, 812.18, 779.16 Q 802.06, 779.42, 791.94, 780.68 Q 781.82, 780.20, 771.71,\
            780.02 Q 761.59, 780.93, 751.47, 781.33 Q 741.35, 780.22, 731.24, 779.86 Q 721.12, 780.45, 711.00, 779.69 Q 700.88, 780.49,\
            690.77, 779.66 Q 680.65, 780.12, 670.53, 780.26 Q 660.41, 780.61, 650.29, 781.00 Q 640.18, 780.96, 630.06, 780.47 Q 619.94,\
            779.89, 609.82, 780.94 Q 599.71, 780.21, 589.59, 780.55 Q 579.47, 780.99, 569.35, 781.20 Q 559.24, 781.39, 549.12, 781.11\
            Q 539.00, 780.65, 528.88, 780.91 Q 518.76, 781.18, 508.65, 780.32 Q 498.53, 780.21, 488.41, 780.17 Q 478.29, 780.91, 468.18,\
            781.32 Q 458.06, 780.75, 447.94, 780.46 Q 437.82, 779.46, 427.71, 780.35 Q 417.59, 779.21, 407.47, 779.39 Q 397.35, 779.47,\
            387.24, 779.03 Q 377.12, 780.21, 367.00, 780.86 Q 356.88, 779.84, 346.76, 779.67 Q 336.65, 781.12, 326.53, 781.30 Q 316.41,\
            781.81, 306.29, 781.55 Q 296.18, 781.28, 286.06, 781.28 Q 275.94, 781.29, 265.82, 781.74 Q 255.71, 782.01, 245.59, 782.20\
            Q 235.47, 782.23, 225.35, 782.04 Q 215.24, 782.32, 205.12, 781.83 Q 195.00, 782.28, 184.88, 782.28 Q 174.76, 781.70, 164.65,\
            781.40 Q 154.53, 781.69, 144.41, 781.89 Q 134.29, 781.77, 124.18, 781.19 Q 114.06, 780.91, 103.94, 780.72 Q 93.82, 781.31,\
            83.71, 781.35 Q 73.59, 781.47, 63.47, 781.82 Q 53.35, 781.91, 43.24, 781.57 Q 33.12, 781.61, 22.17, 780.83 Q 21.59, 770.30,\
            21.28, 759.90 Q 22.44, 749.52, 22.15, 739.34 Q 21.98, 729.16, 21.73, 718.98 Q 21.67, 708.81, 21.51, 698.64 Q 21.39, 688.46,\
            21.30, 678.29 Q 20.88, 668.12, 20.72, 657.95 Q 20.82, 647.78, 21.09, 637.61 Q 21.03, 627.43, 21.51, 617.26 Q 21.99, 607.09,\
            22.22, 596.92 Q 22.12, 586.75, 22.18, 576.58 Q 22.80, 566.41, 22.45, 556.24 Q 22.64, 546.07, 22.64, 535.89 Q 23.58, 525.72,\
            23.71, 515.55 Q 23.09, 505.38, 23.15, 495.21 Q 22.79, 485.04, 22.69, 474.87 Q 22.98, 464.70, 22.25, 454.53 Q 22.66, 444.36,\
            22.49, 434.18 Q 22.82, 424.01, 22.47, 413.84 Q 21.76, 403.67, 21.68, 393.50 Q 21.90, 383.33, 22.00, 373.16 Q 22.35, 362.99,\
            21.93, 352.82 Q 21.61, 342.64, 21.68, 332.47 Q 21.46, 322.30, 21.57, 312.13 Q 21.57, 301.96, 21.50, 291.79 Q 22.19, 281.62,\
            22.16, 271.45 Q 22.67, 261.28, 22.50, 251.11 Q 22.55, 240.93, 22.57, 230.76 Q 22.68, 220.59, 22.63, 210.42 Q 23.30, 200.25,\
            23.30, 190.08 Q 23.04, 179.91, 23.48, 169.74 Q 22.61, 159.57, 23.20, 149.39 Q 21.78, 139.22, 22.04, 129.05 Q 21.96, 118.88,\
            22.02, 108.71 Q 22.56, 98.54, 22.69, 88.37 Q 21.74, 78.20, 22.83, 68.03 Q 23.59, 57.86, 23.17, 47.68 Q 21.33, 37.51, 21.06,\
            27.34 Q 23.00, 17.17, 23.00, 7.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 40.00, 11.00 Q 50.12, 10.71, 60.24, 10.85 Q 70.35, 10.80, 80.47, 10.84 Q 90.59,\
            10.68, 100.71, 11.14 Q 110.82, 11.61, 120.94, 11.06 Q 131.06, 12.07, 141.18, 10.17 Q 151.29, 9.84, 161.41, 10.76 Q 171.53,\
            10.54, 181.65, 10.66 Q 191.76, 10.17, 201.88, 10.32 Q 212.00, 11.62, 222.12, 12.23 Q 232.24, 11.01, 242.35, 9.71 Q 252.47,\
            9.51, 262.59, 9.25 Q 272.71, 10.31, 282.82, 11.23 Q 292.94, 11.16, 303.06, 10.89 Q 313.18, 11.85, 323.29, 10.31 Q 333.41,\
            9.20, 343.53, 9.12 Q 353.65, 8.67, 363.76, 9.16 Q 373.88, 10.47, 384.00, 10.48 Q 394.12, 11.35, 404.24, 11.22 Q 414.35, 10.76,\
            424.47, 10.59 Q 434.59, 10.56, 444.71, 9.90 Q 454.82, 10.01, 464.94, 10.24 Q 475.06, 10.01, 485.18, 9.93 Q 495.29, 10.26,\
            505.41, 10.73 Q 515.53, 10.07, 525.65, 10.81 Q 535.76, 11.71, 545.88, 9.85 Q 556.00, 8.93, 566.12, 9.04 Q 576.24, 9.37, 586.35,\
            10.42 Q 596.47, 9.61, 606.59, 9.21 Q 616.71, 9.26, 626.82, 9.05 Q 636.94, 9.54, 647.06, 10.16 Q 657.18, 9.30, 667.29, 9.26\
            Q 677.41, 9.50, 687.53, 9.53 Q 697.65, 9.68, 707.77, 10.55 Q 717.88, 10.00, 728.00, 9.73 Q 738.12, 10.49, 748.24, 10.29 Q\
            758.35, 9.53, 768.47, 9.75 Q 778.59, 9.70, 788.71, 9.88 Q 798.82, 10.12, 808.94, 10.22 Q 819.06, 10.62, 829.18, 10.83 Q 839.29,\
            9.67, 849.41, 9.70 Q 859.53, 9.42, 869.65, 9.98 Q 879.77, 10.11, 889.88, 10.07 Q 900.00, 9.97, 910.12, 9.95 Q 920.24, 9.66,\
            930.35, 9.59 Q 940.47, 9.13, 950.59, 9.62 Q 960.71, 10.03, 970.82, 10.08 Q 980.94, 9.99, 991.06, 9.54 Q 1001.18, 9.03, 1011.30,\
            9.45 Q 1021.41, 9.99, 1031.53, 10.97 Q 1041.65, 10.81, 1051.77, 9.92 Q 1061.88, 9.43, 1072.00, 9.35 Q 1082.12, 9.26, 1092.24,\
            9.45 Q 1102.35, 9.42, 1112.47, 10.33 Q 1122.59, 10.77, 1132.71, 11.82 Q 1142.83, 11.54, 1152.94, 10.42 Q 1163.06, 10.51, 1173.18,\
            9.84 Q 1183.30, 9.90, 1193.41, 9.79 Q 1203.53, 9.80, 1213.65, 9.87 Q 1223.77, 10.12, 1233.88, 9.91 Q 1244.00, 9.68, 1254.12,\
            10.18 Q 1264.24, 9.83, 1274.35, 8.95 Q 1284.47, 9.54, 1294.59, 9.74 Q 1304.71, 9.43, 1314.83, 9.59 Q 1324.94, 10.73, 1335.06,\
            10.53 Q 1345.18, 10.01, 1355.30, 9.48 Q 1365.41, 9.51, 1375.53, 9.82 Q 1385.65, 9.88, 1395.77, 9.88 Q 1405.88, 9.92, 1416.42,\
            10.59 Q 1416.59, 20.97, 1416.60, 31.26 Q 1417.11, 41.44, 1416.73, 51.66 Q 1417.39, 61.83, 1416.87, 72.02 Q 1416.92, 82.19,\
            1417.25, 92.37 Q 1416.60, 102.54, 1417.21, 112.71 Q 1416.44, 122.88, 1417.05, 133.05 Q 1416.69, 143.22, 1416.73, 153.39 Q\
            1416.11, 163.57, 1415.93, 173.74 Q 1415.81, 183.91, 1415.54, 194.08 Q 1415.71, 204.25, 1415.62, 214.42 Q 1416.45, 224.59,\
            1416.61, 234.76 Q 1417.06, 244.93, 1416.80, 255.11 Q 1416.50, 265.28, 1417.52, 275.45 Q 1415.94, 285.62, 1416.53, 295.79 Q\
            1415.33, 305.96, 1416.29, 316.13 Q 1415.84, 326.30, 1415.90, 336.47 Q 1415.95, 346.64, 1416.22, 356.82 Q 1416.14, 366.99,\
            1416.49, 377.16 Q 1416.84, 387.33, 1416.84, 397.50 Q 1417.31, 407.67, 1416.44, 417.84 Q 1417.37, 428.01, 1417.28, 438.18 Q\
            1416.99, 448.36, 1417.42, 458.53 Q 1416.78, 468.70, 1417.53, 478.87 Q 1416.95, 489.04, 1417.63, 499.21 Q 1416.79, 509.38,\
            1416.39, 519.55 Q 1416.44, 529.72, 1416.34, 539.89 Q 1417.76, 550.07, 1418.12, 560.24 Q 1417.85, 570.41, 1418.25, 580.58 Q\
            1417.59, 590.75, 1416.95, 600.92 Q 1417.18, 611.09, 1416.90, 621.26 Q 1416.91, 631.43, 1417.56, 641.61 Q 1417.33, 651.78,\
            1417.20, 661.95 Q 1417.23, 672.12, 1417.01, 682.29 Q 1416.92, 692.46, 1417.15, 702.63 Q 1417.27, 712.80, 1417.29, 722.97 Q\
            1417.52, 733.15, 1417.58, 743.32 Q 1417.79, 753.49, 1418.07, 763.66 Q 1417.82, 773.83, 1416.22, 784.22 Q 1406.19, 784.91,\
            1395.96, 785.33 Q 1385.71, 784.97, 1375.55, 784.68 Q 1365.42, 784.28, 1355.30, 784.07 Q 1345.18, 784.18, 1335.06, 785.05 Q\
            1324.94, 784.20, 1314.83, 783.99 Q 1304.71, 783.83, 1294.59, 784.00 Q 1284.47, 784.29, 1274.35, 784.46 Q 1264.24, 784.48,\
            1254.12, 784.62 Q 1244.00, 784.96, 1233.88, 784.74 Q 1223.77, 784.57, 1213.65, 784.61 Q 1203.53, 784.02, 1193.41, 783.95 Q\
            1183.30, 784.12, 1173.18, 784.06 Q 1163.06, 784.60, 1152.94, 784.46 Q 1142.83, 783.67, 1132.71, 783.28 Q 1122.59, 783.73,\
            1112.47, 783.90 Q 1102.35, 785.32, 1092.24, 785.43 Q 1082.12, 784.64, 1072.00, 784.35 Q 1061.88, 784.21, 1051.77, 784.07 Q\
            1041.65, 783.73, 1031.53, 784.45 Q 1021.41, 785.40, 1011.30, 784.98 Q 1001.18, 783.55, 991.06, 783.85 Q 980.94, 784.38, 970.82,\
            783.46 Q 960.71, 784.11, 950.59, 783.14 Q 940.47, 784.39, 930.35, 784.01 Q 920.24, 784.40, 910.12, 784.38 Q 900.00, 784.93,\
            889.88, 785.14 Q 879.77, 785.30, 869.65, 785.49 Q 859.53, 785.46, 849.41, 785.57 Q 839.29, 784.86, 829.18, 784.63 Q 819.06,\
            784.48, 808.94, 784.70 Q 798.82, 784.61, 788.71, 784.90 Q 778.59, 784.50, 768.47, 784.49 Q 758.35, 784.82, 748.24, 784.47\
            Q 738.12, 784.85, 728.00, 784.64 Q 717.88, 784.97, 707.77, 785.61 Q 697.65, 785.86, 687.53, 784.38 Q 677.41, 783.76, 667.29,\
            784.41 Q 657.18, 784.91, 647.06, 785.04 Q 636.94, 783.87, 626.82, 783.49 Q 616.71, 784.65, 606.59, 784.48 Q 596.47, 783.63,\
            586.35, 783.95 Q 576.24, 784.43, 566.12, 784.86 Q 556.00, 784.63, 545.88, 783.90 Q 535.76, 783.77, 525.65, 784.19 Q 515.53,\
            783.90, 505.41, 783.58 Q 495.29, 783.38, 485.18, 783.65 Q 475.06, 784.08, 464.94, 783.83 Q 454.82, 783.64, 444.71, 783.98\
            Q 434.59, 784.63, 424.47, 784.70 Q 414.35, 784.11, 404.24, 784.54 Q 394.12, 784.85, 384.00, 785.33 Q 373.88, 784.83, 363.76,\
            784.96 Q 353.65, 785.33, 343.53, 785.67 Q 333.41, 786.10, 323.29, 785.51 Q 313.18, 784.28, 303.06, 784.65 Q 292.94, 785.85,\
            282.82, 786.19 Q 272.71, 785.54, 262.59, 783.52 Q 252.47, 785.16, 242.35, 784.81 Q 232.24, 783.60, 222.12, 782.84 Q 212.00,\
            783.33, 201.88, 784.32 Q 191.76, 783.65, 181.65, 783.73 Q 171.53, 784.26, 161.41, 783.67 Q 151.29, 782.33, 141.18, 783.92\
            Q 131.06, 783.88, 120.94, 783.83 Q 110.82, 784.23, 100.71, 783.46 Q 90.59, 783.90, 80.47, 784.30 Q 70.35, 784.77, 60.24, 784.80\
            Q 50.12, 784.23, 39.77, 784.23 Q 39.93, 773.85, 39.32, 763.76 Q 39.88, 753.50, 40.26, 743.31 Q 39.69, 733.15, 38.81, 722.98\
            Q 39.72, 712.80, 38.95, 702.63 Q 39.46, 692.46, 39.64, 682.29 Q 39.16, 672.12, 39.19, 661.95 Q 38.77, 651.78, 39.45, 641.61\
            Q 38.28, 631.43, 38.56, 621.26 Q 39.54, 611.09, 39.65, 600.92 Q 38.95, 590.75, 38.68, 580.58 Q 39.13, 570.41, 39.75, 560.24\
            Q 40.97, 550.07, 40.27, 539.89 Q 40.83, 529.72, 40.44, 519.55 Q 40.27, 509.38, 40.72, 499.21 Q 39.70, 489.04, 38.54, 478.87\
            Q 37.67, 468.70, 37.88, 458.53 Q 39.20, 448.36, 38.69, 438.18 Q 38.57, 428.01, 37.67, 417.84 Q 37.67, 407.67, 38.16, 397.50\
            Q 39.34, 387.33, 38.98, 377.16 Q 38.77, 366.99, 38.51, 356.82 Q 38.73, 346.64, 38.67, 336.47 Q 38.27, 326.30, 38.58, 316.13\
            Q 38.30, 305.96, 37.74, 295.79 Q 38.14, 285.62, 38.49, 275.45 Q 38.54, 265.28, 38.58, 255.11 Q 38.54, 244.93, 38.59, 234.76\
            Q 38.60, 224.59, 39.38, 214.42 Q 39.32, 204.25, 39.16, 194.08 Q 39.37, 183.91, 39.19, 173.74 Q 38.65, 163.57, 38.86, 153.39\
            Q 38.81, 143.22, 38.83, 133.05 Q 38.84, 122.88, 38.41, 112.71 Q 38.29, 102.54, 38.25, 92.37 Q 38.42, 82.20, 38.02, 72.03 Q\
            37.83, 61.86, 37.81, 51.68 Q 37.74, 41.51, 38.39, 31.34 Q 40.00, 21.17, 40.00, 11.00" style=" fill:white;"/>\
         </svg:svg>\
      </div>\
   </div>\
</div>');