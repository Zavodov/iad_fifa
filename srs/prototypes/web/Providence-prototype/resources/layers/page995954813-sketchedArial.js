rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page995954813-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page995954813" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page995954813-layer-277972261" style="position: absolute; left: 274px; top: 0px; width: 1092px; height: 768px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="277972261" data-review-reference-id="277972261">\
            <div class="stencil-wrapper" style="width: 1092px; height: 768px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 768px;width:1092px;" width="1092" height="768" viewBox="0 0 1092 768">\
                     <svg:g width="1092" height="768">\
                        <svg:rect x="0" y="0" width="1092" height="768" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="1092" y2="768" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="768" x2="1092" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1281359082" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 768px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1281359082" data-review-reference-id="1281359082">\
            <div class="stencil-wrapper" style="width: 275px; height: 768px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 768px; width:275px;" width="275" height="768" viewBox="0 0 275 768">\
                     <svg:g width="275" height="768">\
                        <svg:rect x="0" y="0" width="275" height="768" style="stroke-width:1;stroke:black;fill:#C1C1C1;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1108730242" style="position: absolute; left: 355px; top: 130px; width: 930px; height: 30px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1108730242" data-review-reference-id="1108730242">\
            <div class="stencil-wrapper" style="width: 930px; height: 30px">\
               <div id="1108730242-95509338" style="position: absolute; left: 0px; top: 0px; width: 1335px; height: 665px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="95509338" data-review-reference-id="95509338">\
                  <div class="stencil-wrapper" style="width: 1335px; height: 665px">\
                     <div id="95509338-949097759" style="position: absolute; left: 0px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="949097759" data-review-reference-id="949097759">\
                        <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                           <div title="">\
                              <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                                 <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                                    <svg:path id="95509338-949097759_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                                 </svg:g>\
                              </svg:svg>\
                              <div id="95509338-949097759div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'95509338-949097759\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'95509338-949097759\', \'result\');">\
                                 				RFPL\
                                 				\
                                 <addMouseOverListener></addMouseOverListener>\
                                 				\
                                 <addMouseOutListener></addMouseOutListener>\
                                 			\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1108730242-171458935" style="position: absolute; left: 155px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="171458935" data-review-reference-id="171458935">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="1108730242-171458935_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1108730242-171458935div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1108730242-171458935\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1108730242-171458935\', \'result\');">\
                           				Premier League\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1108730242-105188535" style="position: absolute; left: 310px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="105188535" data-review-reference-id="105188535">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="1108730242-105188535_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1108730242-105188535div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1108730242-105188535\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1108730242-105188535\', \'result\');">\
                           				BundesLiga\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1108730242-1827932383" style="position: absolute; left: 465px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1827932383" data-review-reference-id="1827932383">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="1108730242-1827932383_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1108730242-1827932383div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1108730242-1827932383\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1108730242-1827932383\', \'result\');">\
                           				Ligue 1\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1108730242-1559693441" style="position: absolute; left: 620px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1559693441" data-review-reference-id="1559693441">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="1108730242-1559693441_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1108730242-1559693441div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1108730242-1559693441\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1108730242-1559693441\', \'result\');">\
                           				Ligue BBVA\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1108730242-1199591457" style="position: absolute; left: 775px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1199591457" data-review-reference-id="1199591457">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="1108730242-1199591457_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1108730242-1199591457div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1108730242-1199591457\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1108730242-1199591457\', \'result\');">\
                           				Serie A\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1276864689" style="position: absolute; left: 275px; top: 160px; width: 1091px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1276864689" data-review-reference-id="1276864689">\
            <div class="stencil-wrapper" style="width: 1091px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px; width:1091px;" width="1091" height="465" viewBox="0 0 1091 465">\
                     <svg:g width="1091" height="465">\
                        <svg:rect x="0" y="0" width="1091" height="465" style="stroke-width:1;stroke:black;fill:#a7a77c;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-63566695" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 160px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="63566695" data-review-reference-id="63566695">\
            <div class="stencil-wrapper" style="width: 275px; height: 160px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 160px; width:275px;" width="275" height="160" viewBox="0 0 275 160">\
                     <svg:g width="275" height="160">\
                        <svg:rect x="0" y="0" width="275" height="160" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1687614788" style="position: absolute; left: 15px; top: 35px; width: 240px; height: 87px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1687614788" data-review-reference-id="1687614788">\
            <div class="stencil-wrapper" style="width: 240px; height: 87px">\
               <div id="1687614788-1631949613" style="position: absolute; left: 0px; top: 0px; width: 85px; height: 85px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="1631949613" data-review-reference-id="1631949613">\
                  <div class="stencil-wrapper" style="width: 85px; height: 85px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 85px;width:85px;" width="85" height="85" viewBox="0 0 85 85">\
                           <svg:g width="85" height="85">\
                              <svg:ellipse cx="42.5" cy="42.5" rx="42.5" ry="42.5" style="stroke-width:1;stroke:black;fill:white;"></svg:ellipse>\
                           </svg:g>\
                        </svg:svg>\
                     </div>\
                  </div>\
               </div>\
               <div id="1687614788-2084429644" style="position: absolute; left: 100px; top: 0px; width: 91px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="2084429644" data-review-reference-id="2084429644">\
                  <div class="stencil-wrapper" style="width: 91px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Name</span></p></span></span></div>\
                  </div>\
               </div>\
               <div id="1687614788-1926687529" style="position: absolute; left: 100px; top: 50px; width: 145px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1926687529" data-review-reference-id="1926687529">\
                  <div class="stencil-wrapper" style="width: 145px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Surname </p></span></span></div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1665666048" style="position: absolute; left: 276px; top: 0px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1665666048" data-review-reference-id="1665666048">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:1090px;" width="1090" height="80" viewBox="0 0 1090 80">\
                     <svg:g width="1090" height="80">\
                        <svg:rect x="0" y="0" width="1090" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-1325471400" style="position: absolute; left: 276px; top: 688px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1325471400" data-review-reference-id="1325471400">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:1090px;" width="1090" height="80" viewBox="0 0 1090 80">\
                     <svg:g width="1090" height="80">\
                        <svg:rect x="0" y="0" width="1090" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-743554012" style="position: absolute; left: 355px; top: 10px; width: 230px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="743554012" data-review-reference-id="743554012">\
            <div class="stencil-wrapper" style="width: 230px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:230px;" width="230" height="60" viewBox="0 0 230 60">\
                     <svg:g width="230" height="60">\
                        <svg:rect x="0" y="0" width="230" height="60" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="230" y2="60" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="60" x2="230" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-411069666" style="position: absolute; left: 0px; top: 180px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="411069666" data-review-reference-id="411069666">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Коэффициенты<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page995954813-layer-411069666\', \'276992336\', {"button":"left","id":"2141500830","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"598465367","options":"reloadOnly","target":"page606870902","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page995954813-layer-479308961" style="position: absolute; left: 1117px; top: 20px; width: 174px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="479308961" data-review-reference-id="479308961">\
            <div class="stencil-wrapper" style="width: 174px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Статистика</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-647396613" style="position: absolute; left: 0px; top: 250px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="647396613" data-review-reference-id="647396613">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Изменения<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page995954813-layer-647396613\', \'interaction366609757\', {"button":"left","id":"action466528516","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction3821626","options":"reloadOnly","target":"page189476043","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page995954813-layer-1504055364" style="position: absolute; left: 0px; top: 320px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="1504055364" data-review-reference-id="1504055364">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#a7a77c;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Статистика<br /></button></div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-4427567" style="position: absolute; left: 0px; top: 718px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="4427567" data-review-reference-id="4427567">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Выход<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page995954813-layer-4427567\', \'interaction576353665\', {"button":"left","id":"action1641582","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction70262354","options":"reloadOnly","target":"page940099653","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page995954813-layer-1667912423" style="position: absolute; left: 0px; top: 673px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1667912423" data-review-reference-id="1667912423">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Настройки<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page995954813-layer-1667912423\', \'interaction60967251\', {"button":"left","id":"action743983144","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction367706269","options":"reloadOnly","target":"page47785501","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page995954813-layer-449336035" style="position: absolute; left: 0px; top: 625px; width: 275px; height: 48px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="449336035" data-review-reference-id="449336035">\
            <div class="stencil-wrapper" style="width: 275px; height: 48px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:48px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Контакты<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 48px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page995954813-layer-449336035\', \'interaction898740289\', {"button":"left","id":"action602857057","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction981232967","options":"reloadOnly","target":"page607113527","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page995954813-layer-53444741" style="position: absolute; left: 355px; top: 710px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="53444741" data-review-reference-id="53444741">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page995954813-layer-514781659" style="position: absolute; left: 355px; top: 195px; width: 930px; height: 395px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="514781659" data-review-reference-id="514781659">\
            <div class="stencil-wrapper" style="width: 930px; height: 395px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-page995954813-layer-514781659-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 395px;width:930px;" width="930" height="395">\
                     <svg:g id="__containerId__-page995954813-layer-514781659svg" width="930" height="395"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.07, 1.54, 22.13, 1.39 Q 32.20, 1.30, 42.26, 1.29 Q 52.33, 1.50,\
                        62.39, 1.29 Q 72.46, 1.28, 82.52, 1.61 Q 92.59, 1.89, 102.65, 1.44 Q 112.72, 1.21, 122.78, 1.07 Q 132.85, 1.75, 142.91, 1.11\
                        Q 152.98, 2.03, 163.04, 1.96 Q 173.11, 2.01, 183.17, 2.29 Q 193.24, 1.27, 203.30, 2.00 Q 213.37, 2.13, 223.43, 1.21 Q 233.50,\
                        1.07, 243.57, 1.29 Q 253.63, 2.38, 263.70, 2.29 Q 273.76, 1.59, 283.83, 1.84 Q 293.89, 2.66, 303.96, 2.80 Q 314.02, 2.98,\
                        324.09, 2.36 Q 334.15, 2.28, 344.22, 2.31 Q 354.28, 2.86, 364.35, 3.13 Q 374.41, 2.03, 384.48, 1.60 Q 394.54, 1.95, 404.61,\
                        1.82 Q 414.67, 1.34, 424.74, 1.60 Q 434.80, 1.31, 444.87, 1.02 Q 454.93, 1.76, 465.00, 1.09 Q 475.07, 0.87, 485.13, 0.69 Q\
                        495.20, 1.32, 505.26, 1.09 Q 515.33, 2.10, 525.39, 1.19 Q 535.46, 1.88, 545.52, 1.62 Q 555.59, 1.94, 565.65, 1.20 Q 575.72,\
                        0.52, 585.78, 2.36 Q 595.85, 1.49, 605.91, 1.67 Q 615.98, 1.83, 626.04, 1.79 Q 636.11, 1.29, 646.17, 1.27 Q 656.24, 1.42,\
                        666.30, 2.12 Q 676.37, 2.64, 686.44, 1.90 Q 696.50, 1.56, 706.57, 1.65 Q 716.63, 1.94, 726.70, 2.03 Q 736.76, 2.08, 746.83,\
                        1.61 Q 756.89, 1.45, 766.96, 1.78 Q 777.02, 1.70, 787.09, 2.24 Q 797.15, 1.88, 807.22, 1.86 Q 817.28, 2.00, 827.35, 1.12 Q\
                        837.41, 2.05, 847.48, 1.91 Q 857.54, 2.01, 867.61, 1.70 Q 877.67, 2.19, 887.74, 1.32 Q 897.81, 1.41, 907.87, 2.27 Q 917.94,\
                        0.85, 928.51, 1.49 Q 928.75, 12.04, 929.23, 22.40 Q 929.38, 32.78, 929.54, 43.11 Q 928.32, 53.44, 928.30, 63.73 Q 928.91,\
                        74.02, 928.73, 84.31 Q 927.83, 94.61, 927.11, 104.90 Q 927.02, 115.18, 928.24, 125.47 Q 928.49, 135.76, 927.24, 146.05 Q 927.01,\
                        156.34, 927.61, 166.63 Q 926.94, 176.92, 927.04, 187.21 Q 927.72, 197.50, 928.18, 207.79 Q 929.00, 218.08, 929.37, 228.37\
                        Q 928.49, 238.66, 927.20, 248.95 Q 928.16, 259.24, 928.23, 269.53 Q 928.38, 279.82, 928.38, 290.11 Q 928.74, 300.39, 928.78,\
                        310.68 Q 929.14, 320.97, 929.34, 331.26 Q 929.45, 341.55, 929.39, 351.84 Q 929.03, 362.13, 928.90, 372.42 Q 928.39, 382.71,\
                        928.32, 393.31 Q 918.15, 393.63, 907.95, 393.53 Q 897.81, 393.06, 887.74, 392.87 Q 877.68, 393.47, 867.61, 393.24 Q 857.54,\
                        393.01, 847.48, 393.30 Q 837.41, 393.35, 827.35, 393.40 Q 817.28, 393.25, 807.22, 393.25 Q 797.15, 393.44, 787.09, 393.02\
                        Q 777.02, 392.85, 766.96, 392.21 Q 756.89, 392.51, 746.83, 393.44 Q 736.76, 393.91, 726.70, 393.77 Q 716.63, 393.56, 706.57,\
                        393.47 Q 696.50, 393.45, 686.44, 393.20 Q 676.37, 392.25, 666.30, 392.65 Q 656.24, 393.49, 646.17, 394.42 Q 636.11, 394.39,\
                        626.04, 394.54 Q 615.98, 394.38, 605.91, 393.64 Q 595.85, 394.35, 585.78, 394.31 Q 575.72, 393.35, 565.65, 393.44 Q 555.59,\
                        393.79, 545.52, 393.92 Q 535.46, 393.42, 525.39, 392.95 Q 515.33, 393.04, 505.26, 392.93 Q 495.20, 391.98, 485.13, 392.30\
                        Q 475.07, 392.83, 465.00, 393.78 Q 454.93, 394.85, 444.87, 393.88 Q 434.80, 393.24, 424.74, 392.62 Q 414.67, 394.15, 404.61,\
                        394.72 Q 394.54, 394.87, 384.48, 394.30 Q 374.41, 394.28, 364.35, 394.82 Q 354.28, 394.80, 344.22, 394.90 Q 334.15, 394.74,\
                        324.09, 394.30 Q 314.02, 394.38, 303.96, 394.75 Q 293.89, 394.83, 283.83, 394.45 Q 273.76, 394.59, 263.70, 394.73 Q 253.63,\
                        394.81, 243.57, 394.95 Q 233.50, 394.73, 223.43, 394.00 Q 213.37, 393.57, 203.30, 393.41 Q 193.24, 392.93, 183.17, 392.90\
                        Q 173.11, 392.93, 163.04, 392.58 Q 152.98, 393.12, 142.91, 393.09 Q 132.85, 393.58, 122.78, 393.74 Q 112.72, 393.63, 102.65,\
                        392.34 Q 92.59, 392.94, 82.52, 392.57 Q 72.46, 392.04, 62.39, 392.44 Q 52.33, 391.42, 42.26, 391.94 Q 32.20, 392.33, 22.13,\
                        392.61 Q 12.07, 392.66, 1.61, 393.39 Q 0.61, 383.17, 0.84, 372.59 Q 0.32, 362.24, 0.09, 351.90 Q -0.09, 341.59, -0.11, 331.28\
                        Q -0.25, 320.98, 0.16, 310.69 Q 0.69, 300.40, 1.57, 290.11 Q 1.62, 279.82, 1.14, 269.53 Q 1.58, 259.24, 1.67, 248.95 Q 2.74,\
                        238.66, 1.98, 228.37 Q 1.60, 218.08, 0.90, 207.79 Q 0.17, 197.50, 0.63, 187.21 Q 0.73, 176.92, 0.70, 166.63 Q 0.49, 156.34,\
                        0.62, 146.05 Q 0.29, 135.76, 0.45, 125.47 Q 0.56, 115.18, 0.55, 104.89 Q 1.67, 94.61, 0.87, 84.32 Q 1.48, 74.03, 1.32, 63.74\
                        Q 1.17, 53.45, 0.57, 43.16 Q 0.65, 32.87, 1.42, 22.58 Q 2.00, 12.29, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 926px; height:391px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page995954813-layer-514781659-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page995954813-layer-514781659-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page995954813-layer-514781659-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page995954813-layer-514781659-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-page995954813-layer-514781659-accordion", "926", "391", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-page995954813-layer-chart230116452" style="position: absolute; left: 360px; top: 230px; width: 920px; height: 265px" data-interactive-element-type="static.chart" class="chart stencil mobile-interaction-potential-trigger " data-stencil-id="chart230116452" data-review-reference-id="chart230116452">\
            <div class="stencil-wrapper" style="width: 920px; height: 265px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 265px;width:920px;" viewBox="0 0 920 265" width="920" height="265">\
                     <svg:g width="920" height="265">\
                        <svg:g xmlns:xi="http://www.w3.org/2001/XInclude" transform="scale(5.111111111111111, 1.7666666666666666)">\
                           <svg:rect x="0" y="0" width="180" height="150" style="fill:white;stroke:none;"></svg:rect>\
                           <svg:g name="line" style="fill:none;stroke:black;stroke-width:1px;">\
                              <svg:path d="M 9,3 L 9,140 L 168,140 L 176,140"></svg:path>\
                              <svg:path d="M 167,135 L 177,140 L 167,145"></svg:path>\
                              <svg:path d="M 4,13 L 9,3 L 14,13"></svg:path>\
                              <svg:path d="M 9,140 L 30,112 L 46,98 L 54,106 L 68,89 L 74,82 L 76,74 L 82,67 L 92,96 L 97,76 L 106,66 L 111,50 L 122,37 L 127,45 L 129,51 L 139,51 L 151,29 L 159,26 L 161,16 L 166,12"></svg:path>\
                              <svg:path d="M 10,140 L 18,107 L 29,91 L 34,73 L 44,80 L 51,55 L 62,52 L 74,34 L 86,51 L 99,43 L 111,59 L 121,70 L 128,98 L 139,119 L 147,103 L 154,125 L 165,136"></svg:path>\
                           </svg:g>\
                        </svg:g>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page995954813"] .border-wrapper, body[data-current-page-id="page995954813"] .simulation-container{\
         			width:1366px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page995954813"] .border-wrapper, body.has-frame[data-current-page-id="page995954813"]\
         .simulation-container{\
         			height:768px;\
         		}\
         		\
         		body[data-current-page-id="page995954813"] .svg-border-1366-768{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page995954813"] .border-wrapper .border-div{\
         			width:1366px;\
         			height:768px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page995954813",\
      			"name": "stats",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":1366,\
      			"height":768,\
      			"parentFolder": "",\
      			"frame": "desktop",\
      			"frameOrientation": "landscape"\
      		}\
      	\
   </div>\
</div>');