const request = require('async-request');
const headers = require('../config/headers').footballDataHeaders;
const n = require('../util/normalizer');

const listOfLeagues = [
    //1. Bundesliga
    'BL1',
    //Premiere League
    'PL',
    //Serie A
    'SA',
    //Primera Division
    'PD',
    //Ligue 1
    'FL1',
];

module.exports = (synchronizer) => {

    module.loadSeasonData = async () => {
        // const cachedResults = new Map();
        const leaguesResponse = await request('https://api.football-data.org/v1/competitions', {
            headers: headers,
            method: 'GET'
        });
        await Promise.all(JSON.parse(leaguesResponse.body)
            .filter((leagueResponseValue) => {
                return listOfLeagues.includes(leagueResponseValue.league)
            })
            .map(async (leagueResponseValue) => {
                    const leagueInsertValue = await synchronizer.sequelize.models.League.create({
                        league_code: leagueResponseValue.league,
                        league_name: leagueResponseValue.caption,
                        league_teams_number: leagueResponseValue.numberOfTeams
                    });
                    synchronizer.cachedLeagues.set(leagueInsertValue.league_code, leagueInsertValue.league_id);
                    const teamsResponse = await request('https://api.football-data.org/v1/competitions/' + leagueResponseValue.id + '/teams', {
                        headers: headers,
                        method: 'GET'
                    });
                    await Promise.all(JSON.parse(teamsResponse.body).teams
                        .map(async (teamResponseValue) => {
                            const teamInsertValue = await synchronizer.sequelize.models.Team.create({
                                team_name: teamResponseValue.name,
                                team_logo: teamResponseValue.crestUrl,
                            });
                            synchronizer.cachedTeams.set(n.normalize(teamInsertValue.team_name), teamInsertValue.team_id);
                            const leagueParticipationId = await synchronizer.sequelize.models.LeagueParticipation.create({
                                team_id: teamInsertValue.team_id,
                                league_id: leagueInsertValue.league_id
                            }).get('league_participation_id');
                        })
                    );
                    const matchesResponse = await request('https://api.football-data.org/v1/competitions/' + leagueResponseValue.id + '/fixtures', {
                        headers: headers,
                        method: 'GET'
                    });
                    await Promise.all(
                        JSON.parse(matchesResponse.body).fixtures
                            .map(async (matchResponseValue) => {
                                const matchInsertValue = await synchronizer.sequelize.models.Match.create({
                                    home_team_id: synchronizer.cachedTeams.get(n.normalize(matchResponseValue.homeTeamName)),
                                    away_team_id: synchronizer.cachedTeams.get(n.normalize(matchResponseValue.awayTeamName)),
                                    league_id: leagueInsertValue.get('league_id'),
                                    match_status: matchResponseValue.status,
                                    match_date: matchResponseValue.date,
                                    home_team_goals: matchResponseValue.result.goalsHomeTeam,
                                    away_team_goals: matchResponseValue.result.goalsAwayTeam
                                });
                                 if (matchInsertValue.get('match_status') === 'SCHEDULED' || matchInsertValue.get('match_status') === 'TIMED' || matchInsertValue.get('match_status') === 'POSTPONED') {
                                    const temp = {};
                                    temp['home'] = await synchronizer.sequelize.models.Event.create({
                                        match_id: matchInsertValue.get('match_id'),
                                        event_type: 'HOME',
                                    }).get('event_id');
                                    temp['draw'] = await synchronizer.sequelize.models.Event.create({
                                        match_id: matchInsertValue.get('match_id'),
                                        event_type: 'DRAW'
                                    }).get('event_id');
                                    temp['away'] = await synchronizer.sequelize.models.Event.create({
                                        match_id: matchInsertValue.get('match_id'),
                                        event_type: 'AWAY',
                                    }).get('event_id');
                                    synchronizer.cachedMatches.set(matchInsertValue.get('match_id'), temp);
                                }
                            })
                    )
                }
            )
        );
    };

    module.updateData = async () => {
        const leaguesResponse = await request('https://api.football-data.org/v1/competitions', {
            headers: headers,
            method: 'GET'
        });
        await Promise.all(JSON.parse(leaguesResponse.body)
            .filter((leagueResponseValue) => {
                return listOfLeagues.includes(leagueResponseValue.league)
            })
            .map(async (leagueResponseValue) => {
                const matchesResponse = await request('https://api.football-data.org/v1/competitions/' + leagueResponseValue.id + '/fixtures?timeFrame=n1', {
                    headers: headers,
                    method: 'GET',
                });
                await Promise.all(
                    JSON.parse(matchesResponse.body).fixtures
                        .map(async (matchResponseValue) => {
                                const homeTeamId = synchronizer.cachedTeams.get(n.normalize(matchResponseValue.homeTeamName));
                                const awayTeamId = synchronizer.cachedTeams.get(n.normalize(matchResponseValue.awayTeamName));
                                const leagueId = synchronizer.cachedLeagues.get(leagueResponseValue.league);
                                await synchronizer.sequelize.models.Match.update({
                                    match_date: matchResponseValue.date,
                                    match_status: matchResponseValue.status,
                                    home_team_goals: matchResponseValue.result.goalsHomeTeam,
                                    away_team_goals: matchResponseValue.result.goalsAwayTeam
                                }, {
                                    where: {
                                        home_team_id: homeTeamId,
                                        away_team_id: awayTeamId,
                                        league_id: leagueId
                                    }
                                });
                            }
                        )
                )
            })
        );
    };

    module.updatePreviousDayData = async () => {
        const leaguesResponse = await request('https://api.football-data.org/v1/competitions', {
            headers: headers,
            method: 'GET'
        });
        await Promise.all(JSON.parse(leaguesResponse.body)
            .filter((leagueResponseValue) => {
                return listOfLeagues.includes(leagueResponseValue.league)
            })
            .map(async (leagueResponseValue) => {
                const matchesResponse = await request('https://api.football-data.org/v1/competitions/' + leagueResponseValue.id + '/fixtures?timeFrame=p1', {
                    headers: headers,
                    method: 'GET',
                });
                await Promise.all(
                    JSON.parse(matchesResponse.body).fixtures
                        .map(async (matchResponseValue) => {
                                const homeTeamId = synchronizer.cachedTeams.get(n.normalize(matchResponseValue.homeTeamName));
                                const awayTeamId = synchronizer.cachedTeams.get(n.normalize(matchResponseValue.awayTeamName));
                                const leagueId = synchronizer.cachedLeagues.get(leagueResponseValue.league);
                                await synchronizer.sequelize.models.Match.update({
                                    match_date: matchResponseValue.date,
                                    match_status: matchResponseValue.status,
                                    home_team_goals: matchResponseValue.result.goalsHomeTeam,
                                    away_team_goals: matchResponseValue.result.goalsAwayTeam,
                                }, {
                                    where: {
                                        home_team_id: homeTeamId,
                                        away_team_id: awayTeamId,
                                        league_id: leagueId,
                                        $or: [
                                            {match_status: 'TIMED'},
                                            {match_status: 'IN_PLAY'}
                                        ]
                                    }
                                })
                            }
                        )
                )
            })
        )
    };

    module.loadBookmakerLinesInfo = async () => {
        let bookmakers = require('../config/bookmaker.json');
        for (const bookmaker of bookmakers) {
            const bookmakerInsertValue = await synchronizer.sequelize.models.Bookmaker.create({
                bookmaker_name: bookmaker['name'],
                bookmaker_url: bookmaker['url'],
            });
            for (const [code, league_id] of synchronizer.cachedLeagues) {
                await synchronizer.sequelize.models.BookmakerLine.create({
                    bookmaker_id: bookmakerInsertValue.bookmaker_id,
                    league_id: league_id,
                    line_url: bookmaker[code]
                })
            }
        }
    };

    return module;
};