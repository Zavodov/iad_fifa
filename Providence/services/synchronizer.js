const Sequelize = require('sequelize');
const n = require('../util/normalizer');

const sequelize = new Sequelize(process.env.DATABASE_URL, {
        pool: {
            max: 20,
            min: 0,
            idle: 10000
        },
    }
);

const Team = sequelize.import("../models/Team.js");
const Bookmaker = sequelize.import("../models/Bookmaker.js");
const League = sequelize.import("../models/League.js");
const LeagueParticipation = sequelize.import("../models/LeagueParticipation.js");
const Match = sequelize.import("../models/Match.js");
const Offer = sequelize.import("../models/Offer.js");
const Event = sequelize.import("../models/Event.js");
const BookmakerLine = sequelize.import("../models/BookmakerLine.js");
const User = sequelize.import("../models/User.js");

Team.hasMany(Match, {foreignKey: 'home_team_id', as: 'home_matches'});
Team.hasMany(Match, {foreignKey: 'away_team_id', as: 'away_matches'});
Match.belongsTo(Team, {foreignKey: 'away_team_id', as: 'away_team'});
Match.belongsTo(Team, {foreignKey: 'home_team_id', as: 'home_team'});
Match.hasMany(Event, {foreignKey: 'match_id', as: 'events'});
Bookmaker.hasMany(Offer, {foreignKey: 'bookmaker_id'});
Event.hasMany(Offer, {foreignKey: 'event_id', as: 'offers'});
Match.belongsTo(League, {foreignKey: 'league_id', as: 'league'});
League.hasMany(LeagueParticipation, {foreignKey: 'league_id', as: 'league_participation'});
Team.hasMany(LeagueParticipation, {foreignKey: 'team_id', as: 'league_participation'});
LeagueParticipation.belongsTo(League, {foreignKey: 'league_id', as: 'league'});
LeagueParticipation.belongsTo(Team, {foreignKey: 'team_id', as: 'team'});
Bookmaker.hasMany(BookmakerLine, {foreignKey: 'bookmaker_id', as: 'lines'});
League.hasMany(BookmakerLine, {foreignKey: 'league_id'});

Match.afterCreate(async (match) => {
        if (match.get('match_status') === 'FINISHED') {
            const leagueId = match.get('league_id');
            const homeTeamId = match.get('home_team_id');
            const awayTeamId = match.get('away_team_id');
            const homeLeagueParticipation = await sequelize.models.LeagueParticipation.findOne({
                where: {
                    league_id: leagueId,
                    team_id: homeTeamId
                }
            });
            const awayLeagueParticipation = await sequelize.models.LeagueParticipation.findOne({
                where: {
                    league_id: leagueId,
                    team_id: awayTeamId
                }
            });
            if (match.get('home_team_goals') > match.get('away_team_goals')) {
                await sequelize.models.LeagueParticipation.update({
                    wins: sequelize.literal('wins + 1')
                }, {
                    where: {
                        league_participation_id: homeLeagueParticipation.get('league_participation_id')
                    }
                });
                await sequelize.models.LeagueParticipation.update({
                    loses: sequelize.literal('loses + 1')
                }, {
                    where: {
                        league_participation_id: awayLeagueParticipation.get('league_participation_id')
                    }
                });
            }
            else if (match.get('home_team_goals') < match.get('away_team_goals')) {
                await sequelize.models.LeagueParticipation.update({
                    loses: sequelize.literal('loses + 1')
                }, {
                    where: {
                        league_participation_id: homeLeagueParticipation.get('league_participation_id')
                    }
                });
                await sequelize.models.LeagueParticipation.update({
                    wins: sequelize.literal('wins + 1')
                }, {
                    where: {
                        league_participation_id: awayLeagueParticipation.get('league_participation_id')
                    }
                });
            }
            else {
                await sequelize.models.LeagueParticipation.update({
                    draws: sequelize.literal('draws + 1')
                }, {
                    where: {
                        league_participation_id: homeLeagueParticipation.get('league_participation_id')
                    }
                });
                await sequelize.models.LeagueParticipation.update({
                    draws: sequelize.literal('draws + 1')
                }, {
                    where: {
                        league_participation_id: awayLeagueParticipation.get('league_participation_id')
                    }
                });
            }
        }
    }
);
Match.beforeBulkUpdate(async (match) => {
        const leagueId = match['where']['league_id'];
        const homeTeamId = match['where']['home_team_id'];
        const awayTeamId = match['where']['away_team_id'];
        const status = await sequelize.models.Match.findOne({
            where: {
                home_team_id: homeTeamId,
                away_team_id: awayTeamId,
                league_id: leagueId
            }
        }).get('match_status');
        if (match['attributes']['match_status'] === 'FINISHED' && status !== 'FINISHED') {
            const homeLeagueParticipation = await sequelize.models.LeagueParticipation.findOne({
                where: {
                    league_id: leagueId,
                    team_id: homeTeamId
                }
            });
            const awayLeagueParticipation = await sequelize.models.LeagueParticipation.findOne({
                where: {
                    league_id: leagueId,
                    team_id: awayTeamId
                }
            });
            if (match['attributes']['home_team_goals'] > match['attributes']['away_team_goals']) {
                await sequelize.models.LeagueParticipation.update({
                    wins: sequelize.literal('wins + 1')
                }, {
                    where: {
                        league_participation_id: homeLeagueParticipation.get('league_participation_id')
                    }
                });
                await sequelize.models.LeagueParticipation.update({
                    loses: sequelize.literal('loses + 1')
                }, {
                    where: {
                        league_participation_id: awayLeagueParticipation.get('league_participation_id')
                    }
                });
            }
            else if (match['attributes']['home_team_goals'] < match['attributes']['away_team_goals']) {
                await sequelize.models.LeagueParticipation.update({
                    loses: sequelize.literal('loses + 1')
                }, {
                    where: {
                        league_participation_id: homeLeagueParticipation.get('league_participation_id')
                    }
                });
                await sequelize.models.LeagueParticipation.update({
                    wins: sequelize.literal('wins + 1')
                }, {
                    where: {
                        league_participation_id: awayLeagueParticipation.get('league_participation_id')
                    }
                });
            }
            else {
                await sequelize.models.LeagueParticipation.update({
                    draws: sequelize.literal('draws + 1')
                }, {
                    where: {
                        league_participation_id: homeLeagueParticipation.get('league_participation_id')
                    }
                });
                await sequelize.models.LeagueParticipation.update({
                    draws: sequelize.literal('draws + 1')
                }, {
                    where: {
                        league_participation_id: awayLeagueParticipation.get('league_participation_id')
                    }
                });
            }
        }
    }
);

module.cachedTeams = new Map();
module.cachedLeagues = new Map();
module.cachedMatches = new Map();
module.sequelize = sequelize;

module.exports = () => {
    module.sync = async (arg) => {
        await sequelize.sync({
            force: arg
        });
        const teams = await sequelize.models.Team.findAll();
        for (let team of teams) {
            module.cachedTeams.set(n.normalize(team.get('team_name')), team.get('team_id'));
        }
        const leagues = await sequelize.models.League.findAll();
        for (let league of leagues) {
            module.cachedLeagues.set(league.get('league_code'), league.get('league_id'));
        }
        const matches = await sequelize.models.Match.findAll();
        for (let match of matches) {
            if (match.get('match_status') === 'SCHEDULED' || match.get('match_status') === 'TIMED' || match.get('match_status') === 'POSTPONED') {
                const temp = {};
                temp['home'] = await sequelize.models.Event.findOne({
                    where: {
                        match_id: match.get('match_id'),
                        event_type: 'HOME',
                    }
                }).get('event_id');
                temp['draw'] = await sequelize.models.Event.findOne({
                    where: {
                        match_id: match.get('match_id'),
                        event_type: 'DRAW',
                    }
                }).get('event_id');
                temp['away'] = await sequelize.models.Event.findOne({
                    where: {
                        match_id: match.get('match_id'),
                        event_type: 'AWAY',
                    }
                }).get('event_id');
                module.cachedMatches.set(match.get('match_id'), temp);
            }
        }
    };

    return module;
};
