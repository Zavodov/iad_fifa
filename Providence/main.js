//Импорт модулей
const synchronizer = require('./services/synchronizer')();
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const session = require('express-session');
const expressHandlebars = require('express-handlebars');
const passport = require('passport');
const flash = require('connect-flash');

const app = express();
const sequelize = synchronizer.sequelize;

//const bot = require('./bot/bot')(synchronizer);

const loader = require('./services/loader')(synchronizer);
const parser = require('./services/parser')(synchronizer);

synchronizer.sync(false).then(() =>
    loader.loadSeasonData().then(() =>
        loader.loadBookmakerLinesInfo().then(() => {
            parser.load();
            setInterval(parser.load(), 3600000);
            setInterval(loader.updateData(), 3600000);
            setInterval(loader.updatePreviousDayData(), 86400000);
        })
    )
);

//Настройки Handlebars
app.set('views', './views');
app.engine('hbs', expressHandlebars({
    extname: '.hbs'
}));
app.set('view engine', '.hbs');
//Настройка статического пути (root)
const path = require('path');
app.use(express.static(path.join(__dirname, '/views')));

//Настройки BodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(flash());

//Настройки Passport
app.use(session({secret: 'kek', resave: true, saveUninitialized: true}));
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport/passport')(passport, sequelize.models.User);

//Маршруты
require('./routes/authRoute')(app, passport);
require('./routes/contentRoute')(app, passport);
require('./controllers/dataController')(app, synchronizer);

server = http.createServer(app);

server.listen(process.env.PORT, function (err) {
    let host = server.address().address,
        port = server.address().port;
    console.log('listening at http://localhost:%s', port);
});