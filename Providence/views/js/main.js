Vue.filter('formatDate', function(value) {
    if (value) {
        return (new Date(value))
            .toLocaleDateString( "ru",
                {   day: 'numeric',
                    month: 'numeric',
                    year:'2-digit',
                    hour: '2-digit',
                    minute: '2-digit'
                }
            )
    }
});

let app = new Vue ({
    el : '#app-1',
    data: {
        leagues : null,
        leagues_stats: null,
        leagues_matches: null
    }
});

function loadLeagues() {
    let leagues;

    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'api/leagues', false);
    xhr.send();

    if (xhr.status != 200) {
        Console.log('Ошибка ' + xhr.status + ': ' + xhr.statusText);
    } else {
        leagues = JSON.parse(xhr.responseText);
    }

    app.leagues = leagues
}

function loadStats() {
    let stats = [];

    let xhr = new XMLHttpRequest();

    app.leagues.forEach(function(item, i, arr) {
        xhr.open('GET', 'api/leagues/' + item.league_code , false);
        xhr.send();

        if (xhr.status != 200) {
            Console.log('Ошибка ' + xhr.status + ': ' + xhr.statusText);
        } else {
            stats.push(JSON.parse(xhr.responseText));
        }
    });

    app.leagues_stats = stats
}

function loadMatches() {
    let leagues_matches = [];

    let xhr = new XMLHttpRequest();

    app.leagues.forEach(function(item, i, arr) {
        xhr.open('GET', 'api/leagues/' + item.league_code + '/matches', false);
        xhr.send();

        if (xhr.status != 200) {
            Console.log('Ошибка ' + xhr.status + ': ' + xhr.statusText);
        } else {
            leagues_matches.push(JSON.parse(xhr.responseText));
        }
    });

    app.leagues_matches = leagues_matches
}

loadLeagues();
loadStats();
loadMatches();
