module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Event', {
        event_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        match_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        event_type: {
            type: DataTypes.ENUM('HOME', 'AWAY', 'DRAW'),
            allowNull: false
        }
    }, {
        timestamps: false,
        freezeTableName: true
    })
};