module.exports = function (sequelize, DataTypes) {
    return sequelize.define('League', {
        league_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        league_code: {
            type: DataTypes.STRING(3),
            allowNull: false,
            unique: true
        },
        league_name: {
            type: DataTypes.STRING(32),
            allowNull: false,
            unique: true
        },
        league_teams_number: {
            type: "SMALLINT",
            allowNull: false
        },
    }, {
        timestamps: false,
        freezeTableName: true
    })
};