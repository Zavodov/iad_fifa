module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Offer', {
        offer_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        bookmaker_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        event_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        offer_coefficient: {
            type: DataTypes.FLOAT,
            allowNull: false
        }
    }, {
        createdAt: 'offer_time',
        updatedAt: false,
        freezeTableName: true
    })
};