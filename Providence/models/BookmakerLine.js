module.exports = function (sequelize, DataTypes) {
    return sequelize.define('BookmakerLine', {
        bookmaker_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        league_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        line_url: {
            type: DataTypes.STRING(128),
            allowNull: false,
            unique: true
        }
    }, {
        timestamps: false,
        freezeTableName: true
    })
};