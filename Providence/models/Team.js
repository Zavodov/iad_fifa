module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Team', {
        team_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        team_name: {
            type: DataTypes.STRING(50),
            allowNull: false,
            unique: true
        },
        team_logo: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        //timestamp - время создания, время обновления
        timestamps: false,
        //freezeTableName - sequelize пытается обзывать таблицы во множественном числе
        freezeTableName: true
    })
};