module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Bookmaker', {
        bookmaker_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        bookmaker_name: {
            type: DataTypes.STRING(64),
            allowNull: false
        },
        bookmaker_url: {
            type: DataTypes.STRING(128),
            allowNull: false
        },
    }, {
        timestamps: false,
        freezeTableName: true
    })
};