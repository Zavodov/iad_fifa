const n = require('normalize-strings');

module.exports = {
    normalize: (str) => {
        let res = n(str);
        res = res.replace('AS Milan', 'milan');
        res = res.replace(/^Milan$/, 'milan');
        res = res.split(/HSC|VfB|VfL|TSG|FSV|RCD|CFC|BSC|ACF|AFC|SSC|FCO|OGC|SCO|OSC|SC|FC|CF|UD|EA|CD|RC|SD|ES|AS|US|UC|SS|RB|BC|SM|AC|de Vigo|Vigo|de Futbol|Club|Girondins de|Turin|Calcio|Ferrara|Borussia|Bor|Red Bull|er SV|Munich|Munchen|London|Verona|Albion|Bilbao|Herault|Olympique de|Alsace|Berlin|Milano|Milan|\sand\s|Deportivo|United|Olympique|En Avant/g).join(' ');
        res = res.replace('Cologne', 'koln');
        res = res.replace('Lyonnais', 'lyon');
        res = res.replace('de Madrid', 'madrid');
        res = res.replace('Stade Rennais', 'rennes');
        res = res.replace('Paris Saint-Germain', 'psg');
        res = res.replace('Bromwich', 'Brom');
        res = res.replace(/[^A-Za-z]/g, ' ');
        res = res.toLowerCase();
        res = res.replace(/ /g,'');
        return res;
    }
};